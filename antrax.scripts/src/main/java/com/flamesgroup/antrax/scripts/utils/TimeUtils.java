/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

public class TimeUtils {
  /**
   * this constant indicates maximum digit number in hours.
   */
  private static final int HOURS_UPPER_LIMIT = 99;

  /**
   * @param time
   * @return String estimation of time (i.e. hh:mm:ss)
   */
  public static String writeTime(final int time) {
    int hours = time / (60 * 60);
    int min = (time - hours * 60 * 60) / 60;
    int sec = time - hours * 60 * 60 - min * 60;
    return writeHours(hours) + ":" + write(min) + ":" + write(sec);
  }

  /**
   * @param time
   * @return String estimation of hours, calculated basing HOURS_UPPER_LIMIT
   */
  private static String writeHours(final int time) {
    if (time > HOURS_UPPER_LIMIT) {
      return Integer.toString(time);
    }
    String nulls = countNulls();
    if (String.valueOf(time).length() == 2) {
      nulls = nulls.substring(1, nulls.length());
    }
    return nulls + time;
  }

  /**
   * @return String estimation of time, in case of time == 0
   */
  private static String countNulls() {
    return String.valueOf(HOURS_UPPER_LIMIT).replaceAll("[0-9]", "0");
  }

  /**
   * @param time
   * @return String estimation of minutes or seconds.
   */
  private static String write(final int time) {
    if (time > 9) {
      return Integer.toString(time);
    }
    return "0" + time;
  }

  /**
   * @param millis
   * @return String value of time, converted from long.
   */
  public static String writeTime(final long millis) {
    int time = (int) (millis / 1000);
    return writeTime(time);
  }

  /**
   * @param millis
   * @return String value of time including milliseconds data, separated by a
   * dot. Value converted from long.
   */
  public static String writeTimeMs(final long millis) {
    return String.format("%s.%03d", writeTime(millis), (millis % 1000));
  }

  /**
   * @return constant witch indicates maximum digit number in hours.
   */
  public static int getHoursUpperLimit() {
    return HOURS_UPPER_LIMIT;
  }

}
