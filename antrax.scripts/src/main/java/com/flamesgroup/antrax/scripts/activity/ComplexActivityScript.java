/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.activity;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.helper.activity.SimpleActivityScriptAdapter;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;

@Script(name = "complex activity script", doc = "script wchich allow building complex scripts")
public class ComplexActivityScript extends SimpleActivityScriptAdapter<SimpleActivityScript> {

  private static final long serialVersionUID = 5875112518728416125L;

  public ComplexActivityScript() throws Exception {
    super(new ScripletBlock(ScripletFactory.createScripletFor(ScripletFactory.listScripts()[0])).createActivityScript());
  }

  @ScriptParam(name = "scriplet", doc = "scriplet for script")
  public void setScriplet(final ActivityScriplet scriplet) throws Exception {
    setAdept(scriplet.createActivityScript());
  }

  public ActivityScriplet getScriplet() {
    return new ScripletBlock(ScripletFactory.createScripletFor(ScripletFactory.listScripts()[0]));
  }

}
