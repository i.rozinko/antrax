/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import java.io.Serializable;


public class ActivityTimeoutByEventScriplet implements Serializable {

  private static final long serialVersionUID = -2703065547589210280L;

  private final String event;
  private final TimeInterval timeout;

  public ActivityTimeoutByEventScriplet(final String event, final TimeInterval timeout) {
    this.event = event;
    this.timeout = timeout;
  }

  public String getEvent() {
    return event;
  }

  public TimeInterval getTimeout() {
    return timeout;
  }

  @Override
  public String toString() {
    return String.format("%s; %s", event, timeout);
  }

}
