/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.incoming;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

abstract class ChancesBasedIncomingCallScriptBase implements com.flamesgroup.antrax.automation.scripts.ChancesBasedIncomingCallScript, IncomingCallsListener, StatefullScript {

  private static final long serialVersionUID = -6428393816512955280L;

  @StateField
  private boolean applyIncomingCall;
  @StateField
  private long activeStart;
  @StateField
  private long idleStart;
  @StateField
  private long idleDuration;
  @StateField
  private long activeDuration;

  private volatile TimeInterval idle = new TimeInterval(new TimePeriod(TimePeriod.inSeconds(10)), new TimePeriod(TimePeriod.inSeconds(40)));
  private volatile TimeInterval active = new TimeInterval(new TimePeriod(TimePeriod.inSeconds(10)), new TimePeriod(TimePeriod.inSeconds(60)));

  private volatile Chance acceptChance = new Chance(70);

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "active interval", doc = "duration of active stage of incoming call, if accepts")
  public void setActive(final TimeInterval active) {
    this.active = active;
  }

  public TimeInterval getActive() {
    return active;
  }

  @ScriptParam(name = "idle inteval", doc = "value of delay after which call will be accepted")
  public void setIdle(final TimeInterval idle) {
    this.idle = idle;
  }

  public TimeInterval getIdle() {
    return idle;
  }

  @ScriptParam(name = "accept chance", doc = "accept chance")
  public void setAcceptChance(final Chance chance) {
    this.acceptChance = chance;
  }

  public Chance getAcceptChance() {
    return acceptChance;
  }

  @Override
  public boolean shouldAnswer() {
    return applyIncomingCall && idleTimoutPassed();
  }

  @Override
  public boolean shouldDrop() {
    if (applyIncomingCall) {
      return activeTimeoutPassed();
    } else {
      return idleTimoutPassed();
    }
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    applyIncomingCall = acceptChance.countChance();
    idleStart = System.currentTimeMillis();
    recalculateRandomValues();
    saver.save();
  }

  private boolean idleTimoutPassed() {
    if (idleStart == 0) {
      return false;
    }
    return System.currentTimeMillis() - idleStart >= idleDuration;
  }

  private boolean activeTimeoutPassed() {
    if (activeStart == 0) {
      return false;
    }
    return System.currentTimeMillis() - activeStart >= activeDuration;
  }

  @Override
  public void handleIncomingCallAnswered() {
    activeStart = System.currentTimeMillis();
    saver.save();
  }

  private void recalculateRandomValues() {
    activeDuration = active.random();
    idleDuration = idle.random();
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    applyIncomingCall = false;
    activeStart = 0;
    idleStart = 0;
    saver.save();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void dropAction(final RegisteredInGSMChannel channel) throws  Exception {
  }

}
