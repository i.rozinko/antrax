/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.incoming;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "origination call", doc = "Use this script for origination calls to gateway")
public class OriginationIncomingCallScript implements com.flamesgroup.antrax.automation.scripts.OriginationIncomingCallScript {

  private String callerPattern = "";
  private String callerReplacePattern = "";

  private String called = "";
  private String target = "127.0.0.1";

  @Override
  public PhoneNumber getCalledNumber() {
    return new PhoneNumber(called);
  }

  @Override
  public String getTargetAddress() {
    return target;
  }

  @Override
  public PhoneNumber substituteCallerNumber(final PhoneNumber caller) {
    if (callerPattern.isEmpty() || callerReplacePattern.isEmpty()) {
      return caller;
    } else {
      return new PhoneNumber(caller.getValue().replaceFirst(callerPattern, callerReplacePattern));
    }
  }

  public String getCallerPattern() {
    return callerPattern;
  }

  @ScriptParam(name = "caller number pattern", doc = "caller number pattern")
  public void setCallerPattern(final String callerPattern) {
    this.callerPattern = callerPattern;
  }

  public String getCallerReplacePattern() {
    return callerReplacePattern;
  }

  @ScriptParam(name = "caller number replace pattern", doc = "caller number replace pattern")
  public void setCallerReplacePattern(final String callerReplacePattern) {
    this.callerReplacePattern = callerReplacePattern;
  }

  public String getCalled() {
    return called;
  }

  @ScriptParam(name = "called number", doc = "called number for origination")
  public void setCalled(final String called) {
    this.called = called;
  }

  public String getTarget() {
    return target;
  }

  @ScriptParam(name = "target address", doc = "target address for origination. Patter: address or address:port")
  public void setTarget(final String target) {
    this.target = target;
  }

}
