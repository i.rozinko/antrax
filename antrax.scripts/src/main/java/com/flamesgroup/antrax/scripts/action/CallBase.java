/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.helper.business.CallHelper;
import com.flamesgroup.antrax.helper.business.CallHelper.CallStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class CallBase implements ActionProviderScript, IncomingCallsListener {

  public String actionName;
  protected final Logger logger;

  protected CallBase(final String actionName, final Logger logger) {
    this.actionName = actionName;
    this.logger = logger;
  }

  protected TimeInterval minimumActiveDuration = new TimeInterval(TimePeriod.inSeconds(10), TimePeriod.inSeconds(15));
  protected TimeInterval maximumActiveDuration = new TimeInterval(TimePeriod.inMinutes(1), TimePeriod.inMinutes(2));
  protected TimeInterval maximumAttemptDuration = new TimeInterval(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(30),
      TimePeriod.inMinutes(1) + TimePeriod.inSeconds(50));
  protected TimeInterval intervalBetweenCalls = new TimeInterval(TimePeriod.inSeconds(1), TimePeriod.inSeconds(5));

  protected int attemptsCount = 1;
  protected boolean requireActive = true;
  protected GenericEvent event = GenericEvent.uncheckedEvent("afterCall");
  private GenericEvent eventOnSuccess = GenericEvent.uncheckedEvent("afterSuccessCall");
  private GenericEvent eventOnFailure = GenericEvent.uncheckedEvent("afterFailedCall");
  protected VariableLong callsCount = new VariableLong(1, 1);
  protected boolean skipErrors = false;

  private String numberPattern = "";
  private String numberReplacePattern = "";

  private boolean ignoreIncomingCalls = false;

  private final AtomicBoolean inIcomingCall = new AtomicBoolean(false);

  @ScriptParam(name = "number pattern", doc = "number pattern")
  public void setNumberPattern(final String np) {
    this.numberPattern = np;
  }

  public String getNumberPattern() {
    return numberPattern;
  }

  @ScriptParam(name = "ignore incoming calls", doc = "call anytime, even in incoming call")
  public void setIgnoreIncomingCalls(final boolean value) {
    this.ignoreIncomingCalls = value;
  }

  public boolean getIgnoreIncomingCalls() {
    return this.ignoreIncomingCalls;
  }

  @ScriptParam(name = "number replace pattern", doc = "number replace pattern")
  public void setNumberReplacePattern(final String nrp) {
    this.numberReplacePattern = nrp;
  }

  public String getNumberReplacePattern() {
    return numberReplacePattern;
  }

  @ScriptParam(name = "skip errors", doc = "flag for skip errors or not")
  public void setSkipErrors(final boolean skip) {
    this.skipErrors = skip;
  }

  public boolean getSkipErrors() {
    return this.skipErrors;
  }

  @ScriptParam(name = "interval between calls", doc = "interval between generated calls")
  public void setIntervalBetweenCalls(final TimeInterval interval) {
    this.intervalBetweenCalls = interval;
  }

  public TimeInterval getIntervalBetweenCalls() {
    return this.intervalBetweenCalls;
  }

  @ScriptParam(name = "calls count", doc = "count of generated calls")
  public void setCallsCount(final VariableLong count) {
    this.callsCount = count;
  }

  public VariableLong getCallsCount() {
    return this.callsCount;
  }

  @ScriptParam(name = "event", doc = "event to generate after action")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if call was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = GenericEvent.uncheckedEvent(eventOnSuccess);
  }

  public String getEventOnSuccess() {
    return eventOnSuccess.getEvent();
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if call failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = GenericEvent.uncheckedEvent(eventOnFailure);
  }

  public String getEventOnFailure() {
    return eventOnFailure.getEvent();
  }

  @ScriptParam(name = "action name", doc = "action name")
  public void setAction(final String action) {
    this.actionName = action;
  }

  public String getAction() {
    return actionName;
  }

  @ScriptParam(name = "max active duration", doc = "call will be limited with this duration")
  public void setMaximumActiveDuration(final TimeInterval maximumActiveTime) {
    this.maximumActiveDuration = maximumActiveTime;
  }

  public TimeInterval getMaximumActiveDuration() {
    return maximumActiveDuration;
  }

  @ScriptParam(name = "min active duration", doc = "if call duration will be less than this value, call comes failed")
  public void setMinimumActiveDuration(final TimeInterval minimumActiveTime) {
    this.minimumActiveDuration = minimumActiveTime;
  }

  public TimeInterval getMinimumActiveDuration() {
    return minimumActiveDuration;
  }

  @ScriptParam(name = "max attempt duration", doc = "max waiting time before the call will become active")
  public void setMaximumAttemptDuration(final TimeInterval maximumAttemptDuration) {
    this.maximumAttemptDuration = maximumAttemptDuration;
  }

  public TimeInterval getMaximumAttemptDuration() {
    return maximumAttemptDuration;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on call failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  protected abstract boolean ensureCanExecuteAction(Action action, RegisteredInGSMChannel channel) throws Exception;

  protected abstract void executeConcreteAction(Action action, RegisteredInGSMChannel channel) throws Exception;

  @Override
  public final boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!ignoreIncomingCalls && inIcomingCall.get())
      return false;
    return ensureCanExecuteAction(action, channel);
  }

  @Override
  public final void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!ignoreIncomingCalls && inIcomingCall.get())
      throw new IllegalStateException("Can't execute action in the middle of incoming call");
    executeConcreteAction(action, channel);
  }

  @ScriptParam(name = "require active", doc = "when set to true call will be success only if callie answers call")
  public void setRequireActive(final boolean requireActive) {
    this.requireActive = requireActive;
  }

  public boolean getRequireActive() {
    return requireActive;
  }

  protected String replaceNumber(String phoneNumber) {
    if (!numberPattern.isEmpty() && !numberReplacePattern.isEmpty()) {
      logger.debug("[{}] - replaceNumber: numberPattern: {}", this, numberPattern);
      logger.debug("[{}] - replaceNumber: numberReplacePattern: {}", this, numberReplacePattern);
      phoneNumber = phoneNumber.replaceFirst(numberPattern, numberReplacePattern);
      logger.debug("[{}] - resultNumber: {}", this, phoneNumber);
    }
    return phoneNumber;
  }

  protected void makeCall(final RegisteredInGSMChannel channel, final PhoneNumber dstNumber) throws Exception {
    CallHelper callHelper = new CallHelper(new PhoneNumberGenerator(dstNumber.getValue()));

    callHelper.setRequiresActive(requireActive);
    callHelper.setMaximumActiveDuration(maximumActiveDuration);
    callHelper.setMaximumAttemptDuration(maximumAttemptDuration);
    callHelper.setMinimumActiveDuration(minimumActiveDuration);

    long count = callsCount.random();
    for (int i = 0; i < count; i++) {
      CallStatus callStatus = callHelper.call(channel, attemptsCount);

      if (callStatus != CallStatus.OK && !skipErrors) {
        event.fireEvent(channel);
        eventOnFailure.fireEvent(channel);
        throw new RuntimeException("Failed to make call: " + callStatus);
      }
      if (i < count - 1) {
        Thread.sleep(getIntervalBetweenCalls().random());
      }
    }
    event.fireEvent(channel);
    eventOnSuccess.fireEvent(channel);
  }

  @Override
  public String getProvidedAction() {
    return actionName;
  }

  @Override
  public void handleIncomingCall(final PhoneNumber phone) {
    this.inIcomingCall.set(true);
  }

  @Override
  public void handleIncomingCallAnswered() {
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    this.inIcomingCall.set(false);
  }

}
