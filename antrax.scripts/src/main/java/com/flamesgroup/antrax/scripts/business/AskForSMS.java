/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "ask for sms", doc = "ask other sim card to send SMS to this one (requires PhoneNumber to be set)")
public class AskForSMS implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(AskForSMS.class);

  private String actionName = "sms";

  private String event = "askForSMS";

  private boolean lockOnFail = false;

  private String eventOnSuccess = "";

  private String eventOnFailure = "";

  private TimeInterval executeTimeout = new TimeInterval(TimePeriod.inMinutes(15), TimePeriod.inMinutes(15));

  private volatile GenericEvent incomingEvent = null;

  @ScriptParam(name = "action", doc = "name of action, which can provide send SMS")
  public void setActionName(final String action) {
    this.actionName = action;
  }

  public String getActionName() {
    return actionName;
  }

  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "lock on fail", doc = "lock card if send SMS failed")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if sending SMS was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if sending SMS failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = eventOnFailure;
  }

  public String getEventOnFailure() {
    return eventOnFailure;
  }

  @ScriptParam(name = "execute time", doc = "time of execute call")
  public void setExecuteTime(final TimeInterval interval) {
    this.executeTimeout = interval;
  }

  public TimeInterval getExecuteTime() {
    return executeTimeout;
  }

  @Override
  public String describeBusinessActivity() {
    return "ask for sms";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (channel.getSimData().getPhoneNumber() == null) {
      logger.debug("[{}] - there is no phone number, locking card", this);
      channel.lock("no PhoneNumber but using \"ask for sms\" script");
    }

    try {
      String phoneNumber = channel.getSimData().getPhoneNumber().getValue();
      Long simGroupID = channel.getSimData().getSimGroup().getID();
      Action action = new Action(actionName, phoneNumber, simGroupID);
      logger.debug("[{}] - {} execute action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
      channel.executeAction(action, executeTimeout.random());
      incomingEvent.respondSuccess(channel);

      if (eventOnSuccess != null && !eventOnSuccess.isEmpty()) {
        channel.fireGenericEvent(eventOnSuccess);
      }
    } catch (Exception e) {
      if (lockOnFail) {
        channel.lock("ask for sms failed: " + e.getMessage());
      }

      logger.warn("[{}] - ask for sms failed", this, e);
      incomingEvent.respondFailure(channel, e.getMessage());
      if (eventOnFailure != null && !eventOnFailure.isEmpty()) {
        channel.fireGenericEvent(eventOnFailure);
      }
    } finally {
      incomingEvent = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return incomingEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}
