/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.IvrListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;

@Script(name = "drop call on ivr detect", doc = "drops outgoing call on ivr detect")
public class DropCallOnIvrDetect implements BusinessActivityScript, IvrListener {

  private volatile boolean startActivity;
  private volatile byte callDropReasonCode;

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    startActivity = false;
    channel.dropCall(callDropReasonCode, CdrDropReason.IVR);
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return startActivity;
  }

  @Override
  public String describeBusinessActivity() {
    return "drop call on ivr detect";
  }

  @Override
  public void handleIvr(final String ivrTemplateName, final byte callDropReasonCode) {
    startActivity = true;
    this.callDropReasonCode = callDropReasonCode;
  }

}
