/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business.dtmf;

import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DTMFScriplet implements Serializable {

  private static final long serialVersionUID = 1037163887090337479L;

  private final PhoneNumber number;
  private final List<DTMFScripletAction> actions = new LinkedList<>();

  public DTMFScriplet(final PhoneNumber number) {
    this.number = number;
  }

  public void execute(final RegisteredInGSMChannel channel) throws Exception {
    if (number != null) {
      channel.dial(number, new DummyCallStateChangeHandler());
      try {
        for (DTMFScripletAction part : actions) {
          part.execute(channel);
        }
      } finally {
        channel.dropCall();
      }
    }
  }

  public void addDial(final char c) {
    actions.add(new DTMFScripletDial(c));
  }

  public void addTimeout(final TimePeriod timeout) {
    actions.add(new DTMFScripletTimeout(timeout));
  }

  public PhoneNumber getNumber() {
    return number;
  }

  public List<DTMFScripletAction> getActions() {
    return actions;
  }

  @Override
  public String toString() {
    StringBuilder retVal = new StringBuilder();
    if (getNumber() != null) {
      retVal.append(getNumber().getValue()).append(';');
      List<DTMFScripletAction> actionsList = getActions();
      int length = actionsList.size() % 2 == 0 ? actionsList.size() : actionsList.size() - 1;
      for (int i = 0; i < length; i += 2) {
        retVal.append(actionsList.get(i).toString());
        retVal.append(':');
        retVal.append(actionsList.get(i + 1).toString());
        if (i < length - 2 || length < actionsList.size()) {
          retVal.append(';');
        }
      }
      if (length < actionsList.size()) {
        retVal.append(actionsList.get(actionsList.size() - 1).toString());
      }
    }
    return retVal.toString();
  }

  private static class DummyCallStateChangeHandler implements CallStateChangeHandler {

    @Override
    public void handleStateChange(final CallState newState, final Map<CallState, Long> prevStates) {
    }
  }

}
