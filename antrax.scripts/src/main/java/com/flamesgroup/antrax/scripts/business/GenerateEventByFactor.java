/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "generate event randomly by factor", doc = "generates event after specified number of calls; probability of event generation increases according to call numbers")
public class GenerateEventByFactor implements BusinessActivityScript, CallsListener, GenericEventListener, StatefullScript {

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventByFactor.class);

  private static final long serialVersionUID = 2131297920415613797L;

  private static final String FAILURE_RESPONCE_PREFIX = " failure ";
  private static final String SUCCESS_RESPONCE_PREFIX = " successed ";

  @StateField
  private boolean lock = false;
  @StateField
  private volatile int callsAfterLastEvent;
  @StateField
  private int eventsInPeriod;
  @StateField
  private long periodBegining;

  private GenericEvent sendEvent = GenericEvent.checkedEvent("randomEvent", successedResponse("randomEvent"), failureResponse("randomEvent"));

  private boolean lockOnFailure;

  private int eventFactor;
  private Chance eventChance = new Chance(60);

  private TimePeriod period = new TimePeriod(TimePeriod.inHours(1));
  private int eventLimit = 5;

  @StateField
  private volatile String lockReason;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "send event", doc = "this event generates randomly")
  public void setSendEvent(final String event) {
    this.sendEvent = GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event));
  }

  public String getSendEvent() {
    return sendEvent.getEvent();
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @ScriptParam(name = "event factor", doc = "count of calls when chance comes in play to generate event")
  public void setEventFactor(final int eventFactor) {
    this.eventFactor = eventFactor;
  }

  public int getEventFactor() {
    return eventFactor;
  }

  @ScriptParam(name = "event chance", doc = "chance of event generation in percent")
  public void setEventChance(final Chance eventChance) {
    this.eventChance = eventChance;
  }

  public Chance getEventChance() {
    return eventChance;
  }

  @ScriptParam(name = "event generation period", doc = "period in which eventLimit parameter will have an effect")
  public void setPeriod(final TimePeriod period) {
    this.period = period;
  }

  public TimePeriod getPeriod() {
    return period;
  }

  @ScriptParam(name = "event limit", doc = "limits numbers of generated event in specified period")
  public void setEventLimit(final int eventLimit) {
    this.eventLimit = eventLimit;
  }

  public int getEventLimit() {
    return eventLimit;
  }

  @Override
  public String describeBusinessActivity() {
    return "generates event randomly by factor";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (lock) {
      try {
        if (lockReason != null) {
          channel.lock(lockReason);
        } else {
          channel.lock("failedToExecute " + sendEvent.getEvent());
        }
      } finally {
        lock = false;
        lockReason = null;
      }
    } else {
      sendEvent.fireEvent(channel);
      callsAfterLastEvent = 0;
      eventsInPeriod++;
      logger.debug("[{}] - eventsInPeriod: {}", this, eventsInPeriod);
    }
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    checkPeriod();
    return lock || shouldGenerateEvent();
  }

  private void checkPeriod() {
    long curTime = System.currentTimeMillis();
    if (curTime - periodBegining > period.getPeriod()) {
      logger.debug("[{}] - resetting eventsInPeriod", this);
      periodBegining = curTime;
      eventsInPeriod = 0;
      callsAfterLastEvent = 0;
      saver.save();
    }
  }

  private boolean shouldGenerateEvent() {
    if (eventsInPeriod >= eventLimit) {
      return false;
    }
    if (eventFactor == 0) {
      return eventChance.countChance();
    }
    return ((callsAfterLastEvent / eventFactor) - Math.random()) > (1 - (eventChance.getChance() / 100.0d));
  }

  @Override
  public void handleCallEnd(final long duration) {
    callsAfterLastEvent++;
    logger.debug("[{}] - callsAfterLastEvent: {}", this, callsAfterLastEvent);
    saver.save();
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.sendEvent.isFailureResponce(event, args)) {
      if (lockOnFailure) {
        lock = true;
        lockReason = sendEvent.getFailReason(event, args);
        saver.save();
      }
    }
  }

  private String successedResponse(final String event) {
    return event + SUCCESS_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + FAILURE_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}
