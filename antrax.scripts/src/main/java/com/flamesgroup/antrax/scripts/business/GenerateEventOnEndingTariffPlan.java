/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

@Script(name = "generate event on ending tariff plan", doc = "generate event on ending tariff plan")
public class GenerateEventOnEndingTariffPlan implements BusinessActivityScript, StatefullScript, SimDataListener {

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnEndingTariffPlan.class);

  private String eventBeforeEnd = "event_before_end";
  private String eventAfterEnd = "event_after_end";
  private TimeInterval generateTimeBeforeEnd = new TimeInterval(TimePeriod.inHours(12), TimePeriod.inHours(13));
  private TimeInterval generateTimeAfterEnd = new TimeInterval(TimePeriod.inHours(12), TimePeriod.inHours(13));
  private int dayBeforeEnd = 2;

  private SimData simData;

  @StateField
  private boolean generatedEventBeforeEnd;
  @StateField
  private boolean generatedEventAfterEnd;
  @StateField
  private long tariffPlanEndDate = 0;
  @StateField
  private long timeBeforeEnd;
  @StateField
  private long timeAfterEnd;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event before end", doc = "generated event until the end of the tariff plan")
  public void setEventBeforeEnd(final String eventBeforeEnd) {
    this.eventBeforeEnd = eventBeforeEnd;
  }

  public String getEventBeforeEnd() {
    return eventBeforeEnd;
  }

  @ScriptParam(name = "event after end", doc = "generated event when tariff plan is expires")
  public void setEventAfterEnd(final String eventAfterEnd) {
    this.eventAfterEnd = eventAfterEnd;
  }

  public String getEventAfterEnd() {
    return eventAfterEnd;
  }

  @ScriptParam(name = "generate time for event before end", doc = "time to generate event before end")
  public void setGenerateTimeBeforeEnd(final TimeInterval generateTimeBeforeEnd) {
    this.generateTimeBeforeEnd = generateTimeBeforeEnd;
    timeBeforeEnd = generateTimeBeforeEnd.random();
  }

  public TimeInterval getGenerateTimeBeforeEnd() {
    return generateTimeBeforeEnd;
  }

  @ScriptParam(name = "generate time for event after end", doc = "time to generate event after end")
  public void setGenerateTimeAfterEnd(final TimeInterval generateTimeAfterEnd) {
    this.generateTimeAfterEnd = generateTimeAfterEnd;
    timeAfterEnd = generateTimeAfterEnd.random();
  }

  public TimeInterval getGenerateTimeAfterEnd() {
    return generateTimeAfterEnd;
  }

  @ScriptParam(name = "days number until end", doc = "number of days until the end of the tariff plan")
  public void setDayBeforeEnd(final int dayBeforeEnd) {
    this.dayBeforeEnd = dayBeforeEnd;
  }

  public int getDayBeforeEnd() {
    return dayBeforeEnd;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    LocalDate currentDate = LocalDate.now();
    LocalDate endDate = Instant.ofEpochMilli(simData.getTariffPlanEndDate()).atZone(ZoneId.systemDefault()).toLocalDate();
    long daysBetween = ChronoUnit.DAYS.between(currentDate, endDate);
    if (daysBetween == dayBeforeEnd) {
      generatedEventBeforeEnd = true;
      channel.fireGenericEvent(eventBeforeEnd);
      saver.save();
    } else if (daysBetween == 0) {
      generatedEventAfterEnd = true;
      channel.fireGenericEvent(eventAfterEnd);
      saver.save();
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (tariffPlanEndDate != simData.getTariffPlanEndDate()) {
      tariffPlanEndDate = simData.getTariffPlanEndDate();
      generatedEventBeforeEnd = false;
      generatedEventAfterEnd = false;
      timeBeforeEnd = generateTimeBeforeEnd.random();
      timeAfterEnd = generateTimeAfterEnd.random();
      saver.save();
    }

    if (tariffPlanEndDate <= 0) {
      return false;
    }

    LocalDate currentDate = LocalDate.now();
    LocalDate endDate = Instant.ofEpochMilli(tariffPlanEndDate).atZone(ZoneId.systemDefault()).toLocalDate();
    long daysBetween = ChronoUnit.DAYS.between(currentDate, endDate);

    return (daysBetween == dayBeforeEnd && !generatedEventBeforeEnd && getTimeOfDay() >= timeBeforeEnd)
        || (daysBetween == 0 && !generatedEventAfterEnd && getTimeOfDay() >= timeAfterEnd);
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event on ending tariff plan";
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  long getTimeOfDay() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_YEAR, 1);
    cal.clear(Calendar.MONTH);
    cal.clear(Calendar.YEAR);
    cal.set(Calendar.ZONE_OFFSET, 0);
    return cal.getTimeInMillis();
  }

}
