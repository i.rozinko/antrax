/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.scripts.CallFilterScript;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.LoggerFactory;

@Script(name = "pattern based call filter")
public class PatternBasedCallFilterScript extends CallFilterBase implements CallFilterScript {

  private static final long serialVersionUID = -2160356875282654766L;

  public PatternBasedCallFilterScript() {
    super(LoggerFactory.getLogger(PatternBasedCallFilterScript.class));
    allowedANumber = "(.*)";
    deniedANumber = "";
    allowedBNumber = "\\+?3?8?(.*)";
    deniedBNumber = "";
    replaceBNumber = "$1";
  }

  @Override
  public boolean isCallAccepted(final PhoneNumber aPhoneNumber, final PhoneNumber bPhoneNumber) {
    return isNumberMatches(aPhoneNumber, bPhoneNumber);
  }

  @Override
  public PhoneNumber substituteBNumber(final PhoneNumber bPhoneNumber) {
    return super.substituteBNumber(bPhoneNumber);
  }

}
