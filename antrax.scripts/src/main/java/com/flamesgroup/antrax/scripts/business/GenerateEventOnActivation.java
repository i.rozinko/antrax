/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.EventHelper;
import com.flamesgroup.antrax.helper.business.EventStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Script(name = "events on activation", doc = "generates event on first register of sim card in GSM")
public class GenerateEventOnActivation implements BusinessActivityScript, GenericEventListener, StatefullScript, ActivityListener {

  private static final long serialVersionUID = -7367279710443938965L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnActivation.class);

  @StateField
  private final List<GenericEvent> alreadyDoneEvents = new LinkedList<>();
  private final List<GenericEvent> events = new LinkedList<>();
  private TimePeriod waitEventTimeout = new TimePeriod(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(40));
  private boolean lockOnFailure = true;

  private volatile EventHelper eventHelper;

  private final ScriptSaver saver = new ScriptSaver();

  synchronized GenericEvent getCurrentEvent() {
    Iterator<GenericEvent> doneIter = alreadyDoneEvents.iterator();
    Iterator<GenericEvent> newIter = events.iterator();
    while (doneIter.hasNext() && newIter.hasNext()) {
      GenericEvent doneE = doneIter.next();
      GenericEvent newE = newIter.next();

      if (!doneE.getEvent().equals(newE.getEvent())) {
        doneIter.remove();
        while (doneIter.hasNext()) {
          doneIter.next();
          doneIter.remove();
        }
        return newE;
      }
    }
    if (newIter.hasNext()) {
      return newIter.next();
    }
    return null;
  }

  private synchronized boolean hasEvent() {
    return getCurrentEvent() != null;
  }

  synchronized void nextEvent() {
    alreadyDoneEvents.add(getCurrentEvent());
    saver.save();
  }

  @ScriptParam(name = "wait event timeout", doc = "max time to wait event processing")
  public void setWaitEventTimeout(final TimePeriod waitEventTimeout) {
    this.waitEventTimeout = waitEventTimeout;
  }

  public TimePeriod getWaitEventTimeout() {
    return waitEventTimeout;
  }

  @ScriptParam(name = "event", doc = "event to generate on activation")
  public void addEvent(final String event) {
    String failureResponce = "'" + event + "' activation failed " + Math.random() + System.nanoTime();
    String successResponce = "'" + event + "' activation successed " + Math.random() + System.nanoTime();
    GenericEvent gevent = GenericEvent.checkedEvent(event, successResponce, failureResponce);
    this.events.add(gevent);
  }

  public void addEvent(final int index, final String event) {
    String failureResponce = "'" + event + "' activation failed " + Math.random() + System.nanoTime();
    String successResponce = "'" + event + "' activation successed " + Math.random() + System.nanoTime();
    GenericEvent gevent = GenericEvent.checkedEvent(event, successResponce, failureResponce);
    this.events.add(0, gevent);
  }

  public String getEvent() {
    return "activation";
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @Override
  public String describeBusinessActivity() {
    if (eventHelper == null) {
      return "generate event [" + getCurrentEvent() + "] on activation";
    } else {
      return "event [" + getCurrentEvent() + "] was completed with status [" + eventHelper.getStatus() + "]";
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (eventHelper == null) {
      logger.debug("[{}] - counting current event from done events {} and events {}", this, alreadyDoneEvents, events);
      GenericEvent currentEvent = getCurrentEvent();
      eventHelper = new EventHelper(currentEvent, waitEventTimeout.getPeriod());
      logger.debug("[{}] - generate event {} ", this, currentEvent);
      eventHelper.generateEvent(channel);
    } else {
      if (eventHelper.getStatus() == EventStatus.FAILED || eventHelper.getStatus() == EventStatus.TIMEOUT) {
        logger.debug("[{}] - {}", this, eventHelper.getFailReason());

        if (lockOnFailure) {
          //after the card unlocking current event will be executed again
          logger.debug("[{}] - going to lock card", this);
          channel.lock("Locked: " + eventHelper.getFailReason());
        } else {
          //skip this event to prevents infinite execution of the current event
          nextEvent();
        }

      } else if (eventHelper.getStatus() == EventStatus.SUCCEED) {
        nextEvent();
      }
      eventHelper = null;
    }
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (eventHelper == null) {
      return hasEvent();
    } else {
      return eventHelper.isFinished();
    }
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    logger.debug("[{}] - caught event {}", this, event);
    if (eventHelper != null) {
      eventHelper.handleEvent(event, args);
      saver.save();
    }
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public String toString() {
    return String.format("GenerateEventOnActivation(done=%s, all=%s)", alreadyDoneEvents, events);
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
  }

  @Override
  public void handleActivityEnded() {
    if (eventHelper != null) {
      logger.debug("[{}] - end activity with not completed event [{}]", this, getCurrentEvent());
      eventHelper = null;
      nextEvent(); // skip this event to prevents infinite execution of the current event
      saver.save();
    }
  }

}
