/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.factor;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;

/**
 * Returns user defined constant as factor
 */
@Script(name = "constant", doc = "returns user defined constant as factor")
public class ConstFactor extends FactorBase {

  private int userDefinedConst = 1;

  @ScriptParam(name = "factor", doc = "will be returned as a factor")
  public void setFactorConstant(final int factorConstant) {
    this.userDefinedConst = factorConstant;
  }

  public int getFactorConstant() {
    return userDefinedConst;
  }

  @Override
  public long countFactor() {
    return getPriority().getValue() + userDefinedConst * getMultiplier();
  }

}
