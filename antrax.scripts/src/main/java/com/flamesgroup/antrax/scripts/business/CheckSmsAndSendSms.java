/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Script(name = "check SMS and send SMS", doc = "analyzes incoming SMS and send SMS to the same number")
public class CheckSmsAndSendSms implements BusinessActivityScript, SMSListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(CheckSmsAndSendSms.class);

  private String event = "check_sms_and_send_sms";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));

  private String sendSmsText = "";
  private String incomingTextRegex = "";
  private String smsNumberRegex = "";
  private String smsNumberReplacePattern = "";

  private String incomingSmsNumber;
  private String incomingSmsText;

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private final AtomicLong smsTime = new AtomicLong();

  @ScriptParam(name = "event", doc = "check sms will be started after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  @ScriptParam(name = "incoming sms text regex", doc = "regex for parse text from incoming SMS")
  public void setIncomingTextRegex(final String incomingTextRegex) {
    this.incomingTextRegex = incomingTextRegex;
  }

  public String getIncomingTextRegex() {
    return incomingTextRegex;
  }

  @ScriptParam(name = "send sms text", doc = "sms text to send on incoming sms number")
  public void setSendSmsText(final String sendSmsText) {
    this.sendSmsText = sendSmsText;
  }

  public String getSendSmsText() {
    return sendSmsText;
  }

  @ScriptParam(name = "sms number pattern", doc = "sms number pattern")
  public void setSmsNumberRegex(final String smsNumberRegex) {
    this.smsNumberRegex = smsNumberRegex;
  }

  public String getSmsNumberRegex() {
    return smsNumberRegex;
  }

  @ScriptParam(name = "sms number replace pattern", doc = "sms number replace pattern")
  public void setSmsNumberReplacePattern(final String smsNumberReplacePattern) {
    this.smsNumberReplacePattern = smsNumberReplacePattern;
  }

  public String getSmsNumberReplacePattern() {
    return smsNumberReplacePattern;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && phoneNumber.matches(smsNumberRegex) && text != null && text.matches(incomingTextRegex)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches with pattern [{}]", this, phoneNumber, text, incomingTextRegex);
      smsWait.set(false);
      incomingSmsNumber = phoneNumber;
      incomingSmsText = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (incomingSmsText == null) {
      smsTime.set(0);
      logger.debug("[{}] - waiting sms timeout", this);
      return;
    }

    if (!smsNumberReplacePattern.isEmpty()) {
      incomingSmsNumber = incomingSmsNumber.replaceFirst(smsNumberRegex, smsNumberReplacePattern);
    }
    try {
      channel.sendSMS(new PhoneNumber(incomingSmsNumber), sendSmsText);
    } catch (Exception e) {
      logger.error("[{}] - while send SMS", this, e);
    } finally {
      smsTime.set(0);
      incomingSmsNumber = null;
      incomingSmsText = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (incomingSmsNumber != null && incomingSmsText != null) {
      return true;
    } else if (smsWait.get() && System.currentTimeMillis() - smsTime.get() > smsTimeout.getPeriod()) {
      smsWait.set(false);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "check SMS and send SMS";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - start waiting to receive sms with balance", this);
      smsWait.set(true);
      smsTime.set(System.currentTimeMillis());
    }
  }

}
