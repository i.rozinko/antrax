/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePrediction;

import java.util.Calendar;

public class AllowActivityAfter24hPassed extends BaseSimpleActivityScript {

  private static final long serialVersionUID = 3669551584467588023L;

  @StateField
  private long endTime;

  private final ScriptSaver saver = new ScriptSaver();

  public AllowActivityAfter24hPassed() {
  }

  @Override
  public boolean isActivityAllowed() {
    if (endTime == 0) {
      endTime = getNextMidnight();
      saver.save();
    }
    return System.currentTimeMillis() >= endTime;
  }

  @Override
  public void handlePeriodEnd() {
    endTime = 0;
    saver.save();
  }

  private long getNextMidnight() {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.DAY_OF_YEAR, 1);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTimeInMillis();
  }

  @Override
  public Prediction predictEnd() {
    if (isActivityAllowed()) {
      return new AlwaysFalsePrediction();
    }
    return new AlwaysTruePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    if (endTime == 0)
      return new AlwaysFalsePrediction();
    return new TimePrediction(endTime);
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
