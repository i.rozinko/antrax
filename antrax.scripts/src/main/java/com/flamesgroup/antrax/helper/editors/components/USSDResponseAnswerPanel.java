/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class USSDResponseAnswerPanel extends JPanel {

  private static final long serialVersionUID = -7970678118976524479L;

  private final JTextField pattern;
  private final JTextField answer;
  private final JLabel patternLabel;
  private final JLabel answerLabel;
  private final JCheckBox checkAnswer;

  public USSDResponseAnswerPanel() {
    pattern = new JTextField();
    answer = new JTextField();
    patternLabel = new JLabel("Pattern");
    patternLabel.setLabelFor(pattern);
    answerLabel = new JLabel("Answer");
    answerLabel.setLabelFor(answer);
    checkAnswer = new JCheckBox();
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    pattern.setEditable(true);
    answer.setEditable(true);

    add(patternLabel);
    add(pattern);
    add(checkAnswer);
    add(answerLabel);
    add(answer);
    setMinimumSize(new Dimension(500, this.getPreferredSize().height));
    checkAnswer.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(final ChangeEvent e) {
        if (!checkAnswer.isSelected()) {
          answerLabel.setEnabled(false);
          answer.setEnabled(false);
        } else {
          answerLabel.setEnabled(true);
          answer.setEnabled(true);
        }
      }
    });
  }

  public boolean hasAnswer() {
    return checkAnswer.isSelected();
  }

  public String getPattern() {
    if (pattern.getText() == null) {
      return "";
    }
    return pattern.getText();
  }

  public String getAnswer() {
    if (answer.getText() == null) {
      return "";
    }
    return answer.getText();
  }

  public USSDResponseAnswer getUSSDResponseAnswer() {
    String answ = "";
    boolean hasAnsw = hasAnswer();
    if (hasAnsw) {
      answ = getAnswer();
    }
    return new USSDResponseAnswer(getPattern(), answ, hasAnsw);
  }

  public void setUSSDResponseAnswer(final USSDResponseAnswer ussdResponseAnswer) {
    pattern.setText(ussdResponseAnswer.getResponsePattern());
    String answ = ussdResponseAnswer.getResponseAnswer();
    if (!ussdResponseAnswer.hasResponseAnswer()) {
      answ = null;
    }
    if (answ == null) {
      checkAnswer.setSelected(false);
      answerLabel.setEnabled(false);
      answer.setEnabled(false);

    } else {
      checkAnswer.setSelected(true);
      answer.setText(answ);
    }
  }

}
