/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.callfilter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberMatcher {

  private final Pattern[] deny;
  private final Pattern[] allow;
  private final String[] subst;

  public PhoneNumberMatcher(final String prefixDeny, final String prefixAllow, final String prefixSubstituteAllow) {
    if (prefixDeny == null || prefixDeny.isEmpty()) {
      deny = new Pattern[0];
    } else {
      deny = compile(prefixDeny.split(";"));
    }
    if (prefixAllow == null || prefixAllow.isEmpty()) {
      allow = new Pattern[0];
    } else {
      allow = compile(prefixAllow.split(";"));
    }
    if (prefixSubstituteAllow == null || prefixSubstituteAllow.isEmpty()) {
      subst = new String[0];
    } else {
      subst = prefixSubstituteAllow.split(";");
    }
  }

  private Pattern[] compile(final String[] regexes) {
    Pattern[] retval = new Pattern[regexes.length];
    for (int i = 0; i < retval.length; ++i) {
      retval[i] = Pattern.compile(regexes[i]);
    }
    return retval;
  }

  public boolean matches(final String value) {
    for (Pattern d : deny) {
      if (d.matcher(value).matches()) {
        return false;
      }
    }
    for (Pattern a : allow) {
      if (a.matcher(value).matches()) {
        return true;
      }
    }
    return false;
  }

  public String substituteNumber(final String phone) {
    if (subst.length == 0) {
      return phone;
    }
    for (int i = 0; i < allow.length; ++i) {
      Matcher m = allow[i].matcher(phone);
      if (m.matches()) {
        String s;
        if (subst.length <= i) {
          s = subst[0];
        } else {
          s = subst[i];
        }
        return m.replaceFirst(s);
      }
    }
    return phone;
  }

}
