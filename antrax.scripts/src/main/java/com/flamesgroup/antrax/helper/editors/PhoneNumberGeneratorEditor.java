/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PhoneNumberGeneratorEditor extends BasePropertyEditor<PhoneNumberGenerator> {

  private static class EditorComponent extends JTabbedPane {
    private static final long serialVersionUID = 8513819102289501391L;
    private static final String advDoc = "<html><body>" +
    /*  */"<p align=center>" +
    /*    */"Add advanced generation pattern using this syntax:</p>" +
    /*  */"<table>" +
    /*    */"<tr><td>1,2,3,4,5,6,7,8,9,0<td>the corresponding number" +
    /*    */"<tr><td>. <td> random number (one of 0,1,2,3,4,5,6,7,8,9)" +
    /*    */"<tr><td>[123] <td>  random number from specified list. In this case to one of 1,2 or 3" +
    /*    */"<tr><td>123|456 <td>  one of 123 or 456 selected randomly" +
    /*    */"<tr><td>1|2|3|4|5 <td>  one of 1,2,3,4,5 selected randomly" +
    /*    */"<tr><td>0(123|456) <td>  one of 0123 or 0456 selected randomly" +
    /*    */"<tr><td>\\. <td>  ." +
    /*    */"<tr><td>\\[ <td>  [" +
    /*    */"<tr><td>\\( <td>  (" +
    /*    */"<tr><td>\\| <td>  |</table>" +
    /*  */"</body></html>";
    private static final String basicDoc = "<html><body>" +
    /*  */"<p align=center>" +
    /*    */"Add basic phone patterns only by selecting operators codes.</p>" +
    /*  */"<p>" +
    /*    */"For example you can add <tt>8050</tt> and <tt>8066</tt> to generate numbers to MTS and Jeans.</p>" +
    /*    */"<p>Click on code from list to remove it using button.</p>" +
    /*    */"<p>You can press enter while editing code to add it to the list.</p>" +
    /*  */"</body></html>";
    private final JTextField txtPattern = new JTextField(15);
    private DefaultListModel listModel;

    public EditorComponent() {
      final CardLayout layout = new CardLayout();
      final JPanel basicPane = new JPanel(layout);
      basicPane.add(createBasicPane(), "EDIT");
      basicPane.add(createParsingFailurePane(layout, basicPane), "ERROR");

      addTab("Basic", basicPane);
      addTab("Advanced", createAdvancedPane());

      addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(final ChangeEvent e) {
          if (getSelectedIndex() == 0) {
            updateListModel(layout, basicPane);
          }
        }
      });

      invalidate();
    }

    private JPanel createParsingFailurePane(final CardLayout layout, final JPanel basicPane) {
      JPanel retval = new JPanel(new FlowLayout());
      retval.add(new JLabel("Pattern is too complex"), BorderLayout.PAGE_START);
      JButton button = new JButton("Edit Anyway");
      retval.add(button, BorderLayout.CENTER);

      button.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          layout.show(basicPane, "EDIT");
        }
      });
      return retval;
    }

    private JPanel createBasicPane() {
      JPanel retval = new JPanel();
      GroupLayout layout = new GroupLayout(retval);
      retval.setLayout(layout);
      final JTextField newCode = new JTextField(8);
      final JButton btnAdd = new JButton("add");
      listModel = new DefaultListModel();
      final JList list = new JList(listModel);
      JScrollPane scroll = new JScrollPane(list);
      JLabel doc = new JLabel(basicDoc);

      layout.setVerticalGroup(
      /*  */layout.createSequentialGroup()
      /*    */.addComponent(doc)
      /*    */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      /*      */.addComponent(newCode)
      /*      */.addComponent(btnAdd))
      /*    */.addComponent(scroll));
      layout.setHorizontalGroup(
      /*  */layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      /*    */.addComponent(doc)
      /*    */.addGroup(layout.createSequentialGroup()
      /*      */.addComponent(newCode)
      /*      */.addComponent(btnAdd))
      /*    */.addComponent(scroll));

      btnAdd.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          if ("add".equals(e.getActionCommand())) {
            if (newCode.getText().isEmpty()) {
              return;
            }
            if (!newCode.getText().matches("\\d+")) {
              return;
            }
            if (listModel.contains(newCode.getText())) {
              return;
            }
            listModel.addElement(newCode.getText());
          } else if ("remove".equals(e.getActionCommand())) {
            if (list.getSelectedIndex() >= 0) {
              listModel.removeElementAt(list.getSelectedIndex());
            }
          }
          updateAdvancedText(listModel.toArray());
        }
      });

      list.addFocusListener(new FocusListener() {
        @Override
        public void focusLost(final FocusEvent e) {
        }

        @Override
        public void focusGained(final FocusEvent e) {
          btnAdd.setActionCommand("remove");
          btnAdd.setText("remove");
        }
      });

      newCode.addFocusListener(new FocusListener() {
        @Override
        public void focusLost(final FocusEvent e) {
        }

        @Override
        public void focusGained(final FocusEvent e) {
          btnAdd.setActionCommand("add");
          btnAdd.setText("add");
          newCode.selectAll();
        }
      });

      newCode.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          btnAdd.doClick();
          newCode.selectAll();
        }
      });

      return retval;
    }

    private void updateAdvancedText(final Object[] values) {
      String[] codes = new String[values.length];
      for (int i = 0; i < codes.length; ++i) {
        codes[i] = values[i].toString();
      }
      txtPattern.setText(PhoneNumberGenerator.createGeneratorUsingCodes(codes).toString());
    }

    private void updateListModel(final CardLayout layout, final JPanel pane) {
      String pattern = txtPattern.getText();
      if (pattern.matches("\\(\\d+[.]*([|]\\d+[.]*)*\\)[.]*")) {
        layout.show(pane, "EDIT");
      } else {
        layout.show(pane, "ERROR");
      }

      listModel.clear();
      for (String code : pattern.replaceFirst("\\(([^)]*)[)$]", "$1").split("[|]")) {
        listModel.addElement(code.replaceAll("\\.", ""));
      }
    }

    private JPanel createAdvancedPane() {
      JPanel retval = new JPanel(new BorderLayout());
      retval.add(new JLabel(advDoc), BorderLayout.PAGE_START);
      retval.add(txtPattern, BorderLayout.PAGE_END);

      return retval;
    }

    public String getValue() {
      return txtPattern.getText();
    }

    public void setValue(final String value) {
      txtPattern.setText(value);
      fireStateChanged();
    }
  }

  private final EditorComponent editorComponent = new EditorComponent();

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Class<? extends PhoneNumberGenerator> getType() {
    return PhoneNumberGenerator.class;
  }

  @Override
  public PhoneNumberGenerator getValue() {
    return new PhoneNumberGenerator(editorComponent.getValue());
  }

  @Override
  public void setValue(final PhoneNumberGenerator value) {
    if (value == null) {
      this.editorComponent.setValue("");
    } else {
      this.editorComponent.setValue(value.toString());
    }
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    final EditorComponent component = new EditorComponent();
    frame.getContentPane().add(new JScrollPane(component));
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        component.setValue("(8066|8063)......");
        frame.setVisible(true);
      }
    });
  }

}
