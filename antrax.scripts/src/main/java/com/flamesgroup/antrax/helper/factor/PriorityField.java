/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.factor;

import com.flamesgroup.antrax.scripts.utils.TimePeriod;

public enum PriorityField {

  ZERO("neutral (0)", 10),
  FIRST("1", 9),
  SECOND("2", 8),
  THIRD("3", 7),
  FOURTH("4", 6),
  FIFTH("5", 5),
  SIXTH("6", 4),
  SEVENTH("7", 3),
  EIGHTH("8", 2),
  NINTH("9", 1),
  TENTH("agressive (10)", 0);

  final long base = TimePeriod.inHours(525600); // 1 year

  private final String text;
  private final long value;

  PriorityField(final String text, final long value) {
    this.text = text;
    this.value = base * value;
  }

  @Override
  public String toString() {
    return text;
  }

  public long getValue() {
    return value;
  }

}
