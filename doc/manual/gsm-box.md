## GSM box(GB4, GB8)
**GSM box** is a hardware component of **Antrax system**.
**GSM box** performs the role of switchboard.
**GSM box** manufactured in two types:
  * **GB4 - GSM box** operating with **4 GSM modules**;

![gb4_intro_3](gsm-box.assets/gb4_intro_3.jpg)

![gb4_intro_4](gsm-box.assets/gb4_intro_4.jpg)

  * **GB8 - GSM box** operating with **8 GSM modules**.

![gb4_intro_3](gsm-box.assets/gb8_intro_3.jpg)

![gb4_intro_4](gsm-box.assets/gb8_intro_4.jpg)

### GB4
**Basic characteristics of GB4:**
* **4 GSM channels** on the basis of Telit GL865-QUAD modules. Telit GL865-QUAD modules support the frequency bands: **GSM 850/900/1800/1900** (GSM modules operate only with the second generation of wireless telephone technologies - **2G**);
* Possibility of IMEI change.

**Characteristics of control unit:**
* __CPU__: Allwinner A20 dual core Cortex-A7 processor, each core with a frequency of 1GHz;
* __RAM__: 1GB DDR3;
* __ROM__: 8GB microSD card;
* __Power supply__: 100-240V AC, 50/60Hz;
* __OS__: Debian 8.X armel7.

**Overall dimension GB4 in millimeter: 137х175х44**

![size_gb4](gsm-box.assets/size_gb4.jpg)

**Weight GB4 (net):**
GSMBOX4 - 650g
power supply with wires - 460g


### GB8
**GB8 basic characteristics:**
* **8 GSM channels** on the basis of Telit GL865-QUAD modules. Telit GL865-QUAD modules support the frequency bands: **GSM 850/900/1800/1900** (GSM modules operate only with the second generation of wireless telephone technologies - **2G**);
* Possibility of IMEI change.

**Characteristics of control unit:**
* __CPU__: Allwinner A20 dual core Cortex-A7 processor, each core with the frequency of 1GHz;
* __RAM__: 1GB DDR3;
* __ROM__: 8GB microSD card;
* __Power supply__: 100-240V AC, 50/60Hz;
* __OS__: Debian 8.X armel7.

**Overall dimension GB8 in millimetres: 137х175х44**

![size_gb8](gsm-box.assets/size_gb8.jpg)

**Weight GB8 (net):**
GSMBOX8 - 700g
power supply with wires - 460g


### GSM box indication
GSM box indication: **GB4** and **GB8**.

| State of GSM channel | Green LED | Red LED |
| -------- | -------- | -------- |
| switched on | is not lit | is not lit |
| network search / not registered | rapid blinking | is not lit|
| registered | slow blinking | is not lit |
| active call | solid glow | is not lit |
| switched off | is not lit | is not lit |
| blocked | is not lit | solid glow |
