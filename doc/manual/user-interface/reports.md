## REPORTS

Given section contains the reports on SIM cards and SIM groups activities. There are three types of records in the section:
  * CDR
  * SIM HISTORY
  * SIM GROUP REPORT

### CDR

CDR report contains the information about about all calls, which were performed or accepted  with selected card.

![cdr](reports.assets/cdr.png)

#### Functional buttons

| Button | Function|
| -------- | -------- |
| ![export_cvs](reports.assets/export_cvs.png) | export to csv |
| ![export_cvs](reports.assets/clear_queries.png) | clear up the results of report |
| ![export_cvs](reports.assets/clear_filters.png) | clear up the filter parameters |
| ![export_cvs](reports.assets/find.png) | search application with established filters |

#### Filters

Filters list for setting:

| Filter | Filter description |
| -------- | -------- |
| ![time_filter](reports.assets/time_filter.png) | time period is to be selected in the calendar ( required dates and time), the report will be displayed for the specified time period |
| ![sim_groups](reports.assets/sim_groups.png) | SIM group selection |
| ![gsm_groups](reports.assets/gsm_groups.png) | GSM group selection |
| ![caller_number](reports.assets/caller_number.png) | display the report only on specified caller telephone number |
| ![called_number](reports.assets/called_number.png) | display the report only on the number being called to  |
| ![sim_uid](reports.assets/sim_uid.png) | display the report based on unique serial number of SIM-card  |
| ![page_size](reports.assets/page_size.png) | selection of record quantity on one page of the report |
| ![number_page](reports.assets/number_page.png) | selection of report page number |

#### Assignment of report fields

Table fields of SIM card list.

| Heading | Description|
| -------- | -------- |
| server | name of Voice server |
| caller number | number of caller |
| called number | number being called |
| setup | start time of call setup |
| start | start time of the conversation |
| stop | end time of the conversation |
| duration | call duration |
| PDD | delay between the number dialing and the first beep |
| call type | call direction |
| drop reason | call completion reason |
| drop code | code of call completion |

 Call path info table contains additional information about the call, selected in the basic table of report. If the call was terminated on one of the voice channels for some reasons , it can go directly to the next voice channel. In this case the specified table will contain more than one line.

Call path info table fields and their description.

| Heading | Description|
| -------- | -------- |
| sim channel | SIM holder number and SIM |
| gsm channel | number of GSM channel |
| simUID | unique number of SIM card in the system |
| simGroup | name of SIM group |
| gsmGroup | name of GSM group |
| startTime | start time of the call |
| dropReason | call completion reason |
| dropCode | code of call completion |

A field with a description of the reason for the end of the call.:

**LIMIT_FAS_ATTEMPTS** - reached a limit when searching for a new channel when FAS is detected;

**ANSWER_ERROR** - An error occurred while trying to pick up the phone on an incoming call;

**START_AUDIO_PROBLEM** - An error occurred opening the audio channel to the GSM module;

**ORIGINATING_ERROR** - An error occurred in the beginning of the call to the SIP network origination;

**TERMINATING_ERROR** - There was an error in the beginning of the call in the GSM network, another call is being processed or the channel is being released;

**INVALID_CALLER_FILTER** - Error setting templates for the caller number in the script for the original;

**INVALID_CALLED_FILTER** - Error in the called number in the script for the original or in the call filter.

#### Report formation from specified sim card

To utilize given report you need to go to SIM SERVERS section, select the line with required SIM card and drag it beyond the SIM holder field to the CDR tab of REPORTS section, as it is shown on the pic below.

![sim_to_cdr](reports.assets/sim_to_cdr.png)

### Sim History

Sim History report contains the history of events on selected SIM card.

![sim_history](reports.assets/sim_history.png)

#### Functional buttons

| Button| Function|
| -------- | -------- |
| ![export_to_excel](reports.assets/export_to_excel.png) | export to Excel |
| ![clear_queries](reports.assets/clear_queries.png) | clear up the report results |
| ![clear_filters](reports.assets/clear_filters.png) | clear up the filter parameters |
| ![execute](reports.assets/execute.png) | show the report results on the screen |
| ![execute_plus](reports.assets/execute_plus.png) | adding the report results to the screen |

#### Filters

<table>
	<tr><th>Filter</th><th>Filter description</th></tr>
	<tr><td><img src="reports.assets/time_filter.png" alt="time_filter"></td><td>selection of time range in the calendar (required dates and time), the report will be displayed only for this time period</td></tr>
	<tr><td><img src="reports.assets/sim_channel.png" alt="sim_channel"></td><td>report on number of SIM board and SIM holder</td></tr>
	<tr><td><img src="reports.assets/gsm_channel.png" alt="gsm_channel"></td><td>report only on number of GSM board and GSM channel</td></tr>
	<tr><td><img src="reports.assets/sim_groups.png" alt="sim_groups"></td><td>selection of SIM group</td></tr>
	<tr><td><img src="reports.assets/phone_number.png" alt="phone_number"></td><td>report display only on specified telephone number</td></tr>
	<tr><td><img src="reports.assets/sim_uid.png" alt="sim_uid"></td><td>report on unique serial number of the card</td></tr>
	<tr><td rowspan="5"><img src="reports.assets/active.png" alt="active"></td></tr>
	<tr><td><b>Report on SIM card activity:</b></td></tr>
	<tr><td><b>Any:</b> - report on all SIM cards</td></tr>
	<tr><td><b>Active:</b> - report only on active SIM cards</td></tr>
	<tr><td><b>Inactive:</b> - report only on inactive SIM cards</td></tr>
	<tr><td rowspan="5"><img src="reports.assets/enabled.png" alt="enabled"></td></tr>
	<tr><td><b>Report on utilized/underutilized SIM cards:</b></td></tr>
	<tr><td><b>Any:</b> - report on all SIM cards</td></tr>
	<tr><td><b>Enabled:</b> - report only on utilized SIM cards</td></tr>
	<tr><td><b>Disabled:</b> - report only on underutilized SIM cards</td></tr>
</table>

#### Report fields assignment

Table fields of SIM card list.

| Heading| Description|
| -------- | -------- |
| SIM UID | unique number of SIM card in the system |
| From date | start time of report period |
| To date | end time of report period |
| SIM channel | number of SIM board and SIM holder |
| GSM channel | number of GSM board and GSM channel |
| SIM group | SIM group |
| Active | SIM card is active |
| Enabled | SIM card is involved |
| Phone number | telephone number |

Table fields of sim card behavioral history.

| Heading| Description|
| -------- | -------- |
| start time | time of event |
| description | event description |

#### Report formation from the specified sim card

To form this report, you need to go to SIM SERVERS section, select the line with required SIM card and drag it beyond the SIM holder field to the Sim History tab of REPORTS section, as it is shown on the picture below.

![sim_to_sim_history](reports.assets/sim_to_sim_history.png)

### Sim Groups Report

Sim Groups Report contains the statistics about the activity of all SIM cards in every SIM group in the system.

![sim_group_report](reports.assets/sim_group_report.png)

#### Filters

| Filter | Filter description | 
| -------- | -------- |
| ![non_zero_groups](reports.assets/non_zero_groups.png) | If selected sim groups without any activity will be absent |

#### Report formation from the specified sim card

| Heading| Description|
| -------- | -------- |
| Group | group name |
| Total | overall quantity of SIM cards in the specified group |
| Locked | quantity of blocked SIM cards |
| Unloked | quantity of unblocked SIM cards |
| Total Calls | total amount of calls conducted with given SIM card of specified SIM group |
| Successful Calls | quantity of successful calls |
| Calls Duration | overall duration of calls |
| ACD | average duration of calls |
| Inc. Total Calls | total quantity of incoming calls from GSM |
| Inc. Successful Calls | total quantity of successful incoming calls from GSM |
| Successful SMS | count of successful SMS |
| Total SMS | total quantity of SMS |
| Inc. Total SMS | total quantity of incoming SMS |
| Total SMS statuses | count of SMS statuses |
