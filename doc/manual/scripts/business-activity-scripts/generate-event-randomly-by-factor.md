## generate event randomly by factor

Given script is designed for any event performance randomly by the factor.

![g_e_rand_by_factor](generate-event-randomly-by-factor.assets/g_e_rand_by_factor.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event generation period** | regularity of **send event** performance |
|  **lock on failure** | block the card on unsuccessful performance |
|  **send event** | generated event |
|  **event factor** | call factor (amount of calls after which generating with the possibility **event chance** is required. is not taken into account if 0) |
|  **event chance** | possibility of **send event** performance |
|  **event limit** | quantity of generated **send event** for **event generation period** |
