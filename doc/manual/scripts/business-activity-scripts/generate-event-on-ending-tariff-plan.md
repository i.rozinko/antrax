## generate event on ending tariff plan

Given script is designed to perform any action on before or after the end date of tariff plan.

![g_e_on_ending_tariff_plan](generate-event-on-ending-tariff-plan.assets/g_e_on_ending_tariff_plan.png)

### Parameters

| name| description|
| -------- | -------- |
|  **generate time for event before end** | time to generate **event before end** |
|  **event before end** | event, which will be generated for a number of **days number until end** |
|  **event after end** | event, which will be generated after end tariff plan |
|  **generate time for event after end** | time to generate **event after end** |
|  **days number until end** | days number to generate **event before end** |
