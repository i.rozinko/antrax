## generate event on fas

Given script is designed for any action performance when particular active call duration is reached.

![g_e_on_fas](generate-event-on-fas.assets/g_e_on_fas.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event generated when FAS is detected |
|  **wait event timeout** | period FAS is analyzed for |
|  **lock on failure** | block the card on failure |
|  **FAS limit** | amount of FAS calls which **event** is generated on |
