## call

The task of given script is to conduct a call to a definite number.

![call](call.assets/call.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | initial event essential for activation of this script |
|  event on true | an event on successful call performance |
|  min active duration | minimal call duration. Established by the frequency range |
|  max active duration | maximal call duration. Established by the frequency range |
|  max attempt duration | maximal dial duration. Established by the frequency range |
|  require active | the call will be considered successful if the receiver was picked up |
|  number pattern | number; usually sample is used in the form of regular meaning |
|  event on false | event on unsuccessful call performance |
|  event on true | generate this event on success executing this script |
|  drop calls | terminate an incoming or outgoing call, if the flag is not set, the active call, the script will not be executed |
|  path in registry | the path to the value in the Registry section in the UTILITY section|
|  call attempts count | the number of unsuccessful attempts to make calls to the same number |
|  activity attempts count | the number of unsuccessful attempts to make calls to different numbers |

### Basic number pattern set-up

On the basic setup pad numbers and operator's codes can be added by means of pressing the **add** button

![call_basic](call.assets/call_basic.png)

Bold number can be removed by pressing the **remove** button

![call_basic_remove](call.assets/call_basic_remove.png)

### Advanced number pattern installation

![call_advanced](call.assets/call_advanced.png)

In this particular case supplement of numbers is carried out by means of regular expressions
Basic arrangement:

|  . | random number |
| -------- | -------- |
|  [123] | random number from the list  1,2 or 3 |
|  123%%|%%456 | either 123 or 456 |
|  1%%|%%2%%|%%3%%|%%4%%|%%5 | 1 or 2 or 3 or 4 or 5 |
|  0(123%%|%%456) | either 0123 or 0456 |

Special symbols such as:.  [ ]  ()  | are not to be screened by the backslash symbol.

 

