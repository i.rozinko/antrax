/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager;

import com.flamesgroup.antrax.control.communication.AntraxServerLongInformation;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerShortInfo;
import com.flamesgroup.antrax.voiceserver.IVoiceServerStatusManager;
import com.flamesgroup.antrax.voiceserver.manager.rtstat.ChannelsRuntime;
import com.flamesgroup.antrax.voiceserver.manager.rtstat.LongServerRuntime;
import com.flamesgroup.antrax.voiceserver.manager.rtstat.ShortServerRuntime;

import java.util.List;

public class VoiceServerStatusManager implements IVoiceServerStatusManager {

  private ChannelsRuntime channelsRt;
  private LongServerRuntime longServerRt;
  private ShortServerRuntime shortServerRt;

  @Override
  public List<IVoiceServerChannelsInfo> getChannelsInfo() {
    return channelsRt.getChannelsInfo();
  }

  @Override
  public AntraxServerLongInformation getLongServerInfo() {
    return longServerRt.getLongServerInfo();
  }

  @Override
  public IVoiceServerShortInfo getShortServerInfo() {
    return shortServerRt.getShortServerInfo();
  }

  public void setShortServerRt(final ShortServerRuntime shortServerRt) {
    this.shortServerRt = shortServerRt;
  }

  public void setChannelsRt(final ChannelsRuntime channelsRt) {
    this.channelsRt = channelsRt;
  }

  public void setLongServerRt(final LongServerRuntime longServerRt) {
    this.longServerRt = longServerRt;
  }

}
