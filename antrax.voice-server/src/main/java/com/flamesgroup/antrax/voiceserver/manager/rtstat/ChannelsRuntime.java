/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformationImpl;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SimChannelInformationImpl;
import com.flamesgroup.antrax.control.communication.VoiceServerChannelsInfo;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState.State;
import com.flamesgroup.antrax.voiceserver.actlog.ActivityLogger;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChannelsRuntime implements ActivityLogger {

  private final Logger logger = LoggerFactory.getLogger(ChannelsRuntime.class);

  private final Map<ChannelUID, VoiceServerChannelsInfo> channelsInfoMap = new HashMap<>();
  private final Map<ChannelUID, SimChannelInformationImpl> cemInformationMap = new HashMap<>();
  private final Map<ChannelUID, VoiceServerChannelsInfo> connectedChannelsInfoMap = new HashMap<>();
  private final Map<State, EnumSet<State>> states = new HashMap<>();

  public ChannelsRuntime() {
    states.put(State.IDLE, EnumSet.of(State.DIALING));
    states.put(State.DIALING, EnumSet.of(State.IDLE, State.ALERTING, State.ACTIVE));
    states.put(State.ALERTING, EnumSet.of(State.IDLE, State.ACTIVE));
    states.put(State.ACTIVE, EnumSet.of(State.IDLE));
  }

  public List<IVoiceServerChannelsInfo> getChannelsInfo() {
    return new ArrayList<>(channelsInfoMap.values());
  }

  public IVoiceServerChannelsInfo getIVoiceServerChannelsInfo(final ChannelUID gsmUnit) {
    return channelsInfoMap.get(gsmUnit);
  }

  @Override
  public void logDeclaredGsmUnit(final GSMChannel gsmChannel) {
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = new MobileGatewayChannelInformationImpl(gsmChannel.getChannelUID());
    mobileGatewayChannelInfo.setGsmChannel(gsmChannel);
    VoiceServerChannelsInfo prev = channelsInfoMap.put(gsmChannel.getChannelUID(), new VoiceServerChannelsInfo(mobileGatewayChannelInfo));
    if (prev != null) {
      logger.warn("[{}] - found gsm unit {} before releasing it: {}", this, gsmChannel.getChannelUID(), prev);
    }
  }

  @Override
  public void logActiveGsmUnit(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    updateChannelConfig(gsmUnit, gsmChannel, channelConfig);
  }

  @Override
  public void updateChannelConfig(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
    VoiceServerChannelsInfo channel = channelsInfoMap.get(gsmUnit);
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
    mobileGatewayChannelInfo.setLive(true);
    mobileGatewayChannelInfo.setGsmChannel(gsmChannel);
    mobileGatewayChannelInfo.setChannelConfig(channelConfig);
  }

  @Override
  public void logGsmUnitChangedGroup(final ChannelUID gsmUnit, final GSMGroup group) {
    VoiceServerChannelsInfo channel = channelsInfoMap.get(gsmUnit);
    if (channel != null) {
      MobileGatewayChannelInformationImpl info = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
      if (info != null) {
        info.getGSMChannel().setGsmGroup(group);
      }
    }
  }

  @Override
  public void logGsmUnitLock(final ChannelUID gsmUnit, final boolean lock, final String lockReason) {
    VoiceServerChannelsInfo channel = channelsInfoMap.get(gsmUnit);
    if (channel == null) {
      return;
    }
    GSMChannel getGsmChannel = channel.getMobileGatewayChannelInfo().getGSMChannel();
    getGsmChannel.setLock(lock).setLockReason(lockReason).setLockTime(lock ? System.currentTimeMillis() : 0);
  }

  @Override
  public void logGSMUnitReleased(final ChannelUID gsmUnit) {
    logger.debug("[{}] - gsm unit released: {}", this, gsmUnit);
    VoiceServerChannelsInfo channel = channelsInfoMap.get(gsmUnit);
    MobileGatewayChannelInformationImpl mobileGatewayChannelInfo = (MobileGatewayChannelInformationImpl) channel.getMobileGatewayChannelInfo();
    mobileGatewayChannelInfo.setLive(false);
    mobileGatewayChannelInfo.setChannelConfig(null);
  }

  @Override
  public void logSIMTakenFromSimServer(final ChannelUID simUnit, final ICCID uid) {
    cemInformationMap.put(simUnit, new SimChannelInformationImpl(simUnit, uid));
  }

  @Override
  public void logSIMReturnedToSimServer(final ChannelUID simUnit) {
    cemInformationMap.remove(simUnit);
  }

  @Override
  public void logCallChannelCreated(final ChannelUID gsmUnit, final ChannelUID simUnit) {
    VoiceServerChannelsInfo channelsInfo = channelsInfoMap.get(gsmUnit);
    SimChannelInformationImpl cemInfo = cemInformationMap.get(simUnit);
    if (channelsInfo == null) {
      logger.warn("[{}] - channel <{}:{}> created, but appropriate BekkiChannel was not found", this, gsmUnit, simUnit);
    } else if (cemInfo == null) {
      logger.warn("[{}] - channel <{}:{}> created, but appropriate SimUnit was not taken from SimServer", this, gsmUnit, simUnit);
    } else {
      channelsInfo.setSimChannelInfo(cemInfo);
      connectedChannelsInfoMap.put(simUnit, channelsInfo);
    }
  }

  @Override
  public void logCallChannelStartActivity(final ChannelUID simUnit) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setRegistered(true);
    }
  }

  @Override
  public void logCallChannelReleased(final ChannelUID simUnit) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.remove(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setSimChannelInfo(null);
      channelsInfo.reset();
    } else {
      logger.warn("[{}] - there's no channel with {} for release", this, simUnit);
    }
  }

  @Override
  public void logCallSetup(final ChannelUID simUID, final PhoneNumber phoneNumber) {
    VoiceServerChannelsInfo channel = connectedChannelsInfoMap.get(simUID);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(phoneNumber);
    }
  }

  @Override
  public void logCallStarted(final ChannelUID simUID, final PhoneNumber phoneNumber) {
    VoiceServerChannelsInfo channel = connectedChannelsInfoMap.get(simUID);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(phoneNumber);
    }
  }

  @Override
  public void logCallEnded(final ChannelUID simUnit, final long duration) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incTotalCallsCount();
      if (channelsInfo.getCallState().getState() == State.ACTIVE) {
        channelsInfo.incSuccessfulCallsCount();
        channelsInfo.addCallsDuration(duration);
      }
      logCallChangeState(simUnit, State.IDLE);
      channelsInfo.getCallState().setPhoneNumber(null);
    }
  }

  @Override
  public void logCallRelease(final ChannelUID simUID) {
    VoiceServerChannelsInfo channel = connectedChannelsInfoMap.get(simUID);
    if (channel != null) {
      channel.getCallState().setPhoneNumber(null);
    }
  }

  @Override
  public void logCallChangeState(final ChannelUID simUnit, final State callState) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null && states.get(channelsInfo.getCallState().getState()).contains(callState)) {
      channelsInfo.getCallState().changeState(callState);
    }
  }

  @Override
  public void logCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state, final String advInfo,
      final long periodPrediction) {
    logger.debug("[{}] - callChannelChangedState state={} adv.info={} UID={}", this, state, advInfo, simUnit);
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelState(new CallChannelState(state, periodPrediction));
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }
  }

  @Override
  public void logCallChannelChangedAdvInfo(final ChannelUID simUnit, final String advInfo) {
    logger.debug("[{}] - callChannelChangedAdvInfo adv.info={} UID={}", this, advInfo, simUnit);
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }
  }

  @Override
  public void logGsmUnitChangedState(final ChannelUID gsmUnit, final CallChannelState.State state, final String advInfo,
      final long periodPrediction) {
    VoiceServerChannelsInfo channelsInfo = channelsInfoMap.get(gsmUnit);
    if (channelsInfo != null) {
      channelsInfo.setChannelState(state == null ? null : new CallChannelState(state, periodPrediction));
      channelsInfo.setChannelStateAdvInfo(advInfo);
    }
  }

  @Override
  public void logSIMChangedEnableStatus(final ChannelUID simUnit, final boolean enable) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setEnabled(enable);
    }
  }

  @Override
  public void logSIMChangedGroup(final ChannelUID simUnit, final String group) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setGroup(group);
    }
  }

  @Override
  public void logCallChannelSendUSSD(final ChannelUID simUnit, final String ussd, final String response) {
    SimChannelInformationImpl simChannelInfo = findSimChannelInfo(simUnit);
    if (simChannelInfo != null) {
      simChannelInfo.setLastUSSDResponse(response);
    }
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incUssdCount();
    }
  }

  @Override
  public void logCallChannelSuccessSentSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incSuccessOutgoingSmsCount(parts);
      channelsInfo.incTotalOutgoingSmsCount(parts);
    }
  }

  @Override
  public void logCallChannelFailSentSMS(final ChannelUID simUnit, final String number, final String text, final int totalSmsParts, final int successSendSmsParts) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incTotalOutgoingSmsCount(totalSmsParts);
      channelsInfo.incSuccessOutgoingSmsCount(successSendSmsParts);
    }
  }

  @Override
  public void logCallChannelReceivedSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incIncomingTotalSmsCount(parts);
    }
  }

  @Override
  public void changeServerStatus(final ServerStatus status) {
  }

  SimChannelInformationImpl findSimChannelInfo(final ChannelUID simUnit) {
    return cemInformationMap.get(simUnit);
  }

  @Override
  public void logSignalQualityChange(final ChannelUID gsmUnit, final int signalStrength, final int bitErrorRate) {
    VoiceServerChannelsInfo channelInfo = channelsInfoMap.get(gsmUnit);
    if (channelInfo != null) {
      MobileGatewayChannelInformationImpl info = (MobileGatewayChannelInformationImpl) channelInfo.getMobileGatewayChannelInfo();
      if (info != null) {
        info.setSignalStrength(signalStrength);
        info.setBitErrorRate(bitErrorRate);
      }
    }
  }

  @Override
  public void logGSMNetworkInfoChange(final ChannelUID gsmUnit, final GSMNetworkInfo gsmNetworkInfo) {
    VoiceServerChannelsInfo channelInfo = channelsInfoMap.get(gsmUnit);
    if (channelInfo != null) {
      channelInfo.setGSMNetworkInfoChange(gsmNetworkInfo);
    }
  }

  @Override
  public void logPdd(final ChannelUID simUnit, final long pdd) {
    VoiceServerChannelsInfo channelsInfo = connectedChannelsInfoMap.get(simUnit);
    if (channelsInfo != null) {
      channelsInfo.incPdd(pdd);
    }
  }

  @Override
  public void resetStatistic() {
    channelsInfoMap.values().stream().map(VoiceServerChannelsInfo::getMobileGatewayChannelInfo).forEach(MobileGatewayChannelInformation::reset);
  }

  @Override
  public void logGsmUnitLockToArfcn(final ChannelUID channel, final Integer arfcn) {
    VoiceServerChannelsInfo voiceServerChannelsInfo = channelsInfoMap.get(channel);
    voiceServerChannelsInfo.getMobileGatewayChannelInfo().getGSMChannel().setLockArfcn(arfcn);
  }

}
