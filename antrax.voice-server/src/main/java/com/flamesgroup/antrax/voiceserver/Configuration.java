/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Configuration {

  private Map<GSMGroup, Set<SIMGroup>> gsmToSimLinks = new HashMap<>();
  private long linksRevision;

  private final AntraxPluginsStore pluginsStore;

  private final Lock linkLock = new ReentrantLock();

  public Configuration() {
    this.pluginsStore = new AntraxPluginsStore();
  }

  public boolean applies(final GSMGroup gsmGroup, final SIMGroup simGroup) {
    linkLock.lock();
    try {
      Set<SIMGroup> simLinks = gsmToSimLinks.get(gsmGroup);
      return simLinks != null && simLinks.contains(simGroup);
    } finally {
      linkLock.unlock();
    }
  }

  public void setLinks(final CfgLinksBuilder builder, final long revision) {
    linkLock.lock();
    try {
      gsmToSimLinks = builder.getResultedLinks();
      linksRevision = revision;
    } finally {
      linkLock.unlock();
    }
  }

  public long getLinksRevision() {
    linkLock.lock();
    try {
      return linksRevision;
    } finally {
      linkLock.unlock();
    }
  }

  public AntraxPluginsStore getPluginsStore() {
    return pluginsStore;
  }

}
