/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.timemachine.ConditionalStateImpl;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;

public class UnregisterState extends ConditionalStateImpl {

  private final CallChannel callChannel;
  private boolean released = false;

  public UnregisterState(final CallChannel callChannel) {
    this.callChannel = callChannel;
  }

  @Override
  public boolean isFinished() {
    return released;
  }

  @Override
  public void enterState() {
  }

  @Override
  public void tickRoutine() {
    if (callChannel.isOutgoingCallReleased() && callChannel.isIncomingCallReleased() && !callChannel.isInBusinessActivity()) {
      releaseChannel();
    }
  }

  private void releaseChannel() {
    callChannel.release("stop activity or session");
    released = true;
  }

  @Override
  public State getNextState() {
    return null;
  }

  @Override
  public String toString() {
    return "UnregisterState";
  }

  public void initialize(final StatesBuilder b) {
  }

}
