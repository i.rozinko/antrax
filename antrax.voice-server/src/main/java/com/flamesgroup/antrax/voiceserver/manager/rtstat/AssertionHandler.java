/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState.State;
import com.flamesgroup.antrax.voiceserver.actlog.ActivityLogger;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class AssertionHandler implements ActivityLogger {

  private final Logger logger = LoggerFactory.getLogger(AssertionHandler.class);

  private final Set<ChannelUID> startedCalls = new HashSet<>();

  public AssertionHandler() {
  }

  @Override
  public void changeServerStatus(final ServerStatus status) {
  }

  @Override
  public void logGsmUnitChangedGroup(final ChannelUID gsmUnit, final GSMGroup group) {
  }

  @Override
  public void logGsmUnitLock(final ChannelUID gsmUnit, final boolean lock, final String lockReason) {
  }

  @Override
  public void logDeclaredGsmUnit(final GSMChannel channel) {
  }

  @Override
  public void logActiveGsmUnit(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {
  }

  @Override
  public void updateChannelConfig(final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) {

  }

  @Override
  public void logGSMUnitReleased(final ChannelUID gsmUnit) {
  }

  @Override
  public void logCallChangeState(final ChannelUID simUnit, final State callState) {
  }

  @Override
  public void logCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state, final String advInfo,
      final long periodPrediction) {
  }

  @Override
  public void logCallChannelChangedAdvInfo(final ChannelUID simUnit, final String advInfo) {
  }

  @Override
  public void logGsmUnitChangedState(final ChannelUID gsmUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) {
  }

  @Override
  public void logCallChannelCreated(final ChannelUID gsmUnit, final ChannelUID simUnit) {
  }

  @Override
  public void logCallChannelReceivedSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
  }

  @Override
  public void logCallChannelStartActivity(final ChannelUID simUnit) {
  }

  @Override
  public void logCallChannelReleased(final ChannelUID simUnit) {
  }

  @Override
  public void logCallChannelSendUSSD(final ChannelUID simUnit, final String ussd, final String response) {
  }

  @Override
  public void logCallChannelSuccessSentSMS(final ChannelUID simUnit, final String number, final String text, final int parts) {
  }

  @Override
  public void logCallChannelFailSentSMS(final ChannelUID simUnit, final String number, final String text, final int totalSmsParts, final int successSendSmsParts) {

  }

  @Override
  public void logCallStarted(final ChannelUID simUnit, final PhoneNumber phoneNumber) {
    if (startedCalls.contains(simUnit)) {
      logger.error("[{}] - attempted to start call when previous was not finished for {}. WTF?", this, simUnit);
    }
    startedCalls.add(simUnit);
  }

  @Override
  public void logCallEnded(final ChannelUID simUnit, final long duration) {
    if (!startedCalls.remove(simUnit)) {
      logger.error("[{}] - attempted to end call which was never started. WTF? {}", this, simUnit);
    }
  }

  @Override
  public void logSIMChangedEnableStatus(final ChannelUID simUnit, final boolean enable) {
  }

  @Override
  public void logSIMChangedGroup(final ChannelUID simUnit, final String group) {
  }

  @Override
  public void logSIMReturnedToSimServer(final ChannelUID simUnit) {
  }

  @Override
  public void logSIMTakenFromSimServer(final ChannelUID simUnit, final ICCID uid) {
  }

  @Override
  public void logSignalQualityChange(final ChannelUID gsmUnit, final int signalStrength, final int bitErrorRate) {
  }

  @Override
  public void logGSMNetworkInfoChange(final ChannelUID gsmUnit, final GSMNetworkInfo info) {
  }

  @Override
  public void logPdd(final ChannelUID channelUID, final long pdd) {
  }

  @Override
  public void resetStatistic() {
  }

  @Override
  public void logGsmUnitLockToArfcn(final ChannelUID channel, final Integer arfcn) {
  }

  @Override
  public void logCallSetup(final ChannelUID simUID, final PhoneNumber phoneNumber) {

  }

  @Override
  public void logCallRelease(final ChannelUID simUID) {

  }

}
