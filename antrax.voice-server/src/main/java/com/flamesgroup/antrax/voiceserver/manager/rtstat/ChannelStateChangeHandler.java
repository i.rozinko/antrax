/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import com.flamesgroup.antrax.control.communication.ChannelsStatistic;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.commons.ChannelUID;

import java.util.HashMap;
import java.util.Map;

class ChannelStateChangeHandler {

  private final Map<ChannelUID, CallChannelState.State> prevStates = new HashMap<>();
  private final ChannelsStatistic stat;

  public ChannelStateChangeHandler(final ChannelsStatistic stat) {
    this.stat = stat;
  }

  public void handleCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state) {
    CallChannelState.State prevState = prevStates.get(simUnit);
    if (state == CallChannelState.State.RELEASING) {
      prevStates.remove(simUnit);
    } else {
      prevStates.put(simUnit, state);
    }
    if (prevState != null) {
      decrease(prevState);
    }
    increase(state);
  }

  private void increase(final CallChannelState.State state) {
    switch (state) {
      case IDLE_AFTER_SELF_CALL:
      case IDLE_BEFORE_SELF_CALL:
      case IDLE_BEFORE_UNREG:
      case IDLE_BETWEEN_CALLS:
      case IDLE_AFTER_REGISTRATION:
        stat.incSleeping();
        break;
      case STARTED_BUSINESS_ACTIVITY:
        stat.incInBusinessActivity();
        break;
      case READY_TO_CALL:
        stat.incFree();
        break;
      case OUTGOING_CALL:
        stat.incTalking();
        break;
      case RELEASING:
        stat.incNotConnectedChannelsCount();
        break;
      case WAITING_SELF_CALL:
        stat.incWaitingSelfCall();
        break;
    }
  }

  private void decrease(final CallChannelState.State state) {
    switch (state) {
      case IDLE_AFTER_SELF_CALL:
      case IDLE_BEFORE_SELF_CALL:
      case IDLE_BEFORE_UNREG:
      case IDLE_BETWEEN_CALLS:
      case IDLE_AFTER_REGISTRATION:
        stat.decSleeping();
        break;
      case STARTED_BUSINESS_ACTIVITY:
        stat.decInBusinessActivity();
        break;
      case READY_TO_CALL:
        stat.decFree();
        break;
      case OUTGOING_CALL:
        stat.decTalking();
        break;
      case REGISTERING_IN_GSM:
        stat.decNotConnectedChannelsCount();
        break;
      case RELEASING:
        break;
      case WAITING_SELF_CALL:
        stat.decWaitingSelfCall();
        break;
    }
  }

}
