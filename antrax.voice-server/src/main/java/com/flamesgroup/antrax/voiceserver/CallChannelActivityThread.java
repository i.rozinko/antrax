/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import static com.flamesgroup.unit.RegistrationStatus.InternalRegistrationStatus.POST_INIT_ERROR;
import static com.flamesgroup.unit.RegistrationStatus.InternalRegistrationStatus.TIMEOUT_ERROR;
import static com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus.INTERNAL_ERROR;
import static com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus.NOT_REGISTERED_AND_SEARCHING;
import static com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus.REGISTERED_ROAMING;
import static com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus.REGISTERED_TO_HOME_NETWORK;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.timemachine.TimeMachine;
import com.flamesgroup.antrax.timemachine.TransientStateImpl;
import com.flamesgroup.antrax.voiceserver.actlog.ActivityLogger;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.IGSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.GSMRegistrationError;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.antrax.voiceserver.states.ReadGSMNetworkInfoState;
import com.flamesgroup.antrax.voiceserver.states.ReadSignalQualityState;
import com.flamesgroup.antrax.voiceserver.states.StatesBuilder;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.SCEmulatorError;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.IExtendedNetworkErrorReport;
import com.flamesgroup.unit.IIncomingMobileCallHandler;
import com.flamesgroup.unit.IIncomingSMSHandler;
import com.flamesgroup.unit.IMobileStationUnitExternalErrorHandler;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.IMobileTerminatingCallHandler;
import com.flamesgroup.unit.IServiceInfoHandler;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.RegistrationStatus;
import com.flamesgroup.unit.SupplementaryServiceNotificationCode;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class CallChannelActivityThread extends Thread {

  private static final int TICK_TIMEOUT = 200;
  private static final int SLEEP_TIMEOUT_FOR_SIGNAL_QUALITY = 15000;

  private final CallChannel callChannel;
  private final VSStatus antraxStatus;
  private final ActivityLogger activityLogger;
  private final VoIPPeer voipPeer;
  private final ISCReaderSubChannel scReaderSubChannel;
  private final CallChannelPool callChannelPool;
  private final IRemoteHistoryLogger historyLogger;
  private final CellEqualizerManager cellEqualizerManager;

  private final Logger logger;
  private final TimeMachine timeMachine;
  private final TimeMachine signalQualityTimeMachine;

  private final AtomicBoolean waitRegistration = new AtomicBoolean();
  private final Semaphore registrationSemaphore = new Semaphore(0);
  private RegistrationStatus registrationStatus;
  private IExtendedNetworkErrorReport networkErrorReport;

  private final IMobileStationUnitExternalErrorHandler externalErrorHandler;

  public CallChannelActivityThread(final CallChannel callChannel, final VSStatus antraxStatus, final ActivityLogger activityLogger, final VoIPPeer voipPeer,
      final ISCReaderSubChannel scReaderSubChannel,
      final IMobileStationUnitExternalErrorHandler externalErrorHandler, final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger,
      final CellEqualizerManager cellEqualizerManager) {

    this.callChannel = callChannel;
    this.antraxStatus = antraxStatus;
    this.activityLogger = activityLogger;
    this.voipPeer = voipPeer;
    this.scReaderSubChannel = scReaderSubChannel;
    this.externalErrorHandler = externalErrorHandler;
    this.callChannelPool = callChannelPool;
    this.historyLogger = historyLogger;
    this.cellEqualizerManager = cellEqualizerManager;

    int callChannelNumber = callChannel.getChannelNumber();
    timeMachine = new TimeMachine("CallChannelTimeMachine" + callChannelNumber, buildStates(callChannel));
    signalQualityTimeMachine = new TimeMachine("SignalQualityTimeMachine" + callChannelNumber, buildSignalQualityStates(callChannel));
    logger = LoggerFactory.getLogger(CallChannelActivityThread.class.getName() + "[" + callChannel + "]");

    setName("Activity" + callChannelNumber + "(" + callChannel.getGSMUnit().getChannelUID() + ":" + callChannel.getSIMUnit().getChannelUID() + ")");
    setDaemon(false);
  }

  private com.flamesgroup.antrax.timemachine.State buildSignalQualityStates(final CallChannel callChannel) {
    TransientStateImpl sleepAfterSignalQualityState = new TransientStateImpl(SLEEP_TIMEOUT_FOR_SIGNAL_QUALITY);
    TransientStateImpl sleepAfterGSMNetworkInfoState = new TransientStateImpl(SLEEP_TIMEOUT_FOR_SIGNAL_QUALITY);
    ReadGSMNetworkInfoState readGSMNetworkInfoState = new ReadGSMNetworkInfoState(callChannel);
    ReadSignalQualityState readSignalQualityState = new ReadSignalQualityState(callChannel);

    readGSMNetworkInfoState.setNextState(sleepAfterGSMNetworkInfoState);
    sleepAfterGSMNetworkInfoState.setNextState(readSignalQualityState);
    readSignalQualityState.setNextState(sleepAfterSignalQualityState);
    sleepAfterSignalQualityState.setNextState(readGSMNetworkInfoState);

    return sleepAfterSignalQualityState;
  }

  private com.flamesgroup.antrax.timemachine.State buildStates(final CallChannel callChannel) {
    StatesBuilder statesBuilder = new StatesBuilder(callChannel, callChannelPool, historyLogger);
    statesBuilder.initialize();
    return statesBuilder.getIdleBeforeFirstCallState();
  }

  @Override
  public void run() {
    if (antraxStatus.terminating) {
      logger.debug("[{}] - waiting termination of server, so release units for [{}]", this, callChannel);
      releaseUnits(callChannel.getGSMUnit(), callChannel.getSIMUnit());
      return;
    }

    try {
      initUnits();
    } catch (GsmChannelError | RuntimeException e) {
      logger.error("[{}] - failed to init units", this, e);
      releaseUnits(callChannel.getGSMUnit(), callChannel.getSIMUnit());
      return;
    }

    callChannelPool.add(callChannel);
    callChannel.startProcessCallThread();
    activityLogger.logCallChannelStartActivity(callChannel.getSIMUnit().getChannelUID());

    try {
      invokeActivity();
    } catch (RuntimeException e) {
      logger.error("[{}] - failed to invoke activity", this, e);
    }
    callChannel.stopProcessCallThread();

    releaseUnits();
    callChannelPool.remove(callChannel);
  }

  private void invokeActivity() {
    logger.debug("[{}] - started activity thread", this);
    SIMGroup previousSimGroup = callChannel.getSIMUnit().getSimData().getSimGroup();
    while (!callChannel.isReleased()) {
      long startTime = System.currentTimeMillis();

      signalQualityTimeMachine.tick();

      if (!callChannel.isInBusinessActivity()) {
        SIMGroup currentSimGroup = callChannel.getSIMUnit().getSimData().getSimGroup();
        if (previousSimGroup.getRevision() != currentSimGroup.getRevision()) {
          previousSimGroup = currentSimGroup;
          timeMachine.restate();
        }
        timeMachine.tick();
      }

      processBusinessActivityScript();

      try {
        sleepTickTimeout(startTime);
      } catch (InterruptedException ignored) {
        return;
      }

      checkAntraxTermination();
      checkGsmPlugged();
      checkGsmLock();
      checkSimPlugged();
      checkSimLock();
    }

    while (callChannel.isInBusinessActivity()) {
      logger.trace("[{}] - {} was released, waiting finish business activity", this, callChannel);
      try {
        Thread.sleep(TICK_TIMEOUT);
      } catch (InterruptedException ignored) {
        return;
      }
    }
  }

  private void sleepTickTimeout(final long startTime) throws InterruptedException {
    long lostTime = System.currentTimeMillis() - startTime;
    if (lostTime < TICK_TIMEOUT) {
      Thread.sleep(TICK_TIMEOUT - lostTime);
    }
  }

  private void processBusinessActivityScript() {
    if (!callChannel.getSIMUnit().isLocked() && callChannel.getSIMUnit().isPlugged() && callChannel.isGroupApplies()) {
      callChannel.processBusinessActivity();
    }
  }

  private void checkGsmPlugged() {
    if (!callChannel.getGSMUnit().isPlugged()) {
      logger.debug("[{}] - {} is unplugged, terminating", this, callChannel.getGSMUnit());
      callChannel.release("gsm channel unplugged");
    }
  }

  private void checkGsmLock() {
    GSMChannel gsmChannel = callChannel.getGSMUnit().getGsmChannel();
    if (gsmChannel.isLock()) {
      logger.debug("[{}] - {} is locked, terminating", this, callChannel.getGSMUnit());
      callChannel.release("gsm channel locked: " + gsmChannel.getLockReason());
    }
  }

  private void checkSimPlugged() {
    if (!callChannel.getSIMUnit().isPlugged()) {
      logger.debug("[{}] - {} is unplugged so terminating it", this, callChannel.getSIMUnit());
      callChannel.release("sim channel unplugged");
    }
  }

  private void checkSimLock() {
    if (callChannel.getSIMUnit().isLocked()) {
      logger.debug("[{}] - {} is locked so terminating it", this, callChannel.getSIMUnit());
      callChannel.release("sim channel locked: " + callChannel.getSIMUnit().getSimData().getLockReason());
    }
  }

  private void checkAntraxTermination() {
    if (antraxStatus.terminating) {
      callChannel.release("VS terminated");
    }
  }

  private void initUnits() throws GsmChannelError {
    logger.debug("[{}] - init units {}", this, callChannel);

    GSMUnit gsmUnit = callChannel.getGSMUnit();
    SIMUnit simUnit = callChannel.getSIMUnit();

    simUnit.handleEvent(SimEvent.callChannelBuildStarted(gsmUnit.getChannelUID(), simUnit.getChannelUID()));

    IMEI imei = simUnit.shouldGenerateIMEI() ? simUnit.generateIMEI() : simUnit.getImei();
    if (imei == null) {
      logger.warn("[{}] - {} has no IMEI", this, gsmUnit);
      throw new IllegalStateException();
    }

    logger.debug("[{}] - connecting devices", this);
    try {
      gsmUnit.connect(simUnit, new DefaultIncomingMobileCallHandler(), new DefaultIncomingSMSHandler(), new DefaultServiceInfoHandler(),
          new DefaultMobileStationUnitInternalErrorHandler(), cellEqualizerManager, externalErrorHandler, scReaderSubChannel, imei, callChannel.toString());
    } catch (GsmChannelError e) {
      logger.warn("[{}] - Failed to connect to SIM", this, e);
      throw e;
    }
    logger.debug("[{}] - [{}] connected to [{}]", this, gsmUnit, simUnit);

    callChannel.changeChannelState(CallChannelState.State.REGISTERING_IN_GSM, -1);
    simUnit.gsmRegistration();

    waitForRegistrationInGSM();
    RegistrationStatus registrationStatusLocal = registrationStatus;
    IExtendedNetworkErrorReport networkErrorReportLocal = networkErrorReport;
    switch (registrationStatusLocal.getNetworkRegistrationStatus()) {
      case REGISTRATION_DENIED:
        simUnit.lock(true, "Registration denied: " + networkErrorReportLocal.getMobilityOrSessionManagementCauseDescription());
      case NOT_REGISTERED_AND_SEARCHING:
      case NOT_REGISTERED_NOT_SEARCHING:
      case INTERNAL_ERROR:
      case UNKNOWN:
        GsmChannelError gsmRegistrationError = new GSMRegistrationError(String.format("Can't register in gsm network: [%s,%s]",
            registrationStatusLocal.getNetworkRegistrationStatus(),
            registrationStatusLocal.getInternalRegistrationStatus()));
        logger.warn("[{}] - failed to register in GSM", this, gsmRegistrationError);
        deactiveGSMUnit(gsmUnit);
        throw gsmRegistrationError;
      default:
        break;
    }

    simUnit.gsmRegistered();
    simUnit.handleActivityStarted(gsmUnit.getChannelUID(), gsmUnit.getGsmChannel().getGSMGroup());
  }

  private void waitForRegistrationInGSM() {
    waitRegistration.set(true);
    try {
      registrationSemaphore.acquire();
    } catch (InterruptedException ex) {
      logger.warn("[{}] - wait for registration in GSM was unexpectedly interrupted", this, ex);
    } finally {
      waitRegistration.set(false);
    }
  }

  private void releaseUnits() {
    logger.debug("[{}] - releasing units", this);
    callChannel.changeChannelState(CallChannelState.State.RELEASING, -1);

    callChannel.getSIMUnit().handleActivityStopped(callChannel.getReleaseReason());
    deactiveGSMUnit(callChannel.getGSMUnit());
    releaseUnits(callChannel.getGSMUnit(), callChannel.getSIMUnit());
  }


  private void deactiveGSMUnit(final IGSMUnit gsmUnit) {
    gsmUnit.disconnect();
  }

  private void releaseUnits(final GSMUnit gsmUnit, final SIMUnit simUnit) {
    activityLogger.logCallChannelReleased(simUnit.getSimUnitId());
    simUnit.handleEvent(SimEvent.callChannelReleased());
    simUnit.setOwned(false);
    gsmUnit.unOwn();
  }

  private class DefaultIncomingMobileCallHandler implements IIncomingMobileCallHandler {

    @Override
    public IMobileTerminatingCallHandler handleNewIncomingMobileCall(final PhoneNumber terminatedNumber, final IMobileTerminatingCall terminatedCall) {
      if (callChannel.isReleased()) {
        return null;
      } else {
        logger.debug("[{}] - handle new incoming call [{}]", this, terminatedNumber);
        return callChannel.startIncomingCall(terminatedNumber, terminatedCall, voipPeer);
      }
    }

  }

  private class DefaultIncomingSMSHandler implements IIncomingSMSHandler {

    @Override
    public void handleSMS(final SMSMessageInbound smsMessageInbound) {
      if (callChannel.isReleased()) {
        return;
      }
      logger.debug("[{}] - save incoming sms [{}]", this, smsMessageInbound);
      callChannel.saveSMS(smsMessageInbound);
    }

    @Override
    public void handleSMSBinary(final String originator, final String text, final Date timeStamp) {
      if (callChannel.isReleased()) {
        return;
      }

      logger.debug("[{}] - handling incoming binary sms from [{}] with text [{}]", this, originator, text);
      callChannel.saveSMS(originator, text, timeStamp);
    }

    @Override
    public void handleSMSMultimedia(final String originator, final String text, final Date timeStamp) {
      if (callChannel.isReleased()) {
        return;
      }

      logger.debug("[{}] - handling incoming multimedia sms from [{}] with text [{}]", this, originator, text);
      callChannel.saveSMS(originator, text, timeStamp);
    }

    @Override
    public void handleSMSDeliveryStatus(final SMSStatusReport smsStatusReport) {
      if (callChannel.isReleased()) {
        return;
      }
      logger.debug("[{}] - handling sms status report for [{}]", this, smsStatusReport.getRecipientPhoneNumber());
      callChannel.handleReceivedSmsStatusReport(smsStatusReport);
    }

  }

  private class DefaultServiceInfoHandler implements IServiceInfoHandler {

    private RegistrationStatus prevRegistrationStatus;

    @Override
    public void handleNetworkRegistration(final RegistrationStatus status, final String operator,
        final String locationAreaCode, final String cellID, final IExtendedNetworkErrorReport errorReport) {
      if (callChannel.isReleased()) {
        return;
      }

      prevRegistrationStatus = registrationStatus;
      registrationStatus = status;
      networkErrorReport = errorReport;
      callChannel.setRegistrationStatus(new Pair<>(status.getNetworkRegistrationStatus(), operator));
      handleRegistrationEvent(status, errorReport);
    }

    @Override
    public void handleSupplementaryServiceNotification(final SupplementaryServiceNotificationCode code) {
      if (callChannel.isReleased()) {
        return;
      }

      callChannel.handleSupplementaryServiceNotification(code);
    }

    @Override
    public void handleSignalBitErrorRate(final int rate) {
      if (callChannel.isReleased()) {
        return;
      }

      callChannel.setSignalBitErrorRate(rate);
    }

    @Override
    public void handleSignalReceivedStrength(final int strength) {
      if (callChannel.isReleased()) {
        return;
      }

      callChannel.setSignalReceivedStrength(strength);
    }

    @Override
    public void handleCellMonitoring(final CellInfo[] cellInfos) {
      if (callChannel.isReleased() || cellInfos == null || cellInfos.length == 0) {
        return;
      }

      callChannel.setCellMonitoringInfo(cellInfos);
    }

    private void handleRegistrationEvent(final RegistrationStatus registrationStatus, final IExtendedNetworkErrorReport errorReport) {
      RegistrationStatus.InternalRegistrationStatus internalStatus = registrationStatus.getInternalRegistrationStatus();
      RegistrationStatus.NetworkRegistrationStatus networkStatus = registrationStatus.getNetworkRegistrationStatus();
      if (waitRegistration.get()) {
        if (internalStatus != TIMEOUT_ERROR && networkStatus == NOT_REGISTERED_AND_SEARCHING) {
          return; // still waiting timeout to registration sim card
        }

        registrationSemaphore.release();
        if (networkStatus != REGISTERED_TO_HOME_NETWORK && networkStatus != REGISTERED_ROAMING) {
          callChannel.getSIMUnit().handleEvent(SimEvent.gsmNotRegistered(callChannel.getSIMUnit().getUid(), errorReport.getMobilityOrSessionManagementCauseDescription()));
        }
      } else {
        if (networkStatus != REGISTERED_TO_HOME_NETWORK && networkStatus != REGISTERED_ROAMING) {
          callChannel.getSIMUnit().handleEvent(SimEvent.gsmUnregistered(callChannel.getSIMUnit().getUid(), errorReport.getMobilityOrSessionManagementCauseDescription()));
          callChannel.release("gsm channel unregistered");
        }
      }
    }

  }

  private class DefaultMobileStationUnitInternalErrorHandler implements IMobileStationUnitInternalErrorHandler {

    private static final int AT_ENGINE_EXECUTION_ERROR_LIMIT = 3;

    private int atEngineExecutionErrors;

    @Override
    public void handleATEngineExecutionError(final Exception e) {
      if (registrationStatus == null) { // registration error
        notifyAboutRegistrationError(POST_INIT_ERROR, "Registration error: problem with gsm module");
      } else {
        if (++atEngineExecutionErrors >= AT_ENGINE_EXECUTION_ERROR_LIMIT) {
          logger.error("[{}] - AT engine execution error limit [{}] was exceeded", this, AT_ENGINE_EXECUTION_ERROR_LIMIT);
          callChannel.release("execution error limit exceeded");
        }
      }
    }

    @Override
    public void handleSCReaderSubChannelErrorState(final SCReaderError error, final SCReaderState state) {
      callChannel.getSIMUnit().setWaitRelease(true);
      if (registrationStatus == null) { // registration error
        notifyAboutRegistrationError(POST_INIT_ERROR, "Registration error: problem with sim card");
      } else {
        callChannel.release("problem on sim channel");
      }
    }

    @Override
    public void handleSCReaderSubChannelException(final ChannelException e) {
      if (registrationStatus == null) { // registration error
        notifyAboutRegistrationError(POST_INIT_ERROR, "Registration error: exception on sim channel");
      } else {
        callChannel.release("exception on sim channel");
      }
    }

    @Override
    public void handleSCEmulatorSubChannelErrorState(final SCEmulatorError error, final SCEmulatorState state) {
      if (registrationStatus == null) { // registration error
        notifyAboutRegistrationError(POST_INIT_ERROR, "Registration error: problem on gsm channel");
      } else {
        callChannel.release("problem on gsm channel");
      }
    }

    @Override
    public void handleSCEmulatorSubChannelException(final ChannelException e) {
      if (registrationStatus == null) { // registration error
        notifyAboutRegistrationError(POST_INIT_ERROR, "Registration error: exception on gsm channel");
      } else {
        callChannel.release("exception on gsm channel");
      }
    }

    private void notifyAboutRegistrationError(final RegistrationStatus.InternalRegistrationStatus status, final String reason) {
      registrationStatus = new RegistrationStatus(status, INTERNAL_ERROR);
      registrationSemaphore.release();
      callChannel.getSIMUnit().handleEvent(SimEvent.gsmNotRegistered(callChannel.getSIMUnit().getSimData().getUid(), reason));
    }

  }

}
