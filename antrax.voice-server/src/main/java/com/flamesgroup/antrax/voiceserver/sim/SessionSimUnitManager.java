/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.sim;

import com.flamesgroup.antrax.control.simserver.ChangeGroupFailedError;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.simserver.IllegalSessionUuidException;
import com.flamesgroup.antrax.control.simserver.NoSuchSimError;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class SessionSimUnitManager {

  private static final Logger logger = LoggerFactory.getLogger(SessionSimUnitManager.class);

  private final ISimUnitManager simUnitManager;

  private final String toS;

  private final AtomicBoolean error = new AtomicBoolean();

  public SessionSimUnitManager(final ISimUnitManager simUnitManager, final ChannelUID simChannelUID) {
    this.simUnitManager = simUnitManager;

    toS = getClass().getName() + "[" + simChannelUID + "]";
  }

  ISCReaderSubChannels takeSCReaderSubChannels(final UUID sessionUuid) {
    if (error.get()) {
      return null;
    }
    try {
      return simUnitManager.takeSCReaderSubChannels(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to take SC reader sub channel for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return null;
  }

  void returnSimUnit(final UUID sessionUuid) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.returnSimUnit(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to return sim unit for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  boolean isSimUnitPlugged(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitPlugged(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about plugged sim unit for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  Pair<SimData, IMSI> getSimData(final UUID sessionUuid) {
    if (error.get()) {
      return null;
    }
    try {
      return simUnitManager.getSimData(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to get sim data for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return null;
  }

  void lockSimUnit(final UUID sessionUuid, final boolean lockStatus, final String reason) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.lockSimUnit(sessionUuid, lockStatus, reason);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to lock sim unit for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void enableSimUnit(final UUID sessionUuid, final boolean enableStatus) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.enableSimUnit(sessionUuid, enableStatus);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to enable sim unit for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void handleSimUnitStartedActivity(final UUID sessionUuid, final ChannelUID gsmChannel, final GSMGroup gsmGroup) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitStartedActivity(sessionUuid, gsmChannel, gsmGroup);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to notify about start activity for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void handleSimUnitStoppedActivity(final UUID sessionUuid, final String stopReason) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitStoppedActivity(sessionUuid, stopReason);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to notify about stop activity for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void handleSimUnitCallEnded(final ICCID uid, final long callDuration) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitCallEnded(uid, callDuration);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about outgoing call end for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleSimUnitIncomingCallEnded(final ICCID uid, final long callDuration) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitIncomingCallEnded(uid, callDuration);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about incoming call end for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleSimUnitSMSStatus(final ICCID uid) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitSMSStatus(uid);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about sms status for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleSimUnitSentSMS(final ICCID uid, final int parts) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitSentSMS(uid, parts);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about send sms for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleSimUnitFailSentSMS(final ICCID uid, final int totalSmsParts, final int successSendSmsParts) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitFailSentSMS(uid, totalSmsParts, successSendSmsParts);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about fail send sms for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleSimUnitIncomingSMS(final ICCID uid, final int parts) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleSimUnitIncomingSMS(uid, parts);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about incoming sms for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  public void handleAllowedInternet(final ICCID uid, final boolean allowed) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleAllowedInternet(uid, allowed);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about allowed Internet for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void handleGenericEvent(final ICCID uid, final String event, final byte[]... serializedArgs) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.handleGenericEvent(uid, event, serializedArgs);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to notify about generic event for iccid [{}]", this, uid, e);
      error.set(true);
    }
  }

  void addUserMessage(final UUID sessionUuid, final String message) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.addUserMessage(sessionUuid, message);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to notify about add user message for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  boolean isSimUnitShouldStartActivity(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitShouldStartActivity(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about start activity for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  boolean isSimUnitShouldStopActivity(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitShouldStopActivity(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about stop activity for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  boolean isSimUnitShouldStopSession(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitShouldStopSession(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about stop session for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  boolean isSimUnitRequiresReconfiguration(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitRequiresReconfiguration(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about reconfiguration sim unit for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  SIMGroup changeSimGroup(final ICCID uid, final long simGroup) {
    if (error.get()) {
      return null;
    }
    try {
      return simUnitManager.changeSimGroup(uid, simGroup);
    } catch (RemoteException | ChangeGroupFailedError | NoSuchSimError e) {
      logger.warn("[{}] - failed to change sim group for iccid [{}]", this, uid, e);
      error.set(true);
    }
    return null;
  }

  void resetIMEI(final UUID sessionUuid, final String message) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.resetIMEI(sessionUuid, message);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to reset IMEI for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  boolean isSimUnitShouldGenerateIMEI(final UUID sessionUuid) {
    if (error.get()) {
      return false;
    }
    try {
      return simUnitManager.isSimUnitShouldGenerateIMEI(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to ask about generate IMEI for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return false;
  }

  IMEI generateIMEI(final UUID sessionUuid) {
    if (error.get()) {
      return null;
    }
    try {
      return simUnitManager.generateIMEI(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to generate IMEI for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
    return null;
  }

  void gsmRegistration(final UUID sessionUuid, final int attemptNumber) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.gsmRegistration(sessionUuid, attemptNumber);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to set registration status for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void gsmRegistered(final UUID sessionUuid) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.gsmRegistered(sessionUuid);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to set registered status for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void updatePhoneNumber(final UUID sessionUuid, final PhoneNumber phoneNumber) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.updatePhoneNumber(sessionUuid, phoneNumber);
    } catch (RemoteException | IllegalSessionUuidException e) {
      logger.warn("[{}] - failed to update phone number for uuid [{}]", this, sessionUuid, e);
      error.set(true);
    }
  }

  void setTariffPlanEndDate(final ICCID uid, final long endDate) {
    if (error.get()) {
      return;
    }
    try {
      simUnitManager.setTariffPlanEndDate(uid, endDate);
    } catch (RemoteException | NoSuchSimError e) {
      logger.warn("[{}] - failed to set tariff plan end date for uuid [{}]", this, uid, e);
      error.set(true);
    }
  }

  @Override
  public String toString() {
    return toS;
  }

}
