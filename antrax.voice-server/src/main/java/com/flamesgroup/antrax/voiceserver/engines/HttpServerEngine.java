/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.engines;

import com.flamesgroup.unit.sms.SMSException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;

public class HttpServerEngine {

  private final Logger logger = LoggerFactory.getLogger(HttpServerEngine.class);

  private final HttpServer server;
  private final HttpServerCommandHandler commandHandler;

  public interface HttpServerCommandHandler {
    UUID sendSMS(String number, String text) throws SMSException;
  }

  private HttpServer createHttpServer(final int port) throws IOException {
    HttpServer retval = HttpServer.create(new InetSocketAddress(port), 0);
    retval.createContext("/sms", new SmsContextHandler());
    retval.setExecutor(Executors.newCachedThreadPool());
    return retval;
  }

  public HttpServerEngine(final int port, final HttpServerCommandHandler handler) throws IOException {
    this.commandHandler = handler;
    this.server = createHttpServer(port);
  }

  public void start() {
    server.start();
  }

  public void stop() {
    //stopping with delay in 5 seconds
    server.stop(5);
  }

  private class SmsContextHandler implements HttpHandler {
    private class SendSmsArgs {
      private String number;
      private String text;

      public String getNumber() {
        return number;
      }

      public void setNumber(final String number) {
        this.number = number;
      }

      public String getText() {
        return text;
      }

      public void setText(final String text) {
        this.text = text;
      }

    }

    private SendSmsArgs parseURI(final URI uri) throws IllegalArgumentException {
      SendSmsArgs retval = new SendSmsArgs();

      Map<String, String> argsMap = new HashMap<>();
      String paramStr = uri.getRawQuery();
      for (String pair : paramStr.split("&")) {
        String splitted[] = pair.split("=");
        argsMap.put(URLDecoder.decode(splitted[0]).toLowerCase(), URLDecoder.decode(splitted[1]));
      }

      String value = argsMap.get("to");
      if (null == value) {
        throw new IllegalArgumentException("Can't find destination number in URL parameter 'to'");
      } else {
        retval.setNumber(value);
      }

      value = argsMap.get("text");
      if (null == value) {
        throw new IllegalArgumentException("Can't find sms text in URL parameter 'text'");
      } else {
        retval.setText(value);
      }

      return retval;
    }

    private void sendResponse(final HttpExchange exchange, final int status, final String text) throws IOException {
      Headers responseHeaders = exchange.getResponseHeaders();
      responseHeaders.set("Content-Type", "text/plain");
      exchange.sendResponseHeaders(status, 0);

      OutputStream responseBody = exchange.getResponseBody();
      responseBody.write(text.getBytes());
      responseBody.close();
    }

    private void responseSuccess(final HttpExchange exchange, final UUID smsId) throws IOException {
      sendResponse(exchange, 202, "Ok: " + smsId);
    }

    private void responseError(final HttpExchange exchange, final String error) throws IOException {
      sendResponse(exchange, 503, "Fail: " + error);
    }

    @Override
    public void handle(final HttpExchange exchange) throws IOException {
      String requestMethod = exchange.getRequestMethod();
      if (requestMethod.equalsIgnoreCase("GET")) {
        logger.debug("[{}] - got request: {}", this, URLDecoder.decode(exchange.getRequestURI().toString()));
        try {
          SendSmsArgs smsArgs = parseURI(exchange.getRequestURI());
          UUID smsId = commandHandler.sendSMS(smsArgs.getNumber(), smsArgs.getText());
          responseSuccess(exchange, smsId);
        } catch (Exception e) {
          responseError(exchange, e.getMessage());
        }
      }
    }
  }

}
