/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.properties;

import com.flamesgroup.jiax2.AudioMediaFormat;
import com.flamesgroup.jiax2.AudioMediaFormatOption;
import com.flamesgroup.jiax2.TrunkOptions;
import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

public class Jiax2Properties implements ServerProperties {

  public static final int DEFAULT_IAX_PORT = 4569;

  private static final String IAX_PEER_LISTENER_ADDRESS = "iax.peer.listener.address";

  private static final String AUDIO_OPTION_FORMATS = "audio.option.formats";

  private static final String TRUNK_SEND_INTERVAL = "trunk.send.interval";
  private static final String TRUNK_MAX_LENGTH = "trunk.max.length";

  private static final String TRUNK_ADDRESSES = "trunk.addresses";

  private final AtomicReference<SocketAddress> iaxPeerListenerAddress = new AtomicReference<>();
  private final AtomicReference<List<AudioMediaFormatOption>> audioOptionFormats = new AtomicReference<>();
  private final AtomicReference<Map<SocketAddress, TrunkOptions>> trunkOptionsMap = new AtomicReference<>();

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);
    String[] iaxPeerListenerAddressParameters = propertiesLoader.getString(IAX_PEER_LISTENER_ADDRESS).split(":");
    InetAddress listerAddress;
    try {
      listerAddress = InetAddress.getByName(iaxPeerListenerAddressParameters[0]);
    } catch (UnknownHostException e) {
      throw new IllegalStateException(e);
    }
    int listerPort = iaxPeerListenerAddressParameters.length < 2 ? DEFAULT_IAX_PORT : Integer.parseInt(iaxPeerListenerAddressParameters[1]);
    iaxPeerListenerAddress.set(new InetSocketAddress(listerAddress, listerPort));

    String audioOptionFormatsString = propertiesLoader.getString(AUDIO_OPTION_FORMATS);
    List<AudioMediaFormatOption> audioMediaFormatOptions = new ArrayList<>();
    if (audioOptionFormatsString != null && !audioOptionFormatsString.isEmpty()) {
      String[] split = audioOptionFormatsString.split(";");
      for (String s : split) {
        String[] formatAndCount = s.trim().split(":");
        audioMediaFormatOptions.add(new AudioMediaFormatOption(AudioMediaFormat.valueOf(formatAndCount[0].toUpperCase()), Integer.parseInt(formatAndCount[1])));
      }
    }
    audioOptionFormats.set(audioMediaFormatOptions);

    String trunkSendIntervalString = propertiesLoader.getString(TRUNK_SEND_INTERVAL);
    String trunkMaxLengthString = propertiesLoader.getString(TRUNK_MAX_LENGTH);
    TrunkOptions trunkOptions = new TrunkOptions(Integer.parseInt(trunkSendIntervalString), Integer.parseInt(trunkMaxLengthString));

    String trunkAddressesString = propertiesLoader.getString(TRUNK_ADDRESSES);
    Map<SocketAddress, TrunkOptions> trunkOptionsMapLocal = new HashMap<>();
    if (trunkAddressesString != null && !trunkAddressesString.isEmpty()) {
      String[] split = trunkAddressesString.split(";");
      for (String s : split) {
        String[] addressParameters = s.trim().split(":");
        InetAddress address;
        try {
          address = InetAddress.getByName(addressParameters[0]);
        } catch (UnknownHostException e) {
          throw new IllegalStateException(e);
        }
        int port = addressParameters.length < 2 ? DEFAULT_IAX_PORT : Integer.parseInt(addressParameters[1]);

        trunkOptionsMapLocal.put(new InetSocketAddress(address, port), trunkOptions);
      }
    }
    trunkOptionsMap.set(trunkOptionsMapLocal);
  }

  public SocketAddress getIaxPeerListenerAddress() {
    return iaxPeerListenerAddress.get();
  }

  public List<AudioMediaFormatOption> getAudioOptionFormats() {
    return audioOptionFormats.get();
  }

  public Map<SocketAddress, TrunkOptions> getTrunkOptionsMap() {
    return trunkOptionsMap.get();
  }

}
