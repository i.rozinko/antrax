/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CfgLinksBuilder {

  private final Map<GSMGroup, Set<SIMGroup>> links = new HashMap<>();

  public CfgLinksBuilder addLink(final GSMGroup gsmGroup, final SIMGroup simGroup) {
    Set<SIMGroup> sims;
    if (!links.containsKey(gsmGroup)) {
      sims = new HashSet<>();
      links.put(gsmGroup, sims);
    } else {
      sims = links.get(gsmGroup);
    }
    sims.add(simGroup);
    return this;
  }

  public Map<GSMGroup, Set<SIMGroup>> getResultedLinks() {
    return links;
  }

}
