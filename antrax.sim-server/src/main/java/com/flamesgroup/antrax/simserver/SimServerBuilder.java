/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.configuration.AntraxConfigurationHelper;
import com.flamesgroup.antrax.control.communication.SimServerShortInfo;
import com.flamesgroup.antrax.distributor.DeviceManagerHelper;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.simserver.properties.SimServerPropUtils;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.box.BOXUtil;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.mudp.IMudpConnectionErrorHandler;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.simb.ISIMDevice;
import com.flamesgroup.utils.AntraxProperties;

import java.net.InetAddress;
import java.util.Map;
import java.util.UUID;

public class SimServerBuilder {

  private final SimServer server = new SimServer();

  private SimChannelManager simChannelManager;
  private ControlServerPingThread controlServerPingThread;
  private SimServerShortInfo simServerShortInfo;

  public SimServer createSimServer() throws Exception {
    server.setSimChannelManager(getSimChannelManager());
    server.setSimServerManager(AntraxConfigurationHelper.getInstance().getSimServerManager());
    server.setControlServerPingThread(getControlServerPingThread());
    server.setSimServerShortInfo(getSimServerShortInfo());
    return server;
  }

  public SimChannelManager getSimChannelManager() throws Exception {
    if (simChannelManager == null) {
      switch (DeviceUtil.getDeviceClass()) {
        case ETH:
          InetAddress broadcast = DeviceManagerHelper.getBroadcast();
          Server server = new Server(AntraxProperties.SERVER_NAME, ServerType.SIM_SERVER);
          UUID uuid = UUID.randomUUID();
          Map<DeviceUID, IPConfig> deviceIPConfigs = DeviceManagerHelper.parseDevicesFromProperties(server, uuid);
          simChannelManager = new EthSimChannelManager(broadcast, deviceIPConfigs, uuid);
          break;
        case SPI:
          DeviceUID deviceUID = BOXUtil.readDeviceUID();
          ISIMDevice simDevice = DeviceUtil.createSpiSIMDevice(createMudpConnectionErrorHandler());
          simChannelManager = new SpiSimChannelManager(deviceUID, simDevice);
          break;
      }
    }
    return simChannelManager;
  }

  private SimServerShortInfo getSimServerShortInfo() {
    if (simServerShortInfo == null) {
      simServerShortInfo = new SimServerShortInfo();
    }
    return simServerShortInfo;
  }

  private ControlServerPingThread getControlServerPingThread() {
    if (controlServerPingThread == null) {
      controlServerPingThread = new ControlServerPingThread(AntraxConfigurationHelper.getInstance().getPingSimServerManager(), getSimServerShortInfo(),
          SimServerPropUtils.getInstance().getSimServerProperties().getPingControlServerTimeout());
    }
    return controlServerPingThread;
  }

  private IMudpConnectionErrorHandler createMudpConnectionErrorHandler() {
    return new IMudpConnectionErrorHandler() {
      @Override
      public void handleMudpStreamException(final MudpException e) {
        throw new IllegalStateException(e);
      }
    };
  }

}
