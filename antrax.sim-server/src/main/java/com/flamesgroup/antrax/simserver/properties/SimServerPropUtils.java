/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver.properties;

import com.flamesgroup.properties.PropUtils;
import com.flamesgroup.properties.ServerProperties;

import java.util.HashMap;
import java.util.Map;

public class SimServerPropUtils extends PropUtils {

  private static final String SIM_SERVER_PROPERTIES = "sim-server.properties";

  private static volatile SimServerPropUtils instance;

  private SimServerPropUtils() {
    super();
  }

  public SimServerProperties getSimServerProperties() {
    return (SimServerProperties) properties.get(SIM_SERVER_PROPERTIES);
  }

  @Override
  protected Map<String, ServerProperties> getProperties() {
    Map<String, ServerProperties> localProperties = new HashMap<>();
    localProperties.put(SIM_SERVER_PROPERTIES, new SimServerProperties());
    localProperties.putAll(super.getProperties());
    return localProperties;
  }

  public static SimServerPropUtils getInstance() {
    if (instance == null) {
      synchronized (SimServerPropUtils.class) {
        if (instance == null) {
          instance = new SimServerPropUtils();
        }
      }
    }
    return instance;
  }

  @Override
  public String getName() {
    return SIM_SERVER_PROPERTIES;
  }

}
