/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.List;
import java.util.UUID;

public interface IVoiceServerManager {

  void shutdown();

  void fireEvent(ICCID simUID, String event) throws EventException;

  void sendUSSD(ICCID simUID, String ussd) throws USSDException;

  String startUSSDSession(ICCID simUID, String ussd) throws USSDException;

  String sendUSSDSessionCommand(ICCID simUID, String command) throws USSDException;

  void endUSSDSession(ICCID simUID) throws USSDException;

  void sendSMS(ICCID simUID, String number, String smsText) throws SMSException;

  UUID sendSMS(List<AlarisSms> alarisSmses) throws SMSException;

  void sendDTMF(ICCID simUID, String dtmf) throws DTMFException;

  void lockSIM(ICCID simUID);

  void unlockSIM(ICCID simUID);

  void enableCallChannel(ICCID simUID);

  void disableCallChannel(ICCID simUID);

  void lockGsmChannel(ChannelUID channel, boolean lock, final String lockReason) throws LockException;

  void lockGsmChannelToArfcn(ChannelUID channel, int arfcn) throws LockArfcnException;

  void unLockGsmChannelToArfcn(ChannelUID channel) throws LockArfcnException;

  void executeNetworkSurvey(final ChannelUID channel) throws NetworkSurveyException;

  void resetStatistic();

}
