/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.RegistryRecord;
import com.flamesgroup.storage.map.RegistryMapper;
import org.jooq.DSLContext;
import org.jooq.DeleteQuery;
import org.jooq.Record1;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOffsetStep;
import org.jooq.SelectQuery;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Date;

public class RemoteRegistryDAO implements RemoteRegistryAccess {

  private final static Logger logger = LoggerFactory.getLogger(RemoteRegistryDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public RemoteRegistryDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  private boolean canUpdateRegistryFlag(final RegistryEntry entry, final DSLContext context) throws SQLException {
    logger.debug("[{}] - trying to get update registry flag for entry: [{}]", this, entry);
    if (entry == null) {
      return false;
    }
    SelectQuery<RegistryRecord> selectQuery = context.selectQuery(Tables.REGISTRY);
    selectQuery.addConditions(Tables.REGISTRY.PATH.eq(entry.getPath()));
    selectQuery.addConditions(Tables.REGISTRY.VALUE.eq(entry.getValue()));

    boolean canUpdateRegistry = selectQuery.execute() > 0;
    logger.debug("[{}] - found update registry flag: [{}] by entry: [{}]", this, canUpdateRegistry, entry);

    return canUpdateRegistry;
  }

  @Override
  public RegistryEntry move(final RegistryEntry entry, final String path) throws UnsyncRegistryEntryException, DataModificationException {
    logger.debug("[{}] - trying to move entry: [{}] to [{}]", this, entry, path);
    if (entry == null || path == null || path.isEmpty() || entry.getPath() == null || entry.getValue() == null) {
      throw new DataModificationException(String.format("[%s] - can't move entry: [%s] to path: [%s] (path and entry can't be null, and path can't be empty)", this, entry, path));
    }

    RegistryEntry movedRegistryEntry;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      movedRegistryEntry = context.transactionResult(configuration -> {
        boolean canUpdateRegistry = canUpdateRegistryFlag(entry, context);
        if (!canUpdateRegistry) {
          throw new UnsyncRegistryEntryException(String.format("[%s] - can't move entry: [%s] to [%s], entry was updated before", this, entry, path));
        }

        UpdateQuery<RegistryRecord> updateQuery = context.updateQuery(Tables.REGISTRY);
        updateQuery.addConditions(Tables.REGISTRY.PATH.eq(entry.getPath()));
        updateQuery.addConditions(Tables.REGISTRY.VALUE.eq(entry.getValue()));
        updateQuery.addValue(Tables.REGISTRY.PATH, path);
        Date updatingTime = new Date();
        updateQuery.addValue(Tables.REGISTRY.UPDATING_TIME, updatingTime);

        updateQuery.setReturning();
        int res = updateQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - entry: [{}] was moved to path: [{}]", this, entry, path);
          return RegistryMapper.mapRegistryRecordToRegistryEntry(updateQuery.getReturnedRecord());
        } else {
          logger.warn("[{}] - can't move entry: [{}] to path: [{}] (result after execution is < 1)", this, entry, path);
          return entry;
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - while moving entry: [%s] to path: [%s]", this, entry, path), e);
    } finally {
      connection.close();
    }
    return movedRegistryEntry;
  }

  @Override
  public void remove(final RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException {
    logger.debug("[{}] - trying to remove entry: [{}]", this, entry);
    if (entry == null || entry.getPath() == null || entry.getValue() == null) {
      throw new DataModificationException(String.format("[%s] - can't remove entry: [%s] (entry can't be null, path can't be null and value can't be null)", this, entry));
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        boolean canUpdateRegistry = canUpdateRegistryFlag(entry, context);
        if (!canUpdateRegistry) {
          throw new UnsyncRegistryEntryException(String.format("[%s] - can't remove entry: [%s], entry was updated before", this, entry));
        }
        DeleteQuery<RegistryRecord> deleteQuery = context.deleteQuery(Tables.REGISTRY);
        deleteQuery.addConditions(Tables.REGISTRY.PATH.eq(entry.getPath()));
        deleteQuery.addConditions(Tables.REGISTRY.VALUE.eq(entry.getValue()));

        int res = deleteQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - entry: [{}] was removed", this, entry);
        } else {
          logger.warn("[{}] - can't remove entry: [{}] (result after execution is < 1)", this, entry);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - while removing entry: [%s]", this, entry), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public RegistryEntry[] listEntries(final String path, final int maxSize) {
    logger.trace("[{}] - trying to select listEntries by path: [{}], macSize: [{}]", this, path, maxSize);
    if (path == null || path.isEmpty()) {
      logger.warn("[{}] - can't select listEntries by path: [{}] (path can't be null, and path can't be empty)", this, path);
      return new RegistryEntry[0];
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<RegistryRecord> selectQuery = context.selectQuery(Tables.REGISTRY);
        selectQuery.addConditions(Tables.REGISTRY.PATH.eq(path));
        selectQuery.addOrderBy(Tables.REGISTRY.UPDATING_TIME);
        selectQuery.addLimit(maxSize);

        return selectQuery.fetch().into(RegistryRecord.class).stream().map(RegistryMapper::mapRegistryRecordToRegistryEntry).toArray(RegistryEntry[]::new);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting listEntries by path: [{}]", this, path, e);
      return new RegistryEntry[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public void add(final String path, final String value, final int maxSize) {
    logger.debug("[{}] - trying to add entry to registry (path: [{}], value: [{}], maxSize: [{}])", this, path, value, maxSize);
    if (path == null || path.isEmpty() || value == null) {
      logger.warn("[{}] - can't add entry to registry  (path: [{}], value: [{}]), path or value is not valid", this, path, value);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        RegistryRecord registryRecord = context.newRecord(Tables.REGISTRY);
        registryRecord.setPath(path);
        registryRecord.setValue(value);

        int res = registryRecord.store();

        if (res > 0) {
          logger.debug("[{}] - entry was added to registry path: [{}], value: [{}]", this, path, value);
          DeleteQuery<RegistryRecord> deleteQuery = context.deleteQuery(Tables.REGISTRY);
          SelectOffsetStep<Record1<Long>> subSelect2 = context.select(Tables.REGISTRY.ID).from(Tables.REGISTRY).where(Tables.REGISTRY.PATH.eq(path)).orderBy(Tables.REGISTRY.ID.desc()).limit(
              maxSize);
          SelectJoinStep<Record1<Long>> subSelect = context.select(DSL.min(Tables.REGISTRY.ID.as(Tables.REGISTRY.ID.getName()))).from(subSelect2);
          deleteQuery.addConditions(Tables.REGISTRY.PATH.eq(path).and(Tables.REGISTRY.ID.lessThan(subSelect)));

          res = deleteQuery.execute();

          if (res > 0) {
            logger.debug("[{}] - length of registry was corrected to: [{}] by path: [{}]", this, maxSize, path);
          } else {
            logger.warn("[{}] - can't correct length of registry (path: [{}], size: [{}]), result after execution is < 1", this, path, maxSize);
          }
        } else {
          logger.warn("[{}] - Can't add entry to registry (path: [{}], value: [{}]), result after execution is < 1", this, path, value);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while adding entry to registry (path: [{}], value: [{}]", this, path, value, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public String[] listAllPaths() {
    logger.trace("[{}] - trying to select all keys", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<RegistryRecord> selectQuery = context.selectQuery(Tables.REGISTRY);
        selectQuery.addSelect(Tables.REGISTRY.PATH);
        selectQuery.addDistinctOn(Tables.REGISTRY.PATH);
        selectQuery.addOrderBy(Tables.REGISTRY.PATH.desc());

        return selectQuery.fetch().into(String.class).stream().toArray(String[]::new);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting all keys", this, e);
      return new String[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public void removePath(final String path) {
    logger.debug("[{}] - trying to delete entries by path: [{}])", this, path);
    if (path == null || path.isEmpty()) {
      logger.warn("[{}] - can't delete all entries by path, path is not valid: [{}]", this, path);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<RegistryRecord> deleteQuery = context.deleteQuery(Tables.REGISTRY);
        deleteQuery.addConditions(Tables.REGISTRY.PATH.eq(path));

        int res = deleteQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - [{}] entries was deleted by path: [{}]", this, res, path);
        } else {
          logger.warn("[{}] - can't delete entries by path: [{}], result after execution is < 1", this, path);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while delete entries by path: [{}])", this, path, e);
    } finally {
      connection.close();
    }
  }

}
