/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.util.Collection;
import java.util.List;

public interface IConfigEditDAO {

  IServerData createServer() throws DataModificationException;

  void updateServer(IServerData server) throws DataModificationException;

  void deleteServer(IServerData server) throws DataModificationException;

  void updateSIMGroup(SIMGroup simGroup) throws DataModificationException;

  void deleteSIMGroup(SIMGroup simGroup) throws DataModificationException;

  GSMGroup createGSMGroup() throws DataModificationException;

  void updateGSMGroup(GSMGroup gsmGroup, Collection<SIMGroup> linkedSimGroups) throws DataModificationException;

  void deleteGSMGroup(GSMGroup gsmGroup) throws DataModificationException;

  void updateRouteConfig(RouteConfig routeConfig) throws DataModificationException;

  SIMGroup createSIMGroup() throws DataModificationException;

  void applyScriptConflicts(ScriptFile scriptFile, List<SIMGroup> newSimGroups, ScriptDefinition[] scriptDefinitions) throws DataModificationException;

}
