/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.ALARIS_SMS;
import static com.flamesgroup.storage.jooq.Tables.SENT_SMS;

import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ISentSmsDAO;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.storage.jooq.tables.records.SentSmsRecord;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.DeliveryStatuses;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.Record;
import org.jooq.SelectQuery;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class SentSmsDAO implements ISentSmsDAO {

  private static final Logger logger = LoggerFactory.getLogger(SentSmsDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public SentSmsDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void saveMultiPartOutboundSms(final ICCID iccid, final UUID smsId, final List<SMSMessageOutbound> smsMessageOutbounds) {
    logger.debug("[{}] - trying to insert outbound sms [{}] from [{}]", this, smsMessageOutbounds, iccid);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<SentSmsRecord> insertQuery = context.insertQuery(SENT_SMS);
        for (SMSMessageOutbound smsMessageOutbound : smsMessageOutbounds) {
          SentSmsRecord sentSmsRecord = new SentSmsRecord();
          sentSmsRecord.setIccid(iccid)
              .setSmsId(smsId)
              .setRecipientPhoneNumber(smsMessageOutbound.getRecipientPhoneNumber())
              .setMessage(smsMessageOutbound.getMessage())
              .setMessageClass(smsMessageOutbound.getMessageClass())
              .setEncoding(smsMessageOutbound.getEncoding())
              .setReferenceNumber(smsMessageOutbound.getReferenceNumber() == -1 ? null : smsMessageOutbound.getReferenceNumber())
              .setNumberOfSmsPart(smsMessageOutbound.getNumberOfSmsPart())
              .setCountOfSmsParts(smsMessageOutbound.getCountOfSmsParts())
              .setReceiptRequested(smsMessageOutbound.isReceiptRequested());
          if (smsMessageOutbound.isReceiptRequested()) {
            sentSmsRecord.setDeliveryStatus(DeliveryStatuses.INPROGRESS);
          } else {
            sentSmsRecord.setDeliveryStatus(DeliveryStatuses.UNKNOWN);
          }

          insertQuery.addRecord(sentSmsRecord);
        }

        if (insertQuery.execute() > 0) {
          logger.debug("[{}] - inserted outbound sms [{}] from [{}]", this, smsMessageOutbounds, iccid);
        } else {
          logger.debug("[{}] - failed insert outbound sms [{}] from [{}]", this, smsMessageOutbounds, iccid);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while inserting outbound sms [{}] from [{}]", this, smsMessageOutbounds, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public Pair<UUID, SMSMessageOutbound> saveSmsStatusReport(final ICCID iccid, final SMSStatusReport smsStatusReport) {
    logger.debug("[{}] - trying to set status [{}] of outbound sms for [{}]", this, smsStatusReport, iccid);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        UpdateQuery<SentSmsRecord> updateQuery = context.updateQuery(SENT_SMS);
        updateQuery.addConditions(SENT_SMS.ICCID.eq(iccid),
            DSL.val(smsStatusReport.getRecipientPhoneNumber()).like(DSL.concat("%", SENT_SMS.RECIPIENT_PHONE_NUMBER)),
            SENT_SMS.REFERENCE_NUMBER.eq(smsStatusReport.getReferenceNumber()));

        if (smsStatusReport.getStatus() == DeliveryStatuses.DELIVERED) {
          updateQuery.addValue(SENT_SMS.REFERENCE_NUMBER, (Integer) null);
        }
        updateQuery.addValue(SENT_SMS.DELIVERY_STATUS, smsStatusReport.getStatus());
        updateQuery.addValue(SENT_SMS.SMS_CENTER_NUMBER, smsStatusReport.getSmsCenterNumber());

        updateQuery.setReturning();

        if (updateQuery.execute() > 0) {
          logger.debug("[{}] - updated status [{}] of outbound sms for [{}]", this, smsStatusReport, iccid);
          SentSmsRecord sentSmsRecord = updateQuery.getReturnedRecord();
          if (sentSmsRecord == null) {
            logger.debug("[{}] - can't got sent sms Record for SMSStatusReport: [{}]", this, smsStatusReport);
            return null;
          }
          SMSMessageOutbound smsMessageOutbound = new SMSMessageOutbound(sentSmsRecord.getRecipientPhoneNumber(), sentSmsRecord.getSmsCenterNumber(), sentSmsRecord.getMessage(),
              sentSmsRecord.getMessageClass(), sentSmsRecord.getEncoding(),
              sentSmsRecord.getReferenceNumber() == null ? Integer.MIN_VALUE : sentSmsRecord.getReferenceNumber(),
              sentSmsRecord.getNumberOfSmsPart(), sentSmsRecord.getCountOfSmsParts(), sentSmsRecord.getReceiptRequested());
          return new Pair<>(sentSmsRecord.getSmsId(), smsMessageOutbound);
        } else {
          logger.warn("[{}] - failed to update status [{}] of outbound sms for [{}]", this, smsStatusReport, iccid);
          return null;
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating status [{}] of outbound sms from [{}]", this, smsStatusReport, iccid, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfDeliveredSms(final UUID smsId) {
    logger.debug("[{}] - trying to get count of delivered sms for smsId: [{}]]", this, smsId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SentSmsRecord> selectQuery = context.selectQuery(SENT_SMS);
        selectQuery.addSelect(SENT_SMS.ID);
        selectQuery.addConditions(SENT_SMS.SMS_ID.eq(smsId), SENT_SMS.DELIVERY_STATUS.eq(DeliveryStatuses.DELIVERED));

        return selectQuery.fetch(SENT_SMS.ID).size();
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get count of delivered sms for smsId: [{}]]", this, smsId, e);
    } finally {
      connection.close();
    }
    return 0;
  }

  @Override
  public Map<String, Integer> countAmountForCertainTime(final Date fromDate, final Date toDate, final List<String> iccids, final String clientIp) {
    logger.debug("[{}] - trying to get count AmountForCertainTime for iccids: [{}], fromDate: [{}], toDate: [{}]", this, iccids, fromDate, toDate);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<Record> selectQuery = context.selectQuery();

        selectQuery.addSelect(SENT_SMS.ICCID, DSL.countDistinct(SENT_SMS.ID));
        selectQuery.addFrom(SENT_SMS);

        if (!iccids.isEmpty()) {
          selectQuery.addConditions(SENT_SMS.ICCID.in(iccids.stream().map(ICCID::new).collect(Collectors.toList())));
        }
        if (clientIp != null && !clientIp.isEmpty()) {
          selectQuery.addFrom(ALARIS_SMS);
          selectQuery.addConditions(ALARIS_SMS.SMS_ID.eq(SENT_SMS.SMS_ID));
          selectQuery.addConditions(ALARIS_SMS.CLIENT_IP.eq(clientIp));
        }
        selectQuery.addConditions(SENT_SMS.SENDER_TIME.between(fromDate, toDate));
        selectQuery.addGroupBy(SENT_SMS.ICCID);

        return selectQuery.fetch().stream().collect(Collectors.toMap(r -> r.getValue(0, ICCID.class).getValue(), r -> r.getValue(1, Integer.class)));
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get count AmountForCertainTime for iccids: [{}], fromDate: [{}], toDate: [{}]", this, iccids, fromDate, toDate, e);
    } finally {
      connection.close();
    }
    return new HashMap<>();
  }

}
