/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.CELL_MONITOR;

import com.flamesgroup.antrax.storage.commons.impl.CellMonitor;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ICellMonitorDAO;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.storage.jooq.tables.records.CellMonitorRecord;
import com.flamesgroup.storage.map.CellMonitorMapper;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CellMonitorDAO implements ICellMonitorDAO {

  private static final Logger logger = LoggerFactory.getLogger(CellMonitorDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public CellMonitorDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void saveCellMonitors(final List<CellMonitor> cellMonitors) {
    logger.debug("[{}] - trying to save cell monitors [{}]", this, cellMonitors.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<CellMonitorRecord> insertQuery = context.insertQuery(CELL_MONITOR);
        for (CellMonitor cellMonitor : cellMonitors) {
          CellMonitorRecord cellMonitorRecord = context.newRecord(CELL_MONITOR);
          CellMonitorMapper.mapCellMonitorToCellMonitorRecord(cellMonitor, cellMonitorRecord);
          insertQuery.addRecord(cellMonitorRecord);
        }

        if (insertQuery.execute() > 0) {
          logger.debug("[{}] - saved cell monitors [{}]", this, cellMonitors.hashCode());
        } else {
          logger.debug("[{}] - failed save cell monitors [{}]", this, cellMonitors.hashCode());
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while saving cell monitors [{}]", this, cellMonitors.hashCode(), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<CellMonitor> getCellMonitors(final ChannelUID gsmChannel, final Date startTime, final Date endTime) {
    logger.debug("[{}] - trying to select cell monitors for gsmChannel [{}], startTime [{}], endTime [{}]", this, gsmChannel, startTime, endTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<CellMonitorRecord> selectQuery = context.selectQuery(CELL_MONITOR);
        selectQuery.addConditions(CELL_MONITOR.GSM_CHANNEL.eq(gsmChannel));
        selectQuery.addConditions(CELL_MONITOR.ADD_TIME.greaterOrEqual(startTime));
        selectQuery.addConditions(CELL_MONITOR.ADD_TIME.lessOrEqual(endTime));
        selectQuery.addOrderBy(CELL_MONITOR.ADD_TIME);

        return selectQuery.fetchInto(CellMonitorRecord.class).stream().map(CellMonitorMapper::mapCellMonitorRecordToCellMonitor).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of cell monitors for gsmChannel [{}], startTime [{}], endTime [{}]", this, gsmChannel, startTime, endTime, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

}
