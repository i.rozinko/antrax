/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import com.flamesgroup.unit.PhoneNumber;
import org.jooq.Converter;

public final class PhoneNumberConverter implements Converter<String, PhoneNumber> {

  private static final long serialVersionUID = -1175358219278788585L;

  @Override
  public PhoneNumber from(final String databaseObject) {
    return databaseObject == null ? new PhoneNumber() : new PhoneNumber(databaseObject);
  }

  @Override
  public String to(final PhoneNumber userObject) {
    return (userObject == null || userObject.isPrivate()) ? null : userObject.getValue();
  }

  @Override
  public Class<String> fromType() {
    return String.class;
  }

  @Override
  public Class<PhoneNumber> toType() {
    return PhoneNumber.class;
  }

}
