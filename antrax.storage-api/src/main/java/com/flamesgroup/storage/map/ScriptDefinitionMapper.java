/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.automation.meta.JavaFileName;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptInstanceImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameterImpl;
import com.flamesgroup.storage.jooq.tables.records.ScriptDefinitionRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamDefRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamRecord;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class ScriptDefinitionMapper {

  private ScriptDefinitionMapper() {
  }

  public static ScriptDefinitionImpl mapScriptDefinitionRecordToScriptDefinition(final ScriptDefinitionRecord scriptDefinitionRecord,
      final List<ScriptParamDefRecord> scriptParamDefRecords) {
    Objects.requireNonNull(scriptDefinitionRecord, "scriptDefinitionRecord mustn't be null");
    Objects.requireNonNull(scriptParamDefRecords, "scriptParamDefRecords mustn't be null");

    final List<ScriptParameterDefinitionImpl> scriptParameterDefinitions = scriptParamDefRecords.stream()
        .filter(scriptParamDefRecord -> scriptDefinitionRecord.getId().equals(scriptParamDefRecord.getScriptDefId()))
        .map(ScriptDefinitionMapper::mapScriptParameterDefinitionRecordToScriptParameterDefinition).collect(Collectors.toList());

    ScriptDefinitionImpl scriptDefinition = new ScriptDefinitionImpl(new JavaFileName(scriptDefinitionRecord.getCode()), scriptDefinitionRecord.getName(),
        scriptDefinitionRecord.getDescription(), scriptDefinitionRecord.getType(),
        scriptParameterDefinitions.toArray(new ScriptParameterDefinition[scriptParameterDefinitions.size()]));
    scriptDefinition.setID(scriptDefinitionRecord.getId());
    return scriptDefinition;
  }

  public static void mapScriptDefinitionToScriptDefinitionRecord(final ScriptDefinition scriptDefinition,
      final ScriptDefinitionRecord scriptDefinitionRecord) {
    Objects.requireNonNull(scriptDefinition, "scriptDefinition mustn't be null");
    Objects.requireNonNull(scriptDefinitionRecord, "scriptDefinitionRecord mustn't be null");

    scriptDefinitionRecord.setName(scriptDefinition.getName())
        .setType(scriptDefinition.getType())
        .setCode(scriptDefinition.getCode().getName())
        .setDescription(scriptDefinition.getDescription());
  }

  public static ScriptParameterDefinitionImpl mapScriptParameterDefinitionRecordToScriptParameterDefinition(final ScriptParamDefRecord scriptParamDefRecord) {
    Objects.requireNonNull(scriptParamDefRecord, "scriptParamDefRecord mustn't be null");

    ScriptParameterDefinitionImpl scriptParameterDefinition = new ScriptParameterDefinitionImpl(scriptParamDefRecord.getName(),
        scriptParamDefRecord.getMethod(),
        scriptParamDefRecord.getDescription(),
        scriptParamDefRecord.getType(),
        scriptParamDefRecord.getArray(),
        scriptParamDefRecord.getDefaultValue());
    scriptParameterDefinition.setID(scriptParamDefRecord.getId());
    return scriptParameterDefinition;
  }

  public static void mapScriptParameterDefinitionToScriptParameterDefinitionRecord(final long scriptDefinitionId,
      final ScriptParameterDefinitionImpl scriptParameterDefinition,
      final ScriptParamDefRecord scriptParamDefRecord) {
    Objects.requireNonNull(scriptParameterDefinition, "scriptParameterDefinition mustn't be null");
    Objects.requireNonNull(scriptParamDefRecord, "scriptParamDefRecord mustn't be null");

    scriptParamDefRecord.setScriptDefId(scriptDefinitionId)
        .setDescription(scriptParameterDefinition.getDescription())
        .setName(scriptParameterDefinition.getName())
        .setArray(scriptParameterDefinition.isArray())
        .setMethod(scriptParameterDefinition.getMethodName())
        .setType(scriptParameterDefinition.getType())
        .setDefaultValue(scriptParameterDefinition.getDefaultValue());
  }

  public static ScriptParameter mapScriptParameterRecordAndScriptParameterDefinitionRecordToScriptParameter(
      final ScriptParamRecord scriptParamRecord, final List<ScriptParamDefRecord> scriptParamDefRecords, final ScriptInstanceImpl scriptInstance) {
    Objects.requireNonNull(scriptParamRecord, "scriptParamRecord mustn't be null");
    Objects.requireNonNull(scriptParamDefRecords, "scriptParamDefRecords mustn't be null");

    ScriptParameterDefinition scriptParameterDefinition = mapScriptParameterDefinitionRecordToScriptParameterDefinition(scriptParamDefRecords.get(0));

    ScriptParameter scriptParameter = scriptParameterDefinition.createParameterInstance(scriptInstance);
    ((ScriptParameterImpl) scriptParameter).setID(scriptParamRecord.getId());
    scriptParameter.setSerializedValue(scriptParamRecord.getValue());
    return scriptParameter;
  }

}
