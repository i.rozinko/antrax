/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.CellMonitor;
import com.flamesgroup.storage.jooq.tables.records.CellMonitorRecord;

import java.util.Objects;

public final class CellMonitorMapper {

  private CellMonitorMapper() {
  }

  public static CellMonitor mapCellMonitorRecordToCellMonitor(final CellMonitorRecord cellMonitorRecord) {
    Objects.requireNonNull(cellMonitorRecord, "cellMonitoringRecord mustn't be null");

    return new CellMonitor(cellMonitorRecord.getId())
        .setGsmChannel(cellMonitorRecord.getGsmChannel())
        .setCell(cellMonitorRecord.getCell())
        .setBsic(cellMonitorRecord.getBsic())
        .setLac(cellMonitorRecord.getLac())
        .setCellId(cellMonitorRecord.getCellId())
        .setArfcn(cellMonitorRecord.getArfcn())
        .setRxLev(cellMonitorRecord.getRxlev())
        .setC1(cellMonitorRecord.getC1())
        .setC2(cellMonitorRecord.getC2())
        .setTa(cellMonitorRecord.getTa())
        .setRxQual(cellMonitorRecord.getRxqual())
        .setPlmn(cellMonitorRecord.getPlmn())
        .setAddTime(cellMonitorRecord.getAddTime());
  }

  public static void mapCellMonitorToCellMonitorRecord(final CellMonitor cellMonitor, final CellMonitorRecord cellMonitorRecord) {
    cellMonitorRecord.setGsmChannel(cellMonitor.getGsmChannel())
        .setCell(cellMonitor.getCell())
        .setBsic(cellMonitor.getBsic())
        .setLac(cellMonitor.getLac())
        .setCellId(cellMonitor.getCellId())
        .setArfcn(cellMonitor.getArfcn())
        .setRxlev(cellMonitor.getRxLev())
        .setC1(cellMonitor.getC1())
        .setC2(cellMonitor.getC2())
        .setTa(cellMonitor.getTa())
        .setRxqual(cellMonitor.getRxQual())
        .setPlmn(cellMonitor.getPlmn())
        .setAddTime(cellMonitor.getAddTime());
  }

}
