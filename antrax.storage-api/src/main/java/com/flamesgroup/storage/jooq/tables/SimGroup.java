/**
 * This class is generated by jOOQ
 */
package com.flamesgroup.storage.jooq.tables;


import com.flamesgroup.storage.jooq.Keys;
import com.flamesgroup.storage.jooq.Public;
import com.flamesgroup.storage.jooq.tables.records.SimGroupRecord;

import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SimGroup extends TableImpl<SimGroupRecord> {

	private static final long serialVersionUID = 105951124;

	/**
	 * The reference instance of <code>public.sim_group</code>
	 */
	public static final SimGroup SIM_GROUP = new SimGroup();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<SimGroupRecord> getRecordType() {
		return SimGroupRecord.class;
	}

	/**
	 * The column <code>public.sim_group.id</code>.
	 */
	public final TableField<SimGroupRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.name</code>.
	 */
	public final TableField<SimGroupRecord, String> NAME = createField("name", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_registration_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_REGISTRATION_MEDIANA = createField("idle_after_registration_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_registration_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_REGISTRATION_DELTA = createField("idle_after_registration_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_successful_call_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_SUCCESSFUL_CALL_MEDIANA = createField("idle_after_successful_call_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_successful_call_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_SUCCESSFUL_CALL_DELTA = createField("idle_after_successful_call_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_before_unregistration_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_BEFORE_UNREGISTRATION_MEDIANA = createField("idle_before_unregistration_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_before_unregistration_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_BEFORE_UNREGISTRATION_DELTA = createField("idle_before_unregistration_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.self_dial_factor</code>.
	 */
	public final TableField<SimGroupRecord, Integer> SELF_DIAL_FACTOR = createField("self_dial_factor", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.self_dial_chance</code>.
	 */
	public final TableField<SimGroupRecord, Integer> SELF_DIAL_CHANCE = createField("self_dial_chance", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_before_self_call_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_BEFORE_SELF_CALL_MEDIANA = createField("idle_before_self_call_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_before_self_call_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_BEFORE_SELF_CALL_DELTA = createField("idle_before_self_call_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_self_call_timeout_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_SELF_CALL_TIMEOUT_MEDIANA = createField("idle_after_self_call_timeout_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_self_call_timeout_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_SELF_CALL_TIMEOUT_DELTA = createField("idle_after_self_call_timeout_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.description</code>.
	 */
	public final TableField<SimGroupRecord, String> DESCRIPTION = createField("description", org.jooq.impl.SQLDataType.CLOB, this, "");

	/**
	 * The column <code>public.sim_group.can_be_appointed</code>.
	 */
	public final TableField<SimGroupRecord, Boolean> CAN_BE_APPOINTED = createField("can_be_appointed", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.synthetic_ringing</code>.
	 */
	public final TableField<SimGroupRecord, Boolean> SYNTHETIC_RINGING = createField("synthetic_ringing", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.fas_detection</code>.
	 */
	public final TableField<SimGroupRecord, Boolean> FAS_DETECTION = createField("fas_detection", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.suppress_incoming_call_signal</code>.
	 */
	public final TableField<SimGroupRecord, Boolean> SUPPRESS_INCOMING_CALL_SIGNAL = createField("suppress_incoming_call_signal", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_zero_call_mediana</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_ZERO_CALL_MEDIANA = createField("idle_after_zero_call_mediana", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.idle_after_zero_call_delta</code>.
	 */
	public final TableField<SimGroupRecord, Long> IDLE_AFTER_ZERO_CALL_DELTA = createField("idle_after_zero_call_delta", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.registration_period</code>.
	 */
	public final TableField<SimGroupRecord, Integer> REGISTRATION_PERIOD = createField("registration_period", org.jooq.impl.SQLDataType.INTEGER, this, "");

	/**
	 * The column <code>public.sim_group.registration_count</code>.
	 */
	public final TableField<SimGroupRecord, Integer> REGISTRATION_COUNT = createField("registration_count", org.jooq.impl.SQLDataType.INTEGER, this, "");

	/**
	 * The column <code>public.sim_group.operator_selection</code>.
	 */
	public final TableField<SimGroupRecord, String> OPERATOR_SELECTION = createField("operator_selection", org.jooq.impl.SQLDataType.CLOB.defaulted(true), this, "");

	/**
	 * The column <code>public.sim_group.sms_delivery_report</code>.
	 */
	public final TableField<SimGroupRecord, Boolean> SMS_DELIVERY_REPORT = createField("sms_delivery_report", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this, "");

	/**
	 * Create a <code>public.sim_group</code> table reference
	 */
	public SimGroup() {
		this("sim_group", null);
	}

	/**
	 * Create an aliased <code>public.sim_group</code> table reference
	 */
	public SimGroup(String alias) {
		this(alias, SIM_GROUP);
	}

	private SimGroup(String alias, Table<SimGroupRecord> aliased) {
		this(alias, aliased, null);
	}

	private SimGroup(String alias, Table<SimGroupRecord> aliased, Field<?>[] parameters) {
		super(alias, Public.PUBLIC, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<SimGroupRecord, Long> getIdentity() {
		return Keys.IDENTITY_SIM_GROUP;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<SimGroupRecord> getPrimaryKey() {
		return Keys.SIM_GROUP_PKEY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<SimGroupRecord>> getKeys() {
		return Arrays.<UniqueKey<SimGroupRecord>>asList(Keys.SIM_GROUP_PKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimGroup as(String alias) {
		return new SimGroup(alias, this);
	}

	/**
	 * Rename this table
	 */
	public SimGroup rename(String name) {
		return new SimGroup(name, null);
	}
}
