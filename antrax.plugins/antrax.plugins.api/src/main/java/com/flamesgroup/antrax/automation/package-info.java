/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Provides for creating antrax scripts.
 * <p>
 * <p>
 * Antrax system is highly automated using scripts. In the developers
 * perspective script is the implementation of one of the interfaces packaged in
 * {@link com.flamesgroup.antrax.automation.scripts} and annotated with
 * {@linkplain com.flamesgroup.antrax.automation.annotations.Script @Script}.
 * </p>
 * <p>
 * Scripts is configured by the users of an AntraxManager graphical application,
 * so in the users point of view script is black box provides some functionality
 * which is configured in some manner.
 * </p>
 * <p>
 * Scripts can be statefull. Such scripts lives longer than application
 * start/stop cycle. For example they don't worry about application crashes,
 * restarts.
 * </p>
 */
package com.flamesgroup.antrax.automation;

