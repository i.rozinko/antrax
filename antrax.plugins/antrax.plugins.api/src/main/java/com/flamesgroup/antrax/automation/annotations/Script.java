/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker for script class.
 * <p>
 * Such class should also implement one of the
 * {@linkplain com.flamesgroup.antrax.automation.scripts script interface}. Also it must
 * be public and have constructor without an arguments.
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Script {
  /**
   * Name of the script.
   * <p>
   * By the convention it starts with low case. The name must be clear and
   * short enough. You can separate words with whitespace if required.
   *
   * @return name of a script
   */
  String name();

  /**
   * Documentation string of a script.
   * <p>
   * Describes briefly what script provides to the antrax system.
   *
   * @return documentation string
   */
  String doc() default "unspecified";

}
