/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

/**
 * Prediction which tells that script will never change it's state
 * <p>
 * For example in infinite sessions.
 * </p>
 */
public final class AlwaysFalsePrediction implements Prediction {

  private static final long serialVersionUID = 1949567160522661604L;

  @Override
  public Prediction and(final Prediction other) {
    return this;
  }

  @Override
  public Prediction or(final Prediction other) {
    return other;
  }

  @Override
  public String toLocalizedString() {
    return "never";
  }

  @Override
  public Prediction trimToSmaller(final Prediction other) {
    return other;
  }

  @Override
  public Prediction trimToBigger(final Prediction other) {
    return this;
  }

  @Override
  public String toString() {
    return toLocalizedString();
  }

}
