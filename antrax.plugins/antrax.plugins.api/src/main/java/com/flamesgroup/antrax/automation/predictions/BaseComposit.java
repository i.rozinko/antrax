/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Base class for predictions composites
 * <p>
 * By the way it is prediction too. It combines two predictions triming them if
 * required
 * </p>
 */
public abstract class BaseComposit implements Prediction {

  private static final long serialVersionUID = -7128990404755773390L;

  protected abstract Prediction trim(Prediction a, Prediction b);

  protected abstract String getDelim();

  private List<Prediction> values = new ArrayList<>();

  protected void addPrediction(final Prediction p) {
    values.add(p);
  }

  protected void addPrediction(final List<Prediction> p) {
    values.addAll(p);
  }

  protected List<Prediction> getPredictions() {
    return Collections.unmodifiableList(values);
  }

  // TODO: check and improve in feature task
  protected void trim() {
    values = trim(values);
  }

  private List<Prediction> trim(List<Prediction> val) {
    val = new ArrayList<>(val);
    for (int i = 0; i < val.size(); ++i) {
      Prediction a = val.get(i);
      for (int j = i + 1; j < val.size(); ++j) {
        Prediction trimmed = trim(a, val.get(j));
        if (trimmed == null) {
          continue;
        }
        val.set(i, trimmed);
        val.remove(j);
        return trim(val);
      }
    }
    return val;
  }

  @Override
  public String toLocalizedString() {
    String delim = " " + getDelim() + " ";
    StringBuilder retval = new StringBuilder();
    for (Prediction p : values) {
      if (retval.length() > 0) {
        retval.append(delim);
      }
      boolean inBraces = p instanceof BaseComposit && ((BaseComposit) p).size() > 1;
      if (inBraces) {
        retval.append("(");
      }
      retval.append(p.toLocalizedString());
      if (inBraces) {
        retval.append(")");
      }
    }
    return retval.toString();
  }

  public int size() {
    return values.size();
  }

  @Override
  public Prediction and(final Prediction other) {
    if (other instanceof AlwaysTruePrediction) {
      return this;
    }
    if (other instanceof AlwaysFalsePrediction) {
      return other;
    }
    return new AndComposite(this, other);
  }

  @Override
  public Prediction or(final Prediction other) {
    if (other instanceof AlwaysTruePrediction) {
      return other;
    }
    if (other instanceof AlwaysFalsePrediction) {
      return this;
    }
    return new OrComposite(this, other);
  }

  @Override
  public String toString() {
    return toLocalizedString();
  }

}
