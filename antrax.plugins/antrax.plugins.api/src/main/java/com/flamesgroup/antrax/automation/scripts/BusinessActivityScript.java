/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.scripts;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;

/**
 * Provides activity of sim card during it is registered in GSM network.
 * <p>
 * There is lots of activities required for the sim card during it's activity:
 * checking bill, sending sms, account recharge, limiting calls duration, etc.
 * </p>
 * <p>
 * Any of this activities provided by business activity script.
 * </p>
 *
 * @see RegisteredInGSMChannel
 */
public interface BusinessActivityScript {
  /**
   * NOTE: Invoked in separate thread
   */
  void invokeBusinessActivity(RegisteredInGSMChannel channel) throws Exception;

  boolean shouldStartBusinessActivity();

  String describeBusinessActivity();

}
