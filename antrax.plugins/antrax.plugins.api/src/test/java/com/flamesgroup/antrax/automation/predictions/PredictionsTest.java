/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.predictions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PredictionsTest {

  @Test
  public void testSinglePrediction() {
    String msg = "a5";
    assertEquals(msg, new A(5).toString());
  }

  @Test
  public void testOr() {
    A a5 = new A(5);
    A a3 = new A(3);
    assertEquals("a5 or a3", a5.or(a3).toString());
    assertEquals("a3 or a5", a3.or(a5).toString());
    assertEquals("a3 or a5 or a3", a3.or(a5.or(a3)).toString());
  }

  @Test
  public void testAnd() {
    A a5 = new A(5);
    A a3 = new A(3);
    assertEquals("a5 and a3", a5.and(a3).toString());
    assertEquals("a3 and a5", a3.and(a5).toString());
    assertEquals("a3 and a5 and a3", a3.and(a5.and(a3)).toString());
  }

  @Test
  public void testOrTrue() {
    Prediction a = new A(1);
    Prediction t = new AlwaysTruePrediction();
    assertEquals("any time", a.or(t).toString());
  }

}
