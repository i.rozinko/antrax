/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.ui.guiclient.widgets.table;

import com.flamesgroup.antrax.automation.editors.PropertyTableRenderer;

import java.awt.*;

import javax.swing.*;

public class BooleanPropertyTableRenderer extends JCheckBox implements PropertyTableRenderer<Boolean> {

  private static final long serialVersionUID = -6844791498739809331L;

  public BooleanPropertyTableRenderer() {
    setHorizontalAlignment(JLabel.CENTER);
    setOpaque(false);
  }

  @Override
  public Component getRendererComponent(final Boolean value) {
    boolean selected = (value != null && (value).booleanValue());
    setModel(new UnselectedButtonModel(selected));
    return this;
  }

  private static class UnselectedButtonModel extends ToggleButtonModel {

    private static final long serialVersionUID = -2353948425845712359L;

    private final boolean staticSelected;

    public UnselectedButtonModel(final boolean staticSelected) {
      this.staticSelected = staticSelected;
    }

    @Override
    public boolean isSelected() {
      return staticSelected;
    }

    @Override
    public void setSelected(final boolean b) {
    }

  }

}
