/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils.codebase;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import javax.tools.SimpleJavaFileObject;

public class JavaSourceToMemory extends SimpleJavaFileObject {

  private final AntraxCodeBase codeBase;
  private final String className;

  public JavaSourceToMemory(final String className, final AntraxCodeBase codeBase) {
    super(URI.create("class://" + className + Kind.CLASS.extension), Kind.CLASS);
    this.codeBase = codeBase;
    this.className = className;
  }

  @Override
  public OutputStream openOutputStream() throws IOException {
    return codeBase.addClass(className);
  }

}
