/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

import java.util.Date;

public class GrayListNumber extends ListNumber {

  private static final long serialVersionUID = 2280722367184859566L;

  private Date blockTime;
  private int blockCount;
  private Date blockTimeLeft;
  private String statusDescription;

  public GrayListNumber() {
  }

  public GrayListNumber(final String number, final VoipAntiSpamListNumbersStatus status, final int routingRequestCount, final Date blockTime, final int blockCount, final Date blockTimeLeft,
      final String statusDescription) {
    super(number, status, routingRequestCount);
    this.blockTime = blockTime;
    this.blockCount = blockCount;
    this.blockTimeLeft = blockTimeLeft;
    this.statusDescription = statusDescription;
  }

  public Date getBlockTime() {
    return blockTime;
  }

  public GrayListNumber setBlockTime(final Date blockTime) {
    this.blockTime = blockTime;
    return this;
  }

  public int getBlockCount() {
    return blockCount;
  }

  public GrayListNumber setBlockCount(final int blockCount) {
    this.blockCount = blockCount;
    return this;
  }

  public Date getBlockTimeLeft() {
    return blockTimeLeft;
  }

  public GrayListNumber setBlockTimeLeft(final Date blockTimeLeft) {
    this.blockTimeLeft = blockTimeLeft;
    return this;
  }

  public String getStatusDescription() {
    return statusDescription;
  }

  public GrayListNumber setStatusDescription(final String statusDescription) {
    this.statusDescription = statusDescription;
    return this;
  }

  @Override
  protected void appendToStringProperty(final StringBuilder sb) {
    super.appendToStringProperty(sb);
    sb.append(" bt:").append(blockTime);
    sb.append(" bc:").append(blockCount);
    sb.append(" btl:").append(blockTimeLeft);
    sb.append(" sd:'").append(statusDescription).append('\'');
  }

}
