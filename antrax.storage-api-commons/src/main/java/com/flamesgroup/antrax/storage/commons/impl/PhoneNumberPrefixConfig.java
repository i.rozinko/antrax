/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.Serializable;
import java.util.Objects;

public class PhoneNumberPrefixConfig implements Serializable {

  private static final long serialVersionUID = 4629049504706054005L;

  private boolean enable;
  private String defaultPrefix;

  public boolean isEnable() {
    return enable;
  }

  public PhoneNumberPrefixConfig setEnable(final boolean enable) {
    this.enable = enable;
    return this;
  }

  public String getDefaultPrefix() {
    return defaultPrefix;
  }

  public PhoneNumberPrefixConfig setDefaultPrefix(final String defaultPrefix) {
    this.defaultPrefix = defaultPrefix;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof PhoneNumberPrefixConfig)) {
      return false;
    }
    final PhoneNumberPrefixConfig that = (PhoneNumberPrefixConfig) object;

    return enable == that.enable
        && Objects.equals(defaultPrefix, that.defaultPrefix);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Boolean.hashCode(enable);
    result = prime * result + Objects.hashCode(defaultPrefix);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[enable:" + enable +
        " defaultPrefix:'" + defaultPrefix + "'" +
        ']';
  }

}
