/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.commons.voipantispam.BlackListConfig;
import com.flamesgroup.commons.voipantispam.FasConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;

import java.util.List;
import java.util.Objects;

public class VoipAntiSpamConfiguration extends Domain {

  private static final long serialVersionUID = 8223771922569458862L;

  private boolean enable;

  private List<GrayListConfig> grayListConfigs;
  private List<BlackListConfig> blackListConfigs;

  private boolean gsmAlertingAnalyze;
  private List<GsmAlertingConfig> gsmAlertingConfigs;

  private boolean voipAlertingAnalyze;
  private List<VoipAlertingConfig> voipAlertingConfigs;

  private boolean fasAnalyze;
  private List<FasConfig> fasConfigs;

  public VoipAntiSpamConfiguration() {
  }

  public boolean isEnable() {
    return enable;
  }

  public VoipAntiSpamConfiguration setEnable(final boolean enable) {
    this.enable = enable;
    return this;
  }

  public List<GrayListConfig> getGrayListConfigs() {
    return grayListConfigs;
  }

  public VoipAntiSpamConfiguration setGrayListConfigs(final List<GrayListConfig> grayListConfigs) {
    this.grayListConfigs = grayListConfigs;
    return this;
  }

  public List<BlackListConfig> getBlackListConfigs() {
    return blackListConfigs;
  }

  public VoipAntiSpamConfiguration setBlackListConfigs(final List<BlackListConfig> blackListConfigs) {
    this.blackListConfigs = blackListConfigs;
    return this;
  }

  public boolean isGsmAlertingAnalyze() {
    return gsmAlertingAnalyze;
  }

  public VoipAntiSpamConfiguration setGsmAlertingAnalyze(final boolean gsmAlertingAnalyze) {
    this.gsmAlertingAnalyze = gsmAlertingAnalyze;
    return this;
  }

  public List<GsmAlertingConfig> getGsmAlertingConfigs() {
    return gsmAlertingConfigs;
  }

  public VoipAntiSpamConfiguration setGsmAlertingConfigs(final List<GsmAlertingConfig> gsmAlertingConfigs) {
    this.gsmAlertingConfigs = gsmAlertingConfigs;
    return this;
  }

  public boolean isVoipAlertingAnalyze() {
    return voipAlertingAnalyze;
  }

  public VoipAntiSpamConfiguration setVoipAlertingAnalyze(final boolean voipAlertingAnalyze) {
    this.voipAlertingAnalyze = voipAlertingAnalyze;
    return this;
  }

  public List<VoipAlertingConfig> getVoipAlertingConfigs() {
    return voipAlertingConfigs;
  }

  public VoipAntiSpamConfiguration setVoipAlertingConfigs(final List<VoipAlertingConfig> voipAlertingConfigs) {
    this.voipAlertingConfigs = voipAlertingConfigs;
    return this;
  }

  public boolean isFasAnalyze() {
    return fasAnalyze;
  }

  public VoipAntiSpamConfiguration setFasAnalyze(final boolean fasAnalyze) {
    this.fasAnalyze = fasAnalyze;
    return this;
  }

  public List<FasConfig> getFasConfigs() {
    return fasConfigs;
  }

  public VoipAntiSpamConfiguration setFasConfigs(final List<FasConfig> fasConfigs) {
    this.fasConfigs = fasConfigs;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof VoipAntiSpamConfiguration)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final VoipAntiSpamConfiguration that = (VoipAntiSpamConfiguration) object;

    return enable == that.enable
        && gsmAlertingAnalyze == that.gsmAlertingAnalyze
        && voipAlertingAnalyze == that.voipAlertingAnalyze
        && fasAnalyze == that.fasAnalyze
        && Objects.equals(grayListConfigs, that.grayListConfigs)
        && Objects.equals(blackListConfigs, that.blackListConfigs)
        && Objects.equals(gsmAlertingConfigs, that.gsmAlertingConfigs)
        && Objects.equals(voipAlertingConfigs, that.voipAlertingConfigs)
        && Objects.equals(fasConfigs, that.fasConfigs);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Boolean.hashCode(enable);
    result = prime * result + Objects.hashCode(grayListConfigs);
    result = prime * result + Objects.hashCode(blackListConfigs);
    result = prime * result + Boolean.hashCode(gsmAlertingAnalyze);
    result = prime * result + Objects.hashCode(gsmAlertingConfigs);
    result = prime * result + Boolean.hashCode(voipAlertingAnalyze);
    result = prime * result + Objects.hashCode(voipAlertingConfigs);
    result = prime * result + Boolean.hashCode(fasAnalyze);
    result = prime * result + Objects.hashCode(fasConfigs);
    return result;
  }

}
