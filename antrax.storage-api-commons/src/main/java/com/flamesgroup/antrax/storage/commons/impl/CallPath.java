/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;

import java.util.Date;

public class CallPath extends Domain {

  private static final long serialVersionUID = -251635773140631638L;

  private ChannelUID simChannelUID;
  private ChannelUID gsmChannelUID;
  private ICCID simUID;
  private String simGroupName;
  private String gsmGroupName;
  private long startTime;
  private CdrDropReason cdrDropReason;
  private Short cdrDropCode;

  public CallPath() {
    startTime = System.currentTimeMillis();
  }

  public CallPath(final long id) {
    this.id = id;
  }

  public ChannelUID getSimChannelUID() {
    return simChannelUID;
  }

  public CallPath setSimChannelUID(final ChannelUID simChannelUID) {
    this.simChannelUID = simChannelUID;
    return this;
  }

  public ChannelUID getGsmChannelUID() {
    return gsmChannelUID;
  }

  public CallPath setGsmChannelUID(final ChannelUID gsmChannelUID) {
    this.gsmChannelUID = gsmChannelUID;
    return this;
  }

  public ICCID getSimUID() {
    return simUID;
  }

  public CallPath setSimUID(final ICCID simUID) {
    this.simUID = simUID;
    return this;
  }

  public CdrDropReason getCdrDropReason() {
    return cdrDropReason;
  }

  public CallPath setCdrDropReason(final CdrDropReason cdrDropReason) {
    this.cdrDropReason = cdrDropReason;
    return this;
  }

  public Short getCdrDropCode() {
    return cdrDropCode;
  }

  public CallPath setCdrDropCode(final Short cdrDropCode) {
    this.cdrDropCode = cdrDropCode;
    return this;
  }

  public String getSimGroupName() {
    return simGroupName;
  }

  public CallPath setStartTime(final long startTime) {
    this.startTime = startTime;
    return this;
  }

  public long getStartTime() {
    return startTime;
  }

  public Date getStartDate() {
    return new Date(getStartTime());
  }

  public CallPath setSimGroupName(final String simGroupName) {
    this.simGroupName = simGroupName;
    return this;
  }

  public String getGsmGroupName() {
    return gsmGroupName;
  }

  public CallPath setGsmGroupName(final String gsmGroupName) {
    this.gsmGroupName = gsmGroupName;
    return this;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }

  @Override
  public String toString() {
    return simChannelUID + ":" + gsmChannelUID;
  }

}
