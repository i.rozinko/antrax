package com.flamesgroup.antrax.control.swingwidgets.dialog;

import com.flamesgroup.antrax.control.swingwidgets.ExtFileFilter;

import java.awt.*;
import java.io.File;

import javax.swing.*;

public class FileSelectDialog {

  private final Component invoker;
  private final File currentDir;
  private final String ext;
  private final String extDescription;

  public FileSelectDialog(final Component invoker, final File currentDir, final String ext, final String extDescription) {
    this.invoker = invoker;
    this.currentDir = currentDir;
    this.ext = ext;
    this.extDescription = extDescription;
  }

  private String attachExtension(final File file) {
    String fileName = file.getAbsolutePath();
    int i = fileName.lastIndexOf(ext);
    if (i != fileName.length() - ext.length()) {
      fileName += ext;
    }

    return fileName;
  }

  public File selectFile(final String defaultFileName, final boolean forSave) {
    JFileChooser chooser = new JFileChooser(currentDir) {
      private static final long serialVersionUID = -3146740515805075817L;

      @Override
      public void approveSelection() {
        if (forSave) {
          File saveFile = getSelectedFile();
          saveFile = new File(attachExtension(saveFile));
          if (saveFile.exists()) {
            int result = JOptionPane.showConfirmDialog(
                getTopLevelAncestor(),
                "The selected file already exists. "
                    + "Do you want to overwrite it?",
                "The file already exists",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);
            switch (result) {
              case JOptionPane.YES_OPTION:
                super.approveSelection();
                return;
              case JOptionPane.NO_OPTION:
                return;
              case JOptionPane.CANCEL_OPTION:
                cancelSelection();
                return;
            }
          }
        }
        super.approveSelection();
      }

    };
    chooser.setFileFilter(new ExtFileFilter(ext, extDescription));
    //chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setMultiSelectionEnabled(false);
    if (forSave && defaultFileName != null && !defaultFileName.isEmpty()) {
      chooser.setSelectedFile(new File(defaultFileName));
    }

    int result = (forSave) ? chooser.showSaveDialog(invoker) : chooser.showOpenDialog(invoker);

    File saveFile = null;
    if (result == JFileChooser.APPROVE_OPTION) {
      saveFile = chooser.getSelectedFile();
      // Check file extension
      saveFile = new File(attachExtension(saveFile));
    }

    return saveFile;
  }

  public File saveFile(final String defaultFileName) {
    return selectFile(defaultFileName, true);
  }

  public File openFile() {
    return selectFile("", false);
  }

} 
