package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import javax.swing.*;

/**
 * Context search menu item
 */
public class ContextSearchMenuItem extends JCheckBoxMenuItem {

  private static final long serialVersionUID = 9054274619961131052L;

  private final SearchItem item;

  public ContextSearchMenuItem(final SearchItem item, final boolean enabled) {
    this(item, enabled, item.getSearchItemName());
  }

  public ContextSearchMenuItem(final SearchItem item, final boolean enabled, final String text) {
    super(text, enabled);
    this.item = item;
  }

  public SearchItem getSearchItem() {
    return item;
  }

}
