package com.flamesgroup.antrax.control.swingwidgets.editor;

import javax.swing.*;

public class JEditCheckBox extends JCheckBox {

  private static final long serialVersionUID = 9117495985853731728L;

  public static class JEditCheckBoxBuilder {
    private String text;
    private Icon icon;
    private int horizontalAlignment;

    public JEditCheckBoxBuilder() {
      setText("");
      setHorizontalAlignment(SwingConstants.LEADING);
    }

    public JEditCheckBoxBuilder setText(final String text) {
      this.text = text;
      return this;
    }

    public JEditCheckBoxBuilder setIcon(final Icon icon) {
      this.icon = icon;
      return this;
    }

    public JEditCheckBoxBuilder setHorizontalAlignment(final int horizontalAlignment) {
      this.horizontalAlignment = horizontalAlignment;
      return this;
    }

    public JEditCheckBox build() {
      return new JEditCheckBox(this);
    }

  }

  public JEditCheckBox() {
    super();
    //    initUI();
  }

  public JEditCheckBox(final String text) {
    this((new JEditCheckBoxBuilder()).setText(text));
  }

  protected JEditCheckBox(final JEditCheckBoxBuilder builder) {
    this();
    setText(builder.text);
    setIcon(builder.icon);
    setHorizontalAlignment(builder.horizontalAlignment);
  }

}
