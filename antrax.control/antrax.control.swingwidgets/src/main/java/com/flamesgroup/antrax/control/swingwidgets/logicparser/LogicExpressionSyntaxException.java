package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class LogicExpressionSyntaxException extends Exception {

  private static final long serialVersionUID = 4139822323304853558L;

  private final int errorPosition;

  public LogicExpressionSyntaxException(final String message, final int errorPosition) {
    super(message);
    this.errorPosition = errorPosition;
  }

  public LogicExpressionSyntaxException(final Throwable cause, final int errorPosition) {
    super(cause);
    this.errorPosition = errorPosition;
  }

  public int getErrorPosition() {
    return errorPosition;
  }

}
