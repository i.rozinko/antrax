package com.flamesgroup.antrax.control.swingwidgets.layouts;

public enum TwoColumnedInfoLayoutConstraint {
  LEFT_LABEL,
  LEFT_VALUE,
  RIGHT_LABEL,
  RIGHT_VALUE
}
