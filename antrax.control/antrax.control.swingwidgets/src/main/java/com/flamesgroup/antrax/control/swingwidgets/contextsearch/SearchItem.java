package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

public interface SearchItem extends Searchable {

  String getSearchItemName();

  void setSearchItemName(String name);

}
