package com.flamesgroup.antrax.control.swingwidgets.field;

import javax.swing.*;

public class NullLabel extends JLabel {

  private static final long serialVersionUID = -6582783696803586717L;

  public NullLabel() {
  }

  public NullLabel(final String text, final Icon icon, final int horizontalAlignment) {
    super(text, icon, horizontalAlignment);
  }

  public NullLabel(final String text, final int horizontalAlignment) {
    super(text, horizontalAlignment);
  }

  public NullLabel(final String text) {
    super(text);
  }

  public NullLabel(final Icon icon, final int horizontalAlignment) {
    super(icon, horizontalAlignment);
  }

  public NullLabel(final Icon icon) {
    super(icon);
  }

  @Override
  public void updateUI() {
    super.updateUI();
    init();
  }

  private void init() {
    setFont(null);
    setBackground(null);
    setForeground(null);
  }
}
