package com.flamesgroup.antrax.control.swingwidgets.calendar;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.accessibility.AccessibleAction;
import javax.swing.*;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JFormattedTextField.AbstractFormatterFactory;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.DefaultFormatterFactory;

public class JXDatePicker extends JComponent {
  private static final long serialVersionUID = 6927489867441988682L;

  /**
   * The editable date field that displays the date
   */
  protected JFormattedTextField dateField;

  /**
   * Popup that displays the month view with controls for traversing/selecting
   * dates.
   */
  protected JXDatePickerPopup popup;
  private final JButton popupButton;
  private static final int popupButtonWidth = 16;
  private JXMonthView monthView;
  private JXTimeView timeView;
  private final Handler handler;
  private String actionCommand = "selectionChanged";

  /**
   * Create a new date picker using the current date as the initial selection.
   */
  public JXDatePicker() {
    this(System.currentTimeMillis());
  }

  /**
   * Create a new date picker using the specified time as the initial
   * selection.
   *
   * @param millis initial time in milliseconds
   */
  public JXDatePicker(final long millis) {
    UIManager.put("JXDatePicker.arrowDown.image", IconPool.getShared("arrow-down.png"));
    dateField = new JFormattedTextField(new JXDatePickerFormatterFactory());
    dateField.setName("dateField");
    dateField.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
    dateField.setBorder(null);
    handler = new Handler();
    popupButton = new JButton();
    popupButton.setPreferredSize(new Dimension(16, 16));
    popupButton.setMaximumSize(new Dimension(16, 16));
    popupButton.setName("popupButton");
    popupButton.setRolloverEnabled(false);
    popupButton.addMouseListener(handler);
    popupButton.addMouseMotionListener(handler);

    KeyStroke enterKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false);

    InputMap inputMap = dateField.getInputMap(JComponent.WHEN_FOCUSED);
    inputMap.put(enterKey, "COMMIT_EDIT");

    ActionMap actionMap = dateField.getActionMap();
    actionMap.put("COMMIT_EDIT", new CommitEditAction());

    KeyStroke spaceKey = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false);

    inputMap = popupButton.getInputMap(JComponent.WHEN_FOCUSED);
    inputMap.put(spaceKey, AccessibleAction.TOGGLE_POPUP);

    actionMap = popupButton.getActionMap();
    actionMap.put(AccessibleAction.TOGGLE_POPUP, new TogglePopupAction());

    add(dateField);
    add(popupButton);

    updateUI();

    dateField.setValue(new Date(millis));

  }

  /**
   * Resets the UI property to a value from the current look and feel.
   */
  @Override
  public void updateUI() {
    int cols = UIManager.getInt("JXDatePicker.numColumns");
    if (cols == -1) {
      cols = 10;
    }
    dateField.setColumns(cols);

    String str = UIManager.getString("JXDatePicker.arrowDown.tooltip");
    if (str == null) {
      str = "Show Calendar";
    }
    popupButton.setToolTipText(str);

    Icon icon = UIManager.getIcon("JXDatePicker.arrowDown.image");
    if (icon == null) {
      icon = IconPool.getShared("down.png");
    }
    popupButton.setIcon(icon);

    Border border = UIManager.getBorder("JXDatePicker.border");
    if (border == null) {
      border = BorderFactory.createCompoundBorder(LineBorder.createGrayLineBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3));
    }
    dateField.setBorder(border);
  }

  /**
   * Formatted data
   *
   * @param formats
   */
  public void setFormats(final String... formats) {
    DateFormat[] dateFormats = new DateFormat[formats.length];
    for (int counter = formats.length - 1; counter >= 0; counter--) {
      dateFormats[counter] = new SimpleDateFormat(formats[counter]);
    }
    setFormats(dateFormats);
  }

  public void setFormats(final DateFormat... formats) {
    dateField.setFormatterFactory(new DefaultFormatterFactory(new JXDatePickerFormatter(formats)));
  }

  public DateFormat[] getFormats() {
    // Dig this out from the factory, if possible, otherwise return null.
    AbstractFormatterFactory factory = dateField.getFormatterFactory();
    if (factory != null) {
      AbstractFormatter formatter = factory.getFormatter(dateField);
      if (formatter instanceof JXDatePickerFormatter) {
        return ((JXDatePickerFormatter) formatter).getFormats();
      }
    }
    return null;
  }

  /**
   * Set the currently selected date.
   *
   * @param date date
   */
  public void setDate(final Date date) {
    dateField.setValue(date);
  }

  /**
   * Set the currently selected date.
   *
   * @param millis milliseconds
   */
  public void setDateInMillis(final long millis) {
    dateField.setValue(new Date(millis));
  }

  /**
   * Returns the currently selected date.
   *
   * @return Date
   */
  public Date getDate() {
    return (Date) dateField.getValue();
  }

  /**
   * Returns the currently selected date in milliseconds.
   *
   * @return the date in milliseconds
   */
  public long getDateInMillis() {
    return ((Date) dateField.getValue()).getTime();
  }

  /**
   * Returns the formatted text field used to edit the date selection.
   *
   * @return the formatted text field
   */
  public JFormattedTextField getEditor() {
    return dateField;
  }

  /**
   * Return the AbstractFormatterFactory for this instance of the
   * JXDatePicker.
   *
   * @return the AbstractFormatterFactory
   */
  public AbstractFormatterFactory getDateFormatterFactory() {
    return dateField.getFormatterFactory();
  }

  /**
   * Set the AbstractFormatterFactory to be used by this instance of the
   * JXDatePicker.
   *
   * @param dateFormatterFactory the AbstractFormatterFactory
   */
  public void setDateFormatterFactory(final AbstractFormatterFactory dateFormatterFactory) {
    dateField.setFormatterFactory(dateFormatterFactory);
  }

  /**
   * Returns true if the current value being edited is valid.
   *
   * @return true if the current value being edited is valid.
   */
  public boolean isEditValid() {
    return dateField.isEditValid();
  }

  /**
   * Forces the current value to be taken from the AbstractFormatter and set
   * as the current value. This has no effect if there is no current
   * AbstractFormatter installed.
   */
  public void commitEdit() throws ParseException {
    dateField.commitEdit();
  }

  /**
   * Enables or disables the date picker and all its subComponents.
   *
   * @param value true to enable, false to disable
   */
  @Override
  public void setEnabled(final boolean value) {
    if (isEnabled() == value) {
      return;
    }

    super.setEnabled(value);
    dateField.setEnabled(value);
    popupButton.setEnabled(value);
  }

  /**
   * Returns the string currently used to identify fired ActionEvents.
   *
   * @return String The string used for identifying ActionEvents.
   */
  public String getActionCommand() {
    return actionCommand;
  }

  /**
   * Sets the string used to identify fired ActionEvents.
   *
   * @param actionCommand The string used for identifying ActionEvents.
   */
  public void setActionCommand(final String actionCommand) {
    this.actionCommand = actionCommand;
  }

  /**
   * Adds an ActionListener.
   * <p>
   * The ActionListener will receive an ActionEvent when a selection has been
   * made.
   *
   * @param l The ActionListener that is to be notified
   */
  public void addActionListener(final ActionListener l) {
    listenerList.add(ActionListener.class, l);
  }

  /**
   * Removes an ActionListener.
   *
   * @param l The action listener to remove.
   */
  public void removeActionListener(final ActionListener l) {
    listenerList.remove(ActionListener.class, l);
  }

  /**
   * Fires an ActionEvent to all listeners.
   */
  protected void fireActionPerformed() {
    Object[] listeners = listenerList.getListenerList();
    ActionEvent e = null;
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ActionListener.class) {
        if (e == null) {
          e = new ActionEvent(JXDatePicker.this, ActionEvent.ACTION_PERFORMED, actionCommand);
        }
        ((ActionListener) listeners[i + 1]).actionPerformed(e);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void doLayout() {
    int width = getWidth();
    int height = getHeight();

    Insets insets = getInsets();
    dateField.setBounds(insets.left, insets.bottom, width - popupButtonWidth, height);
    popupButton.setBounds(width - popupButtonWidth + insets.left, insets.bottom, popupButtonWidth, height);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Dimension getPreferredSize() {
    Dimension dim = dateField.getPreferredSize();
    dim.width += popupButton.getPreferredSize().width;
    Insets insets = getInsets();
    dim.width += insets.left + insets.right;
    dim.height += insets.top + insets.bottom;
    return dim;
  }

  /**
   * Action used to commit the current value in the JFormattedTextField. This
   * action is used by the keyboard bindings.
   */
  private class TogglePopupAction extends AbstractAction {
    private static final long serialVersionUID = -8653895347568776584L;

    public TogglePopupAction() {
      super("TogglePopup");
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      handler.toggleShowPopup();
    }
  }

  /**
   * Action used to commit the current value in the JFormattedTextField. This
   * action is used by the keyboard bindings.
   */
  private class CommitEditAction extends AbstractAction {
    private static final long serialVersionUID = 6666031779271567450L;

    public CommitEditAction() {
      super("CommitEditPopup");
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      try {
        // Commit the current value.
        dateField.commitEdit();

        // Reformat the value according to the formatter.
        dateField.setValue(dateField.getValue());
        fireActionPerformed();
      } catch (java.text.ParseException ignored) {
      }
    }
  }

  private class Handler implements MouseListener, MouseMotionListener {
    private boolean _forwardReleaseEvent = false;

    @Override
    public void mouseClicked(final MouseEvent ev) {
    }

    @Override
    public void mousePressed(final MouseEvent ev) {
      if (!isEnabled()) {
        return;
      }

      if (dateField.isEditValid()) {
        try {
          dateField.commitEdit();
        } catch (java.text.ParseException ignored) {
        }
      }
      toggleShowPopup();
    }

    @Override
    public void mouseReleased(MouseEvent ev) {
      // Retarget mouse event to the month view.
      if (_forwardReleaseEvent) {
        if (monthView != null) {
          ev = SwingUtilities.convertMouseEvent(popupButton, ev, monthView);
          monthView.dispatchEvent(ev);
        }
        _forwardReleaseEvent = false;
      }
    }

    @Override
    public void mouseEntered(final MouseEvent ev) {
    }

    @Override
    public void mouseExited(final MouseEvent ev) {
    }

    @Override
    public void mouseDragged(MouseEvent ev) {
      _forwardReleaseEvent = true;

      if (popup == null || !popup.isShowing()) {
        return;
      }

      // Retarget mouse event to the month view.
      ev = SwingUtilities.convertMouseEvent(popupButton, ev, monthView);
      monthView.dispatchEvent(ev);
    }

    @Override
    public void mouseMoved(final MouseEvent ev) {
    }

    public void toggleShowPopup() {
      if (popup == null) {
        popup = new JXDatePickerPopup();
      }

      if (!popup.isVisible()) {
        if (dateField.getValue() == null) {
          dateField.setValue(new Date(System.currentTimeMillis()));
        }
        DateSpan span = new DateSpan((java.util.Date) dateField.getValue(), (java.util.Date) dateField.getValue());
        monthView.setSelectedDateSpan(span);
        monthView.ensureDateVisible(((Date) dateField.getValue()).getTime());
        // Point loc = _dateField.getLocationOnScreen();
        popup.show(JXDatePicker.this, 0, JXDatePicker.this.getHeight());
        popup.setVisible(true);
      } else {
        popup.setVisible(false);
      }
    }
  }

  /**
   * Popup component that shows a JXMonthView component along with controlling
   * buttons to allow traversal of the months. Upon selection of a date the
   * popup will automatically hide itself and enter the selection into the
   * editable field of the JXDatePicker.
   */
  private class JXDatePickerPopup extends JPopupMenu implements ActionListener, JXTimeView.TimeChangerListener {
    private static final long serialVersionUID = -4107308083320289648L;
    private final JButton nextButton;
    private final JButton previousButton;
    private final JButton todayButton;

    public JXDatePickerPopup() {
      timeView = new JXTimeView((Date) dateField.getValue());
      timeView.addListener(this);

      monthView = new JXMonthView();
      monthView.setActionCommand("MONTH_VIEW");
      monthView.addActionListener(this);
      monthView.setAntialiased(true);

      Icon icon = UIManager.getIcon("JXMonthView.monthUp.image");
      if (icon == null) {
        icon = IconPool.getShared("previous.gif");
      }
      previousButton = new JButton(icon);
      previousButton.setPreferredSize(new Dimension(20, 20));
      previousButton.setActionCommand("PREVIOUS_MONTH");
      previousButton.addActionListener(this);

      icon = UIManager.getIcon("JXMonthView.monthDown.image");
      if (icon == null) {
        icon = IconPool.getShared("next.gif");
      }
      nextButton = new JButton(icon);
      nextButton.setPreferredSize(new Dimension(20, 20));
      nextButton.setActionCommand("NEXT_MONTH");
      nextButton.addActionListener(this);

      icon = UIManager.getIcon("JXMonthView.monthCurrent.image");
      if (icon == null) {
        icon = IconPool.getShared("currentmonth.gif");
      }
      DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
      String label = formatter.format(new Date());
      todayButton = new JButton(label);
      todayButton.setFont(new Font(Font.DIALOG, Font.PLAIN, 11));
      todayButton.setPreferredSize(new Dimension(20, 20));
      todayButton.setActionCommand("TODAY");
      todayButton.addActionListener(this);

      setLayout(new BorderLayout());
      add(monthView, BorderLayout.CENTER);

      JPanel panel = new JPanel();
      panel.setLayout(new BorderLayout());
      panel.add(timeView, BorderLayout.NORTH);
      panel.add(previousButton, BorderLayout.WEST);
      panel.add(todayButton, BorderLayout.CENTER);
      panel.add(nextButton, BorderLayout.EAST);
      add(panel, BorderLayout.NORTH);
    }

    private void setTime(final Calendar time) {
      Date old = ((Date) dateField.getValue());
      Calendar cal = Calendar.getInstance();
      cal.setTime(old);
      cal.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
      cal.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
      cal.set(Calendar.SECOND, time.get(Calendar.SECOND));
      dateField.setValue(cal.getTime());
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      String command = ev.getActionCommand();
      if ("MONTH_VIEW".equals(command)) {
        DateSpan span = monthView.getSelectedDateSpan();
        dateField.setValue(span.getStartAsDate());
        setTime(timeView.getTime());

        popup.setVisible(false);
        fireActionPerformed();
      } else if ("MONTH_VIEW_CHANGED".equals(command)) {
        DateSpan span = monthView.getSelectedDateSpan();
        dateField.setValue(span.getStartAsDate());
        setTime(timeView.getTime());
      } else if ("PREVIOUS_MONTH".equals(command)) {
        monthView.setFirstDisplayedDate(DateUtils.getPreviousMonth(monthView.getFirstDisplayedDate()));
      } else if ("NEXT_MONTH".equals(command)) {
        monthView.setFirstDisplayedDate(DateUtils.getNextMonth(monthView.getFirstDisplayedDate()));
      } else if ("TODAY".equals(command)) {
        DateSpan span = new DateSpan(System.currentTimeMillis(), System.currentTimeMillis());
        monthView.ensureDateVisible(span.getStart());
      }
    }

    @Override
    public void onTimeChanged(final Calendar time) {
      setTime(time);
    }

  }
}
