package com.flamesgroup.antrax.control.swingwidgets.table;

public interface UpdatableTableFilter<T> {
  boolean isAccepted(T elem);
}
