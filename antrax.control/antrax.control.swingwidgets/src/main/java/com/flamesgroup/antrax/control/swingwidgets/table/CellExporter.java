package com.flamesgroup.antrax.control.swingwidgets.table;

public interface CellExporter {

  String getExportValue(Object value);

}
