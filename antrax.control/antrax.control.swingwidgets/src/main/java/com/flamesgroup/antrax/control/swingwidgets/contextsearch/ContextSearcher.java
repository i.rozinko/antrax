package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import java.util.Collection;

public interface ContextSearcher extends Searchable {

  void registerSearchItem(SearchItem item);

  void unregisterSearchItem(SearchItem item);

  Collection<SearchItem> getSearchItems();

  void setItemFilterEnabled(SearchItem item, boolean enabled);

  boolean isItemFilterEnabled(SearchItem item);

  void addContextSearcherListener(ContextSearcherListener l);

  void removeContextSearcherListener(ContextSearcherListener l);

  void registerSearchItem(SearchItem item, String name);

}
