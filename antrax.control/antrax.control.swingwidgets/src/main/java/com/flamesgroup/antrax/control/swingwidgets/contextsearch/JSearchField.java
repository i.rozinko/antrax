package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * JSearchField is a component like the Cocoa NSSearchField. If the component
 * has no focus and no text form a user input, it show the default text in
 * default color and default font. If it has a user text, a cancel icon is show.
 * By click on the cancel icon the user input is reset to the default settings.
 * You also can set a JPopupMenu that is show if the user clicks the search
 * icon.
 */
public class JSearchField extends JTextField implements Border, MouseListener, MouseMotionListener {

  private static final long serialVersionUID = 7721869265521477072L;

  private static final Icon defaultSearchIcon = IconPool.getShared("search_magglass.gif");
  private static final Icon defaultCancelIcon = IconPool.getShared("search_remove.png");
  private static final Icon defaultCancelIconPressed = IconPool.getShared("search_remove_pressed.png");

  //  private static final int DEFAULT_COLUMNS = 18;
  private static final int CORNERS_COUNT = 4;

  private static final int EDGE_WIDTH = 20;
  private static final int EDGE_HEIGHT = 20;
  //  private static final int EDGE_THICKENS = 2;

  private static final Color BOTTOM_LINE_COLOR = new Color(227, 227, 227);
  private static final Color TOP_LINE_1_COLOR = new Color(104, 104, 104);
  private static final Color TOP_LINE_2_COLOR = new Color(178, 178, 178);
  private static final Color TOP_LINE_3_COLOR = new Color(228, 228, 228);

  private static final Cursor TEXT_CURSOR = new Cursor(Cursor.TEXT_CURSOR);
  private static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);

  private final List<ActionListener> cancelActionListeners = new LinkedList<>();
  private final List<ActionListener> updateActionListeners = new LinkedList<>();
  private String emptyText;

  private Icon searchIcon;
  private Icon cancelIcon;
  private Icon searchIconPressed;
  private Icon cancelIconPressed;

  private final Rectangle searchIconBounds;
  private final Rectangle cancelIconBounds;
  //  private boolean isSearchIconPressed;
  private boolean isCancelIconPressed;

  private JPopupMenu popupMenu;
  private Arc2D.Double[] arc;
  private BasicStroke borderStroke;

  private int errorPosition = -1;

  public JSearchField() {
    this(defaultSearchIcon, defaultCancelIcon, null);
  }

  public JSearchField(final Icon searchIcon, final Icon cancelIcon) {
    this(searchIcon, cancelIcon, null);
  }

  public JSearchField(final JPopupMenu popupMenu) {
    this(defaultSearchIcon, defaultCancelIcon, popupMenu);
  }

  public JSearchField(final Icon searchIcon, final Icon cancelIcon, final JPopupMenu popupMenu) {
    super("");

    setToolTipText(getHelpMessage());

    this.popupMenu = popupMenu;
    this.searchIcon = searchIcon;
    this.cancelIcon = cancelIcon;
    this.searchIconPressed = defaultSearchIcon;
    this.cancelIconPressed = defaultCancelIconPressed;

    this.searchIconBounds = new Rectangle();
    this.cancelIconBounds = new Rectangle();
    //    this.isSearchIconPressed = false;
    this.isCancelIconPressed = false;

    intiUI();
    setupListeners();
  }

  private void setupListeners() {
    addMouseListener(this);
    addMouseMotionListener(this);
    registerKeyboardAction(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        setCancelValue();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), 0);

    getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void changedUpdate(final DocumentEvent e) {
        clearErrorPosition();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        clearErrorPosition();
        fireUpdate();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        clearErrorPosition();
        fireUpdate();
      }

    });
  }

  private void fireUpdate() {
    ActionEvent event = new ActionEvent(JSearchField.this, ActionEvent.ACTION_PERFORMED, "Search field updated");
    for (ActionListener l : cancelActionListeners) {
      l.actionPerformed(event);
    }
  }

  private void intiUI() {
    arc = new Arc2D.Double[CORNERS_COUNT];
    for (int i = 0; i < CORNERS_COUNT; i++) {
      arc[i] = new Arc2D.Double(0.0D, 0.0D, EDGE_WIDTH, EDGE_HEIGHT, 90D, 90D, Arc2D.OPEN);
    }
    arc[0].setArcType(Arc2D.OPEN);
    arc[0].x = 0.0D;
    arc[0].y = 0.0D;
    arc[0].width = 20D;
    arc[0].height = 20D;
    arc[0].start = 90D;
    arc[0].extent = 90D;

    arc[1].setArcType(Arc2D.OPEN);
    arc[1].x = 0.0D;
    //arc[1].y = height - 20;
    arc[1].width = 20D;
    arc[1].height = 20D;
    arc[1].start = 180D;
    arc[1].extent = 90D;

    arc[2].setArcType(Arc2D.OPEN);
    //arc[2].x = width - 20;
    arc[2].y = 0.0D;
    arc[2].width = 20D;
    arc[2].height = 20D;
    arc[2].start = 360D;
    arc[2].extent = 90D;

    arc[3].setArcType(Arc2D.OPEN);
    //arc[3].x = width - 20;
    //arc[3].y = height - 20;
    arc[3].width = 20D;
    arc[3].height = 20D;
    arc[3].start = 270D;
    arc[3].extent = 90D;

    borderStroke = new BasicStroke(2.0f);

    setOpaque(false);
    setBackground(Color.WHITE);
    setMargin(getBorderInsets(null));
    setBorder(this);
  }

  private void fillBackground(final Graphics2D g2) {
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    int height = getHeight();
    int width = getWidth();
    g2.setColor(Color.WHITE);
    arc[0].setArcType(Arc2D.PIE);
    arc[0].x = 0.0D;
    arc[0].y = 0.0D;
    g2.fill(arc[0]);

    arc[1].setArcType(Arc2D.PIE);
    arc[1].x = 0.0D;
    arc[1].y = height - 20;
    g2.fill(arc[1]);

    arc[2].setArcType(Arc2D.PIE);
    arc[2].x = width - 20;
    arc[2].y = 0.0D;
    g2.fill(arc[2]);

    arc[3].setArcType(Arc2D.PIE);
    arc[3].x = width - 20;
    arc[3].y = height - 20;
    g2.fill(arc[3]);

    g2.fillRect(10, 0, width - 2 * 10, height);
  }

  @Override
  public void paintComponent(final Graphics g) {
    fillBackground((Graphics2D) g);
    super.paintComponent(g);
    drawErrorLine((Graphics2D) g);

    String txt = emptyText;
    if (txt != null && !hasFocus() && getText().trim().length() <= 0) {
      g.setFont(getFont());
      g.setColor(Color.GRAY);
      g.drawString(txt, 20, getHeight() - g.getFontMetrics().getDescent() - 2);
    }
  }

  public void showErrorLine(final int startFormChar) {
    errorPosition = startFormChar;
    repaint();
  }

  private void clearErrorPosition() {
    errorPosition = -1;
  }

  private int getErrorPosition() {
    return errorPosition;
  }

  private boolean isHasError() {
    return getErrorPosition() >= 0;
  }

  private void drawErrorLine(final Graphics2D gc) {
    if (!isHasError()) {
      return;
    }
    gc.setColor(Color.RED);
    String text = getText();
    //gc.getFontMetrics().getLineMetrics("", gc).getBaselineIndex()

    int strWidth = gc.getFontMetrics().stringWidth(text);
    String validStr = text.substring(0, getErrorPosition());
    int validGap = gc.getFontMetrics().stringWidth(validStr);

    int xBegin = 23 + validGap;
    int xEnd = 23 + strWidth;
    int y = getHeight() - gc.getFontMetrics().getDescent() - 1;

    int yTop = y - 1;
    int yBottom = y + 1;
    int x0 = xBegin;
    while (x0 < xEnd) {
      int x1 = x0;
      int x2 = x1 + 2;
      int x3 = x2 + 2;
      gc.drawLine(x1, yBottom, x2, yTop);
      gc.drawLine(x2, yTop, x3, yBottom);

      x0 = x3;
    }

    //gc.drawLine(x1, y, x2, y);
  }

  public String getEmptyText() {
    return emptyText;
  }

  public void setEmptyText(final String newEmptyText) {
    emptyText = newEmptyText;
    repaint();
  }

  public JPopupMenu getPopupMenu() {
    return popupMenu;
  }

  public void setPopupMenu(final JPopupMenu value) {
    JPopupMenu oldValue = popupMenu;
    popupMenu = value;
    firePropertyChange("popupMenu", oldValue, value);
  }

  public Icon getSearchIcon() {
    return searchIcon;
  }

  public void setSearchIcon(final Icon value) {
    searchIcon = value;
  }

  public Icon getCancelIcon() {
    return cancelIcon;
  }

  public void setCancelIcon(final Icon value) {
    cancelIcon = value;
  }

  public Icon getSearchIconPressed() {
    return searchIconPressed;
  }

  public void setSearchIconPressed(final Icon value) {
    searchIconPressed = value;
  }

  public Icon getCancelIconPressed() {
    return cancelIconPressed;
  }

  public void setCancelIconPressed(final Icon value) {
    cancelIconPressed = value;
  }

  private void setCancelValue() {
    setText("");
    isCancelIconPressed = false;

    for (ActionListener l : cancelActionListeners) {
      ActionEvent event = new ActionEvent(JSearchField.this, ActionEvent.ACTION_PERFORMED, "Clear search");
      l.actionPerformed(event);
    }
  }

  public void addCancelListener(final ActionListener l) {
    cancelActionListeners.add(l);
  }

  public void removeCancelListener(final ActionListener l) {
    cancelActionListeners.remove(l);
  }

  public void addUpdateListener(final ActionListener l) {
    updateActionListeners.add(l);
  }

  public void removeUpdateListener(final ActionListener l) {
    updateActionListeners.remove(l);
  }

  @Override
  public Dimension getMinimumSize() {
    Dimension dim = super.getMinimumSize();
    dim.height = searchIcon.getIconHeight() + 4;
    return dim;
  }

  @Override
  public Dimension getPreferredSize() {
    Dimension dim = super.getPreferredSize();
    dim.height = searchIcon.getIconHeight() + 5;
    return dim;
  }

  @Override
  public boolean isBorderOpaque() {
    return false;
  }

  @Override
  public Insets getBorderInsets(final Component c) {
    return new Insets(4, searchIcon.getIconWidth() + 5, 4, cancelIcon.getIconWidth() + 4);
  }

  @Override
  public void paintBorder(final Component c, final Graphics g, final int x, final int y, final int width, final int height) {
    Graphics2D g2 = (Graphics2D) g;

    int edge_left_x = x + 10;
    int edge_right_x = (x + width) - 10;

    if (!isCancelIconPressed) {
      searchIconBounds.x = 3;
      searchIconBounds.y = y + (height - searchIcon.getIconHeight()) / 2 + 1;
      searchIconBounds.width = searchIcon.getIconWidth();
      searchIconBounds.height = searchIcon.getIconHeight();
      searchIcon.paintIcon(c, g, searchIconBounds.x, searchIconBounds.y);
    } else {
      searchIconBounds.x = 3;
      searchIconBounds.y = y + (height - searchIconPressed.getIconHeight()) / 2 + 1;
      searchIconBounds.width = searchIconPressed.getIconWidth();
      searchIconBounds.height = searchIconPressed.getIconHeight();
      searchIconPressed.paintIcon(c, g, searchIconBounds.x, searchIconBounds.y);
    }

    if (getText().length() > 0) {
      if (!isCancelIconPressed) {
        cancelIconBounds.x = ((x + width) - 5 - cancelIcon.getIconWidth()) + 2;
        cancelIconBounds.y = y + (height - cancelIcon.getIconHeight()) / 2 + 1;
        cancelIconBounds.width = cancelIcon.getIconWidth();
        cancelIconBounds.height = cancelIcon.getIconHeight();
        cancelIcon.paintIcon(c, g, cancelIconBounds.x, cancelIconBounds.y);
      } else {
        cancelIconBounds.x = ((x + width) - 5 - cancelIconPressed.getIconWidth()) + 2;
        cancelIconBounds.y = y + (height - cancelIconPressed.getIconHeight()) / 2 + 1;
        cancelIconBounds.width = cancelIconPressed.getIconWidth();
        cancelIconBounds.height = cancelIconPressed.getIconHeight();
        cancelIconPressed.paintIcon(c, g, cancelIconBounds.x, cancelIconBounds.y);
      }
    }

    g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

    g.setColor(TOP_LINE_1_COLOR);
    Shape s = new Line2D.Double(edge_left_x, y, edge_right_x + 1, y);
    g2.draw(s);

    g.setColor(TOP_LINE_2_COLOR);
    s = new Line2D.Double(edge_left_x - 1, y + 1, edge_right_x + 2, y + 1);
    g2.draw(s);

    g.setColor(TOP_LINE_3_COLOR);
    s = new Line2D.Double(edge_left_x - 3, y + 2, edge_right_x + 4, y + 2);
    g2.draw(s);

    g2.setColor(BOTTOM_LINE_COLOR);
    s = new Line2D.Double(edge_left_x - 2, (y + height) - 1, edge_right_x + 4, (y + height) - 1);
    g2.draw(s);

    g2.setStroke(borderStroke);
    Paint p = new GradientPaint(0f, 0f, TOP_LINE_1_COLOR, 0f, height, BOTTOM_LINE_COLOR);
    g2.setPaint(p);

    arc[0].setArcType(Arc2D.OPEN);
    g2.draw(arc[0]);

    if (y + 10 < (y + height) - 10) {
      s = new Line2D.Double(x, y + 10, x, (y + height) - 10);
      g2.draw(s);
    }

    arc[1].setArcType(Arc2D.OPEN);
    arc[1].y = height - 20;
    g2.draw(arc[1]);

    p = new GradientPaint(width - 20, 0f, TOP_LINE_1_COLOR, width - 20, height, BOTTOM_LINE_COLOR);
    g2.setPaint(p);

    arc[2].setArcType(Arc2D.OPEN);
    arc[2].x = width - 20;
    g2.draw(arc[2]);

    if (y + 10 < (y + height) - 10) {
      s = new Line2D.Double(x + width, y + 10, x + width, (y + height) - 10);
      g2.draw(s);
    }

    arc[3].setArcType(Arc2D.OPEN);
    arc[3].x = width - 20;
    arc[3].y = height - 20;
    g2.draw(arc[3]);
  }

  @Override
  public void mouseEntered(final MouseEvent e) {
  }

  @Override
  public void mouseExited(final MouseEvent e) {
  }

  @Override
  public void mouseClicked(final MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    if (searchIconBounds.contains(x, y)) {
      if (popupMenu != null) {
        if (popupMenu.getSubElements().length > 0) {
          Point p = e.getComponent().getLocation();
          Dimension dim = e.getComponent().getSize();

          popupMenu.show(e.getComponent(), p.x, p.y + dim.height - 5);
        }
      }
      //      isSearchIconPressed = false;
    }
    if (cancelIconBounds.contains(x, y)) {
      setCancelValue();
      repaint();
    }
  }

  @Override
  public void mousePressed(final MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    //    isSearchIconPressed = searchIconBounds.contains(x, y);
    isCancelIconPressed = cancelIconBounds.contains(x, y);
    if (isCancelIconPressed) {
      setSelectionStart(0);
      setSelectionEnd(getText().length());
    }
    repaint();
  }

  @Override
  public void mouseReleased(final MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    //    isSearchIconPressed = searchIconBounds.contains(x, y);
    isCancelIconPressed = cancelIconBounds.contains(x, y);
    repaint();
  }

  @Override
  public void mouseDragged(final MouseEvent e) {
  }

  @Override
  public void mouseMoved(final MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    if (searchIconBounds.contains(x, y) || cancelIconBounds.contains(x, y) && getText().length() > 0) {
      setCursor(DEFAULT_CURSOR);
    } else {
      setCursor(TEXT_CURSOR);
    }
  }

  private String getHelpMessage() {
    return "<html><body>" + //
        "Type filter here: <br>" + //
        "a b - filters by a and b<br>" + //
        "a | b - filters by a or b<br>" + //
        "a & b - filters by a and b<br>" + //
        "!a - filters by not a<br>" + //
        "!(a b) - filters by not (a and b)<br>" + //
        "\"a b c\" - filters by phrase a b c<br>" + //
        "You can combine expressions";
  }

}
