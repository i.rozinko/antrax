package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.GuiProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class JUpdatableTableProperties extends GuiProperties {

  private static final String COLUMNS_ORDER = "columns_order";
  private static final String ROW_SORTER = "row_sorter";
  private static final String COLUMNS_WIDTH = "columns_width";

  private final JMetalTable jMetalTable;

  public JUpdatableTableProperties(final JMetalTable jMetalTable) {
    super(jMetalTable.getName() + "_table");
    this.jMetalTable = jMetalTable;
  }

  private void saveColumnsOrder() {
    Enumeration<TableColumn> columns = jMetalTable.getColumnModel().getColumns();
    List<Integer> columnsOrder = new ArrayList<>();
    while (columns.hasMoreElements()) {
      columnsOrder.add(columns.nextElement().getModelIndex());
    }
    properties.setProperty(COLUMNS_ORDER, gson.toJson(columnsOrder));
  }

  private void saveRowSorter() {
    RowSorter<? extends TableModel> rowSorter = jMetalTable.getRowSorter();
    if (rowSorter == null) {
      return;
    }
    List<? extends RowSorter.SortKey> sortKeys = rowSorter.getSortKeys();
    properties.setProperty(ROW_SORTER, gson.toJson(sortKeys));
  }

  private void saveColumnWidth() {
    Enumeration<TableColumn> columns = jMetalTable.getColumnModel().getColumns();
    List<Integer> widthOfColumns = new ArrayList<>();
    while (columns.hasMoreElements()) {
      widthOfColumns.add(columns.nextElement().getPreferredWidth());
    }
    properties.setProperty(COLUMNS_WIDTH, gson.toJson(widthOfColumns));
  }

  private void loadColumnOrder() {
    String columnsOrderProperty = properties.getProperty(COLUMNS_ORDER);
    if (columnsOrderProperty != null) {
      Integer[] columnsOrder = gson.fromJson(columnsOrderProperty, Integer[].class);

      setColumnOrder(columnsOrder, jMetalTable.getColumnModel());
    }
  }

  private void loadRowSorter() {
    String rowSorterProperty = properties.getProperty(ROW_SORTER);
    RowSorter<? extends TableModel> rowSorter = jMetalTable.getRowSorter();
    if (rowSorterProperty != null && rowSorter != null) {
      rowSorter.setSortKeys(Arrays.asList(gson.fromJson(rowSorterProperty, RowSorter.SortKey[].class)));
    }
  }

  private void loadColumnWidth() {
    String widthOfColumnsProperty = properties.getProperty(COLUMNS_WIDTH);
    if (widthOfColumnsProperty != null) {
      Integer[] columnsWidth = gson.fromJson(widthOfColumnsProperty, Integer[].class);
      for (int i = 0; i < columnsWidth.length; i++) {
        jMetalTable.getColumnModel().getColumn(i).setPreferredWidth(columnsWidth[i]);
      }
    }
  }

  private void setColumnOrder(final Integer[] indices, final TableColumnModel columnModel) {
    TableColumn column[] = new TableColumn[indices.length];

    for (int i = 0; i < column.length; i++) {
      column[i] = columnModel.getColumn(indices[i]);
    }

    while (columnModel.getColumnCount() > 0) {
      columnModel.removeColumn(columnModel.getColumn(0));
    }

    for (TableColumn aColumn : column) {
      columnModel.addColumn(aColumn);
    }
  }

  @Override
  protected void restoreOutProperties() {
    loadColumnOrder();
    loadRowSorter();
    loadColumnWidth();
  }

  @Override
  protected void saveOutProperties() {
    saveColumnsOrder();
    saveRowSorter();
    saveColumnWidth();
  }

}
