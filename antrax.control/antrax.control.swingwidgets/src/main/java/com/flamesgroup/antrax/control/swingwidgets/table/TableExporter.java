package com.flamesgroup.antrax.control.swingwidgets.table;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.swing.*;
import javax.swing.table.TableColumnModel;

public class TableExporter {

  private static final String DELIMITER = ",";

  public static <T, K extends Comparable<K>> void writeCSVFile(final JUpdatableTable<T, K> table, final File csvFile) throws TableExportException {
    RowSorter<?> rowSorter = table.getRowSorter();
    UpdatableTableModel tm = (UpdatableTableModel) table.getModel();
    TableColumnModel columnModel = table.getColumnModel();
    int rowCount = rowSorter.getViewRowCount();
    int columnCount = tm.getColumnCount();
    try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile)))) {
      //write header information
      StringBuilder bufferHeader = new StringBuilder();
      for (int j = 0; j < columnCount; j++) {
        String columnName = tm.getColumnName(columnModel.getColumn(j).getModelIndex());
        bufferHeader.append("\"");
        bufferHeader.append(columnName == null ? "" : columnName);
        bufferHeader.append("\"");
        if (j + 1 != columnCount) {
          bufferHeader.append(DELIMITER);
        }
      }
      writer.write(bufferHeader.toString() + System.lineSeparator());

      //write row information
      for (int i = 0; i < rowCount; i++) {
        int rowIndex;
        try {
          rowIndex = rowSorter.convertRowIndexToModel(i);
        } catch (IndexOutOfBoundsException ignore) {
          continue; // may be we already out of sync
        }
        StringBuilder buffer = new StringBuilder();
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
          buffer.append("\"");
          buffer.append(tm.getExportValueAt(rowIndex, columnModel.getColumn(columnIndex).getModelIndex()));
          buffer.append("\"");
          if (columnIndex + 1 != columnCount) {
            buffer.append(DELIMITER);
          }
        }
        writer.write(buffer.toString() + System.lineSeparator());
      }
    } catch (IOException e) {
      throw new TableExportException("Error while export to CSV file: '" + csvFile.getName() + "'.", e);
    }
  }

}
