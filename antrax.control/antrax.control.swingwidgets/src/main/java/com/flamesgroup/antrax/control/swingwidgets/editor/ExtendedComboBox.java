package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ExtendedComboBox extends JComboBox {

  private static final long serialVersionUID = 3096619401129790923L;

  private boolean synteticChange = false;

  @Override
  public void addActionListener(final ActionListener l) {
    super.addActionListener(wrap(l));
  }

  public void setSynteticSelectedItem(final Object anObject) {
    this.synteticChange = true;
    try {
      super.setSelectedItem(anObject);
    } finally {
      this.synteticChange = false;
    }
  }

  public void setSynteticSelectedIndex(final int index) {
    this.synteticChange = true;
    try {
      super.setSelectedIndex(index);
    } finally {
      this.synteticChange = false;
    }
  }

  @Override
  public void removeAllItems() {
    this.synteticChange = true;
    try {
      super.removeAllItems();
    } finally {
      this.synteticChange = false;
    }
  }

  @Override
  public void addItem(final Object anObject) {
    this.synteticChange = true;
    try {
      super.addItem(anObject);
    } finally {
      this.synteticChange = false;
    }
  }

  @Override
  public void removeItem(final Object anObject) {
    this.synteticChange = true;
    try {
      super.removeItem(anObject);
    } finally {
      this.synteticChange = false;
    }
  }

  @Override
  public void removeItemAt(final int anIndex) {
    this.synteticChange = true;
    try {
      super.removeItemAt(anIndex);
    } finally {
      this.synteticChange = false;
    }
  }

  private ActionListener wrap(final ActionListener l) {
    return new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        if (!synteticChange) {
          l.actionPerformed(e);
        }
      }
    };
  }

  public void setData(final Object[] data, final boolean addNull) {
    Object selected = getSelectedItem();
    removeAllItems();
    if (addNull) {
      addItem(null);
    }
    if (data == null) {
      return;
    }
    for (Object o : data) {
      addItem(o);
    }
    setSynteticSelectedItem(selected);
  }

}
