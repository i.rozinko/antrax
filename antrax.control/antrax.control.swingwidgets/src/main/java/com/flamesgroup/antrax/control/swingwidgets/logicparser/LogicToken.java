package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public enum LogicToken {

  AND(LogicLexeme.AND, true),
  OR(LogicLexeme.OR, true),
  EQUAL(LogicLexeme.EQ, true),
  RIGHT_BRACE(LogicLexeme.RIGHT_BRACE, true),
  LEFT_BRACE(LogicLexeme.LEFT_BRACE, true),

  NOT(LogicLexeme.NOT, true),
  NOT_EQ(LogicLexeme.NOT, LogicLexeme.EQ, true),

  GT(LogicLexeme.GT, true),
  GE(LogicLexeme.GT, LogicLexeme.EQ, true),

  LT(LogicLexeme.LT, true),
  LE(LogicLexeme.LT, LogicLexeme.EQ, true),

  STRING("", false, false),
  END("", false, false),;

  private final String lexeme;
  private final boolean operator;
  private final boolean combined;

  LogicToken(final char lexeme, final boolean operator) {
    this(String.valueOf(lexeme), operator, false);
  }

  LogicToken(final char lexeme1, final char lexeme2, final boolean operator) {
    this(String.valueOf(new char[] {lexeme1, lexeme2}), operator, true);
  }

  LogicToken(final String lexeme, final boolean operator, final boolean combined) {
    this.lexeme = lexeme;
    this.operator = operator;
    this.combined = combined;
  }

  public String getLexeme() {
    return lexeme;
  }

  public boolean isOperator() {
    return operator;
  }

  public boolean isCombined() {
    return combined;
  }

  public static LogicToken parseChar(final char symbol) {
    switch (symbol) {
      case LogicLexeme.AND:
        return LogicToken.AND;

      case LogicLexeme.OR:
        return LogicToken.OR;

      case LogicLexeme.NOT:
        return LogicToken.NOT;

      case LogicLexeme.LEFT_BRACE:
        return LogicToken.LEFT_BRACE;

      case LogicLexeme.RIGHT_BRACE:
        return LogicToken.RIGHT_BRACE;

      case LogicLexeme.EQ:
        return LogicToken.EQUAL;

      case LogicLexeme.GT:
        return LogicToken.GT;

      case LogicLexeme.LT:
        return LogicToken.LT;

      default:
        return null;
    }
  }

}
