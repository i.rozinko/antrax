package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ContextSearcherImpl implements ContextSearcher {

  private final SearchFilter acceptedSearchFilter = new AcceptedSearchFilter();
  private final List<SearchItem> items = new LinkedList<>();
  private final List<ContextSearcherListener> listeners = new LinkedList<>();
  private final Map<SearchItem, Boolean> enableMap = new HashMap<>();

  @Override
  public void addContextSearcherListener(final ContextSearcherListener l) {
    listeners.add(l);
  }

  @Override
  public void removeContextSearcherListener(final ContextSearcherListener l) {
    listeners.remove(l);
  }

  private void fireItemRegistrationChanged(final SearchItem item, final boolean isRegistered) {
    for (ContextSearcherListener l : listeners) {
      l.itemRegistrationChanged(item, isRegistered);
    }
  }

  @Override
  public void registerSearchItem(final SearchItem item, final String name) {
    item.setSearchItemName(name);
    items.add(item);
    setItemFilterEnabled(item, true);
    fireItemRegistrationChanged(item, true);
  }

  @Override
  public void registerSearchItem(final SearchItem item) {
    items.add(item);
    setItemFilterEnabled(item, true);
    fireItemRegistrationChanged(item, true);
  }

  @Override
  public void unregisterSearchItem(final SearchItem item) {
    items.remove(item);
    enableMap.remove(item);
    fireItemRegistrationChanged(item, false);
  }

  @Override
  public Collection<SearchItem> getSearchItems() {
    List<SearchItem> it = new LinkedList<>(items);
    return Collections.unmodifiableList(it);
  }

  @Override
  public void applySearchFilter(final SearchFilter searchFilter) {
    for (SearchItem item : items) {
      final SearchFilter filter = (isItemFilterEnabled(item)) ? searchFilter : acceptedSearchFilter;
      item.applySearchFilter(filter);
    }
  }

  @Override
  public boolean isItemFilterEnabled(final SearchItem item) {
    Boolean enabled = enableMap.get(item);
    return (enabled == null) ? false : enabled;
  }

  @Override
  public void setItemFilterEnabled(final SearchItem item, final boolean enabled) {
    enableMap.put(item, enabled);
  }

}
