package com.flamesgroup.antrax.control.swingwidgets.table;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JUpdatableTableTest {

  @Test
  public void testInsertElements() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.selectRow(table.insertElem("a"));
    String selected = table.getSelectedElem();

    table.selectRow(table.insertElem("b"));
    selected = table.getSelectedElem();

    table.selectRow(table.insertElem("c"));
    selected = table.getSelectedElem();

    table.selectRow(table.insertElem("d"));
    selected = table.getSelectedElem();
  }

  @Test
  public void testSetDataDoesNotLoseSelection() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("b");
    table.getSelectionModel().setSelectionInterval(0, 0);

    assertEquals(0, table.getSelectedRow());

    table.setData("a", "b", "c");

    assertEquals(1, table.getSelectedRow());

    table.getSelectionModel().setSelectionInterval(2, 2);
    table.setData("b", "c");

    assertEquals(1, table.getSelectedRow());
  }

  @Test
  public void testSelectionLooseOnRemovedElem() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("a");
    table.getSelectionModel().setSelectionInterval(0, 0);
    table.setData("b");
    assertEquals(0, table.getSelectedRows().length);
  }

  @Test
  public void testSelectionMovedForward() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("b");
    table.getSelectionModel().setSelectionInterval(0, 0);
    table.setData("a", "b");
    assertEquals(1, table.getSelectedRows().length);
    assertEquals(1, table.getSelectedRows()[0]);
  }

  @Test
  public void testSelectionMovedBackward() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("a", "b");
    table.getSelectionModel().setSelectionInterval(1, 1);
    table.setData("b");
    assertEquals(1, table.getSelectedRows().length);
    assertEquals(0, table.getSelectedRows()[0]);
  }

  @Test
  public void testSelectionRemovedFromRemovedElem() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("a", "b", "c");
    table.getSelectionModel().setSelectionInterval(0, 2);
    table.setData("a", "c", "e");
    int[] rowsSel = table.getSelectedRows();
    assertEquals(2, rowsSel.length);
    assertEquals(0, rowsSel[0]);
    assertEquals(1, rowsSel[1]);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testSelectionListenerWillNotFireOnDataRefresh() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a"));

    table.setData("a");
    table.getSelectionModel().addSelectionInterval(0, 0);

    table.addElementSelectionListener((currSelection, prevSelection) -> {
      throw new AssertionError();
    });
    table.setData("a");
  }

  @Test
  public void testSortingDoesNotBreaksSelection() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a", "b"));

    table.setData("a:d", "b:c", "c:a", "d:b");
    table.getSelectionModel().addSelectionInterval(1, 1); // b:c
    table.getRowSorter().toggleSortOrder(1);
    // c:a, d:b, b:c, a:d
    assertEquals(2, table.getSelectedRow());
    table.setData("a:d", "b:c", "c:a", "d:b");
    assertEquals(2, table.getSelectedRow());

  }

  @Test
  public void testUpdateElementAtRemembersSelection() {
    JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a", "b"));

    table.setData("a:d", "b:c", "c:a", "d:b");
    table.getSelectionModel().addSelectionInterval(1, 1);
    table.updateElemAt("z:c", 1);
    assertEquals(1, table.getSelectedRows().length);
    assertEquals("z:c", table.getElemAt(table.getSelectedRows()[0]));
  }

  @Test
  public void testProductivityOfSetData() {
    final JUpdatableTable<String, String> table = new JUpdatableTable<>(new UpdatableTableModelTest.StringWithColonTableBuilder("a", "b", "c", "d"));
    final String[] data1 = new String[1000];
    initializeData(data1, 0);
    assertExecutionTime(200, new Runnable() {
      @Override
      public void run() {
        table.clearData();
        table.setData(data1);
      }
    });
    final String[] data2 = new String[1000];
    initializeData(data2, 1);
    assertExecutionTime(500, new Runnable() {
      @Override
      public void run() {
        table.setData(data1);
        table.setData(data2);
      }
    });
  }

  private void assertExecutionTime(final long maximum, final Runnable runnable) {
    int iterationCount = 5;
    long timeout = 0;
    for (int i = 0; i < iterationCount; ++i) {
      long start = System.currentTimeMillis();
      runnable.run();
      long passedTime = System.currentTimeMillis() - start;
      timeout += passedTime;
    }
    long avgTimeout = timeout / iterationCount;
    assertTimeout(avgTimeout, maximum);
  }

  private void assertTimeout(final long timeout, final long expected) {
    assertTrue("Expected timeout " + expected + " is greater then " + timeout, timeout <= expected);
  }

  private void initializeData(final String[] data, final int prefix) {
    for (int i = 0; i < data.length; ++i) {
      data[i] = prefix + "_" + i + ":test:test:test";
    }
  }
}
