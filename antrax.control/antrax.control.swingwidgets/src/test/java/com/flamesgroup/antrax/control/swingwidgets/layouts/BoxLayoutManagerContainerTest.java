package com.flamesgroup.antrax.control.swingwidgets.layouts;

import java.awt.*;

import javax.swing.*;

public class BoxLayoutManagerContainerTest {

  public static class BoxPanel extends JPanel {

    private static final long serialVersionUID = 2156562821882183227L;

    @Override
    public void paintChildren(final Graphics g) {
      super.paintChildren(g);
      Dimension size = getSize();
      LayoutManager manager = getLayout();
      if ((manager != null) && (manager instanceof BoxLayout)) {
        BoxLayout layout = (BoxLayout) manager;
        boolean vertical = true;
        if (vertical) {
          int axis = (int) (layout.getLayoutAlignmentX(this) * size.width);
          g.fillRect(axis - 1, 0, 3, size.height);
        } else {
          int axis = (int) (layout.getLayoutAlignmentY(this) * size.height);
          g.fillRect(0, axis - 1, size.width, 3);
        }
      }
    }

  }

  public static void main(final String[] args) {
    JFrame f = new JFrame("Vertical BoxLayout-managed container");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Container pane = new BoxPanel();
    f.setContentPane(pane);
    pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
    for (float align = 0.0f; align <= 1.0f; align += 0.25f) {
      JTextField tf = new JTextField("X Alignment = " + align, 10);
      tf.setAlignmentX(align);
      pane.add(tf);
    }
    f.setSize(400, 300);
    f.setVisible(true);
  }

}

