package com.flamesgroup.antrax.control.swingwidgets.list;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheckBoxListModelTest {

  private CheckBoxListModel model;

  @Before
  public void initialize() {
    model = new DefaultCheckBoxListModel();
    String[] items = {
        "item0",
        "item1",
        "item2",
    };
    for (String item : items) {
      model.addItem(item);
    }
  }

  @Test
  public void testInitialization() {
    assertEquals("item0", model.getElementAt(0));
    assertEquals("item1", model.getElementAt(1));
    assertEquals("item2", model.getElementAt(2));

    assertEquals(false, model.isChecked("item0"));
    assertEquals(false, model.isChecked("item1"));
    assertEquals(false, model.isChecked("item2"));
  }

  @Test
  public void testInsertItemMiddle() {
    model.insertItemAt("item01", 2);
    assertEquals("item0", model.getElementAt(0));
    assertEquals("item1", model.getElementAt(1));
    assertEquals("item01", model.getElementAt(2));
    assertEquals("item2", model.getElementAt(3));
  }

  @Test
  public void testInsertItemBegin() {
    model.insertItemAt("item01", 0);
    assertEquals("item01", model.getElementAt(0));
    assertEquals("item0", model.getElementAt(1));
    assertEquals("item1", model.getElementAt(2));
    assertEquals("item2", model.getElementAt(3));
  }

  @Test
  public void testInsertItemEnd() {
    model.insertItemAt("item01", 3);
    assertEquals("item0", model.getElementAt(0));
    assertEquals("item1", model.getElementAt(1));
    assertEquals("item2", model.getElementAt(2));
    assertEquals("item01", model.getElementAt(3));
  }

  @Test
  public void testSetChecking() {
    model.setChecked("item0", true);
    model.setChecked("item1", false);
    model.setChecked("item2", true);
    assertEquals(true, model.isChecked("item0"));
    assertEquals(false, model.isChecked("item1"));
    assertEquals(true, model.isChecked("item2"));

    model.setChecked("item0", false);
    model.setChecked("item1", true);
    model.setChecked("item2", true);
    assertEquals(false, model.isChecked("item0"));
    assertEquals(true, model.isChecked("item1"));
    assertEquals(true, model.isChecked("item2"));

    model.setChecked("item0", false);
    model.setChecked("item1", true);
    model.setChecked("item2", false);
    assertEquals(false, model.isChecked("item0"));
    assertEquals(true, model.isChecked("item1"));
    assertEquals(false, model.isChecked("item2"));
  }

  @Test
  public void testInvertChecking() {
    model.setChecked("item0", true);
    model.setChecked("item1", false);
    model.setChecked("item2", true);
    assertEquals(true, model.isChecked("item0"));
    assertEquals(false, model.isChecked("item1"));
    assertEquals(true, model.isChecked("item2"));

    model.invertChecking("item0");
    model.invertChecking("item1");
    model.invertChecking("item2");
    assertEquals(false, model.isChecked("item0"));
    assertEquals(true, model.isChecked("item1"));
    assertEquals(false, model.isChecked("item2"));
  }

  @Test
  public void testCheckingAfterInsertion() {
    assertEquals(false, model.isChecked("item0"));
    assertEquals(false, model.isChecked("item1"));
    assertEquals(false, model.isChecked("item2"));

    model.setChecked("item2", true);

    model.insertItemAt("item01", 2);
    assertEquals(false, model.isChecked("item0"));
    assertEquals(false, model.isChecked("item1"));
    assertEquals(false, model.isChecked("item01"));
    assertEquals(true, model.isChecked("item2"));
  }
}
