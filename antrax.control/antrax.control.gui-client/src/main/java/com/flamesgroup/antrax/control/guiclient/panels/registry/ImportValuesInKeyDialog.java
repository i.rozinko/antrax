/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class ImportValuesInKeyDialog extends JDialog {

  private static final long serialVersionUID = 7688262264940463756L;
  private final JTextField txtKey = new JTextField();
  private final JTextArea txtValues = new JTextArea();
  private final JButton btnBrowse = new JButton("Browse");
  private final JButton btnCancel = new JButton("Cancel");
  private final JButton btnImport = new JButton("Import");
  private boolean applied;
  private String[] values;

  public ImportValuesInKeyDialog(final Window parent) {
    super(parent);
    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    JPanel contentPanel = new JPanel();
    contentPanel.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
    setContentPane(contentPanel);
    createGUI();

    ActionListener controlListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        applied = e.getSource() == btnImport;
        if (applied) {
          if (requiresNormalize()) {
            JOptionPane.showMessageDialog(ImportValuesInKeyDialog.this, "Text contains empty lines. They will be ignored");
            values = normalizeValues();
          } else {
            values = txtValues.getText().split("\r\n|\n|\n\r|\r");
          }
        }
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }
      }
    };
    btnImport.addActionListener(controlListener);
    btnCancel.addActionListener(controlListener);
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        try {
          if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(ImportValuesInKeyDialog.this)) {
            File file = fileChooser.getSelectedFile();
            FileReader fileReader = new FileReader(file);
            readFromFile(fileReader);
            fileReader.close();
          }
        } catch (Exception ignored) {
          JOptionPane.showMessageDialog(ImportValuesInKeyDialog.this, "Failed to load file");
        }
      }
    });
    setPreferredSize(new Dimension(300, 450));
    pack();
  }

  private void readFromFile(final FileReader fileReader) throws IOException {
    BufferedReader reader = new BufferedReader(fileReader);
    StringBuilder builder = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      builder.append(line);
      builder.append('\n');
    }
    txtValues.setText(builder.toString());
    reader.close();
  }

  private boolean requiresNormalize() {
    String text = txtValues.getText();
    int length = text.length();
    int lineWidth = 0;
    for (int i = 0; i < length; ++i) {
      char c = text.charAt(i);
      if (c == '\n') {
        if (lineWidth == 0) {
          return true;
        }
        lineWidth = 0;
      } else if (!Character.isSpaceChar(c)) {
        lineWidth++;
      }
    }
    return false;
  }

  private String[] normalizeValues() {
    List<String> retval = new LinkedList<>();
    String text = txtValues.getText();
    int length = text.length();
    StringBuilder str = new StringBuilder();
    for (int i = 0; i < length; ++i) {
      char c = text.charAt(i);
      if (c == '\n') {
        if (str.length() > 0) {
          retval.add(str.toString());
          str = new StringBuilder();
        }
      } else {
        str.append(c);
      }
    }
    return retval.toArray(new String[retval.size()]);
  }

  private void createGUI() {
    JLabel lblKey = new JLabel("Key");
    lblKey.setLabelFor(txtKey);
    JScrollPane scrollPane = new JScrollPane(txtValues);
    getContentPane().setLayout(new BorderLayout());
    JPanel top = new JPanel(new BorderLayout());
    top.add(lblKey, BorderLayout.LINE_START);
    top.add(txtKey, BorderLayout.CENTER);
    JPanel center = new JPanel();
    center.setLayout(new BorderLayout());
    center.add(scrollPane, BorderLayout.CENTER);
    center.setBorder(BorderFactory.createTitledBorder("Values"));
    JPanel bottom = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    bottom.add(btnBrowse);
    bottom.add(btnCancel);
    bottom.add(btnImport);

    getContentPane().add(top, BorderLayout.NORTH);
    getContentPane().add(center, BorderLayout.CENTER);
    getContentPane().add(bottom, BorderLayout.SOUTH);
  }

  public boolean isApplied() {
    return applied;
  }

  public String[] getValues() {
    return values;
  }

  public String getKey() {
    return txtKey.getText();
  }

  public static void main(final String[] args) throws InterruptedException, InvocationTargetException {
    final ImportValuesInKeyDialog dlg = new ImportValuesInKeyDialog(null);
    dlg.setModalityType(ModalityType.APPLICATION_MODAL);
    dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dlg.pack();
    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        dlg.setVisible(true);
        System.out.println(Arrays.toString(dlg.getValues()));
      }
    });
  }

}
