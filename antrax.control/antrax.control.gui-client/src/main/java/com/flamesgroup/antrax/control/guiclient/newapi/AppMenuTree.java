/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.ColorUtils;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeSelectionModel;

public class AppMenuTree {
  private final JTree tree = new JTree();

  private final AppMenuNode menuNode = new AppMenuNode(tree);
  final List<MenuSelectionListener> listeners = new LinkedList<>();
  private final List<PanelRemovedListener> panelRemoveListeners = new LinkedList<>();

  public AppMenuTree() {
    ToolTipManager.sharedInstance().registerComponent(tree);
    setupUIDefaults(tree);
    tree.setModel(new AppNodesModel(menuNode));
    tree.setRootVisible(false);
    tree.setDragEnabled(true);
    tree.setTransferHandler(new AppMenuTreeTransferHandler());
    tree.setCellRenderer(new AppMenuTreeCellRenderer());
    tree.setShowsRootHandles(true);
    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.getModel().addTreeModelListener(new TreeModelListener() {

      @Override
      public void treeNodesRemoved(final TreeModelEvent e) {
        AppMenuNode node = (AppMenuNode) e.getTreePath().getLastPathComponent();
        firePanelRemoved(node);
      }

      private void firePanelRemoved(final AppMenuNode node) {
        if (node.getPanel() != null) {
          for (PanelRemovedListener l : panelRemoveListeners) {
            l.handlePanelRemoved(node.getPanel());
          }
        }

        for (int i = 0; i < node.getChildCount(); ++i) {
          firePanelRemoved(node.getChild(i));
        }
      }

      @Override
      public void treeNodesChanged(final TreeModelEvent e) {
      }

      @Override
      public void treeNodesInserted(final TreeModelEvent e) {
      }

      @Override
      public void treeStructureChanged(final TreeModelEvent e) {
      }
    });
    tree.addTreeSelectionListener(new BlockEmptyNodesSelectionListener(listeners));
  }

  private void setupUIDefaults(final JTree tree) {
    tree.setBackground(ColorUtils.darker(tree.getBackground()));
  }

  public Component getComponent() {
    return tree;
  }

  public AppMenuNode getMenuNode() {
    return menuNode;
  }

  public void addMenuSelectedListener(final MenuSelectionListener listener) {
    this.listeners.add(listener);
  }

  public void addPanelRemovedListener(final PanelRemovedListener panelRemovedListener) {
    this.panelRemoveListeners.add(panelRemovedListener);
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    AppMenuTree tree = new AppMenuTree();
    AppMenuNode voiceServers = new AppMenuNode("Voice Servers", null, new Badge(null, null, "1"), null);
    Badge vsBadge = new Badge("1", "2", null);
    vsBadge.setInfoHelp("Calls count");
    vsBadge.setNoticeHelp("Unused BEKKI Channels");
    vsBadge.setErrorHelp("Test");
    voiceServers.addNode(new AppMenuNode("VS-0", IconPool.shared().get("/img/report/sim_history.png"), vsBadge, createPanelProvider()));
    voiceServers.addNode(new AppMenuNode("VS-1", IconPool.shared().get("/img/report/sim_history.png"), new Badge(), createPanelProvider()));

    tree.getMenuNode().addNode(voiceServers);
    frame.getContentPane().add(new JScrollPane(tree.getComponent()));
    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

  private static AppPanelProvider createPanelProvider() {
    return new AppPanelProvider() {
      @Override
      public boolean checkPermissions(final BeansPool pool) {
        return false;
      }

      @Override
      public AppPanel createPanel() {
        return new AppPanel() {

          @Override
          public void setEditable(final boolean editable) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Not implemented yet");
          }

          @Override
          public void setActive(final boolean active) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Not implemented yet");
          }

          @Override
          public void release() {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Not implemented yet");
          }

          @Override
          public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Not implemented yet");
          }

          @Override
          public JComponent getComponent() {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Not implemented yet");
          }
        };
      }
    };
  }

  public void release() {
    getMenuNode().release();
  }

}
