/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanelProvider;
import com.flamesgroup.antrax.control.guiclient.panels.GsmViewPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;

import javax.swing.*;

public class GsmViewPanelProvider implements AppPanelProvider, AppPanel {

  private final GsmViewPanel gsmViewPanel;

  public GsmViewPanelProvider(final String serverName) {
    this.gsmViewPanel = new GsmViewPanel(serverName);
  }

  public GsmViewPanelProvider() {
    this.gsmViewPanel = new GsmViewPanel(null);
  }

  @Override
  public boolean checkPermissions(final BeansPool pool) throws NotPermittedException {
    return pool.getControlBean().checkPermission(MainApp.clientUID, "listVoiceServerChannels") && pool.getControlBean().checkPermission(MainApp.clientUID, "listVoiceServers");
  }

  @Override
  public AppPanel createPanel() {
    return this;
  }

  @Override
  public JComponent getComponent() {
    return gsmViewPanel;
  }

  @Override
  public void release() {
    gsmViewPanel.release();
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    gsmViewPanel.postInitialize(refresher, transactionManager);
  }

  @Override
  public void setEditable(final boolean editable) {
    gsmViewPanel.setEditable(editable);
  }

  @Override
  public void setActive(final boolean active) {
    gsmViewPanel.setActive(active);
  }

}
