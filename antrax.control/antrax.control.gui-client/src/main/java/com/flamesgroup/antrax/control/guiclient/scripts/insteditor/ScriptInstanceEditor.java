/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.NotFoundScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/**
 * Panel with comboBox for selecting script, table for script params and
 * mechanism for adding arrays of params
 *
 * @see #initialize(AntraxPluginsStore,
 * PropertyEditorsSource, ScriptDefinition[])
 * @see #setScriptInstance(ScriptInstance, ScriptCommonsSource)
 */
public class ScriptInstanceEditor extends JPanel implements SharedReferenceContainer {

  private static final long serialVersionUID = 9177113131712759955L;
  private final JComboBox scriptsComboBox = new JComboBox();
  private final ScriptParamsTable scriptParamsTable;
  private final ScriptType scriptType;
  private AntraxPluginsStore pluginsStore;
  private ScriptCommonsSource scriptCommonsSource;
  private final List<ScriptInstanceChangeListener> listeners = new LinkedList<>();
  private ScriptDefinition[] definitions = new ScriptDefinition[0];

  private class MultipleKeySelectionManager implements JComboBox.KeySelectionManager {
    long lastKeyTime = 0;
    String pattern = "";

    @Override
    public int selectionForKey(final char aKey, final ComboBoxModel model) {
      //Find index of selected item
      int selIx = 1;
      Object sel = model.getSelectedItem();
      if (sel != null) {
        for (int i = 0; i < model.getSize(); i++) {
          if (sel.equals(model.getElementAt(i))) {
            selIx = i;
            break;
          }
        }
      }

      //Get the current time
      long curTime = System.currentTimeMillis();

      // If last key was typed less than 500 ms ago, append to current pattern
      if (curTime - lastKeyTime < 500) {
        pattern += ("" + aKey).toLowerCase();
      } else {
        pattern = ("" + aKey).toLowerCase();
      }

      // Save current time
      lastKeyTime = curTime;

      // Search forward from current selection
      for (int i = selIx + 1; i < model.getSize(); i++) {
        String s = model.getElementAt(i).toString().toLowerCase();
        if (s.startsWith(pattern)) {
          return i;
        }
      }

      //Search from top to current selection
      for (int i = 0; i < selIx; i++) {
        if (model.getElementAt(i) != null) {
          String s = model.getElementAt(i).toString().toLowerCase();
          if (s.startsWith(pattern)) {
            return i;
          }
        }
      }
      return -1;
    }
  }

  public ScriptInstanceEditor(final ScriptType scriptType) {
    assert scriptType != null;
    this.scriptType = scriptType;

    if (ScriptType.ACTIVITY_PERIOD == scriptType) {
      scriptParamsTable = new PeriodScriptParamsTable();
    } else {
      scriptParamsTable = new ScriptParamsTable();
    }

    setLayout(new BorderLayout());
    scriptsComboBox.setRenderer(new ScriptInstanceWrapperComboBoxRenderer());
    scriptsComboBox.setKeySelectionManager(new MultipleKeySelectionManager());
    add(scriptsComboBox, BorderLayout.NORTH);
    add(scriptParamsTable, BorderLayout.CENTER);
  }

  public ScriptInstanceEditor(final String title, final ScriptType scriptType) {
    this(scriptType);
    this.setBorder(BorderFactory.createTitledBorder(title));
    setName(title);
  }

  private void updateComboBoxData() {
    ScriptInstance oldScriptInstance = scriptParamsTable.getScriptInstance();
    scriptParamsTable.setScriptInstance(getScriptInstance(), scriptCommonsSource);
    ScriptInstance newScriptInstance = scriptParamsTable.getScriptInstance();

    fireScriptChanged(oldScriptInstance, newScriptInstance);
    if (getScriptInstance() == null) {
      scriptsComboBox.setToolTipText("");
    } else {
      scriptsComboBox.setToolTipText(getScriptInstance().getDefinition().getDescription());
    }
  }

  public void initialize(final AntraxPluginsStore pluginsStore, final PropertyEditorsSource propEditors, final ScriptDefinition[] allDefinitions) {
    assert pluginsStore != null;
    assert propEditors != null;
    assert allDefinitions != null;
    this.pluginsStore = pluginsStore;
    this.definitions = allDefinitions;
    scriptParamsTable.initialize(pluginsStore, propEditors);

    scriptsComboBox.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        JComboBox source = (JComboBox) e.getSource();
        if (!source.isPopupVisible()) {
          updateComboBoxData();
        }
      }
    });

    scriptsComboBox.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuCanceled(final PopupMenuEvent arg0) {
      }

      @Override
      public void popupMenuWillBecomeInvisible(final PopupMenuEvent arg0) {
        updateComboBoxData();
      }

      @Override
      public void popupMenuWillBecomeVisible(final PopupMenuEvent arg0) {
      }
    });
  }

  private void fireScriptChanged(final ScriptInstance oldScriptInstance, final ScriptInstance newScriptInstance) {
    for (ScriptInstanceChangeListener l : listeners) {
      l.handleInstanceChanged(newScriptInstance, oldScriptInstance);
    }
  }

  public void setScriptInstance(final ScriptInstance scriptInstance, final ScriptCommonsSource scriptCommonsSource) {
    this.scriptCommonsSource = scriptCommonsSource;
    fillComboBox(pluginsStore, definitions, scriptInstance);
  }

  public ScriptInstance getScriptInstance() {
    ScriptInstanceWrapper wrapper = (ScriptInstanceWrapper) scriptsComboBox.getSelectedItem();
    if (wrapper == null) {
      return null;
    }
    return wrapper.getInstance();
  }

  @Override
  public SharedReference[] listSharedReferences() {
    ScriptInstance inst = getScriptInstance();
    if (inst == null) {
      return new SharedReference[0];
    }
    List<SharedReference> retval = new LinkedList<>();
    for (ScriptParameter param : inst.getParameters()) {
      Object value = param.getValue(pluginsStore.getClassLoader());
      if (value instanceof SharedReference) {
        retval.add((SharedReference) value);
      }
    }
    return retval.toArray(new SharedReference[retval.size()]);
  }

  @Override
  public void handleScriptCommonsRemoved(final ScriptCommons ref) {
    ComboBoxModel model = scriptsComboBox.getModel();
    int size = model.getSize();
    for (int i = 0; i < size; ++i) {
      ScriptInstanceWrapper w = (ScriptInstanceWrapper) model.getElementAt(i);
      if (w == null) {
        continue;
      }
      ScriptInstance inst = w.getInstance();
      for (ScriptParameter param : inst.getParameters()) {
        Object value = param.getValue(pluginsStore.getClassLoader());
        if (value instanceof SharedReference) {
          SharedReference curr = (SharedReference) value;
          if (curr.getName().equals(ref.getName())) {
            param.setValue(ref.getValue(pluginsStore.getClassLoader()), pluginsStore.getClassLoader());
          }
        }
      }
    }
  }

  public void addScriptInstanceChangeListener(final ScriptInstanceChangeListener listener) {
    listeners.add(listener);
    scriptParamsTable.addScriptChangeListener(listener);
  }

  public void removeScriptInstanceChangeListener(final ScriptInstanceChangeListener listener) {
    listeners.remove(listener);
    scriptParamsTable.removeScriptChangeListener(listener);
  }

  public void setEditable(final boolean value) {
    scriptsComboBox.setEnabled(value);
    scriptParamsTable.setEditable(value);
  }

  private void fillComboBox(final AntraxPluginsStore pluginsStore, final ScriptDefinition[] definitions, final ScriptInstance scriptInstance) {
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    model.addElement(null);
    ScriptInstanceWrapper current = null;

    Arrays.sort(definitions);

    for (ScriptDefinition scriptDef : definitions) {
      if (scriptDef.getType() == scriptType) {
        if (scriptInstance != null && scriptInstance.getDefinition().getName().equals(scriptDef.getName())) {
          current = new ScriptInstanceWrapper(scriptInstance);
          model.addElement(current);
        } else {
          ScriptInstanceWrapper wrapper = new ScriptInstanceWrapper(scriptDef);
          model.addElement(wrapper);
        }
      }
    }

    // was selected script instance with not found script definition
    if (current == null && scriptInstance != null && scriptInstance.getDefinition() instanceof NotFoundScriptDefinition) {
      current = new ScriptInstanceWrapper(scriptInstance);
    }

    scriptsComboBox.setModel(model);
    scriptsComboBox.setSelectedItem(current);
  }

}
