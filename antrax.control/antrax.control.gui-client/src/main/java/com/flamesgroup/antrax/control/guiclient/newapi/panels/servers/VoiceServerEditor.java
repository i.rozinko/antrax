/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.swingwidgets.JLabeledComponent;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.VoiceServerConfig;
import com.flamesgroup.commons.CallRouteConfig;
import com.flamesgroup.commons.SmsRouteConfig;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class VoiceServerEditor {
  private final Box advancedPanel = new Box(BoxLayout.Y_AXIS);

  private final JCheckBox basicCheckBox = new JCheckBox();
  private final JCheckBox advancedCheckBox = new JCheckBox();

  private final CallRouteConfigPanel callRouteConfigPanel = new CallRouteConfigPanel();
  private final SmsRouteConfigPanel smsRouteConfigPanel = new SmsRouteConfigPanel();

  private final JLabeledComponent lblBasicCheckBox;

  private final List<JLabeledComponent> components = new LinkedList<>();

  private final BasicAdvancedPanel basicAdvancedPanel;

  private boolean editable;
  private IServerData server;

  public VoiceServerEditor() {
    JLabeledComponent lblAdvancedCheckBox = r(new JLabeledComponent("Enable", advancedCheckBox));
    lblBasicCheckBox = r(new JLabeledComponent("Enable", basicCheckBox));

    advancedPanel.add(lblAdvancedCheckBox);
    advancedPanel.add(callRouteConfigPanel);
    advancedPanel.add(smsRouteConfigPanel);

    basicCheckBox.addActionListener(e -> advancedCheckBox.setSelected(basicCheckBox.isSelected()));
    advancedCheckBox.addActionListener(e -> {
      basicCheckBox.setSelected(advancedCheckBox.isSelected());
      callRouteConfigPanel.setEnabled(advancedCheckBox.isSelected());
      smsRouteConfigPanel.setEnabled(advancedCheckBox.isSelected());
    });

    basicAdvancedPanel = new BasicAdvancedPanel(lblBasicCheckBox, advancedPanel);
  }

  private JLabeledComponent r(final JLabeledComponent jLabeledComponent) {
    components.add(jLabeledComponent);
    return jLabeledComponent;
  }

  public void setEditable(final boolean value) {
    editable = value;
    basicCheckBox.setEnabled(value);
    advancedCheckBox.setEnabled(value);
    callRouteConfigPanel.setEnabled(value && (server != null && server.isVoiceServerEnabled()));
    smsRouteConfigPanel.setEnabled(value && (server != null && server.isVoiceServerEnabled()));
  }

  public BasicAdvancedPanel getComponent() {
    return basicAdvancedPanel;
  }

  public void readEditorsToElem(final IServerData server) {
    if (basicCheckBox.isSelected()) {
      server.setVoiceServerEnabled(true);
      server.setVoiceServerConfig(new VoiceServerConfig(callRouteConfigPanel.getCallRouteConfig(), smsRouteConfigPanel.getSmsRouteConfig()));
    } else {
      server.setVoiceServerEnabled(false);
      server.setVoiceServerConfig(null);
    }
  }

  public void writeElemToEditors(final IServerData server) {
    this.server = server;
    callRouteConfigPanel.setEnabled(editable && server.isVoiceServerEnabled());
    smsRouteConfigPanel.setEnabled(editable && server.isVoiceServerEnabled());

    basicCheckBox.setSelected(server.isVoiceServerEnabled());
    advancedCheckBox.setSelected(server.isVoiceServerEnabled());
    VoiceServerConfig voiceServerConfig = server.getVoiceServerConfig();
    if (voiceServerConfig == null) {
      callRouteConfigPanel.setCallRouteConfig(null);
      smsRouteConfigPanel.setSmsRouteConfig(null);
    } else {
      callRouteConfigPanel.setCallRouteConfig(voiceServerConfig.getCallRouteConfig());
      smsRouteConfigPanel.setSmsRouteConfig(voiceServerConfig.getSmsRouteConfig());
    }
  }

  public List<JLabeledComponent> listLabeledComponents() {
    return components;
  }

  public Object exportToObject() {
    Map<Object, Object> retval = new HashMap<>();
    retval.put(advancedCheckBox, advancedCheckBox.isSelected());
    retval.put(callRouteConfigPanel, callRouteConfigPanel.exportToObject());
    retval.put(smsRouteConfigPanel, smsRouteConfigPanel.exportToObject());
    return retval;
  }

  private class CallRouteConfigPanel extends JPanel {

    private static final long serialVersionUID = -4751582337810843435L;

    private final JTextField calledRegex = new JTextField(20);
    private final JEditIntField priority = new JEditIntField(0, 100);

    private final DirectionEditor directionEditor = new DirectionEditor();

    public CallRouteConfigPanel() {
      setBorder(BorderFactory.createTitledBorder("Call Route Config"));
      setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

      JLabeledComponent lblCalledRegex = new JLabeledComponent("Called regex", calledRegex);
      JLabeledComponent lblPriority = new JLabeledComponent("Priority", priority);

      JLabeledComponent[] c = new JLabeledComponent[] {lblCalledRegex, lblPriority};
      JLabeledComponent.fitLabelWidths(c);
      JLabeledComponent.fitComponentWidths(c);

      directionEditor.setBorder(BorderFactory.createTitledBorder("Directions"));

      add(lblCalledRegex);
      add(lblPriority);
      add(directionEditor);
    }

    @Override
    public void setEnabled(final boolean enabled) {
      super.setEnabled(enabled);
      calledRegex.setEnabled(enabled);
      priority.setEnabled(enabled);
      directionEditor.setEnabled(enabled);
    }

    public CallRouteConfig getCallRouteConfig() {
      return new CallRouteConfig(calledRegex.getText(), priority.getIntValue(), directionEditor.getDirections());
    }

    public void setCallRouteConfig(final CallRouteConfig callRouteConfig) {
      if (callRouteConfig == null) {
        calledRegex.setText("");
        priority.setValue(0);
        directionEditor.setDirections(Collections.emptyList());
      } else {
        calledRegex.setText(callRouteConfig.getCalledRegex());
        priority.setValue(callRouteConfig.getPriority());
        directionEditor.setDirections(callRouteConfig.getDirections());
      }
    }

    public Object exportToObject() {
      Map<Object, Object> retval = new HashMap<>();
      retval.put(calledRegex, calledRegex.getText());
      retval.put(priority, priority.getText());
      retval.put(directionEditor, directionEditor.getDirections());
      return retval;
    }

  }

  private class SmsRouteConfigPanel extends JPanel {

    private static final long serialVersionUID = -3641029596521260889L;

    private final JTextField dnisRegex = new JTextField(20);
    private final JEditIntField priority = new JEditIntField(0, 100);

    public SmsRouteConfigPanel() {
      setBorder(BorderFactory.createTitledBorder("Sms Route Config"));
      setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

      JLabeledComponent lblCalledRegex = new JLabeledComponent("Dnis regex", dnisRegex);
      JLabeledComponent lblPriority = new JLabeledComponent("Priority", priority);

      JLabeledComponent[] c = new JLabeledComponent[] {lblCalledRegex, lblPriority};
      JLabeledComponent.fitLabelWidths(c);
      JLabeledComponent.fitComponentWidths(c);

      add(lblCalledRegex);
      add(lblPriority);
    }

    @Override
    public void setEnabled(final boolean enabled) {
      super.setEnabled(enabled);
      dnisRegex.setEnabled(enabled);
      priority.setEnabled(enabled);
    }

    public SmsRouteConfig getSmsRouteConfig() {
      return new SmsRouteConfig(dnisRegex.getText(), priority.getIntValue());
    }

    public void setSmsRouteConfig(final SmsRouteConfig smsRouteConfig) {
      if (smsRouteConfig == null) {
        dnisRegex.setText(".*");
        priority.setValue(0);
      } else {
        dnisRegex.setText(smsRouteConfig.getDnisRegex());
        priority.setValue(smsRouteConfig.getPriority());
      }
    }

    public Object exportToObject() {
      Map<Object, Object> retval = new HashMap<>();
      retval.put(dnisRegex, dnisRegex.getText());
      retval.put(priority, priority.getText());
      return retval;
    }

  }

}
