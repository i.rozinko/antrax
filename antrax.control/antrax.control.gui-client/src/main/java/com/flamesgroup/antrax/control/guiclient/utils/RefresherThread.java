/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.*;

public class RefresherThread extends Thread {
  private final Logger logger = LoggerFactory.getLogger(RefresherThread.class);

  private final List<Refresher> jobs = new LinkedList<>();
  private final List<Refresher> justRemoved = new LinkedList<>();
  private final List<Refresher> justAdded = new LinkedList<>();

  private volatile AtomicInteger sessionJobsDone = new AtomicInteger(0);
  private volatile int sessionJobs = 0;
  private volatile long sessionStartTime = 0;
  private boolean forcingRefresh = false;

  private final BeansPool beansPool;

  private final long refreshTimeout;

  private final List<RefreshFailedListener> listeners = new LinkedList<>();

  public RefresherThread(final BeansPool beansPool, final long refreshTimeout) {
    super("RefresherThread");
    this.beansPool = beansPool;
    this.refreshTimeout = refreshTimeout;
  }

  public BeansPool getBeansPool() {
    return beansPool;
  }

  public void addRefresher(final Refresher refresher) {
    synchronized (jobs) {
      if (!jobs.contains(refresher)) {
        refresher.postInitialize(beansPool);
        jobs.add(refresher);
        if (justRemoved.contains(refresher)) {
          justRemoved.remove(refresher);
        }
        if (!justAdded.contains(refresher)) {
          justAdded.add(refresher);
        }
      }
    }
  }

  public void removeRefresher(final Refresher refresher) {
    synchronized (jobs) {
      if (jobs.contains(refresher)) {
        jobs.remove(refresher);
        if (justAdded.contains(refresher)) {
          justAdded.remove(refresher);
        }
        if (!justRemoved.contains(refresher)) {
          justRemoved.add(refresher);
        }
      }

    }
  }

  public boolean containsRefresher(final Refresher refresher) {
    synchronized (jobs) {
      return jobs.contains(refresher);
    }
  }

  private boolean isHasAnyJob() {
    return (sessionJobs > sessionJobsDone.get());
  }

  @Override
  public void run() {
    while (!interrupted()) {
      final List<Refresher> completedJobs = new CopyOnWriteArrayList<>();
      final List<Throwable> exceptions = new CopyOnWriteArrayList<>();
      synchronized (jobs) {
        // clear job counters
        synchronized (this) {
          sessionJobs = jobs.size();
          sessionJobsDone = new AtomicInteger(0);
        }

        // Create refresh finished listener
        RefreshingFinishedListener listener = new RefreshingFinishedListener() {
          final List<Refresher> completed = completedJobs;
          final AtomicInteger jobsDone = sessionJobsDone;

          @Override
          public void handleRefreshFinished(final Refresher refresher) {
            completed.add(refresher);
            jobsDone.incrementAndGet();
            synchronized (RefresherThread.this) {
              RefresherThread.this.notifyAll();
            }
          }

          @Override
          public void handleRefreshFailed(final Refresher refresher, final Exception e) {
            exceptions.add(e);
            handleRefreshFinished(refresher);
          }
        };

        // Callback for refresh data
        for (Refresher r : jobs) {
          r.refreshData(listener);
        }
      }

      forcingRefresh = false;
      boolean interrupted = false;
      while (isHasAnyJob() && !forcingRefresh) {
        synchronized (this) {
          try {
            this.wait();
          } catch (InterruptedException ignored) {
            interrupted = true;
            break;
          }
        }
      }

      if (interrupted) {
        break;
      }

      if (forcingRefresh) {
        continue;
      }

      synchronized (jobs) {
        for (Refresher r : justAdded) {
          r.handleEnabled(beansPool);
        }

        for (Refresher r : justRemoved) {
          r.handleDisabled(beansPool);
        }
      }

      // Callback for refresh GUI
      for (Refresher r : completedJobs) {
        if (r != null) {
          final Refresher refresher = r;
          SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
              try {
                refresher.updateGuiElements();
              } catch (Throwable e) {
                logger.warn("[{}] - failed to invoke update GUI elements", this, e);
              }
            }
          });
        }
      }

      // Notify listeners about exceptions
      for (Throwable e : exceptions) {
        fireExceptionOccurred(e);
      }

      try {
        synchronized (this) {
          if (!(forcingRefresh) && !isInterrupted()) {// && isHasAnyJob())) {
            sessionStartTime = System.currentTimeMillis();
            wait(refreshTimeout);
            sessionStartTime = 0;
          }
        }
      } catch (InterruptedException ignored) {
        break;
      }
    }
  }

  public void addRefreshFailedListener(final RefreshFailedListener l) {
    synchronized (listeners) {
      listeners.add(l);
    }
  }

  public void removeRefreshFailedListener(final RefreshFailedListener l) {
    synchronized (listeners) {
      listeners.remove(l);
    }
  }

  private void fireExceptionOccurred(final Throwable e) {
    synchronized (listeners) {
      for (RefreshFailedListener l : listeners) {
        l.handleRefreshFailed(e);
      }
    }
  }

  protected boolean isConnectionError(final Throwable e) {
    Throwable root = e;
    while (root.getCause() != null) {
      root = root.getCause();
    }
    return (root instanceof java.net.SocketException) || (root instanceof java.net.UnknownHostException);
  }

  public synchronized void forceRefresh() {
    forcingRefresh = true;
    this.notifyAll();
  }

  public void stopRefresher() {
    synchronized (this) {
      for (Refresher r : jobs) {
        r.interrupt();
      }
      this.interrupt();

      //try {
      //  this.join();
      //} catch (InterruptedException e) {
      //}
    }
    synchronized (listeners) {
      listeners.clear();
    }
    Refresher.interrupAll();
  }

  /**
   * @return 0-100 percent of refreshing progress
   */
  public synchronized int getTimeProgress() {
    long sessionStartTime = this.sessionStartTime;
    if (sessionStartTime == 0) {
      return 100;
    }
    long timeout = System.currentTimeMillis() - sessionStartTime;
    return (int) ((((double) timeout) / refreshTimeout) * 100);
  }

  public synchronized int getRefreshProgress() {
    int sessionJobsDone;
    int sessionJobs;
    synchronized (this) {
      sessionJobsDone = this.sessionJobsDone.get();
      sessionJobs = this.sessionJobs;
    }

    if (sessionJobs == 0) {
      return 0;
    }

    if (sessionJobs == sessionJobsDone) {
      return 100;
    }

    return (sessionJobsDone / sessionJobs) * 100;
  }

}
