/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;

import java.util.Date;

class TemporaryRegistryEntry implements RegistryEntry {

  private static final long serialVersionUID = 6768592332982030338L;

  private final String path;
  private final String value;
  private final Date date;

  public TemporaryRegistryEntry(final String path, final String value, final Date date) {
    this.path = path;
    this.value = value;
    this.date = date;
  }

  @Override
  public String getPath() {
    return path;
  }

  @Override
  public Date getUpdatingTime() {
    return date;
  }

  @Override
  public String getValue() {
    return value;
  }

}
