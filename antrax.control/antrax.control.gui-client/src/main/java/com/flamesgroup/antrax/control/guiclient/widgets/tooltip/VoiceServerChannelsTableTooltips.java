/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;

import java.util.EnumSet;

public class VoiceServerChannelsTableTooltips {

  private VoiceServerChannelsTableTooltips() {
  }

  public static String getGsmChannelTooltip() {
    return new TooltipHeaderBuilder("GSM channel").addText("The number of device and GSM module").build();
  }

  public static String getSimHolderTooltip() {
    return new TooltipHeaderBuilder("SIM holder").addText("Number of SIM module and holder of specified SIM card").build();
  }

  public static String getSimCardStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("SIM card states");
    builder.createImageTable().addValue(getResourceToString("/img/simstate/simstate_enable.png"), "SIM card is ready to conduct calls")
        .addValue(getResourceToString("/img/simstate/simstate_disable.png"), "SIM card is blocked to conduct calls");
    return builder.build();
  }

  public static String getSimCardLockStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("SIM card lock states");
    builder.createImageTable().addValue(getResourceToString("/img/simlockstate/simlockstate_l.gif"), "GSM channel is blocked")
        .addValue(getResourceToString("/img/simlockstate/simlockstate_u.gif"), "GSM channel is unblocked");
    return builder.build();
  }

  public static String getLockTimeoutTooltip() {
    return new TooltipHeaderBuilder("Lock timeout").addText("Time period of channel block").build();
  }

  public static String getLockReasonTooltip() {
    return new TooltipHeaderBuilder("Lock or unlock reason").addText("Reason of lock or unlock SIM card").build();
  }

  public static String getCallChannelStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Call channel states");
    TooltipHeaderBuilder.ImageTable imageTable = builder.createImageTable();
    EnumSet.allOf(CallChannelState.State.class).forEach(s -> imageTable.addValue(getCallChannelStateImage(s), getCallChannelStateText(s)));
    return builder.build();
  }

  public static String getCallChannelStateTimeoutTooltip() {
    return new TooltipHeaderBuilder("Call channel state timeout").addText("Time period of channel in current state").build();
  }

  public static String getAdvancedInfoTooltip() {
    return new TooltipHeaderBuilder("Call channel state description").addText("Description of current call channel state").build();
  }

  public static String getCallStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Call states");
    TooltipHeaderBuilder.ImageTable imageTable = builder.createImageTable();
    EnumSet.allOf(CallState.State.class).forEach(s -> imageTable.addValue(getCallStateImage(s), getCallStateText(s)));
    return builder.build();
  }

  public static String getCallStateTimeoutTooltip() {
    return new TooltipHeaderBuilder("Call state timeout").addText("Time period of call in current state").build();
  }

  public static String getPhoneNumberTooltip() {
    return new TooltipHeaderBuilder("Phone number").addText("Phone number being called to").build();
  }

  public static String getVoiceServerTooltip() {
    return new TooltipHeaderBuilder("Voice server name").addText("Name of voice server on which the specified channel is located").build();
  }

  public static String getSimGroupTooltip() {
    return new TooltipHeaderBuilder("SIM group name").addText("SIM group of registered SIM card on channel").build();
  }

  public static String getGsmGroupTooltip() {
    return new TooltipHeaderBuilder("GSM group name").build();
  }

  public static String getSignalTooltip() {
    return new TooltipHeaderBuilder("Level of GSM signal").build();
  }

  public static String getLockArfcnTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Lock arfcn state");
    TooltipHeaderBuilder.TextTooltipHeader textTable = builder.createTextTable();
    textTable.addValue("Unsupported", "Channel does not support arfcn settings").addValue("Default", "Default settings of arfcn").addValue("[0-9]+", "Arfcn is set up");
    return builder.build();
  }

  public static String getOkCallsTooltip() {
    return new TooltipHeaderBuilder("Successful calls per activity").build();
  }

  public static String getTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total calls per activity").build();
  }

  public static String getSuccessSmsTooltip() {
    return new TooltipHeaderBuilder("Successful SMS count per activity").build();
  }

  public static String getTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total SMS count per activity").build();
  }

  public static String getCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration per activity").build();
  }

  public static String getUssdTimeoutTooltip() {
    return new TooltipHeaderBuilder("Duration of current USSD request").build();
  }

  public static String getLastUssdTooltip() {
    return new TooltipHeaderBuilder("Last USSD response received from GSM operator").build();
  }

  public static String getChannelConnectionStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Channel connection state");
    builder.createImageTable().addValue(getResourceToString("/img/channels/live.png"), "Channel is connected")
        .addValue(getResourceToString("/img/channels/unlive.png"), "Channel is disconnected");
    return builder.build();
  }

  public static String getChannelLicenseStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Channel license state");
    TooltipHeaderBuilder.ImageTable imageTable = builder.createImageTable();
    EnumSet.allOf(ChannelConfig.ChannelState.class).forEach(s -> imageTable.addValue(getChannelLicenseStateImage(s), getChannelLicenseStateText(s)));
    imageTable.addValue(getResourceToString("/img/channels/license_expiring.png"), "Channel license expires after 3 days");
    return builder.build();
  }

  private static String getCallChannelStateImage(final CallChannelState.State state) {
    if (state == null) {
      return getResourceToString("/img/invisiblebox.gif");
    }

    switch (state) {
      case READY_TO_CALL:
        return getResourceToString("/img/channelstate/channelstate_ready_to_call.gif");
      case OUTGOING_CALL:
        return getResourceToString("/img/channelstate/channelstate_outgoing_call.png");
      case INCOMING_CALL:
        return getResourceToString("/img/channelstate/channelstate_incoming_call.png");
      case INCOMING_SCRIPT_CALL:
        return getResourceToString("/img/channelstate/channelstate_incoming_script_call.png");
      case IDLE_AFTER_REGISTRATION:
        return getResourceToString("/img/channelstate/channelstate_idle_after_registration.gif");
      case IDLE_BEFORE_UNREG:
        return getResourceToString("/img/channelstate/channelstate_idle_before_unreg.gif");
      case IDLE_BETWEEN_CALLS:
        return getResourceToString("/img/channelstate/channelstate_idle_between_calls.gif");
      case IDLE_BEFORE_SELF_CALL:
        return getResourceToString("/img/channelstate/channelstate_idle_before_self_call.gif");
      case IDLE_AFTER_SELF_CALL:
        return getResourceToString("/img/channelstate/channelstate_idle_after_self_call.gif");
      case RELEASING:
        return getResourceToString("/img/channelstate/channelstate_released.gif");
      case WAITING_SELF_CALL:
        return getResourceToString("/img/channelstate/channelstate_self_call.png");
      case REGISTERING_IN_GSM:
        return getResourceToString("/img/channelstate/channelstate_registering_in_gsm.png");
      case STARTED_BUSINESS_ACTIVITY:
        return getResourceToString("/img/channelstate/channelstate_business_activity.gif");
      case NETWORK_SURVEY:
        return getResourceToString("/img/registration.png");
      case SETTING_IMEI:
        return getResourceToString("/img/barcode.png");
      case TURNING_ON_MODULE:
        return getResourceToString("/img/mobile_channel.png");
      case CELL_EQUALIZER:
        return getResourceToString("/img/cell_equalizer.png");
      default:
        return VoiceServerChannelsTableTooltips.class.getResource("/img/invisiblebox.gif").toString();
    }
  }

  private static String getCallChannelStateText(final CallChannelState.State state) {
    if (state == null) {
      return null;
    }

    switch (state) {
      case READY_TO_CALL:
        return "Channel is ready to accept the call";
      case OUTGOING_CALL:
        return "Outgoing call to GSM network";
      case INCOMING_CALL:
        return "Incoming call from GSM network";
      case INCOMING_SCRIPT_CALL:
        return "Incoming call on the basis of scripts";
      case IDLE_AFTER_REGISTRATION:
        return "Timeout after channel registered in GSM network";
      case IDLE_BEFORE_UNREG:
        return "Timeout before channel leaving the GSM network";
      case IDLE_BETWEEN_CALLS:
        return "Timeout between calls";
      case IDLE_BEFORE_SELF_CALL:
        return "Timeout before starting self call";
      case IDLE_AFTER_SELF_CALL:
        return "Timeout after self call";
      case RELEASING:
        return "Releasing channel";
      case WAITING_SELF_CALL:
        return "Waiting self call";
      case REGISTERING_IN_GSM:
        return "Registering in GSM network";
      case STARTED_BUSINESS_ACTIVITY:
        return "Started business activity";
      case NETWORK_SURVEY:
        return "Network survey";
      case SETTING_IMEI:
        return "Setting IMEI on GSM module";
      case TURNING_ON_MODULE:
        return "Turning on GSM module";
      case CELL_EQUALIZER:
        return "Running cell equalizer";
      default:
        return null;
    }
  }

  private static String getCallStateImage(final CallState.State state) {
    switch (state) {
      case IDLE:
        return getResourceToString("/img/callstate/callstate_idle.gif");
      case DIALING:
        return getResourceToString("/img/callstate/callstate_dialing.gif");
      case ALERTING:
        return getResourceToString("/img/callstate/callstate_alerting.gif");
      case ACTIVE:
        return getResourceToString("/img/callstate/callstate_active.gif");
      default:
        return getResourceToString("/img/invisiblebox.gif");
    }
  }

  private static String getCallStateText(final CallState.State state) {
    switch (state) {
      case IDLE:
        return "Channel is ready to register a SIM card or accept calls";
      case DIALING:
        return "Dial of number is performed";
      case ALERTING:
        return "Call of subscriber";
      case ACTIVE:
        return "Conversation";
      default:
        return null;
    }
  }

  private static String getChannelLicenseStateImage(final ChannelConfig.ChannelState state) {
    switch (state) {
      case ACTIVE:
        return getResourceToString("/img/channels/license_correct.png");
      case LICENSE_INVALID:
        return getResourceToString("/img/channels/license_invalid.png");
      case LICENSE_EXPIRE:
        return getResourceToString("/img/channels/license_expire.png");
      case IP_CONFIG_PARAMETERS_INVALID:
        return getResourceToString("/img/channels/ip_config_parameters_invalid.png");
      case SET_IP_CONFIG_ERROR:
        return getResourceToString("/img/channels/set_ip_config_error.png");
      case LICENSE_LOCK:
        return getResourceToString("/img/channels/outdated_firmware_version.png");
      default:
        return null;
    }
  }

  private static String getChannelLicenseStateText(final ChannelConfig.ChannelState state) {
    switch (state) {
      case ACTIVE:
        return "License is active";
      case LICENSE_INVALID:
        return "Channel has invalid license";
      case LICENSE_EXPIRE:
        return "Channel has expired license";
      case IP_CONFIG_PARAMETERS_INVALID:
        return "Channel get invalid one of ip config parameters";
      case SET_IP_CONFIG_ERROR:
        return "Channel has setting ip config error";
      case LICENSE_LOCK:
        return "Channel has locked license";
      default:
        return null;
    }
  }

  private static String getResourceToString(final String name) {
    return VoiceServerChannelsTableTooltips.class.getResource(name).toString();
  }

}
