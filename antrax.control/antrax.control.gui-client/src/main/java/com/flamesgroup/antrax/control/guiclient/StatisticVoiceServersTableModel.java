/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.guiclient.commadapter.StatisticGatewaysAdapter;
import com.flamesgroup.antrax.control.guiclient.domain.ServerChannelRowKey;
import com.flamesgroup.antrax.control.guiclient.utils.RefreshableObject;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.ChannelUID;

import java.util.List;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

public class StatisticVoiceServersTableModel extends AbstractTableModel {

  private static final long serialVersionUID = 4281957898429084280L;

  public enum Column {
    VOICE_SERVER("Voice Server", String.class),
    MOBILE_GATEWAY("Mobile chan.", ChannelUID.class),
    ASR("ASR", Integer.class),
    ACD("ACD", Long.class),
    SUCCESS_CALLS("Success calls", Integer.class),
    TOTAL_CALLS("Total calls", Integer.class),
    CALLS_DURATION("Calls dur.", Long.class),
    SUCCESS_SMSES("Success SMS", Integer.class),
    TOTAL_SMSES("Total SMS", Integer.class),
    SMS_ASR("SMS ASR", Integer.class),
    INCOMING_TOTAL_SMSES("Inc. Total SMS", Integer.class);

    private final String name;
    private final Class<?> clazz;

    Column(final String name, final Class<?> clazz) {
      this.name = name;
      this.clazz = clazz;
    }

    public String getName() {
      return name;
    }

    public Class<?> getColumnClass() {
      return clazz;
    }

    public int getColumnIndex() {
      return ordinal();
    }

    public static Column getColumnByIndex(final int columnIndex) {
      return Column.values()[columnIndex];
    }

    public static String getColumnName(final int columnIndex) {
      return getColumnByIndex(columnIndex).getName();
    }

    public static Class<?> getColumnClass(final int columnIndex) {
      return getColumnByIndex(columnIndex).getColumnClass();
    }

    public static int size() {
      return values().length;
    }
  }

  private final RefreshableObject<List<StatisticGatewaysAdapter>> adapters =
      new RefreshableObject<>();

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return Column.getColumnClass(columnIndex);
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return Column.getColumnName(columnIndex);
  }

  @Override
  public int getColumnCount() {
    return Column.size();
  }

  @Override
  public synchronized int getRowCount() {
    if (adapters.isEmpty()) {
      return 0;
    } else {
      int servesCount = adapters.getData().size();
      int channels = 0;
      for (StatisticGatewaysAdapter adapter : adapters.getData()) {
        channels += adapter.getChannelsCount();
      }
      return servesCount + channels;
    }
  }

  private int getServerIndex(final int row) {
    if (adapters.isEmpty()) {
      return -1;
    }

    int top = 0;
    int bottom;

    int serverIndex = 0;
    for (StatisticGatewaysAdapter adapter : adapters.getData()) {
      bottom = top + adapter.getChannelsCount();
      if (top <= row && row <= bottom) {
        return serverIndex;
      }
      top = bottom + 1;
      serverIndex++;
    }

    return -1;
  }

  private int getChannelIndex(final int row) {
    if (adapters.isEmpty()) {
      return -1;
    }

    int top = 0;
    int bottom;
    for (StatisticGatewaysAdapter adapter : adapters.getData()) {
      bottom = top + adapter.getChannelsCount();
      if (row >= top && row <= bottom) {
        if (row == top) { // this is server row
          return -1;
        } else {
          return row - top - 1;
        }
      }
      top = bottom + 1;
    }

    return -1;
  }

  @Override
  public synchronized Object getValueAt(final int row, final int column) {
    if (adapters.isEmpty()) {
      return null;
    }

    int serverIndex = getServerIndex(row);
    int channelIndex = getChannelIndex(row);
    boolean isServer = (channelIndex == -1);

    StatisticGatewaysAdapter adapter = adapters.getData().get(serverIndex);
    if (isServer) {
      switch (Column.getColumnByIndex(column)) {
        case VOICE_SERVER:
          return adapter.getServer().getName();
        case ASR:
          return adapter.getServerASR();
        case ACD:
          return adapter.getServerACD();
        case TOTAL_CALLS:
          return adapter.getServerTotalCalls();
        case SUCCESS_CALLS:
          return adapter.getServerSuccessCalls();
        case CALLS_DURATION:
          return adapter.getServerCallsDuration();
        case SUCCESS_SMSES:
          return adapter.getSuccessOutgoingSmsCount();
        case TOTAL_SMSES:
          return adapter.getServerTotalSmses();
        case SMS_ASR:
          return adapter.getServerSMSASR();
        case INCOMING_TOTAL_SMSES:
          return adapter.getServerIncomingTotalSmses();
        default:
          return null;
      }
    } else {
      switch (Column.getColumnByIndex(column)) {
        case VOICE_SERVER:
          return "";
        case MOBILE_GATEWAY:
          return adapter.getMobileChannelUID(channelIndex);
        case ASR:
          return adapter.getChannelASR(channelIndex);
        case ACD:
          return adapter.getChannelACD(channelIndex);
        case TOTAL_CALLS:
          return adapter.getChannelTotalCalls(channelIndex);
        case SUCCESS_CALLS:
          return adapter.getChannelSuccessCalls(channelIndex);
        case CALLS_DURATION:
          return adapter.getChannelCallsDuration(channelIndex);
        case SUCCESS_SMSES:
          return adapter.getSuccessOutgoingSmsCount(channelIndex);
        case TOTAL_SMSES:
          return adapter.getChannelTotalSmses(channelIndex);
        case SMS_ASR:
          return adapter.getASRSms(channelIndex);
        case INCOMING_TOTAL_SMSES:
          return adapter.getChannelIncomingTotalSmses(channelIndex);
        default:
          return null;
      }
    }
  }

  public IServerData getServer(final int row) {
    if (isServerRow(row)) {
      int serverIndex = getServerIndex(row);
      return adapters.getData().get(serverIndex).getServer();
    } else {
      return null;
    }
  }

  public void refreshData(final List<StatisticGatewaysAdapter> data) {
    synchronized (this) {
      adapters.setRefreshData(data);
    }
  }

  public synchronized void refreshUI() {
    adapters.refresh();
    fireTableChanged(new TableModelEvent(this));
  }

  public synchronized ServerChannelRowKey getKey(final int row) {
    if (adapters.isEmpty() || row < 0 || row >= getRowCount()) {
      return null;
    } else {
      int serverIndex = getServerIndex(row);
      int channelIndex = getChannelIndex(row);
      StatisticGatewaysAdapter adapter = adapters.getData().get(serverIndex);
      return adapter.getKey(channelIndex);
    }
  }

  public synchronized int getRow(final ServerChannelRowKey key) {
    for (int row = 0; row < getRowCount(); ++row) {
      if (key.equals(getKey(row))) {
        return row;
      }
    }
    return -1;
  }

  public synchronized boolean isServerRow(final int row) {
    if (!adapters.isEmpty()) {
      int top = 0;
      int bottom;
      for (StatisticGatewaysAdapter adapter : adapters.getData()) {
        bottom = top + adapter.getChannelsCount();
        if (row >= top && row <= bottom) {
          return (row == top);
        }
        top = bottom + 1;
      }
    }
    return false;
  }

}
