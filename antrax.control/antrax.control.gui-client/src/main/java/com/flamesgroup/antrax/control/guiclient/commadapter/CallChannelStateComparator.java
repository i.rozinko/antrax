/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.storage.state.CallChannelState;

import java.io.Serializable;
import java.util.Comparator;

public class CallChannelStateComparator implements Comparator<CallChannelState>, Serializable {

  private static final long serialVersionUID = -4471767973801145267L;

  public static int calculateStateDec(final CallChannelState state) {
    return state.getState().ordinal();
  }

  @Override
  public int compare(final CallChannelState state1, final CallChannelState state2) {
    if (state1 == null) {
      return (state2 == null) ? 0 : 1;
    } else if (state2 == null) {
      return -1;
    }
    return calculateStateDec(state1) - calculateStateDec(state2);
  }

}
