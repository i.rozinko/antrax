/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.communication.GsmGroupEditInfo;
import com.flamesgroup.antrax.control.guiclient.commadapter.GsmGroupEditAdapter;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.list.JCheckBoxList;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.*;

public class GsmGroupEditorBox extends BaseEditorBox<GsmGroupEditAdapter> {

  private static final long serialVersionUID = 4528535287885588709L;

  private JTextField fieldGsmGroupName;
  private JButton buttonSelectAll;
  private JButton buttonDeselectAll;
  private JCheckBoxList cboxListSimGroups;

  private void layoutGUIElements() {
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] {0, 200, 0, 95, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};

    // GSM group name
    add(new JEditLabel("GSM group name:"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(10, 3, 5, 5), 0, 0));
    add(fieldGsmGroupName, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(10, 0, 5, 5), 0, 0));

    // Linked SIM groups list
    add(new JEditLabel("Linked SIM groups:"), new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(10, 3, 5, 5), 0, 0));
    JScrollPane scrollPane1 = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(cboxListSimGroups.getComponent());
    scrollPane1.setFocusable(true);
    add(scrollPane1, new GridBagConstraints(1, 3, 1, 2, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 10, 5), 0, 0));

    // Button 'Select All'
    add(buttonSelectAll, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));

    // Button 'Deselect All'
    add(buttonDeselectAll, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));

  }

  @Override
  protected void createGUIElements() {
    fieldGsmGroupName = new JTextField(10);
    cboxListSimGroups = new JCheckBoxList();
    buttonSelectAll = createButton("Select All", e -> cboxListSimGroups.checkAll(true));
    buttonDeselectAll = createButton("Deselect All", e -> cboxListSimGroups.checkAll(false));
    layoutGUIElements();
  }

  private JButton createButton(final String text, final ActionListener l) {
    JButton button = new JButton(text);
    button.putClientProperty("JButton.buttonType", "textured");
    button.addActionListener(l);
    return button;
  }

  @Override
  protected synchronized void readEditorsToElem(final GsmGroupEditAdapter dest) {
    // Set GSM group name
    String text = fieldGsmGroupName.getText();
    dest.getGsmGroup().setName((text == null) ? "" : text.trim());

    Object[] items = cboxListSimGroups.getCheckedItems();
    List<SIMGroup> simGroups = new ArrayList<>(items.length);
    for (Object item : items) {
      simGroups.add((SIMGroup) item);
    }
    GsmGroupEditInfo info = dest.getGsmGroupInfo();
    info.setLinkedSimGroups(simGroups);
  }

  @Override
  protected synchronized void writeElemToEditors(final GsmGroupEditAdapter src) {
    // GSM group name
    fieldGsmGroupName.setText(src.getGsmGroup().getName());

    // Linked SIM groups list
    cboxListSimGroups.removeAllItems();
    for (SIMGroup simGroup : src.getSimGroups()) {
      cboxListSimGroups.addItem(simGroup);
    }
    Collection<SIMGroup> linkedSimGroups = src.getGsmGroupInfo().getLinkedSimGroups();
    for (SIMGroup simGroup : linkedSimGroups) {
      cboxListSimGroups.setChecked(simGroup, true);
    }
  }

  @Override
  public void setEditable(final boolean editable) {
    super.setEditable(editable);
    buttonSelectAll.setEnabled(editable);
    buttonDeselectAll.setEnabled(editable);
    cboxListSimGroups.getComponent().setEnabled(editable);
  }

  @Override
  protected Object getCustomComponentValue(Component component) {
    if (component instanceof JScrollPane) {
      component = ((JScrollPane) component).getViewport().getView();
    }

    if (component instanceof JList && cboxListSimGroups.getComponent() == component) {
      return Arrays.asList(cboxListSimGroups.getCheckedItems());
    }
    return super.getCustomComponentValue(component);
  }

}
