/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.StatisticVoiceServersTable;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.refreshers.StatisticGatewaysRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.awt.*;

import javax.swing.*;

public class StatisticVoiceServersPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = -5486810122690359383L;

  private final StatisticVoiceServersTable statisticVoiceServersTable;
  private StatisticGatewaysRefresher refresher;

  private RefresherThread refresherThread;

  private JButton resetButton;

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    this.refresherThread = refresherThread;
    refresher = new StatisticGatewaysRefresher(refresherThread, transactionManager, this);
    refresher.initializePermanentRefresher(statisticVoiceServersTable);
  }

  @Override
  public void release() {
    statisticVoiceServersTable.getUpdatableTableProperties().saveProperties();
  }

  public StatisticVoiceServersPanel() {
    super(new BorderLayout());

    statisticVoiceServersTable = new StatisticVoiceServersTable();

    Box box = new Box(BoxLayout.Y_AXIS);
    box.add(createTopToolbar());
    add(box, BorderLayout.NORTH);
    add(createContent(), BorderLayout.CENTER);

    statisticVoiceServersTable.getSelectionModel().addListSelectionListener(e -> {
      ListSelectionModel lsm = (ListSelectionModel) e.getSource();
      int selectedRow = lsm.getMinSelectionIndex();
      if (selectedRow < 0) {
        return;
      }

      resetButton.setEnabled(statisticVoiceServersTable.isSelectedServer(selectedRow));
    });
    statisticVoiceServersTable.getUpdatableTableProperties().restoreProperties();
  }

  private Component createContent() {
    return ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(statisticVoiceServersTable);
  }

  private JComponent createTopToolbar() {
    JReflectiveBar bar = new JReflectiveBar();
    bar.addToRight(getResetButton());
    return bar;
  }

  private JButton getResetButton() {
    if (null == resetButton) {
      resetButton = new JReflectiveButton.JReflectiveButtonBuilder().setToolTipText("Reset statistic of selected servers").setText("Reset").build();

      resetButton.setEnabled(false);
      resetButton.addActionListener(e -> {
        IServerData serverData = statisticVoiceServersTable.getSelectedServer();
        if (serverData == null) {
          return;
        }

        int confirm = JOptionPane.showConfirmDialog(StatisticVoiceServersPanel.this, "Are you sure want to reset statistic of " + serverData.getName(),
            UIManager.getString("OptionPane.titleText"), JOptionPane.YES_NO_OPTION);
        if (confirm != JOptionPane.YES_OPTION) {
          return;
        }

        refresher.resetStatistic(serverData);
      });
    }
    return resetButton;
  }


}
