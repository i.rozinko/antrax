/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableCellEditor;

public class ArrayOperationEditor extends AbstractCellEditor implements TableCellEditor {

  private static final long serialVersionUID = -5083744070552376344L;

  private final JButton button;
  private final JPopupMenu popup;

  private final JMenuItem itemAdd;

  private final JMenuItem itemRemove;

  private final JMenuItem itemUp;

  private final JMenuItem itemDown;

  public ArrayOperationEditor() {
    button = new JButton(IconPool.getShared("/img/bricks.png"));
    popup = new JPopupMenu();

    button.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        popup.show(button, -popup.getPreferredSize().width, -popup.getPreferredSize().height / 4);
      }
    });

    popup.add(itemUp = new JMenuItem("Up"));
    popup.add(itemAdd = new JMenuItem("Add"));
    popup.add(itemDown = new JMenuItem("Down"));
    popup.add(itemRemove = new JMenuItem("Remove"));

    itemAdd.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        currValue.getParam().getInstance().insertParameter(currValue.getParam());
        fireEditingStopped();
      }
    });
    itemRemove.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        currValue.getParam().getInstance().removeParameter(currValue.getParam());
        fireEditingStopped();
      }
    });
    itemDown.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        currValue.getParam().getInstance().moveParamDown(currValue.getParam());
        fireEditingStopped();
      }
    });

    itemUp.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        currValue.getParam().getInstance().moveParamUp(currValue.getParam());
        fireEditingStopped();
      }
    });
  }

  private ScriptParamWrapper currValue;

  @Override
  public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
    this.currValue = (ScriptParamWrapper) value;
    itemRemove.setEnabled(!isParamSingle(this.currValue.getParam()));
    itemUp.setEnabled(!isParamFirst(this.currValue.getParam()));
    itemDown.setEnabled(!isParamLast(this.currValue.getParam()));
    return button;
  }

  private boolean isParamLast(final ScriptParameter param) {
    ScriptParameter[] parameters = param.getInstance().getParameters(param.getDefinition().getName());
    return parameters[parameters.length - 1] == param;
  }

  private boolean isParamFirst(final ScriptParameter param) {
    ScriptParameter[] parameters = param.getInstance().getParameters(param.getDefinition().getName());
    return parameters[0] == param;
  }

  private boolean isParamSingle(final ScriptParameter param) {
    return param.getInstance().getParameters(param.getDefinition().getName()).length == 1;
  }

  @Override
  public Object getCellEditorValue() {
    return currValue;
  }

}
