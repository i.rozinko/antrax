/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.commons.TimePeriodWriter;
import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.storage.state.CallState;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class CallStateTimeRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = -5249319719578949218L;

  private transient final TimePeriodWriter timePeriodWriter;

  public CallStateTimeRenderer() {
    super();
    setHorizontalAlignment(SwingConstants.CENTER);
    timePeriodWriter = new TimePeriodWriter() {

      @Override
      public Object writeLeftTime(long time) {
        if (time < 0) {
          time = 0;
        }
        return ((time > 0) ? "-" : "") + TimeUtils.writeTimeFull(time);
      }

      @Override
      public Object writePassedTime(final long time) {
        return TimeUtils.writeTimeFull(time);
      }

    };
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    String labelText;
    if (value instanceof CallState) {
      CallState state = (CallState) value;
      labelText = state.writeTime(timePeriodWriter).toString();
    } else if (value == null) {
      labelText = "";
    } else {
      labelText = value.toString();
    }
    setText(labelText);

    return cmp;
  }

}
