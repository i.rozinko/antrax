/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

/**
 * @see #initialize(AntraxPluginsStore,
 * PropertyEditorsSource)
 * @see #setScriptInstance(ScriptInstance, ScriptCommonsSource)
 */
public class ScriptParamsTable extends JUpdatableTable<ScriptParamWrapper, Integer> {
  private static final int COL_ARRAY_OP = 0;
  private static final int COL_REF = 2;
  private static final int COL_VAL = 3;

  private static final long serialVersionUID = 3493273081082187702L;
  private PropertyEditorsSource propertyEditors;
  private final CellEditorWrapper cellEditorWrapper = new CellEditorWrapper();
  private final CellRendererWrapper cellRendererWrapper = new CellRendererWrapper();
  private final ScriptCommonsCellEditor scriptCommonsEditor = new ScriptCommonsCellEditor();
  private final ArrayOperationEditor arrayOperationEditor = new ArrayOperationEditor();

  private ScriptInstance scriptInstance;
  private ScriptCommonsSource scriptCommonsSource;
  private boolean editable = true;
  private final List<ScriptInstanceChangeListener> listeners = new LinkedList<>();
  private AntraxPluginsStore pluginsStore;

  public ScriptParamsTable() {
    this(new ScriptParamsTableBuilder());
  }

  public ScriptParamsTable(final TableBuilder<ScriptParamWrapper, Integer> tableBuilder) {
    super(tableBuilder);

    getModel().addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(final TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE && e.getFirstRow() == e.getLastRow()) {
          if (e.getColumn() != TableModelEvent.ALL_COLUMNS) {
            SwingUtilities.invokeLater(new Runnable() {

              @Override
              public void run() {
                update(e.getFirstRow(), e.getColumn());
              }
            });
          }
        }
      }

      private void update(final int row, final int column) {
        if (column == COL_REF) {
          ScriptCommons sc = (ScriptCommons) getValueAt(row, column);
          if (sc == null) { // Reset back
            removeReference(getElemAt(row));
          } else {
            putReference(getElemAt(row), sc);
          }
          updateElemAt(row);
        }
        if (column == COL_VAL) {
          getElemAt(row).getParam().setValue(getValueAt(row, column), pluginsStore.getClassLoader());
          updateElemAt(row);
        }
        if (column == COL_ARRAY_OP) {
          setScriptInstance(scriptInstance, scriptCommonsSource);
        }
        requestFocus();
        fireScriptChanged(scriptInstance);
      }

      private void removeReference(final ScriptParamWrapper elem) {
        Object val = elem.getParam().getValue(pluginsStore.getClassLoader());
        if (val instanceof SharedReference) {
          String name = ((SharedReference) val).getName();
          elem.getParam().setValue(scriptCommonsSource.getCommonsByName(name).getValue(pluginsStore.getClassLoader()), pluginsStore.getClassLoader());
        }
      }

      private void putReference(final ScriptParamWrapper elem, final ScriptCommons sc) {
        elem.getParam().setValue(new SharedReference(sc.getName(), sc.getValue(pluginsStore.getClassLoader()).getClass().getCanonicalName()), pluginsStore.getClassLoader());
      }
    });
    getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setPreferredScrollableViewportSize(new Dimension(400, 200));
  }

  @Override
  protected ScriptParamsTableBuilder getTableBuilder() {
    return (ScriptParamsTableBuilder) super.getTableBuilder();
  }

  public void initialize(final AntraxPluginsStore pluginsStore, final PropertyEditorsSource propertyEditors) {
    assert pluginsStore != null;
    assert propertyEditors != null;
    this.propertyEditors = propertyEditors;
    this.scriptCommonsEditor.setPropertyEditorsSource(propertyEditors);
    this.scriptCommonsEditor.setAntraxPluginsStore(pluginsStore);
    this.pluginsStore = pluginsStore;
    //((ScriptParamsTableBuilder) getTableBuilder()).setAntraxPluginsStore(pluginsStore);
    getTableBuilder().setAntraxPluginsStore(pluginsStore);
  }

  public void setScriptInstance(final ScriptInstance instance, final ScriptCommonsSource commonsSource) {
    this.scriptInstance = instance;
    this.scriptCommonsSource = commonsSource;
    this.scriptCommonsEditor.setScriptCommonsSource(commonsSource);
    //((ScriptParamsTableBuilder) getTableBuilder()).setScriptCommonsSorce(commonsSource);
    getTableBuilder().setScriptCommonsSorce(commonsSource);

    if (instance != null) {
      this.setData(ScriptParamWrapper.wrap(instance.getParameters()));
    } else {
      this.setData();
    }
  }

  public ScriptInstance getScriptInstance() {
    return scriptInstance;
  }

  @Override
  public TableCellEditor getCellEditor(final int row, final int column) {
    if (column == COL_ARRAY_OP) {
      return arrayOperationEditor;
    }
    if (column == COL_VAL) {
      ScriptParameter param = getElemAt(row).getParam();
      PropertyEditor<?> propEditor = getCellEditorForValue(param.getValue(pluginsStore.getClassLoader()), param.getDefinition().getType());
      cellEditorWrapper.wrap(propEditor);
      return cellEditorWrapper;
    }
    if (column == COL_REF) {
      scriptCommonsEditor.setType(getElemAt(row).getParam().getDefinition().getType());
      return scriptCommonsEditor;
    }
    return super.getCellEditor(row, column);
  }

  @Override
  public TableCellRenderer getCellRenderer(final int row, final int column) {
    if (column == COL_VAL) {
      ScriptParameter param = getElemAt(row).getParam();
      PropertyEditor<?> propEditor = getCellEditorForValue(param.getValue(pluginsStore.getClassLoader()), param.getDefinition().getType());
      cellRendererWrapper.wrap(propEditor);
      return cellRendererWrapper;
    }
    return super.getCellRenderer(row, column);
  }

  @Override
  public boolean isCellEditable(final int row, final int column) {
    if (!editable) {
      return false;
    }
    if (column == COL_ARRAY_OP) {
      return getElemAt(row).getParam().getDefinition().isArray();
    }
    if (column == COL_VAL) {
      return !(getElemAt(row).getParam().getValue(pluginsStore.getClassLoader()) instanceof SharedReference);
    }
    return column == COL_REF;
  }

  public void setEditable(final boolean value) {
    this.editable = value;
  }

  private PropertyEditor<?> getCellEditorForValue(final Object value, final String type) {
    return propertyEditors.getEditorFor(value, type);
  }

  public void addScriptChangeListener(final ScriptInstanceChangeListener listener) {
    listeners.add(listener);
  }

  public void removeScriptChangeListener(final ScriptInstanceChangeListener listener) {
    listeners.remove(listener);
  }

  private void fireScriptChanged(final ScriptInstance scriptInstance) {
    for (ScriptInstanceChangeListener l : listeners) {
      l.handleInstanceChanged(scriptInstance, scriptInstance);
    }
  }

}
