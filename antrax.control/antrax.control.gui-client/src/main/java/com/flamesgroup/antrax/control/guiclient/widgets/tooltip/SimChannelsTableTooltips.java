/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.antrax.distributor.ChannelConfig;

import java.util.EnumSet;

public class SimChannelsTableTooltips {

  private SimChannelsTableTooltips() {
  }

  public static String getGatewayTooltip() {
    return new TooltipHeaderBuilder("Current voice server").addText("Voice server on which SIM card is registered").build();
  }

  public static String getPreviousGatewayTooltip() {
    return new TooltipHeaderBuilder("Previous voice server").addText("Voice server on which SIM card was registered before").build();
  }

  public static String getSimHolderTooltip() {
    return new TooltipHeaderBuilder("SIM holder").addText("Number of SIM module and holder on which SIM card is located").build();
  }

  public static String getMobileChannelTooltip() {
    return new TooltipHeaderBuilder("GSM channel").addText("GSM channel on which SIM card is registered").build();
  }

  public static String getSimCardStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("SIM card states");
    builder.createImageTable().addValue(getResourceToString("/img/simstate/simstate_enable.png"), "SIM card is ready to conduct calls")
        .addValue(getResourceToString("/img/simstate/simstate_disable.png"), "SIM card is blocked to conduct calls");
    return builder.build();
  }

  public static String getSimCardLockStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("SIM card lock states");
    builder.createImageTable().addValue(getResourceToString("/img/simlockstate/simlockstate_l.gif"), "GSM channel is blocked")
        .addValue(getResourceToString("/img/simlockstate/simlockstate_u.gif"), "GSM channel is unblocked");
    return builder.build();
  }

  public static String getLockTimeoutTooltip() {
    return new TooltipHeaderBuilder("Lock timeout").addText("Time period of sim card block").build();
  }

  public static String getLockReasonTooltip() {
    return new TooltipHeaderBuilder("Lock or unlock reason").addText("Reason of lock or unlock SIM card").build();
  }

  public static String getSimGroupTooltip() {
    return new TooltipHeaderBuilder("SIM group name").addText("SIM group where SIM card is located").build();
  }

  public static String getPhoneNumberTooltip() {
    return new TooltipHeaderBuilder("Phone number of SIM card").build();
  }

  public static String getStatusTooltip() {
    return new TooltipHeaderBuilder("SIM card status").build();
  }

  public static String getStatusTimeoutTooltip() {
    return new TooltipHeaderBuilder("SIM card status timeout").addText("Time period of SIM card being in status mentioned above").build();
  }

  public static String getAsrTooltip() {
    return new TooltipHeaderBuilder("Answer seizure ratio per all time").build();
  }

  public static String getAcdTooltip() {
    return new TooltipHeaderBuilder("Average call duration per all time").build();
  }

  public static String getSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful calls per all time").build();
  }

  public static String getTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total calls per all time").build();
  }

  public static String getCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration per all time").build();
  }

  public static String getIncomingSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful incoming calls per all time").build();
  }

  public static String getIncomingTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total incoming calls per all time").build();
  }

  public static String getIncomingCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Incoming calls duration per all time").build();
  }

  public static String getLastCallDurationTooltip() {
    return new TooltipHeaderBuilder("Last call duration").build();
  }

  public static String getSuccessSmsTooltip() {
    return new TooltipHeaderBuilder("Successful SMS count per all time").build();
  }

  public static String getTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total SMS count per all time").build();
  }

  public static String getSmsAsrTooltip() {
    return new TooltipHeaderBuilder("Successful sending seizure ratio of SMS per all time").build();
  }

  public static String getIncomingTotalSmsTooltip() {
    return new TooltipHeaderBuilder("Total incoming SMS count per all time").build();
  }

  public static String getTotalSmsStatusesTooltip() {
    return new TooltipHeaderBuilder("Total SMS statuses about successful delivering per all time").build();
  }

  public static String getIccidTooltip() {
    return new TooltipHeaderBuilder("Unique serial number of the SIM card").build();
  }

  public static String getImeiTooltip() {
    return new TooltipHeaderBuilder("International Mobile Equipment Identity").addText("It will be assigned to GSM module on registration").build();
  }

  public static String getLastReconfigurationTimeTooltip() {
    return new TooltipHeaderBuilder("Last reconfiguration time").addText("Time when the last changes occurred to card").build();
  }

  public static String getAllowedInternetTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Internet state");
    TooltipHeaderBuilder.TextTooltipHeader textTable = builder.createTextTable();
    textTable.addValue("Allowed", "Allowed internet traffic on SIM card").addValue("Disallowed", "Disallowed internet traffic on SIM card");
    return builder.build();
  }

  public static String getNoteTooltip() {
    return new TooltipHeaderBuilder("Note for SIM card").build();
  }

  public static String getUserMessageTooltip() {
    return new TooltipHeaderBuilder("Last USSD messages from the scripts").build();
  }

  public static String getTariffPlanExpiresTooltip() {
    return new TooltipHeaderBuilder("Time to left to expire tariff plan on SIM card").build();
  }

  public static String getChannelConnectionStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Channel connection state");
    builder.createImageTable().addValue(getResourceToString("/img/channels/live.png"), "Channel is connected")
        .addValue(getResourceToString("/img/channels/unlive.png"), "Channel is disconnected");
    return builder.build();
  }

  public static String getChannelLicenseStateTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("Channel license state");
    TooltipHeaderBuilder.ImageTable imageTable = builder.createImageTable();
    EnumSet.allOf(ChannelConfig.ChannelState.class).forEach(s -> imageTable.addValue(getChannelLicenseStateImage(s), getChannelLicenseStateText(s)));
    imageTable.addValue(getResourceToString("/img/channels/license_expiring.png"), "Channel license expires after 3 days");
    return builder.build();
  }

  private static String getChannelLicenseStateImage(final ChannelConfig.ChannelState state) {
    switch (state) {
      case ACTIVE:
        return getResourceToString("/img/channels/license_correct.png");
      case LICENSE_INVALID:
        return getResourceToString("/img/channels/license_invalid.png");
      case LICENSE_EXPIRE:
        return getResourceToString("/img/channels/license_expire.png");
      case IP_CONFIG_PARAMETERS_INVALID:
        return getResourceToString("/img/channels/ip_config_parameters_invalid.png");
      case SET_IP_CONFIG_ERROR:
        return getResourceToString("/img/channels/set_ip_config_error.png");
      case LICENSE_LOCK:
        return getResourceToString("/img/channels/outdated_firmware_version.png");
      default:
        return null;
    }
  }

  private static String getChannelLicenseStateText(final ChannelConfig.ChannelState state) {
    switch (state) {
      case ACTIVE:
        return "License is active";
      case LICENSE_INVALID:
        return "Channel has invalid license";
      case LICENSE_EXPIRE:
        return "Channel has expired license";
      case IP_CONFIG_PARAMETERS_INVALID:
        return "Channel get invalid one of ip config parameters";
      case SET_IP_CONFIG_ERROR:
        return "Channel has setting ip config error";
      case LICENSE_LOCK:
        return "Channel has locked license";
      default:
        return null;
    }
  }

  private static String getResourceToString(final String name) {
    return VoiceServerChannelsTableTooltips.class.getResource(name).toString();
  }

}
