/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PropertyEditorsSource;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptInstanceEditor;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptInstancesListEditor;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.awt.*;

import javax.swing.*;

public class SimGroupScriptConflictEditorPanel extends JPanel {
  private static final long serialVersionUID = 7562438256496949523L;

  private final EditorsPool left = new EditorsPool();
  private final EditorsPool right = new EditorsPool();

  public SimGroupScriptConflictEditorPanel() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    addPanel(left.scriptCommonsEditor, right.scriptCommonsEditor);
    addPanel(left.scriptCallFilter, right.scriptCallFilter);
    addPanel(left.scriptSmsFilters, right.scriptSmsFilters);
    addPanel(left.scriptIMEIGenerator, right.scriptIMEIGenerator);
    addPanel(left.scriptsBussinessActivity, right.scriptsBussinessActivity);
    addPanel(left.scriptsActionProvider, right.scriptsActionProvider);
    addPanel(left.scriptSSFactor, right.scriptSSFactor);
    addPanel(left.scriptGatewaySelector, right.scriptGatewaySelector);
    addPanel(left.scriptVSFactor, right.scriptVSFactor);
    addPanel(left.scriptIncomingCallManagement, right.scriptIncomingCallManagement);
    addPanel(left.scriptActivityPeriod, right.scriptActivityPeriod);
    addPanel(left.scriptSession, right.scriptSession);
    left.setEditable(true);
    right.setEditable(true);
  }

  public void initialize(final AntraxPluginsStore leftPS, final AntraxPluginsStore rightPS, final RegistryAccess registry) {
    left.initialize(leftPS, registry);
    right.initialize(rightPS, registry);
  }

  public void setConflict(final SimGroupScriptModificationConflict conflict) {
    left.setGroup(conflict.getOldGroup());
    right.setGroup(conflict.getNewGroup());
  }

  private void addPanel(final Component a, final Component b) {
    add(new ConnectedPanel(a, b));
  }

  public void applyConflict() {
    right.applyConflict();
  }

  private static class EditorsPool {
    private final ScriptCommonsEditor scriptCommonsEditor = new ScriptCommonsEditor();
    private final ScriptInstancesListEditor scriptsBussinessActivity = new ScriptInstancesListEditor("Business Activity Scripts", ScriptType.BUSINESS_ACTIVITY);
    private final ScriptInstancesListEditor scriptsActionProvider = new ScriptInstancesListEditor("Action Provider Scripts", ScriptType.ACTION_PROVIDER);
    private final ScriptInstanceEditor scriptSSFactor = new ScriptInstanceEditor("SIM Server Factor Script", ScriptType.FACTOR);
    private final ScriptInstanceEditor scriptGatewaySelector = new ScriptInstanceEditor("Gateway Selector Script", ScriptType.GATEWAY_SELECTOR);
    private final ScriptInstanceEditor scriptVSFactor = new ScriptInstanceEditor("Voice Server Factor Script", ScriptType.FACTOR);
    private final ScriptInstanceEditor scriptIncomingCallManagement = new ScriptInstanceEditor("Incoming Call Management Script", ScriptType.INCOMING_CALL_MANAGEMENT);
    private final ScriptInstanceEditor scriptActivityPeriod = new ScriptInstanceEditor("Activity Period Script", ScriptType.ACTIVITY_PERIOD);
    private final ScriptInstanceEditor scriptSession = new ScriptInstanceEditor("Session Period Script", ScriptType.ACTIVITY_PERIOD);
    private final ScriptInstanceEditor scriptIMEIGenerator = new ScriptInstanceEditor("IMEI Generator Script", ScriptType.IMEI_GENERATOR);
    private final ScriptInstancesListEditor scriptCallFilter = new ScriptInstancesListEditor("Call Filter", ScriptType.CALL_FILTER);
    private final ScriptInstancesListEditor scriptSmsFilters = new ScriptInstancesListEditor("SMS Filter", ScriptType.SMS_FILTER);

    private SIMGroup group;

    private EditorsPool() {
      scriptCommonsEditor.registerSharedReferenceContainers(
          scriptCallFilter,
          scriptSmsFilters,
          scriptsBussinessActivity,
          scriptsActionProvider,
          scriptSSFactor,
          scriptGatewaySelector,
          scriptVSFactor,
          scriptIncomingCallManagement,
          scriptActivityPeriod,
          scriptSession,
          scriptIMEIGenerator);
    }

    public void initialize(final AntraxPluginsStore pluginsStore, final RegistryAccess registry) {
      PropertyEditorsSource propEditors = new PropertyEditorsSource(pluginsStore, registry);
      scriptCommonsEditor.initialize(pluginsStore, propEditors);
      scriptCallFilter.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptSmsFilters.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptsBussinessActivity.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptsActionProvider.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptSSFactor.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptGatewaySelector.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptVSFactor.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptIncomingCallManagement.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptActivityPeriod.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptSession.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
      scriptIMEIGenerator.initialize(pluginsStore, propEditors, pluginsStore.listDefinitions());
    }

    public void setEditable(final boolean editable) {
      scriptCommonsEditor.setEditable(editable);
      scriptCallFilter.setEditable(editable);
      scriptSmsFilters.setEditable(editable);
      scriptsBussinessActivity.setEditable(editable);
      scriptsActionProvider.setEditable(editable);
      scriptSSFactor.setEditable(editable);
      scriptGatewaySelector.setEditable(editable);
      scriptVSFactor.setEditable(editable);
      scriptIncomingCallManagement.setEditable(editable);
      scriptActivityPeriod.setEditable(editable);
      scriptSession.setEditable(editable);
      scriptIMEIGenerator.setEditable(editable);
    }

    public void setGroup(final SIMGroup simGroup) {
      this.group = simGroup;
      scriptCommonsEditor.setScriptCommons(simGroup.listScriptCommons());
      scriptCallFilter.setScriptInstances(simGroup.getCallFilterScripts(), scriptCommonsEditor);
      scriptSmsFilters.setScriptInstances(simGroup.getSmsFilterScripts(), scriptCommonsEditor);
      scriptsBussinessActivity.setScriptInstances(simGroup.getBusinessActivityScripts(), scriptCommonsEditor);
      scriptsActionProvider.setScriptInstances(simGroup.getActionProviderScripts(), scriptCommonsEditor);
      scriptSSFactor.setScriptInstance(simGroup.getFactorScript(), scriptCommonsEditor);
      scriptGatewaySelector.setScriptInstance(simGroup.getGWSelectorScript(), scriptCommonsEditor);
      scriptVSFactor.setScriptInstance(simGroup.getVSFactorScript(), scriptCommonsEditor);
      scriptIncomingCallManagement.setScriptInstance(simGroup.getIncomingCallManagementScript(), scriptCommonsEditor);
      scriptActivityPeriod.setScriptInstance(simGroup.getActivityPeriodScript(), scriptCommonsEditor);
      scriptSession.setScriptInstance(simGroup.getSessionScript(), scriptCommonsEditor);
      scriptIMEIGenerator.setScriptInstance(simGroup.getIMEIGeneratorScript(), scriptCommonsEditor);
    }

    public void applyConflict() {
      group.setScriptCommons(scriptCommonsEditor.listScriptCommons());
      group.setCallFilterScripts(scriptCallFilter.getScriptInstances());
      group.setSmsFilterScripts(scriptSmsFilters.getScriptInstances());
      group.setBusinessActivityScripts(scriptsBussinessActivity.getScriptInstances());
      group.setActionProviderScripts(scriptsActionProvider.getScriptInstances());
      group.setFactorScript(scriptSSFactor.getScriptInstance());
      group.setGWSelectorScript(scriptGatewaySelector.getScriptInstance());
      group.setVSFactorScript(scriptVSFactor.getScriptInstance());
      group.setIncomingCallManagementScript(scriptIncomingCallManagement.getScriptInstance());
      group.setActivityPeriodScript(scriptActivityPeriod.getScriptInstance());
      group.setSessionScript(scriptSession.getScriptInstance());
      group.setIMEIGeneratorScript(scriptIMEIGenerator.getScriptInstance());
    }
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    frame.setContentPane(new SimGroupScriptConflictEditorPanel());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }
}
