/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.activity.ISimCallHistoryManager;
import com.flamesgroup.antrax.activity.ISimHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.control.server.VoipAntiSpam;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.antrax.storage.dao.ISIMHistoryDAO;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.SIMEvent;
import com.flamesgroup.unit.ICCID;

import java.util.Date;

public class SimHistoryLogger implements ISimHistoryLogger {

  private final VoipAntiSpam voipAntiSpam;
  private final ISimCallHistoryManager simCallHistoryManager;
  private final ISIMHistoryDAO dao;
  private final ICallDAO callDao;

  public SimHistoryLogger(final VoipAntiSpam voipAntiSpam, final ISimCallHistoryManager simCallHistoryManager, final ISIMHistoryDAO dao, final ICallDAO callDao) {
    this.voipAntiSpam = voipAntiSpam;
    this.simCallHistoryManager = simCallHistoryManager;
    this.dao = dao;
    this.callDao = callDao;
  }

  @Override
  public void handleEvent(final SimEvent simEvent) {
    if (simEvent.getEventType() == SIMEvent.INCOMING_CALL_ENDED || simEvent.getEventType() == SIMEvent.OUTGOING_CALL_ENDED) {
      logCDR((CDR) simEvent.getArg(0), simEvent.getEventType(), simEvent.getDate());
    } else if (simEvent.getEventType() == SIMEvent.BUSINESS_ACTIVITY_STARTED) {
      logStartedBusinessActivity(simEvent.getSim(), (String) simEvent.getArg(0), simEvent.getDate());
    } else if (simEvent.getEventType() == SIMEvent.BUSINESS_ACTIVITY_STOPED) {
      logEndedBusinessActivity(simEvent.getSim(), simEvent.getDate());
    } else if (simEvent.getEventType() == SIMEvent.AUTOMATION_ACTION_EXECUTION_STARTED) {
      logStartedActionExecution(simEvent);
    } else if (simEvent.getEventType() == SIMEvent.AUTOMATION_ACTION_EXECUTION_FAILED) {
      logEndedActionExecution(simEvent);
    } else if (simEvent.getEventType() == SIMEvent.AUTOMATION_ACTION_EXECUTION_STOPED) {
      logEndedActionExecution(simEvent);
    } else {
      saveEvent(simEvent);
    }
  }

  private void saveEvent(final SimEvent simEvent) {
    dao.simEventOccurred(simEvent.getSim(), simEvent.getEventType(), simEvent.getDate().getTime(), simEvent.getArgs());
  }

  private void logCDR(final CDR cdr, final SIMEvent event, final Date date) {
    long cdrID = callDao.insertCDR(cdr);

    voipAntiSpam.analyzeAcd(cdr.getCalledPhoneNumber().getValue(), cdr.getDuration());
    voipAntiSpam.analyzeAlerting(cdr.getCalledPhoneNumber().getValue(), cdr);
    voipAntiSpam.analyzeFas(cdr.getCalledPhoneNumber().getValue(), cdr);
    cdr.getCallPath().stream().filter(cp -> cp.getSimUID() != null).forEach(cp ->
        dao.simEventOccurred(cp.getSimUID(), event, date.getTime(), new String[] {String.valueOf(cdrID), String.valueOf(cdr.getDuration())}));

    if (cdr.getCallType() == CallType.VOIP_TO_GSM) {
      cdr.getCallPath().stream().filter(cp -> cp.getSimUID() != null).forEach(cp -> simCallHistoryManager.updateHistory(cp.getSimUID(), cdr.getCalledPhoneNumber()));
    }
  }

  private void logStartedBusinessActivity(final ICCID simUID, final String businessDescription, final Date date) {
    dao.simEventOccurred(simUID, SIMEvent.BUSINESS_ACTIVITY_STARTED, date.getTime(), new String[] {businessDescription});
  }

  private void logEndedBusinessActivity(final ICCID simUID, final Date date) {
    dao.simEventOccurred(simUID, SIMEvent.BUSINESS_ACTIVITY_STOPED, date.getTime(), new String[] {});
  }

  private void logEndedActionExecution(final SimEvent simEvent) {
    dao.simEventOccurred(simEvent.getSim(), simEvent.getEventType(), simEvent.getDate().getTime(), simEvent.getArgs());
  }

  private void logStartedActionExecution(final SimEvent simEvent) {
    dao.simEventOccurred(simEvent.getSim(), simEvent.getEventType(), simEvent.getDate().getTime(), simEvent.getArgs());
  }

}
