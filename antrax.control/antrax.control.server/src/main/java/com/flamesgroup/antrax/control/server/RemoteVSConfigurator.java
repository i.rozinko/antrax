/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class RemoteVSConfigurator implements IRemoteVSConfigurator {

  private static final Logger logger = LoggerFactory.getLogger(RemoteVSConfigurator.class);

  private final IConfigViewDAO configViewDAO;
  private final ISimpleConfigEditDAO simpleConfigEditDAO;
  private final ISimpleConfigViewDAO simpleConfigViewDAO;

  public RemoteVSConfigurator(final IConfigViewDAO configViewDAO, final ISimpleConfigEditDAO simpleConfigEditDAO, final ISimpleConfigViewDAO simpleConfigViewDAO) {
    this.configViewDAO = configViewDAO;
    this.simpleConfigEditDAO = simpleConfigEditDAO;
    this.simpleConfigViewDAO = simpleConfigViewDAO;
  }

  @Override
  public void saveScript(final ICCID uid, final ScriptType scriptType, final byte[] script) throws RemoteException {
    simpleConfigEditDAO.saveScript(uid, scriptType, script);
  }

  @Override
  public long getLatestScriptsRevision() {
    return Revisions.getInstance().getScriptFilesRevision();
  }

  @Override
  public ScriptFile getLastScriptFile() throws RemoteException {
    return configViewDAO.getLastScriptFile();
  }

  @Override
  public long getLatestGSMToSIMGroupLinksRevision() throws RemoteException {
    return Revisions.getInstance().getGsmToSimGroupLinksRevision();
  }

  @Override
  public LinkWithSIMGroup[] listSIMGSMLinks() throws RemoteException {
    try {
      return configViewDAO.listSIMGSMLinks();
    } catch (DataSelectionException e) {
      logger.error("[{}] - can't get linkWithSIMGroups", this, e);
      return new LinkWithSIMGroup[0];
    }
  }

  @Override
  public byte[] getBusinessActivityScript(final ICCID iccid) {
    return simpleConfigViewDAO.getBusinessActivityScript(iccid);
  }

  @Override
  public byte[] getActionProviderScript(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.getActionProviderScript(iccid);
  }

  @Override
  public byte[] getIncomingCallManagementScript(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.getIncomingCallManagementScript(iccid);
  }

  @Override
  public byte[] getCallFilterScript(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.getCallFilterScript(iccid);
  }

  @Override
  public byte[] getSmsFilterScript(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.getSmsFilterScript(iccid);
  }

  @Override
  public byte[] getVSFactorScript(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.getVSFactorScript(iccid);
  }

  @Override
  public GSMChannel getGSMChannel(final ChannelUID channelUID) throws RemoteException {
    return configViewDAO.getGSMChannel(channelUID);
  }

  @Override
  public void updateResetVsScriptsFlag(final ICCID iccid, final boolean flag) throws RemoteException {
    simpleConfigEditDAO.updateResetVsScriptsFlag(iccid, flag);
  }

  @Override
  public boolean shouldResetVsScripts(final ICCID iccid) throws RemoteException {
    return simpleConfigViewDAO.shouldResetVsScripts(iccid);
  }

}
