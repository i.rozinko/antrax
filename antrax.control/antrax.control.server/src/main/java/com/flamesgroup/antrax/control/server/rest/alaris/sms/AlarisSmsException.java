/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rest.alaris.sms;

import javax.ws.rs.core.Response;

public class AlarisSmsException extends RuntimeException {

  private static final long serialVersionUID = -8423308868470577876L;

  private Response.Status status = null;

  public AlarisSmsException() {
  }

  public AlarisSmsException(final String message) {
    super(message);
  }

  public AlarisSmsException(final String message, final Response.Status status) {
    super(message);
    this.status = status;
  }

  public AlarisSmsException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public AlarisSmsException(final String message, final Throwable cause, final Response.Status status) {
    super(message, cause);
    this.status = status;
  }

  public AlarisSmsException(final Throwable cause) {
    super(cause);
  }

  public AlarisSmsException(final Throwable cause, final Response.Status status) {
    super(cause);
    this.status = status;
  }

  public Response.Status getStatus() {
    return status;
  }

}
