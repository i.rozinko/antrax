/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rest.alaris.sms;


import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

@Path("/api")
public class AlarisSmsResource {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsResource.class);

  private static final AlarisSmsManager alarisSmsManager = AlarisSmsManager.getInstance();

  private static final String NULL_CHARACTER_TEMPLATE = "[\\000]+";
  private static final Pattern nullCharacterPattern = Pattern.compile(NULL_CHARACTER_TEMPLATE);

  @GET
  @Produces("text/html;charset=UTF-8")
  public String get(@Context final UriInfo ui, @Context final HttpServletRequest httpServletRequest) throws AlarisSmsException {
    MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    if (queryParams.isEmpty()) {
      throw new AlarisSmsException("GET params are empty");
    }
    String clientIp = httpServletRequest.getRemoteAddr();
    String command = getQueryParam(queryParams, "command");
    switch (command) {
      case "submit":
        return processSubmitCommand(queryParams, clientIp);
      case "query":
        return processQueryCommand(queryParams);
      default:
        throw new AlarisSmsException(String.format("Unsupported command: [%s]", command));
    }
  }

  private String processSubmitCommand(final MultivaluedMap<String, String> queryParams, final String clientIp) {
    logger.debug("[{}] - try process submit command, queryParams: {}", this, queryParams);
    String ani = getQueryParam(queryParams, "ani");
    String dnis = getQueryParam(queryParams, "dnis");
    String message = getQueryParam(queryParams, "message");
    String serviceType = getQueryParam(queryParams, "serviceType");
    String longMessageModeString = queryParams.getFirst("longMessageMode");
    String sar = queryParams.getFirst("sar");
    Integer dataCoding = null;
    AlarisSms.LongMessageMode longMessageMode = AlarisSms.LongMessageMode.CUT;
    if (longMessageModeString != null) {
      try {
        longMessageMode = AlarisSms.LongMessageMode.valueOf(longMessageModeString.toUpperCase());
      } catch (IllegalArgumentException e) {
        throw new AlarisSmsException(String.format("Unsupported value: [%s] of param: [longMessageMode], must be one of: [%s] ", longMessageModeString,
            Arrays.toString(AlarisSms.LongMessageMode.values())), e);
      }
    }
    if (longMessageMode == AlarisSms.LongMessageMode.SPLIT || longMessageMode == AlarisSms.LongMessageMode.SPLIT_SAR) {
      try {
        dataCoding = Integer.parseInt(getQueryParam(queryParams, "dataCoding"));
      } catch (NumberFormatException e) {
        throw new AlarisSmsException(String.format("Unsupported value: [%s] of param: [dataCoding], must be only digits", getQueryParam(queryParams, "dataCoding")), e);
      }
      if (dataCoding < 0 || dataCoding > 8) {
        throw new AlarisSmsException(String.format("Unsupported value: [%s] of param: [dataCoding], must be from 0 to 8", dataCoding));
      }
    }
    Matcher nullCharacterMatcher = nullCharacterPattern.matcher(message);
    if (nullCharacterMatcher.find()) {
      logger.warn("[{}] - find NULL(0x0) character at message [{}], delete it from message", this, message);
      message = nullCharacterMatcher.replaceAll("");
    }

    UUID uuid = alarisSmsManager.sendSms(clientIp, ani, dnis, message, serviceType, longMessageMode, dataCoding, sar);
    logger.debug("[{}] - end process submit command, message_id: [{}]", this, uuid);
    return "{\"message_id\": \"" + uuid + "\"}";
  }

  private String processQueryCommand(final MultivaluedMap<String, String> queryParams) {
    logger.debug("[{}] - try process query command, queryParams: {}", this, queryParams);
    String messageId = getQueryParam(queryParams, "messageId");
    String username = getQueryParam(queryParams, "username");
    AlarisSms.Status status = alarisSmsManager.getSmsStatus(messageId, username);
    logger.debug("[{}] - end process query command messageId: [{}], status: [{}]", this, messageId, status);
    return "{\"status\":\"" + (status != null ? status.toString() : "message ID " + messageId + " not found") + "\"}";
  }

  private String getQueryParam(final MultivaluedMap<String, String> queryParams, final String param) {
    String res = queryParams.getFirst(param);
    if (res == null) {
      throw new AlarisSmsException(String.format("Can't get param: [%s] from query", param));
    }
    return res;
  }

}
