/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.configuration.diff.DiffUtil;
import com.flamesgroup.antrax.configuration.diff.ListContainer;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.IGsmViewBean;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.simserver.GsmUnitManager;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerBasicAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GsmViewBean implements IGsmViewBean {

  private static final Logger logger = LoggerFactory.getLogger(GsmViewBean.class);

  private final DAOProvider daoProvider;
  private final GsmUnitManager gsmUnitManager;
  private final IGsmViewManager gsmViewManager;

  private final Map<ClientUID, Map<String, List<GsmView>>> prevGsmViewAll = new ConcurrentHashMap<>();

  public GsmViewBean(final DAOProvider daoProvider, final GsmUnitManager gsmUnitManager, final IGsmViewManager gsmViewManager) {
    this.daoProvider = daoProvider;
    this.gsmUnitManager = gsmUnitManager;
    this.gsmViewManager = gsmViewManager;
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return true;
  }

  @Override
  public List<GraphComparator.Delta> getGsmViewDiff(final ClientUID clientUid, final String server) throws NotPermittedException {
    List<GsmView> newGsmViews = new ArrayList<>();
    try {
      newGsmViews = gsmViewManager.listGsmView(server).stream()
          .map(e -> new GsmView().setTrusted(e.isTrusted())
              .setServingCell(e.isServingCell())
              .setCellKey(e.getCellKey())
              .setFirstAppearance(e.getFirstAppearance())
              .setLastAppearance(e.getLastAppearance())
              .setNetworkSurveyLastRxLev(e.getNetworkSurveyLastRxLev())
              .setCellInfoLastRxLev(e.getCellInfoLastRxLev())
              .setLastMcc(e.getLastMcc())
              .setLastMnc(e.getLastMnc())
              .setNetworkSurveyNumberOccurrences(e.getNetworkSurveyNumberOccurrences())
              .setServingNumberOccurrences(e.getServingNumberOccurrences())
              .setNeighborsNumberOccurrences(e.getNeighborsNumberOccurrences())
              .setServerId(e.getServerId())
              .setServerName(e.getServerName())
              .setNotes(e.getNotes())
              .setSuccessfulCallsCount(e.getSuccessfulCallsCount())
              .setTotalCallsCount(e.getTotalCallsCount())
              .setCallsDuration(e.getCallsDuration())
              .setOutgoingSmsCount(e.getOutgoingSmsCount())
              .setIncomingSmsCount(e.getIncomingSmsCount())
              .setUssdCount(e.getUssdCount())
              .setAmountPdd(e.getAmountPdd())).collect(Collectors.toList());
    } catch (RemoteException e) {
      logger.error("[{}] - can't get GsmView", this, e);
    }
    Map<String, List<GsmView>> serversGsmView = prevGsmViewAll.getOrDefault(clientUid, new HashMap<>());
    List<GsmView> prevGsmViews = serversGsmView.getOrDefault(server, new ArrayList<>());
    List<GraphComparator.Delta> diff = GraphComparator.compare(new ListContainer(1, prevGsmViews), new ListContainer(1, newGsmViews), DiffUtil.getIdFetcher());
    serversGsmView.put(server, newGsmViews);
    prevGsmViewAll.put(clientUid, serversGsmView);
    return diff;
  }

  @Override
  public List<GsmView> getGsmView(final ClientUID clientUid, final String server) throws NotPermittedException {
    List<GsmView> gsmViews = null;
    try {
      gsmViews = new ArrayList<>(gsmViewManager.listGsmView(server));
    } catch (RemoteException e) {
      logger.error("[{}] - can't get GsmView", this, e);
    }
    Map<String, List<GsmView>> serversGsmView = prevGsmViewAll.getOrDefault(clientUid, new HashMap<>());
    serversGsmView.put(server, gsmViews);
    prevGsmViewAll.put(clientUid, serversGsmView);
    return gsmViews;
  }

  @Override
  public Map<String, Date> getLastNetworkSurvey(final ClientUID clientUid) throws NotPermittedException {
    return gsmUnitManager.getLastNetworkSurveyTime();
  }

  @Override
  public void saveGsmView(final ClientUID clientUid, final String server, final List<GsmView> gsmViews) throws NotPermittedException {
    IServerData serverData = daoProvider.getConfigViewDAO().getServerByName(server);

    gsmViews.forEach(gsmView -> gsmView.setServerId(serverData.getID()));

    try {
      gsmViewManager.updateRuntimeGsmView(server, gsmViews);
      gsmViewManager.setServerForUpdateCellAdjustments(server, true);
      gsmViewManager.setServerForUpdateImsiCatcher(server, true);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't set update for server [{}]", this, server, e);
    }
  }

  @Override
  public void clearGsmView(final ClientUID clientUID, final String server) throws NotPermittedException {
    logger.info("[{}] - clear Gsm View for server [{}], clientUID: [{}]", this, server, clientUID);
    IServerData serverData = daoProvider.getConfigViewDAO().getServerByName(server);

    daoProvider.getGsmViewDAO().clearGsmView(serverData.getID());

    try {
      gsmViewManager.clearGsmView(server);
      gsmViewManager.setServerForUpdateCellAdjustments(server, true);
      gsmViewManager.setServerForUpdateImsiCatcher(server, true);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't set update for server [{}]", this, server, e);
    }
  }

  @Override
  public CellEqualizerAlgorithm getCellEqualizerAlgorithm(final ClientUID clientUID, final String server) {
    IServerData serverData = daoProvider.getConfigViewDAO().getServerByName(server);

    return daoProvider.getGsmViewDAO().getCellEqualizerConfiguration(serverData.getID());
  }

  @Override
  public void updateCellEqualizerAlgorithm(final ClientUID clientUID, final String server, final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws NotPermittedException {
    IServerData serverData = daoProvider.getConfigViewDAO().getServerByName(server);

    cellEqualizerAlgorithm.setServerId(serverData.getID());
    try {
      if (cellEqualizerAlgorithm instanceof CellEqualizerBasicAlgorithm) {
        daoProvider.getGsmViewDAO().upsertCellEqualizerBasicAlgorithmConfiguration((CellEqualizerBasicAlgorithm) cellEqualizerAlgorithm);
      } else if (cellEqualizerAlgorithm instanceof CellEqualizerRandomAlgorithm) {
        daoProvider.getGsmViewDAO().upsertCellEqualizerRandomAlgorithmConfiguration((CellEqualizerRandomAlgorithm) cellEqualizerAlgorithm);
      } else if (cellEqualizerAlgorithm instanceof CellEqualizerStatisticAlgorithm) {
        daoProvider.getGsmViewDAO().upsertCellEqualizerStatisticAlgorithmConfiguration((CellEqualizerStatisticAlgorithm) cellEqualizerAlgorithm);
      } else {
        throw new IllegalArgumentException("Unsupported argument: " + cellEqualizerAlgorithm.getClass().getName());
      }
    } catch (DataModificationException e) {
      logger.error("[{}] - can't update cellEqualizerAlgorithm", this, e);
    }
  }

  @Override
  public void resetGsmViewStatistic(final ClientUID clientUID, final List<GsmView> gsmViews, final String server) throws NotPermittedException {
    try {
      gsmViewManager.resetGsmViewStatistic(gsmViews, server);
      gsmViewManager.setServerForUpdateImsiCatcher(server, true);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't reset GsmView Statistic for server [{}]", this, server, e);
    }
  }

}
