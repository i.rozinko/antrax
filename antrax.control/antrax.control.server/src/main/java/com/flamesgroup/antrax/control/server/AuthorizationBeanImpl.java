/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.control.authorization.AuthorizationData;
import com.flamesgroup.antrax.control.authorization.AuthorizationFailedException;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.communication.AuthorizationBean;
import com.flamesgroup.antrax.control.communication.VersionCheckData;
import com.flamesgroup.antrax.control.server.config.UsersConfig;
import com.flamesgroup.antrax.control.server.utils.SelfCleaner;
import com.flamesgroup.antrax.control.server.utils.SessionsPool;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AuthorizationBeanImpl implements AuthorizationBean, SelfCleaner {

  private final Logger logger = LoggerFactory.getLogger(AuthorizationBeanImpl.class);

  private final long REQUEST_LIFETIME = 10 * 60 * 1000; // 10 minutes will be enough

  private final Map<ClientUID, Pair<AuthorizationData, Long>> requests = new HashMap<>();

  private final UsersConfig usersConfig = new UsersConfig();
  private final SessionsPool sessionsPool = SessionsPool.getInstance();

  @Override
  public synchronized AuthorizationData requestAuthorization(final ClientUID client, final AuthorizationData data) throws AuthorizationFailedException {
    cleanup();
    logger.debug("[{}] - requested authorization from {}", this, client);
    // Checking authorization
    if (!usersConfig.contains(data.getName())) {
      logger.debug("[{}] - forbidding authorization, because there is no such user: {}", this, data.getName());
      throw new AuthorizationFailedException();
    }
    if (!data.checkPassword(usersConfig.getPasswd(data.getName()))) {
      logger.debug("[{}] - forbidding authorization, because password is wrong", this);
      throw new AuthorizationFailedException();
    }
    AuthorizationData retval;
    retval = new AuthorizationData(data.getName());
    requests.put(client, new Pair<>(retval, System.currentTimeMillis()));
    return retval;
  }

  @Override
  public synchronized void confirmAuthorization(final ClientUID client, final AuthorizationData data) throws AuthorizationFailedException {
    logger.debug("[{}] - authorization confirmed from {}", this, client);
    if (!requests.containsKey(client) || !requests.get(client).first().equals(data)) {
      logger.debug("[{}] - authorization confirmation failed, because user send wrong request", this);
      throw new AuthorizationFailedException();
    }
    if (!usersConfig.contains(data.getName())) {
      logger.debug("[{}] - authorization confirmation failed, because user in no longer exists: {}", this, data.getName());
      throw new AuthorizationFailedException();
    }
    requests.remove(client);
    if (!data.checkPassword(usersConfig.getPasswd(data.getName()))) {
      logger.debug("[{}] - authorization confirmation failed, because password is invalid", this);
      throw new AuthorizationFailedException();
    }
    try {
      sessionsPool.startSession(client, data.getName(), usersConfig.getGroup(data.getName()), RemoteServer.getClientHost());
    } catch (ServerNotActiveException ignored) {
      throw new AuthorizationFailedException();
    }
  }

  @Override
  public synchronized void cleanup() {
    Iterator<ClientUID> iter = requests.keySet().iterator();
    while (iter.hasNext()) {
      ClientUID key = iter.next();
      if (System.currentTimeMillis() - requests.get(key).second() > REQUEST_LIFETIME) {
        iter.remove();
      }
    }
  }

  @Override
  public VersionCheckData getVersionCheckData() {
    return new VersionCheckData();
  }
}
