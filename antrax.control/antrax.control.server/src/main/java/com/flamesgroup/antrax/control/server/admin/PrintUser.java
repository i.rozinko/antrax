/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.admin;

import com.flamesgroup.antrax.control.server.config.UsersConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Should be run as java application: print-user user_name user_group It will
 * ask you for password without any notifications. This application should be
 * wrapped with shell script.
 */
public class PrintUser {

  public static void main(final String[] args) {
    if (args.length != 3) {
      System.err.println("Wrong command input: " + Arrays.toString(args));
      die(1);
      return;
    }
    String user = args[0];
    String group = args[1];
    if (!user.matches("\\w+")) {
      System.err.println("Wrong user name: " + user);
      die(2);
      return;
    }
    if (!group.matches("\\w+")) {
      System.err.println("Wrong group name: " + group);
      die(3);
      return;
    }

    String passwd = null;
    try {
      passwd = readPass();
    } catch (IOException ignored) {
      System.exit(1);
    }
    System.out.println(user + "=" + group + ":" + UsersConfig.writePasswd(passwd));
  }

  private static void die(final int exitCode) {
    System.err.println("Usage: PrintUser user_name user_group");
    System.exit(exitCode);
  }

  private static String readPass() throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      return reader.readLine();
    } finally {
      reader.close();
    }
  }

}
