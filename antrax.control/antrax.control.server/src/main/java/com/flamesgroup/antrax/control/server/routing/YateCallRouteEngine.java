/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.routing;

import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.properties.ControlServerProperties;
import com.flamesgroup.antrax.control.server.PrefixMarkLists;
import com.flamesgroup.antrax.control.server.VoipAntiSpam;
import com.flamesgroup.antrax.control.server.VoipAntiSpamAnalyzeResult;
import com.flamesgroup.antrax.control.simserver.SimCallHistoryManager;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CallAllocationAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.commons.CallRouteConfig;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.yextmodule.IYExtM;
import com.flamesgroup.yextmodule.IYExtMHandler;
import com.flamesgroup.yextmodule.IYExtMMessageHandler;
import com.flamesgroup.yextmodule.Message;
import com.flamesgroup.yextmodule.YExtM;
import com.flamesgroup.yextmodule.YExtMChannel;
import com.flamesgroup.yextmodule.YExtMConnection;
import com.flamesgroup.yextmodule.YExtMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;

public class YateCallRouteEngine implements IConfigurationHandler {

  private static final Logger logger = LoggerFactory.getLogger(YateCallRouteEngine.class);

  private final VoipAntiSpam voipAntiSpam;
  private final PrefixMarkLists prefixMarkLists;

  private final SocketAddress yateSocketAddress;
  private final YateCallRouteEngine.YExtMHandler yExtMHandler = new YateCallRouteEngine.YExtMHandler();
  private final List<IYExtMMessageHandler> messageHandlers = new ArrayList<>();

  private final List<String> rerouteReasons;
  private final Map<String, TrunkOptions> trunkOptionsMap;

  private final Map<String, CallContext> incomingCallContexts = new ConcurrentHashMap<>();
  private final Map<String, CallContext> outgoingCallContexts = new ConcurrentHashMap<>();

  private final Lock messageProcessLock = new ReentrantLock();

  private final AtomicReference<Map<String, RouteServer>> servers = new AtomicReference<>(new HashMap<>());

  private final AtomicReference<CallAllocationAlgorithm> callAllocationAlgorithm = new AtomicReference<>();

  private IYExtM yExtM;
  private ExecutorService dispatchExecutor;
  private SimCallHistoryManager simCallHistoryManager;

  public YateCallRouteEngine(final VoipAntiSpam voipAntiSpam, final PrefixMarkLists prefixMarkLists, final SimCallHistoryManager simCallHistoryManager) throws UnknownHostException {
    Objects.requireNonNull(voipAntiSpam, "voipAntiSpam mustn't be null");
    Objects.requireNonNull(prefixMarkLists, "prefixMarkLists mustn't be null");
    Objects.requireNonNull(simCallHistoryManager, "simCallHistoryManager mustn't be null");
    this.voipAntiSpam = voipAntiSpam;
    this.prefixMarkLists = prefixMarkLists;
    this.simCallHistoryManager = simCallHistoryManager;

    ControlServerProperties controlServerProperties = ControlPropUtils.getInstance().getControlServerProperties();
    InetAddress yateAddress = InetAddress.getByName(controlServerProperties.getExternalModuleYateAddress());
    int yatePort = controlServerProperties.getExternalModuleYatePort();

    yateSocketAddress = new InetSocketAddress(yateAddress, yatePort);

    rerouteReasons = controlServerProperties.getCallRerouteReasons();
    trunkOptionsMap = ControlPropUtils.getInstance().getControlServerProperties().getTrunkOptionsMap();
  }

  public void start() throws IOException, YExtMException, InterruptedException {
    if (yExtM != null) {
      throw new IllegalStateException("IYExtM already started");
    }

    YExtM yExtMLocal = new YExtM(new YExtMConnection(new YExtMChannel()));
    yExtMLocal.connect(yateSocketAddress, yExtMHandler);

    yExtM = yExtMLocal;
    dispatchExecutor = Executors.newCachedThreadPool(r -> {
      Thread thread = new Thread(r);
      thread.setName("DispatchExecutor@" + thread.hashCode());
      return thread;
    });

    List<IYExtMMessageHandler> messageHandlersLocal = new ArrayList<>();
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "call.preroute", this::callPrerouteHandler));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "call.route", this::callRouteHandler));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "call.ringing", this::finishRouting));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "call.answered", this::finishRouting));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "call.forward", this::callForwardHandler));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "chan.hangup", this::chanHangupHandler));
    messageHandlersLocal.add(createIYExtMMessageHandler(5, "chan.connected", this::chanConnectedHandler));

    try {
      for (final IYExtMMessageHandler messageHandler : messageHandlersLocal) {
        yExtM.install(messageHandler);
        messageHandlers.add(messageHandler);
      }
    } catch (YExtMException | IOException e) {
      stop();
      throw e;
    }
  }

  public void stop() {
    if (yExtM == null) {
      return;
    }

    for (final IYExtMMessageHandler messageHandler : messageHandlers) {
      try {
        yExtM.uninstall(messageHandler);
      } catch (YExtMException | IOException | InterruptedException e) {
        logger.warn("[{}] - can't uninstall massage handler [{}]", this, messageHandler.getName(), e);
      }
    }
    messageHandlers.clear();

    dispatchExecutor.shutdown();
    try {
      while (!dispatchExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of DispatchExecutor", this);
      }
    } catch (InterruptedException e) {
      logger.debug("[{}] - while awaiting completion of DispatchExecutor", this, e);
    }

    try {
      yExtM.disconnect();
    } catch (IOException e) {
      logger.warn("[{}] - can't disconnect from yExtM", this, e);
    }

    yExtM = null;
  }

  @Override
  public void reconfigure(final IConfigViewDAO configViewDAO) {
    RouteConfig routeConfig = configViewDAO.getRouteConfig();
    if (routeConfig == null) {
      logger.warn("[{}] - no route config found for servers", this);
      return;
    }

    Map<String, RouteServer> localServers = new HashMap<>();
    for (final IServerData server : configViewDAO.listServers()) {
      if (server.isVoiceServerEnabled()) {
        CallRouteConfig callRouteConfig = server.getVoiceServerConfig().getCallRouteConfig();
        if (callRouteConfig == null) {
          logger.warn("[{}] - no call route config found for server [{}]", this, server.getName());
          continue;
        }
        localServers.put(server.getName(), new RouteServer(server.getName(), callRouteConfig, trunkOptionsMap));
      }
    }

    callAllocationAlgorithm.set(routeConfig.getCallAllocationAlgorithm());
    servers.set(Collections.unmodifiableMap(localServers));
  }

  public void ping(final Server server, final IVoiceServerStatus voiceServerStatus) {
    Map<String, RouteServer> localServers = servers.get();
    if (!localServers.containsKey(server.getName())) {
      return;
    }
    long limit = voiceServerStatus.getChannelsInfo().stream().filter(IVoiceServerChannelsInfo::isRegistered).count();
    List<ICCID> readChannelUIDS = voiceServerStatus.getChannelsInfo().stream().filter(i -> i.getChannelState() != null && i.getChannelState().getState() == CallChannelState.State.READY_TO_CALL)
        .map(e -> e.getSimChannelInfo().getSimUID()).collect(Collectors.toList());
    localServers.get(server.getName()).updateFreeChannels(readChannelUIDS, (int) limit);
  }

  private Boolean callPrerouteHandler(final Message message) {
    if (servers.get().size() == 0) {
      logger.warn("[{}] - no route servers to route", message.getName());
      message.setRetvalue("-");
      message.getParams().put("error", "channel-congestion");
      message.getParams().put("reason", "channel-congestion");
      return true;
    }

    String channelId = message.getParams().get("id");
    String caller = message.getParams().get("caller");
    logger.debug("[{}] - incoming call on channel [{}] and caller [{}]", message.getName(), channelId, caller);
    CallContext callContext;
    messageProcessLock.lock();
    try {
      callContext = incomingCallContexts.get(channelId);
      if (callContext == null) {
        callContext = new CallContext(channelId, caller);
        callContext.state.set(CallState.ACCEPTED);
        incomingCallContexts.put(channelId, callContext);
        return false;
      }

      callContext.lock();
      try {
        message.setRetvalue("-");
        message.getParams().put("error", "rejected");
        message.getParams().put("reason", "rejected");
        incomingCallContexts.remove(channelId);
        return true;
      } finally {
        callContext.unlock();
      }
    } finally {
      messageProcessLock.unlock();
    }
  }

  private Boolean callRouteHandler(final Message message) {
    String channelId = message.getParams().get("id");
    String called = message.getParams().get("called");
    String caller = message.getParams().get("caller");

    if (called == null) {
      logger.debug("[{}] - called param is missing: [{}]", message.getName(), message);
      return false;
    }

    if (voipAntispamFilter(caller, called, channelId, message)) {
      return true;
    }

    CallContext callContext = incomingCallContexts.get(channelId);
    if (callContext == null) {
      logger.info("[{}] - call [{} --> {}] on unknown channel [{}]", message.getName(), caller, called, channelId);
      message.setRetvalue("-");
      message.getParams().put("error", "unknown-channel");
      message.getParams().put("reason", "unknown-channel");
      return true;
    }

    callContext.lock();
    try {
      logger.debug("[{}] - received [call.route]", callContext);
      if (!callContext.state.compareAndSet(CallState.ACCEPTED, CallState.ROUTING)) {
        logger.info("[{}] - not in allowed state. Rejecting", callContext);
        message.setRetvalue("-");
        message.getParams().put("error", "rejected");
        message.getParams().put("reason", "rejected");
        return true;
      }

      callContext.called = called;
      setRouteServerIterator(callContext);
      RouteServer.RouteDirection route = callContext.findNextRoute();
      if (route == null) {
        logger.debug("[{}] - no route for call", callContext);
        message.setRetvalue("-");
        if (callContext.isCallToSimUidEnable()) {
          message.getParams().put("error", "busy");
          message.getParams().put("reason", "busy");
          logger.debug("[{}] - call to the same sim is enabled, so the reason changed to: [{}]", callContext, message.getParams().get("reason"));
        } else {
          message.getParams().put("error", ControlPropUtils.getInstance().getCallRouteErrorsProperties().getCallNorouteError());
          message.getParams().put("reason", ControlPropUtils.getInstance().getCallRouteErrorsProperties().getCallNorouteReason());
        }
        return true;
      }

      dispatchExecutor.execute(() -> makeOutgoingCall(callContext, route));

      Map<String, String> params = message.getParams();
      params.put("autoring", "false");
      params.put("autoprogress", "false");
      params.put("earlymedia", "true");
      message.setRetvalue("dumb/");
      logger.trace("[{}] - routed to dumb chan", callContext);
      return true;
    } finally {
      callContext.unlock();
    }
  }

  private Boolean callForwardHandler(final Message message) {
    String channelId = message.getParams().get("id");
    String caller = message.getParams().get("caller");
    String called = message.getParams().get("called");

    if (voipAntispamFilter(caller, called, channelId, message)) {
      return true;
    }

    CallContext callContext = incomingCallContexts.get(channelId);
    if (callContext == null) {
      logger.info("[{}] - call [{} --> {}] on unknown channel [{}]", message.getName(), caller, called, channelId);
      return false;
    }

    callContext.lock();
    try {
      logger.debug("[{}] - received [call.forward]", callContext);
      if (!callContext.state.compareAndSet(CallState.ACCEPTED, CallState.ROUTING)) {
        logger.info("[{}] - not in allowed state. Rejecting", callContext);
        return false;
      }

      callContext.called = called;
      setRouteServerIterator(callContext);
      RouteServer.RouteDirection route = callContext.findNextRoute();
      if (route == null) {
        logger.debug("[{}] - no route for call", callContext);
        message.setRetvalue("-");
        if (callContext.isCallToSimUidEnable()) {
          message.getParams().put("error", "busy");
          message.getParams().put("reason", "busy");
          logger.debug("[{}] - call to the same sim is enabled, so the reason changed to: [{}]", callContext, message.getParams().get("reason"));
        } else {
          message.getParams().put("error", "channel-congestion");
          message.getParams().put("reason", "channel-congestion");
        }
        return false;
      }
      dispatchExecutor.execute(() -> makeOutgoingCall(callContext, route));

      logger.trace("[{}] - searching route to forward ", callContext);
      return true;
    } finally {
      callContext.unlock();
    }
  }

  private Boolean chanConnectedHandler(final Message message) {
    String channelId = message.getParams().get("id");
    String peerId = message.getParams().get("peerid");

    CallContext callContext = incomingCallContexts.get(channelId);
    if (callContext != null) {
      callContext.lock();
      try {
        callContext.peerId = peerId;
        logger.trace("[{}] - connected to [{}]", callContext, peerId);
      } finally {
        callContext.unlock();
      }
    }
    return false;
  }

  private Boolean chanHangupHandler(final Message message) {
    String direction = message.getParams().get("direction");
    if (direction.equals("incoming")) {
      return incomingHangupHandler(message);
    } else {
      return outgoingHangupHandler(message);
    }
  }

  private boolean incomingHangupHandler(final Message message) {
    String channelId = message.getParams().get("id");
    if (channelId.startsWith("dumb")) {
      return false;
    }

    CallContext callContext;
    messageProcessLock.lock();
    try {
      callContext = incomingCallContexts.get(channelId);
      if (callContext == null) {
        CallContext haltedCall = new CallContext(channelId, "");
        haltedCall.state.set(CallState.HALTED);
        incomingCallContexts.put(channelId, haltedCall);
        logger.debug("[{}] - channel [{}] hangup before preroute", message.getName(), channelId);
        return false;
      }
      incomingCallContexts.remove(callContext.incomingChannelId);
    } finally {
      messageProcessLock.unlock();
    }

    callContext.lock();
    try {
      logger.debug("[{}] - received incoming [chan.hangup], reason: [{}]", callContext, message.getParams().get("reason"));
      callContext.releaseRoute();
      if (callContext.state.get() != CallState.ROUTED && callContext.state.get() != CallState.CONNECTED) {
        if (callContext.outgoingChannelId != null) {
          if (message.getParams().get("reason") == null) {
            dropCallLeg(callContext.outgoingDumbChannelId, "rejected");
          } else {
            dropCallLeg(callContext.outgoingDumbChannelId, message.getParams().get("reason"));
          }
          outgoingCallContexts.remove(callContext.outgoingChannelId);
        }
      }
      callContext.state.set(CallState.HALTED);
    } finally {
      callContext.unlock();
    }
    return false;
  }

  private boolean outgoingHangupHandler(final Message message) {
    String channelId = message.getParams().get("id");
    if (!channelId.startsWith("iax")) {
      return false;
    }

    CallContext callContext = getCallContextWithAttempts(channelId);
    if (callContext == null) {
      logger.warn("[{}] - received outgoing [chan.hangup] without [call.execute] with id [{}]", this, channelId);
      return false;
    }

    callContext.lock();
    try {
      logger.debug("[{}] - received outgoing [chan.hangup], reason: [{}]", callContext, message.getParams().get("reason"));
      outgoingCallContexts.remove(callContext.outgoingChannelId);
      callContext.outgoingChannelId = null;
    } finally {
      callContext.unlock();
    }

    if (!rerouteReasons.contains(message.getParams().get("reason"))) {
      dropCallLeg(callContext.incomingChannelId, message.getParams().get("reason"));
      return true;
    }

    RouteServer.RouteDirection route = callContext.findNextRoute();
    if (route == null) {
      String reason;
      if (callContext.isCallToSimUidEnable()) {
        reason = "busy";
        logger.debug("[{}] - call to the same sim is enabled, so the reason changed to: [{}]", callContext, reason);
      } else {
        reason = "channel-congestion";
      }
      logger.debug("[{}] - no route for call, reason: [{}]", callContext, reason);
      dropCallLeg(callContext.incomingChannelId, reason);
      return true;
    }

    dispatchExecutor.execute(() -> makeOutgoingCall(callContext, route));
    return false;
  }

  private CallContext getCallContextWithAttempts(final String channelId) {
    for (int i = 0; i < 20; i++) {
      CallContext callContext = outgoingCallContexts.get(channelId);
      if (callContext == null) {
        try {
          Thread.sleep(50);
        } catch (InterruptedException e) {
          return null;
        }
      } else {
        return callContext;
      }
    }
    return null;
  }

  private Boolean finishRouting(final Message message) {
    String outgoingChannelId = message.getParams().get("id");
    CallContext callContext = outgoingCallContexts.get(outgoingChannelId);
    if (callContext == null) {
      return false;
    }

    callContext.lock();
    try {
      if (callContext.outgoingChannelId == null) {
        logger.info("[{}] - processing finishing message for call after received outgoing [chan.hangup]", callContext);
        return true;
      }

      switch (callContext.state.get()) {
        case ROUTING:
          callContext.state.set(CallState.ROUTED);
          break;
        case ROUTED:
          logger.trace("[{}] - duplicating [{}] on already routed channel", callContext, message.getName());
          dispatchExecutor.execute(() -> {
            callContext.lock();
            try {
              if (!duplicateMessage(message, callContext, outgoingChannelId)) {
                if (callContext.peerId == null || !duplicateMessage(message, callContext, callContext.peerId)) {
                  logger.info("[{}] - failed to duplicate message from [{}]: [{}]. Dropping call", callContext, outgoingChannelId, message);
                  dropCallLeg(callContext.incomingChannelId, "rejected");
                  dropCallLeg(outgoingChannelId, "rejected");
                  return;
                }
              }
              callContext.state.set(CallState.CONNECTED);
            } finally {
              callContext.unlock();
            }
          });
          return false;
        case CONNECTED:
          return false;
        default:
          logger.info("[{}] - received finishing message for call that is in wrong state: [{}]", callContext, callContext.state.get());
          return false;
      }

      String incomingChannelIdLocal = callContext.incomingChannelId;
      String outgoingChannelIdLocal = callContext.outgoingChannelId;
      logger.debug("[{}] - connecting to [{}]", callContext, callContext.outgoingChannelId);
      dispatchExecutor.execute(() -> connectCallChannels(incomingChannelIdLocal, outgoingChannelIdLocal));
      dispatchExecutor.execute(() -> {
        callContext.lock();
        try {
          if (callContext.peerId == null || !duplicateMessage(message, callContext, callContext.peerId)) {
            if (!duplicateMessage(message, callContext, outgoingChannelId)) {
              logger.info("[{}] - failed to duplicate message from [{}]: [{}]. Dropping call", callContext, outgoingChannelId, message);
              dropCallLeg(callContext.incomingChannelId, "rejected");
              dropCallLeg(outgoingChannelId, "rejected");
              return;
            }
          }
          callContext.state.set(CallState.CONNECTED);
        } finally {
          callContext.unlock();
        }
      });
    } finally {
      callContext.unlock();
    }
    return true;
  }

  private boolean duplicateMessage(final Message message, final CallContext callContext, final String peerId) {
    logger.debug("[{}] - forwarding [{}] from [{}]", callContext, message.getName(), peerId);
    Message forwardMessage = new Message("chan.masquerade");
    HashMap<String, String> forwardMessageParameters = new HashMap<>();
    forwardMessage.setParams(forwardMessageParameters);
    forwardMessageParameters.put("earlymedia", "true");
    forwardMessageParameters.put("message", message.getName());
    forwardMessageParameters.put("id", peerId);

    try {
      return yExtM.dispatch(forwardMessage);
    } catch (YExtMException | IOException e) {
      logger.warn("[{}] - can't dispatch message [{}]", callContext, forwardMessage, e);
      return false;
    } catch (InterruptedException e) {
      throw new AssertionError(e);
    }
  }

  private void connectCallChannels(final String incomingChannelId, final String outgoingChannelId) {
    Message connectMessage = new Message("chan.connect");
    HashMap<String, String> messageParameters = new HashMap<>();
    connectMessage.setParams(messageParameters);
    messageParameters.put("id", incomingChannelId);
    messageParameters.put("targetid", outgoingChannelId);

    try {
      yExtM.dispatch(connectMessage);
    } catch (YExtMException | IOException e) {
      logger.warn("[{}] - can't dispatch message [{}]", this, connectMessage, e);
    } catch (InterruptedException e) {
      throw new AssertionError(e);
    }
  }

  private void makeOutgoingCall(final CallContext callContext, final RouteServer.RouteDirection route) {
    callContext.lock();
    try {
      String calledWithPrefix = prefixMarkLists.markPhoneNumber(callContext.called);
      String calledParty = "iax/iax:" + route.getAddress();

      Message callMessage = new Message("call.execute");
      Map<String, String> callMessageParams = new HashMap<>();
      callMessage.setParams(callMessageParams);

      callMessageParams.put("caller", callContext.caller);
      callMessageParams.put("called", calledWithPrefix);
      if (callContext.getCallToSimUidList() != null) {
        callMessageParams.put("iaxcontext", callContext.getCallToSimUidList());
      }
      callMessageParams.put("target", calledWithPrefix);
      callMessageParams.put("direct", calledParty);
      callMessageParams.put("callto", "dumb/");
      callMessageParams.put("copyparams", "formats, trunkout, trunkin, trunk_sendinterval, trunk_maxlen, iaxcontext");
      callMessageParams.put("formats", route.getFormatString());
      if (route.getTrunkOptions() != null) {
        callMessageParams.put("trunkout", "true");
        callMessageParams.put("trunkin", "true");
        callMessageParams.put("trunk_sendinterval", route.getTrunkOptions().getSendIntervalMillis());
        callMessageParams.put("trunk_maxlen", route.getTrunkOptions().getMaxFrameLength());
      }

      logger.debug("[{}] - call routed to [{}]", callContext, route);
      boolean executeResult;
      try {
        executeResult = yExtM.dispatch(callMessage);
      } catch (YExtMException | IOException e) {
        logger.warn("[{}] - can't dispatch message [{}]. Dropping call", callContext, callMessage, e);
        dropCallLeg(callContext.incomingChannelId, "channel-congestion");
        return;
      } catch (InterruptedException e) {
        throw new AssertionError(e);
      }

      if (!executeResult) {
        logger.error("[{}] - no one can handle [call.execute]. Dropping call", callContext);
        dropCallLeg(callContext.incomingChannelId, "channel-congestion");
        return;
      }

      String peerid = callMessage.getParams().get("peerid");
      String outgoingDumbId = callMessage.getParams().get("id");
      logger.trace("[{}] - received party channel [{}]", callContext, peerid);
      callContext.outgoingChannelId = peerid;
      callContext.outgoingDumbChannelId = outgoingDumbId;
      outgoingCallContexts.put(peerid, callContext);
    } finally {
      callContext.unlock();
    }
  }

  private void dropCallLeg(final String channelId, final String reason) {
    Message message = new Message("call.drop");
    Map<String, String> messageParams = new HashMap<>();
    message.setParams(messageParams);
    messageParams.put("reason", reason);
    messageParams.put("id", channelId);

    try {
      yExtM.dispatch(message);
    } catch (YExtMException | IOException e) {
      logger.warn("[{}] - can't dispatch message [{}]", this, message, e);
    } catch (InterruptedException e) {
      throw new AssertionError(e);
    }
  }

  private boolean voipAntispamFilter(final String caller, final String called, final String channelId, final Message message) {
    String error = null;
    String reason = null;
    VoipAntiSpamAnalyzeResult analyzeResult = voipAntiSpam.isValidNumber(called);
    switch (analyzeResult) {
      case GRAY_NUMBER:
        error = ControlPropUtils.getInstance().getCallRouteErrorsProperties().getVoipantispamGraylistError();
        reason = ControlPropUtils.getInstance().getCallRouteErrorsProperties().getVoipantispamGraylistReason();
        break;
      case BLACK_NUMBER:
        error = ControlPropUtils.getInstance().getCallRouteErrorsProperties().getVoipantispamBlacklistError();
        reason = ControlPropUtils.getInstance().getCallRouteErrorsProperties().getVoipantispamBlacklistReason();
        break;
      case ANALYZE_ERROR:
        error = "channel-congestion";
        reason = "channel-congestion";
        break;
    }

    if (error != null && reason != null) {
      logger.debug("[{}] - call [{} --> {}] on channel [{}] dropped by VoIP Antispam", message.getName(), caller, called, channelId);
      message.setRetvalue("-");
      message.getParams().put("error", error);
      message.getParams().put("reason", reason);
      return true;
    } else {
      return false;
    }
  }

  private void setRouteServerIterator(final CallContext callContext) {
    List<RouteServer> routeServers;
    if (ControlPropUtils.getInstance().getControlServerProperties().getCallToSameSimCard()) {
      Map<ICCID, Integer> iccidForNumber = simCallHistoryManager.getICCIDForNumber(new PhoneNumber(callContext.called));
      logger.trace("[{}] - try create router to [{}], via servers {} and history iccid {}", this, callContext.called, servers, iccidForNumber);
      if (iccidForNumber != null && !iccidForNumber.isEmpty()) {
        callContext.setCallToSimUidEnable(true);
        routeServers = servers.get().values().stream().filter(server -> server.testCalled(callContext.called))
            .filter(s -> s.getFreeChannelUIDS().stream().anyMatch(iccidForNumber::containsKey)).collect(Collectors.toList());
        logger.trace("[{}] - routeServers after filter {}", this, routeServers);
        if (!routeServers.isEmpty()) {
          Map<RouteServer, Integer> maxCount = new HashMap<>();
          for (RouteServer routeServer : routeServers) {
            int maxRsCount = 0;
            for (ICCID iccid : routeServer.getFreeChannelUIDS()) {
              int curCount = iccidForNumber.getOrDefault(iccid, 0);
              maxRsCount = maxRsCount > curCount ? maxRsCount : curCount;
            }
            maxCount.put(routeServer, maxRsCount);
          }
          routeServers.sort((rs1, rs2) -> maxCount.get(rs2).compareTo(maxCount.get(rs1)));
          logger.trace("[{}] - routeServers after resort: {}", this, routeServers);

          callContext.setRouteServers(routeServers.iterator());

          final int maxSizeOfIccids = 12; // 255/(20 + 1) where 255 - max size of iax2 called context. 20 - max length of ICCID. 1 - length of delimiter.
          String descSortedIccid = iccidForNumber.keySet().stream().sorted((i1, i2) -> iccidForNumber.get(i2).compareTo(iccidForNumber.get(i1))).map(ICCID::getValue).limit(maxSizeOfIccids).collect(Collectors.joining(";"));
          logger.trace("[{}] - descSortedIccid: {}", this, descSortedIccid);
          callContext.setCallToSimUidList(descSortedIccid);
          return;
        }
      }
    }
    routeServers = servers.get().values().stream().filter(server -> server.testCalled(callContext.called)).collect(Collectors.toList());
    switch (callAllocationAlgorithm.get()) {
      case RANDOM: {
        Collections.shuffle(routeServers);
        break;
      }
      case UNIFORMLY_ACTIVE_CALLS: {
        Collections.sort(routeServers, createFactorComparator(this::countUniformlyActiveCallsFactor));
        break;
      }
      case UNIFORMLY_LOAD_CHANNELS: {
        Collections.sort(routeServers, createFactorComparator(this::countUniformlyLoadChannelsFactor));
        break;
      }
    }

    callContext.setRouteServers(routeServers.iterator());
  }

  private Comparator<RouteServer> createFactorComparator(final Function<RouteServer, Integer> factorFunction) {
    return (o1, o2) -> factorFunction.apply(o2) - factorFunction.apply(o1);
  }

  private int countUniformlyActiveCallsFactor(final RouteServer routeServer) {
    if (routeServer.getLimit() <= routeServer.getOccupy()) {
      return Integer.MIN_VALUE;
    } else {
      return routeServer.getPriority() - routeServer.getOccupy();
    }
  }

  private int countUniformlyLoadChannelsFactor(final RouteServer routeServer) {
    if (routeServer.getLimit() <= routeServer.getOccupy()) {
      return Integer.MIN_VALUE;
    } else {
      return (int) (routeServer.getPriority() * (1.0f - (float) routeServer.getOccupy() / (float) routeServer.getLimit()));
    }
  }

  private IYExtMMessageHandler createIYExtMMessageHandler(final int priority, final String name, final String filterName, final String filterValue, final Function<Message, Boolean> function) {
    return new IYExtMMessageHandler() {

      @Override
      public int getPriority() {
        return priority;
      }

      @Override
      public String getName() {
        return name;
      }

      @Override
      public String getFilterName() {
        return filterName;
      }

      @Override
      public String getFilterValue() {
        return filterValue;
      }

      @Override
      public boolean handleReceived(final Message message) {
        return function.apply(message);
      }

    };
  }

  private IYExtMMessageHandler createIYExtMMessageHandler(final int priority, final String name, final Function<Message, Boolean> function) {
    return createIYExtMMessageHandler(priority, name, null, null, function);
  }

  private enum CallState {
    ACCEPTED, ROUTING, ROUTED, CONNECTED, HALTED
  }

  private class CallContext {

    private final String incomingChannelId;
    private final String caller;
    private final AtomicReference<CallState> state = new AtomicReference<>();
    private final Lock lock = new ReentrantLock();

    private String peerId;
    private String called;
    private String outgoingChannelId;
    private String outgoingDumbChannelId;
    private String callToSimUidList = null;
    private boolean callToSimUidEnable = false;

    private Iterator<RouteServer> routeServers;
    private RouteServer.RouteDirection currentRouteDirection;
    private RouteServer currentRouteServer;

    CallContext(final String incomingChannelId, final String caller) {
      this.incomingChannelId = incomingChannelId;
      this.caller = caller;
    }

    void setRouteServers(final Iterator<RouteServer> routeServers) {
      this.routeServers = routeServers;
    }

    void setCallToSimUidList(final String callToSimUidList) {
      this.callToSimUidList = callToSimUidList;
    }

    public String getCallToSimUidList() {
      return callToSimUidList;
    }

    public boolean isCallToSimUidEnable() {
      return callToSimUidEnable;
    }

    public void setCallToSimUidEnable(final boolean callToSimUidEnable) {
      this.callToSimUidEnable = callToSimUidEnable;
    }

    void lock() {
      lock.lock();
    }

    void unlock() {
      lock.unlock();
    }

    RouteServer.RouteDirection findNextRoute() {
      releaseRoute();
      while (routeServers.hasNext()) {
        currentRouteServer = routeServers.next();

        routeServers.remove();
        currentRouteDirection = currentRouteServer.acquireRoute();
        if (currentRouteDirection != null) {
          return currentRouteDirection;
        }
      }
      return null;
    }

    void releaseRoute() {
      if (currentRouteDirection == null) {
        return;
      }

      currentRouteServer.releaseRoute(currentRouteDirection);
      currentRouteDirection = null;
      currentRouteServer = null;
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
          "[incomingChannelId:'" + incomingChannelId + "' call: '" +
          caller + "' --> '" + called +
          "']";
    }
  }

  private final class YExtMHandler implements IYExtMHandler {

    private final long reconnectTimeout;

    private final Lock processRestartLock = new ReentrantLock();
    private final AtomicBoolean processRestart = new AtomicBoolean();

    private YExtMHandler() {
      reconnectTimeout = ControlPropUtils.getInstance().getControlServerProperties().getExternalModuleReconnectTimeout();
    }

    @Override
    public void handleDisconnect() {
      logger.info("[{}] - handleDisconnect", this);
      restart();
    }

    @Override
    public void handleException(final IOException e) {
      logger.info("[{}] - handleException", this, e);
      restart();
    }

    private void restart() {
      boolean processRestartLocal;
      processRestartLock.lock();
      try {
        processRestartLocal = processRestart.compareAndSet(false, true);
      } finally {
        processRestartLock.unlock();
      }

      if (!processRestartLocal) {
        return;
      }

      new Thread(() -> {
        stop();

        incomingCallContexts.clear();
        outgoingCallContexts.clear();
        servers.get().values().forEach(RouteServer::reset);

        while (true) {
          try {
            Thread.sleep(reconnectTimeout);
            logger.info("[{}] - reconnect to Yate external module", this);
            start();
            logger.info("[{}] - reconnected", this);
            break;
          } catch (IOException | YExtMException e) {
            logger.warn("[{}] - can't reconnect to Yate external module, try more time late", this, e);
          } catch (InterruptedException e) {
            logger.warn("[{}] - was interrupted", this, e);
            break;
          }
        }

        processRestartLock.lock();
        try {
          processRestart.set(false);
        } finally {
          processRestartLock.unlock();
        }
      }, YateCallRouteEngine.class.getSimpleName()).start();
    }

  }

}
