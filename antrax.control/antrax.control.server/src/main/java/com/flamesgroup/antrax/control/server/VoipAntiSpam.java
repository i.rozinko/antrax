/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.utils.VoipAntiSpamHelper;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.BlackListConfig;
import com.flamesgroup.commons.voipantispam.FasConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class VoipAntiSpam {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpam.class);

  private final IVoipAntiSpamDAO voipAntiSpamDAO;
  private final ICallDAO callDAO;

  public VoipAntiSpam(final DAOProvider daoProvider) {
    Objects.requireNonNull(daoProvider, "daoProvider mustn't be null");
    this.voipAntiSpamDAO = daoProvider.getVoipAntiSpamDAO();
    this.callDAO = daoProvider.getCallDAO();
  }

  public VoipAntiSpamAnalyzeResult isValidNumber(final String number) {
    try {
      return checkValidNumber(number);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check number", this, e);
      return VoipAntiSpamAnalyzeResult.ANALYZE_ERROR;
    }
  }

  private VoipAntiSpamAnalyzeResult checkValidNumber(final String number) throws DataModificationException {
    Objects.requireNonNull(number, "number mustn't be null");
    final long currentTime = System.currentTimeMillis();
    logger.debug("[{}] - try to check for valid number [{}], currentTime [{}]", this, number, currentTime);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
      logger.debug("[{}] - number [{}] is valid: VoIp AntiSpam is disable", this, number);
      return VoipAntiSpamAnalyzeResult.NORMAL_NUMBER;
    }

    voipAntiSpamDAO.insertHistoryRoutingRequest(number, currentTime);

    if (voipAntiSpamDAO.incrementRoutingRequestWhiteNumber(number)) {
      logger.debug("[{}] - number [{}] is valid: Number at white list", this, number);
      return VoipAntiSpamAnalyzeResult.WHITE_NUMBER;
    }

    int routingCount;
    Set<String> numbers = new HashSet<>();
    numbers.add(number);
    if (voipAntiSpamDAO.incrementRoutingRequestBlackNumber(number)) {
      logger.debug("[{}] - number [{}] isn't valid: Number at black list", this, number);
      return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
    } else {
      for (BlackListConfig blackListConfig : voipAntiSpamConfiguration.getBlackListConfigs()) {
        long inPeriod = currentTime - blackListConfig.getPeriod() * 1000 * 60;
        routingCount = voipAntiSpamDAO.getRoutingCount(number, inPeriod);
        if (routingCount > blackListConfig.getMaxRoutingRequestPerPeriod()) {
          voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numbers);
          Set<BlackListNumber> blackListNumbers = numbers.stream().map(s -> {
            return new BlackListNumber(s, VoipAntiSpamListNumbersStatus.ROUTING, 0, VoipAntiSpamHelper.createDescriptionForBlackListRoutingFilter(blackListConfig), new Date());
          }).collect(Collectors.toSet());
          voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
          logger.debug("[{}] - number [{}] isn't valid: Number routingCount [{}] > BlackListMaxRoutingRequestPerPeriod [{}] then inserted blackListNumbers {}", this, number, routingCount,
              blackListConfig.getMaxRoutingRequestPerPeriod(), blackListNumbers);
          return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
        }
      }
    }

    int blockCount = 0;
    int routingRequestCount = 0;
    long blockTimeLeftGrayList = -1;
    GrayListNumber grayListNumber = voipAntiSpamDAO.incrementRoutingRequestGrayNumber(number);
    if (grayListNumber != null) {
      blockCount = grayListNumber.getBlockCount();
      blockTimeLeftGrayList = grayListNumber.getBlockTimeLeft().getTime();
      routingRequestCount = grayListNumber.getRoutingRequestCount();
    }

    boolean isBanned = false;
    if (blockTimeLeftGrayList > currentTime) {
      isBanned = true;
    }

    for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
      long inPeriod = currentTime - grayListConfig.getPeriod() * 1000 * 60;
      routingCount = voipAntiSpamDAO.getRoutingCount(number, inPeriod);
      if (routingCount > grayListConfig.getMaxRoutingRequestPerPeriod()) {
        long blockTimeLeft = currentTime + grayListConfig.getBlockPeriod() * 1000 * 60;
        voipAntiSpamDAO.insertHistoryBlockNumbers(number, blockTimeLeft);

        if (!isBanned) {
          blockCount++;
        }

        if (blockCount > grayListConfig.getMaxBlockCountBeforeMoveToBlackList()) {
          voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numbers);
          Set<BlackListNumber> blackListNumbers = numbers.stream().map(s -> {
            return new BlackListNumber(number, VoipAntiSpamListNumbersStatus.ROUTING, 0, VoipAntiSpamHelper.createDescriptionForBlackListBlockCountFilter(grayListConfig), new Date());
          }).collect(Collectors.toSet());
          voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
          logger.debug("[{}] - number [{}] isn't valid: Number blockCount [{}] > BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted blackListNumbers {}", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), blackListNumbers);
          return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
        } else {
          if (grayListNumber == null) {
            voipAntiSpamDAO.deleteVoipAntiSpamWhiteListNumbers(numbers);
          }
          Set<GrayListNumber> grayListNumbers = new HashSet<>();
          grayListNumbers.add(new GrayListNumber(number, VoipAntiSpamListNumbersStatus.ROUTING, routingRequestCount, new Date(currentTime), blockCount, new Date(blockTimeLeft),
              VoipAntiSpamHelper.createDescriptionForGrayListRoutingFilter(grayListConfig)));
          voipAntiSpamDAO.insertVoipAntiSpamGrayListNumbers(grayListNumbers);
          logger.debug("[{}] - number [{}] isn't valid: Number blockCount [{}] <= BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted grayListNumbers {}", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), grayListNumbers);
          return VoipAntiSpamAnalyzeResult.GRAY_NUMBER;
        }
      }
    }
    return VoipAntiSpamAnalyzeResult.NORMAL_NUMBER;
  }

  public void analyzeAcd(final String number, final long duration) {
    try {
      checkAcd(number, duration);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check acd", this, e);
    }
  }

  private void checkAcd(final String number, final long duration) throws DataModificationException {
    Objects.requireNonNull(number, "number mustn't be null");
    final long currentTime = System.currentTimeMillis();
    logger.debug("[{}] - try to analyze acd for number [{}], duration [{}], currentTime [{}]", this, number, duration, currentTime);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
      logger.debug("[{}] - analyze acd disable for number [{}]", this, number);
      return;
    }

    BlackListNumber blackListNumber = voipAntiSpamDAO.getBlackListNumber(number);
    if (blackListNumber != null) {
      logger.debug("[{}] - analyze acd: number [{}] at black list", this, number);
      return;
    }

    for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
      AcdConfig acdConfig = grayListConfig.getAcdConfig();
      if (acdConfig == null) {
        continue;
      }

      long minACD = acdConfig.getMinAcd() * 1000;
      if (duration < minACD) {
        long inPeriod = currentTime - acdConfig.getPeriod() * 1000 * 60;
        int cdrCount = callDAO.getCountNumbersInPeriod(number, minACD, inPeriod);
        if (cdrCount >= acdConfig.getMaxMinAcdCallPerPeriod()) {
          int blockCount = 0;
          long blockTimeLeftGrayList = 0;
          int routingRequestCount = 0;

          GrayListNumber grayListNumber = voipAntiSpamDAO.getGrayListNumber(number);
          if (grayListNumber != null) {
            blockCount = grayListNumber.getBlockCount();
            blockTimeLeftGrayList = grayListNumber.getBlockTimeLeft().getTime();
            routingRequestCount = grayListNumber.getRoutingRequestCount();
          }

          if (blockTimeLeftGrayList < currentTime) {
            blockCount++;
            voipAntiSpamDAO.insertHistoryBlockNumbers(number, currentTime);
          }

          if (blockCount > grayListConfig.getMaxBlockCountBeforeMoveToBlackList()) {
            Set<String> numbers = new HashSet<>();
            numbers.add(number);
            voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numbers);
            Set<BlackListNumber> blackListNumbers = new HashSet<>();
            blackListNumbers.add(new BlackListNumber(number, VoipAntiSpamListNumbersStatus.ACD, routingRequestCount,
                VoipAntiSpamHelper.createDescriptionForACDBlockCountFilter(acdConfig, grayListConfig.getMaxBlockCountBeforeMoveToBlackList()), new Date()));
            voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
            logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] > BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted blackListNumbers {} ", this, number, blockCount,
                grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), blackListNumbers);
            return;
          } else {
            long blockTimeLeft = currentTime + grayListConfig.getBlockPeriod() * 1000 * 60;
            Set<GrayListNumber> grayListNumbers = new HashSet<>();
            grayListNumbers.add(new GrayListNumber(number, VoipAntiSpamListNumbersStatus.ACD, routingRequestCount, new Date(currentTime), blockCount, new Date(blockTimeLeft),
                VoipAntiSpamHelper.createDescriptionForACDFilter(acdConfig)));
            voipAntiSpamDAO.insertVoipAntiSpamGrayListNumbers(grayListNumbers);
            logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] <= BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted grayListNumbers {}", this, number, blockCount,
                grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), grayListNumbers);
            return;
          }
        }
      }
    }
  }

  public void analyzeCdr(final long time, final long duration, final int count) {
    try {
      checkCdr(time, duration, count);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check cdr", this, e);
    }
  }

  private void checkCdr(final long time, final long duration, final int count) throws DataModificationException {
    logger.debug("[{}] - try analyze cdr time [{}], duration [{}], count [{}]", this, time, duration, count);
    List<WhiteListNumber> whiteListNumbers = voipAntiSpamDAO.getListNumbersForCdrAnalyze(time, duration);

    if (whiteListNumbers.isEmpty()) {
      logger.debug("[{}] - analyze cdr: whiteListNumbers is empty", this);
      return;
    }

    Set<WhiteListNumber> wl = whiteListNumbers.stream()
        .filter(whiteListNumber -> whiteListNumber.getRoutingRequestCount() >= count)
        .map(whiteListNumber -> new WhiteListNumber(whiteListNumber.getNumber(), VoipAntiSpamListNumbersStatus.AUTO, 0, new Date()))
        .collect(Collectors.toSet());

    voipAntiSpamDAO.insertVoipAntiSpamWhiteListNumbers(wl);
    logger.debug("[{}] - analyze cdr: inserted whiteListNumbers {}", this, whiteListNumbers);
  }

  public void analyzeAlerting(final String number, final CDR cdr) {
    try {
      checkGsmAlerting(number, cdr);
      checkVoipAlerting(number, cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check alerting", this, e);
    }
  }

  private void checkGsmAlerting(final String number, final CDR cdr) throws DataModificationException {
    logger.debug("[{}] - try analyze GSM alerting, cdr [{}] for number [{}]", this, cdr, number);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isGsmAlertingAnalyze()) {
      logger.debug("[{}] - analyze GSM alerting disable for number [{}], voipAntiSpamConfiguration [{}]", this, number, voipAntiSpamConfiguration);
      return;
    }

    if (cdr.getDropReason() != CdrDropReason.GSM) {
      logger.debug("[{}] - analyze GSM alerting, CdrDropReason: {} != GSM", this, cdr.getDropReason());
      return;
    }

    for (GsmAlertingConfig gsmAlertingConfig : voipAntiSpamConfiguration.getGsmAlertingConfigs()) {
      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(gsmAlertingConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(number, inPeriod);

      if (routingCount < gsmAlertingConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, gsmAlertingConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      if (cdr.getDropCode() == 17 && (cdr.getStopTime() - cdr.getSetupTime() - cdr.getPDD() < TimeUnit.SECONDS.toMillis(gsmAlertingConfig.getAlertingTime()))) {
        routingCount++;
        Set<BlackListNumber> blackListNumbers = new HashSet<>();
        blackListNumbers.add(new BlackListNumber(number, VoipAntiSpamListNumbersStatus.GSM_ALERTING, routingCount,
            VoipAntiSpamHelper.createDescriptionForGsmAlertingBlockCountFilter(gsmAlertingConfig), new Date(currentTime)));
        voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
        logger.debug("[{}] - analyze GSM alerting, add number: {} to black list", this, blackListNumbers);
        return;
      }
    }
  }

  private void checkVoipAlerting(final String number, final CDR cdr) throws DataModificationException {
    logger.debug("[{}] - try analyze VOIP alerting, cdr [{}] for number [{}]", this, cdr, number);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isVoipAlertingAnalyze()) {
      logger.debug("[{}] - analyze VOIP alerting disable for number [{}], voipAntiSpamConfiguration [{}]", this, number, voipAntiSpamConfiguration);
      return;
    }

    if (cdr.getDropReason() != CdrDropReason.VOIP) {
      logger.debug("[{}] - analyze VOIP alerting, CdrDropReason: {} != VOIP", this, cdr.getDropReason());
      return;
    }

    for (VoipAlertingConfig voipAlertingConfig : voipAntiSpamConfiguration.getVoipAlertingConfigs()) {
      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(voipAlertingConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(number, inPeriod);

      if (routingCount < voipAlertingConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, voipAlertingConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      if (cdr.getStopTime() - cdr.getSetupTime() - cdr.getPDD() < TimeUnit.SECONDS.toMillis(voipAlertingConfig.getAlertingTime())) {
        routingCount++;
        Set<BlackListNumber> blackListNumbers = new HashSet<>();
        blackListNumbers.add(new BlackListNumber(number, VoipAntiSpamListNumbersStatus.VOIP_ALERTING, routingCount,
            VoipAntiSpamHelper.createDescriptionForVoipAlertingBlockCountFilter(voipAlertingConfig), new Date(currentTime)));
        voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
        logger.debug("[{}] - analyze VOIP alerting, add number: {} to black list", this, blackListNumbers);
        return;
      }
    }
  }

  public void analyzeFas(final String number, final CDR cdr) {
    try {
      checkFas(number, cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check FAS", this, e);
    }
  }

  private void checkFas(final String number, final CDR cdr) throws DataModificationException {
    logger.debug("[{}] - try analyze FAS, cdr [{}] for number [{}]", this, cdr, number);
    if (cdr.getDropReason() != CdrDropReason.LIMIT_FAS_ATTEMPTS) {
      return;
    }
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isFasAnalyze()) {
      logger.debug("[{}] - analyze FAS disable for number [{}], voipAntiSpamConfiguration [{}]", this, number, voipAntiSpamConfiguration);
      return;
    }

    for (FasConfig fasConfig : voipAntiSpamConfiguration.getFasConfigs()) {
      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(fasConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(number, inPeriod);

      if (routingCount < fasConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, fasConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      routingCount++;
      Set<BlackListNumber> blackListNumbers = new HashSet<>();
      blackListNumbers.add(new BlackListNumber(number, VoipAntiSpamListNumbersStatus.FAS, routingCount,
          VoipAntiSpamHelper.createDescriptionForFasBlockCountFilter(fasConfig), new Date(currentTime)));
      voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
      logger.debug("[{}] - analyze FAS, add number: {} to black list", this, blackListNumbers);
      return;
    }
  }

}
