/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.utils;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.control.communication.PermissionChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PermissionsCheckerProxy {

  public static class PermissionsHandler implements InvocationHandler {
    private final Logger logger = LoggerFactory.getLogger(PermissionsHandler.class);

    SessionsPool sessions = SessionsPool.getInstance();
    private final Object delegate;
    private final Class<?> clazz;

    private final Method checkPermissionMethod;

    private final Map<Method, Set<String>> groupsCache = new HashMap<>();

    public PermissionsHandler(final Object delegate, final Class<?> clazz) {
      this.delegate = delegate;
      this.clazz = clazz;
      try {
        this.checkPermissionMethod = PermissionChecker.class.getMethod("checkPermission", ClientUID.class, String.class);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      if (args != null) {
        try {
          ClientUID client = (ClientUID) args[0];
          if (method.equals(checkPermissionMethod)) {
            return checkPermission(client, (String) args[1]);
          }
          if (checkPermission(client, method)) {
            try {
              sessions.markAsUsing(client, true);
              return method.invoke(delegate, args);
            } finally {
              sessions.markAsUsing(client, false);
            }
          }
        } catch (InvocationTargetException e) {
          if (e.getTargetException() != null) {
            throw e.getTargetException();
          }
          logger.warn("[{}] - unexpected exception", this, e);
        }
        throw new NotPermittedException(method);
      } else {
        return clazz.getSimpleName();
      }
    }

    private boolean checkPermission(final ClientUID client, final Method method) {
      Set<String> groups = getGroups(method);
      if (groups == null) {
        return true;
      }
      String group = sessions.getCurrentSessionGroup(client);
      if (group == null) {
        return false;
      }
      return groups.contains(group);
    }

    private Set<String> getGroups(final Method method) {
      if (groupsCache.containsKey(method)) {
        return groupsCache.get(method);
      }
      PermitTo annotation = method.getAnnotation(PermitTo.class);
      HashSet<String> retval = null;
      if (annotation != null) {
        UserGroup[] groups = annotation.groups();
        retval = new HashSet<>();
        for (UserGroup g : groups) {
          retval.add(g.getName());
        }
      }
      groupsCache.put(method, retval);
      return retval;
    }

    private boolean checkPermission(final ClientUID client, final String method) {
      for (Method m : clazz.getMethods()) {
        if (m.getName().equals(method)) {
          return checkPermission(client, m);
        }
      }
      throw new IllegalArgumentException("Could not check permission of " + method + ": it does not exists in " + clazz.getName());
    }

  }

  public static <T extends PermissionChecker> T createProxy(final Class<T> clazz, final T delegate) {
    for (Method m : clazz.getMethods()) {
      if (!m.isAnnotationPresent(PermitTo.class)) {
        throw new IllegalArgumentException("Method has no annotation PermitTo: " + m);
      }
      Class<?>[] args = m.getParameterTypes();
      if (args.length == 0 || !args[0].equals(ClientUID.class)) {
        throw new IllegalArgumentException("Method has no leading argument SessionUID: " + m);
      }
    }
    InvocationHandler handler = new PermissionsHandler(delegate, clazz);
    return clazz.cast(Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] {clazz}, handler));
  }
}
