/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;

public class MobileGatewayChannelInformationImpl implements MobileGatewayChannelInformation {

  private static final long serialVersionUID = 1112513429826769968L;

  private final ChannelUID channelUID;

  private long callsDuration;
  private int successfulCallsCount;
  private int totalCallsCount;
  private int successOutgoingSmsCount;
  private int totalOutgoingSmsCount;
  private int incomingTotalSmsCount;
  private int signalStrength;
  private int bitErrorRate;
  private GSMNetworkInfo gsmNetworkInfo;
  private boolean live;
  private ChannelConfig channelConfig;
  private GSMChannel gsmChannel;

  public MobileGatewayChannelInformationImpl(final ChannelUID channelUID) {
    this.channelUID = channelUID;
  }

  @Override
  public ChannelUID getGSMChannelUID() {
    return channelUID;
  }

  @Override
  public long getCallsDuration() {
    return callsDuration;
  }

  @Override
  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  @Override
  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  @Override
  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  @Override
  public int getTotalOutgoingSmsCount() {
    return totalOutgoingSmsCount;
  }

  @Override
  public int getIncomingTotalSmsCount() {
    return incomingTotalSmsCount;
  }

  @Override
  public int getSignalStrength() {
    return signalStrength;
  }

  @Override
  public int getBitErrorRate() {
    return bitErrorRate;
  }

  @Override
  public GSMNetworkInfo getGsmNetworkInfo() {
    return gsmNetworkInfo;
  }

  @Override
  public boolean isLive() {
    return live;
  }

  @Override
  public ChannelConfig getChannelConfig() {
    return channelConfig;
  }

  @Override
  public GSMChannel getGSMChannel() {
    return gsmChannel;
  }

  @Override
  public void reset() {
    callsDuration = 0;
    successfulCallsCount = 0;
    totalCallsCount = 0;
    successOutgoingSmsCount = 0;
    totalOutgoingSmsCount = 0;
    incomingTotalSmsCount = 0;
  }

  public void setSignalStrength(final int signalStrength) {
    this.signalStrength = signalStrength;
  }

  public void setBitErrorRate(final int bitErrorRate) {
    this.bitErrorRate = bitErrorRate;
  }

  public void addCallsDuration(final long duration) {
    callsDuration += duration;
  }

  public void incSuccessfulCallsCount() {
    successfulCallsCount++;
  }

  public void incTotalCallsCount() {
    totalCallsCount++;
  }

  public void incSuccessOutgoingSmsCount(final int parts) {
    successOutgoingSmsCount += parts;
  }

  public void incTotalOutgoingSmsCount(final int parts) {
    totalOutgoingSmsCount += parts;
  }

  public void incIncomingTotalSmsCount(final int parts) {
    incomingTotalSmsCount += parts;
  }

  public void setGsmNetworkInfo(final GSMNetworkInfo gsmNetworkInfo) {
    this.gsmNetworkInfo = gsmNetworkInfo;
  }

  public void setLive(final boolean live) {
    this.live = live;
  }

  public void setChannelConfig(final ChannelConfig channelConfig) {
    this.channelConfig = channelConfig;
  }

  public void setGsmChannel(final GSMChannel gsmChannel) {
    this.gsmChannel = gsmChannel;
  }

}
