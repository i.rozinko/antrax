/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class LongInfoRequest implements Serializable {

  private static final long serialVersionUID = 5412629375747230393L;

  private boolean fetchSimCardsTotal;
  private boolean fetchSimCardsLocked;
  private boolean fetchSimCardsGood;
  private boolean fetchSimCardsRegdeny;
  private boolean fetchSimCardsFAS;
  private boolean fetchSimCardsManually;

  private boolean fetchBekkies;
  private boolean fetchBekkiesChannels;
  private boolean fetchCems;
  private boolean fetchCemsChannels;

  private boolean fetchVersion;
  private boolean fetchUptime;
  private boolean fetchASR;
  private boolean fetchACD;

  private boolean fetchTalkChannels;
  private boolean fetchWaitCallsChannels;
  private boolean fetchSleepingChannels;
  private boolean fetchNotConnectedChannels;
  private boolean fetchUsableChannels;
  private boolean fetchTotalChannels;

  private boolean fetchSuccessfulPerSession;
  private boolean fetchSuccessfulPerDay;
  private boolean fetchSuccessfulPerHour;
  private boolean fetchSuccessfulPerHistory;

  private boolean fetchTotalPerSession;
  private boolean fetchTotalPerDay;
  private boolean fetchTotalPerHour;
  private boolean fetchTotalPerHistory;

  private boolean fetchZeroPerSession;
  private boolean fetchZeroPerDay;
  private boolean fetchZeroPerHour;
  private boolean fetchZeroPerHistory;

  private boolean fetchCurrentOutgoingCallsCount;

  private boolean fetchCurrentIncomingCallsCount;

  private boolean fetchFreeChannels;

  public LongInfoRequest() {
    super();
  }

  @Override
  public int hashCode() {
    return LongInfoRequest.class.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof LongInfoRequest))
      return false;

    LongInfoRequest other = (LongInfoRequest) obj;

    if (other.fetchVersion && other.fetchVersion != fetchVersion)
      return false;

    if (other.fetchUptime && other.fetchUptime != fetchUptime)
      return false;

    if (other.fetchASR && other.fetchASR != fetchASR)
      return false;

    if (other.fetchACD && other.fetchACD != fetchACD)
      return false;

    if (other.fetchSimCardsTotal && other.fetchSimCardsTotal != fetchSimCardsTotal)
      return false;

    if (other.fetchSimCardsLocked && other.fetchSimCardsLocked != fetchSimCardsLocked)
      return false;

    if (other.fetchSimCardsGood && other.fetchSimCardsGood != fetchSimCardsGood)
      return false;

    if (other.fetchSimCardsRegdeny && other.fetchSimCardsRegdeny != fetchSimCardsRegdeny)
      return false;

    if (other.fetchSimCardsFAS && other.fetchSimCardsFAS != fetchSimCardsFAS)
      return false;

    if (other.fetchSimCardsManually && other.fetchSimCardsManually != fetchSimCardsManually)
      return false;

    if (other.fetchBekkies && other.fetchBekkies != fetchBekkies)
      return false;

    if (other.fetchBekkiesChannels && other.fetchBekkiesChannels != fetchBekkiesChannels)
      return false;

    if (other.fetchCems && other.fetchCems != fetchCems)
      return false;

    if (other.fetchCemsChannels && other.fetchCemsChannels != fetchCemsChannels)
      return false;

    if (other.fetchTalkChannels && other.fetchTalkChannels != fetchTalkChannels)
      return false;

    if (other.fetchWaitCallsChannels && other.fetchWaitCallsChannels != fetchWaitCallsChannels)
      return false;

    if (other.fetchSleepingChannels && other.fetchSleepingChannels != fetchSleepingChannels)
      return false;

    if (other.fetchNotConnectedChannels && other.fetchNotConnectedChannels != fetchNotConnectedChannels)
      return false;

    if (other.fetchUsableChannels && other.fetchUsableChannels != fetchUsableChannels)
      return false;

    if (other.fetchTotalChannels && other.fetchTotalChannels != fetchTotalChannels)
      return false;

    if (other.fetchSuccessfulPerSession && other.fetchSuccessfulPerSession != fetchSuccessfulPerSession)
      return false;

    if (other.fetchSuccessfulPerDay && other.fetchSuccessfulPerDay != fetchSuccessfulPerDay)
      return false;

    if (other.fetchSuccessfulPerHour && other.fetchSuccessfulPerHour != fetchSuccessfulPerHour)
      return false;

    if (other.fetchSuccessfulPerHistory && other.fetchSuccessfulPerHistory != fetchSuccessfulPerHistory)
      return false;

    if (other.fetchTotalPerSession && other.fetchTotalPerSession != fetchTotalPerSession)
      return false;

    if (other.fetchTotalPerDay && other.fetchTotalPerDay != fetchTotalPerDay)
      return false;

    if (other.fetchTotalPerHour && other.fetchTotalPerHour != fetchTotalPerHour)
      return false;

    if (other.fetchTotalPerHistory && other.fetchTotalPerHistory != fetchTotalPerHistory)
      return false;

    if (other.fetchZeroPerSession && other.fetchZeroPerSession != fetchZeroPerSession)
      return false;

    if (other.fetchZeroPerDay && other.fetchZeroPerDay != fetchZeroPerDay)
      return false;

    if (other.fetchZeroPerHour && other.fetchZeroPerHour != fetchZeroPerHour)
      return false;

    if (other.fetchZeroPerHistory && other.fetchZeroPerHistory != fetchZeroPerHistory)
      return false;

    if (other.fetchCurrentOutgoingCallsCount && other.fetchCurrentOutgoingCallsCount != fetchCurrentOutgoingCallsCount)
      return false;

    if (other.fetchCurrentIncomingCallsCount && other.fetchCurrentIncomingCallsCount != fetchCurrentIncomingCallsCount)
      return false;

    if (other.fetchFreeChannels && other.fetchFreeChannels != fetchFreeChannels)
      return false;

    return true;
  }

  public void setFetchCallsPerHistory(final boolean fetch) {
    fetchTotalPerHistory = fetch;
    fetchZeroPerHistory = fetch;
    fetchSuccessfulPerHistory = fetch;
  }

  public void setFetchCallsPerDay(final boolean fetch) {
    fetchTotalPerDay = fetch;
    fetchZeroPerDay = fetch;
    fetchSuccessfulPerDay = fetch;
  }

  public void setFetchCallsPerHour(final boolean fetch) {
    fetchTotalPerHour = fetch;
    fetchZeroPerHour = fetch;
    fetchSuccessfulPerHour = fetch;
  }

  public void setFetchCallsPerSession(final boolean fetch) {
    fetchTotalPerSession = fetch;
    fetchZeroPerSession = fetch;
    fetchSuccessfulPerSession = fetch;
  }

  public void setFetchVersion(final boolean fetchVersion) {
    this.fetchVersion = fetchVersion;
  }

  public boolean isFetchVersion() {
    return fetchVersion;
  }

  public boolean isFetchUptime() {
    return fetchUptime;
  }

  public void setFetchUptime(final boolean fetchUptime) {
    this.fetchUptime = fetchUptime;
  }

  public boolean isFetchASR() {
    return fetchASR;
  }

  public void setFetchASR(final boolean fetchASR) {
    this.fetchASR = fetchASR;
    // Required by algorithm
    setFetchTotalPerSession(true);
    setFetchSuccessfulPerSession(true);
  }

  public void setFetchACD(final boolean fetchACD) {
    this.fetchACD = fetchACD;
    // Required by algorithm
    setFetchSuccessfulPerSession(true);
  }

  public boolean isFetchACD() {
    return fetchACD;
  }

  public boolean isFetchSimCardsTotal() {
    return fetchSimCardsTotal;
  }

  public void setFetchSimCardsTotal(final boolean fetchSimCardsTotal) {
    this.fetchSimCardsTotal = fetchSimCardsTotal;
  }

  public boolean isFetchSimCardsLocked() {
    return fetchSimCardsLocked;
  }

  public void setFetchSimCardsLocked(final boolean fetchSimCardsLocked) {
    this.fetchSimCardsLocked = fetchSimCardsLocked;
  }

  public boolean isFetchSimCardsGood() {
    return fetchSimCardsGood;
  }

  public void setFetchSimCardsGood(final boolean fetchSimCardsGood) {
    this.fetchSimCardsGood = fetchSimCardsGood;
  }

  public boolean isFetchSimCardsRegdeny() {
    return fetchSimCardsRegdeny;
  }

  public void setFetchSimCardsRegdeny(final boolean fetchSimCardsRegdeny) {
    this.fetchSimCardsRegdeny = fetchSimCardsRegdeny;
  }

  public boolean isFetchSimCardsFAS() {
    return fetchSimCardsFAS;
  }

  public void setFetchSimCardsFAS(final boolean fetchSimCardsFAS) {
    this.fetchSimCardsFAS = fetchSimCardsFAS;
  }

  public boolean isFetchSimCardsManually() {
    return fetchSimCardsManually;
  }

  public void setFetchSimCardsManually(final boolean fetchSimCardsManually) {
    this.fetchSimCardsManually = fetchSimCardsManually;
  }

  public boolean isFetchBekkies() {
    return fetchBekkies;
  }

  public void setFetchBekkies(final boolean fetchBekkies) {
    this.fetchBekkies = fetchBekkies;
  }

  public boolean isFetchBekkiesChannels() {
    return fetchBekkiesChannels;
  }

  public void setFetchBekkiesChannels(final boolean fetchBekkiesChannels) {
    this.fetchBekkiesChannels = fetchBekkiesChannels;
  }

  public boolean isFetchCems() {
    return fetchCems;
  }

  public void setFetchCems(final boolean fetchCems) {
    this.fetchCems = fetchCems;
  }

  public boolean isFetchCemsChannels() {
    return fetchCemsChannels;
  }

  public void setFetchCemsChannels(final boolean fetchCemsChannels) {
    this.fetchCemsChannels = fetchCemsChannels;
  }

  public boolean isFetchTalkChannels() {
    return fetchTalkChannels;
  }

  public void setFetchTalkChannels(final boolean fetchTalkChannels) {
    this.fetchTalkChannels = fetchTalkChannels;
  }

  public boolean isFetchWaitCallsChannels() {
    return fetchWaitCallsChannels;
  }

  public void setFetchWaitCallsChannels(final boolean fetchWaitCallsChannels) {
    this.fetchWaitCallsChannels = fetchWaitCallsChannels;
  }

  public boolean isFetchSleepingChannels() {
    return fetchSleepingChannels;
  }

  public void setFetchSleepingChannels(final boolean fetchSleepingChannels) {
    this.fetchSleepingChannels = fetchSleepingChannels;
  }

  public boolean isFetchNotConnectedChannels() {
    return fetchNotConnectedChannels;
  }

  public void setFetchNotConnectedChannels(final boolean fetchNotConnectedChannels) {
    this.fetchNotConnectedChannels = fetchNotConnectedChannels;
  }

  public boolean isFetchUsableChannels() {
    return fetchUsableChannels;
  }

  public void setFetchUsableChannels(final boolean fetchUsableChannels) {
    this.fetchUsableChannels = fetchUsableChannels;
  }

  public boolean isFetchTotalChannels() {
    return fetchTotalChannels;
  }

  public void setFetchTotalChannels(final boolean fetchTotalChannels) {
    this.fetchTotalChannels = fetchTotalChannels;
  }

  public boolean isFetchSuccessfulPerSession() {
    return fetchSuccessfulPerSession;
  }

  public void setFetchSuccessfulPerSession(final boolean fetchSuccessfulPerSession) {
    this.fetchSuccessfulPerSession = fetchSuccessfulPerSession;
  }

  public boolean isFetchSuccessfulPerDay() {
    return fetchSuccessfulPerDay;
  }

  public void setFetchSuccessfulPerDay(final boolean fetchSuccessfulPerDay) {
    this.fetchSuccessfulPerDay = fetchSuccessfulPerDay;
  }

  public boolean isFetchSuccessfulPerHour() {
    return fetchSuccessfulPerHour;
  }

  public void setFetchSuccessfulPerHour(final boolean fetchSuccessfulPerHour) {
    this.fetchSuccessfulPerHour = fetchSuccessfulPerHour;
  }

  public boolean isFetchSuccessfulPerHistory() {
    return fetchSuccessfulPerHistory;
  }

  public void setFetchSuccessfulPerHistory(final boolean fetchSuccessfulPerHistory) {
    this.fetchSuccessfulPerHistory = fetchSuccessfulPerHistory;
  }

  public boolean isFetchTotalPerSession() {
    return fetchTotalPerSession;
  }

  public void setFetchTotalPerSession(final boolean fetchTotalPerSession) {
    this.fetchTotalPerSession = fetchTotalPerSession;
  }

  public boolean isFetchTotalPerDay() {
    return fetchTotalPerDay;
  }

  public void setFetchTotalPerDay(final boolean fetchTotalPerDay) {
    this.fetchTotalPerDay = fetchTotalPerDay;
  }

  public boolean isFetchTotalPerHour() {
    return fetchTotalPerHour;
  }

  public void setFetchTotalPerHour(final boolean fetchTotalPerHour) {
    this.fetchTotalPerHour = fetchTotalPerHour;
  }

  public boolean isFetchTotalPerHistory() {
    return fetchTotalPerHistory;
  }

  public void setFetchTotalPerHistory(final boolean fetchTotalPerHistory) {
    this.fetchTotalPerHistory = fetchTotalPerHistory;
  }

  public boolean isFetchZeroPerSession() {
    return fetchZeroPerSession;
  }

  public void setFetchZeroPerSession(final boolean fetchZeroPerSession) {
    this.fetchZeroPerSession = fetchZeroPerSession;
  }

  public boolean isFetchZeroPerDay() {
    return fetchZeroPerDay;
  }

  public void setFetchZeroPerDay(final boolean fetchZeroPerDay) {
    this.fetchZeroPerDay = fetchZeroPerDay;
  }

  public boolean isFetchZeroPerHour() {
    return fetchZeroPerHour;
  }

  public void setFetchZeroPerHour(final boolean fetchZeroPerHour) {
    this.fetchZeroPerHour = fetchZeroPerHour;
  }

  public boolean isFetchZeroPerHistory() {
    return fetchZeroPerHistory;
  }

  public void setFetchZeroPerHistory(final boolean fetchZeroPerHistory) {
    this.fetchZeroPerHistory = fetchZeroPerHistory;
  }

  @Override
  public String toString() {
    List<String> retval = new LinkedList<>();
    if (fetchVersion)
      retval.add("fetchVersion");
    if (fetchUptime)
      retval.add("fetchUptime");
    if (fetchASR)
      retval.add("fetchASR");
    if (fetchACD)
      retval.add("fetchACD");

    if (fetchSimCardsTotal)
      retval.add("fetchSimCardsTotal");
    if (fetchSimCardsLocked)
      retval.add("fetchSimCardsLocked");
    if (fetchSimCardsGood)
      retval.add("fetchSimCardsGood");
    if (fetchSimCardsRegdeny)
      retval.add("fetchSimCardsRegdeny");
    if (fetchSimCardsFAS)
      retval.add("fetchSimCardsFAS");
    if (fetchSimCardsManually)
      retval.add("fetchSimCardsManually");

    if (fetchBekkies)
      retval.add("fetchBekkies");
    if (fetchBekkiesChannels)
      retval.add("fetchBekkiesChannels");
    if (fetchCems)
      retval.add("fetchCems");
    if (fetchCemsChannels)
      retval.add("fetchCemsChannels");

    if (fetchTalkChannels)
      retval.add("fetchTalkChannels");
    if (fetchWaitCallsChannels)
      retval.add("fetchWaitCallsChannels");
    if (fetchSleepingChannels)
      retval.add("fetchSleepingChannels");
    if (fetchNotConnectedChannels)
      retval.add("fetchNotConnectedChannels");
    if (fetchUsableChannels)
      retval.add("fetchUsableChannels");
    if (fetchTotalChannels)
      retval.add("fetchTotalChannels");

    if (fetchSuccessfulPerSession)
      retval.add("fetchSuccessfulPerSession");
    if (fetchSuccessfulPerDay)
      retval.add("fetchSuccessfulPerDay");
    if (fetchSuccessfulPerHour)
      retval.add("fetchSuccessfulPerHour");
    if (fetchSuccessfulPerHistory)
      retval.add("fetchSuccessfulPerHistory");

    if (fetchTotalPerSession)
      retval.add("fetchTotalPerSession");
    if (fetchTotalPerDay)
      retval.add("fetchTotalPerDay");
    if (fetchTotalPerHour)
      retval.add("fetchTotalPerHour");
    if (fetchTotalPerHistory)
      retval.add("fetchTotalPerHistory");

    if (fetchZeroPerSession)
      retval.add("fetchZeroPerSession");
    if (fetchZeroPerDay)
      retval.add("fetchZeroPerDay");
    if (fetchZeroPerHour)
      retval.add("fetchZeroPerHour");
    if (fetchZeroPerHistory)
      retval.add("fetchZeroPerHistory");
    if (fetchCurrentOutgoingCallsCount)
      retval.add("fetchCurrentOutgoingCallsCount");
    if (fetchCurrentIncomingCallsCount)
      retval.add("fetchCurrentIncomingCallsCount");
    if (fetchFreeChannels)
      retval.add("fetchFreeChannels");

    return retval.toString();
  }

  public void setFetchCurrentOutgoingCallsCount(final boolean flag) {
    fetchCurrentOutgoingCallsCount = flag;
  }

  public boolean isFetchCurrentOutgoingCallsCount() {
    return fetchCurrentOutgoingCallsCount;
  }

  public void setFetchCurrentIncomingCallsCount(final boolean flag) {
    fetchCurrentIncomingCallsCount = flag;
  }

  public boolean isFetchCurrentIncomingCallsCount() {
    return fetchCurrentIncomingCallsCount;
  }

  public boolean isFetchFreeChannels() {
    return fetchFreeChannels;
  }

  public void setFetchFreeChannels(final boolean fetchFreeChannels) {
    this.fetchFreeChannels = fetchFreeChannels;
  }
}
