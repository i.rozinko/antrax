/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.commons.ChannelUID;

import java.rmi.RemoteException;

public class RemoteHistoryLogger implements IRemoteHistoryLogger {

  private final ISimHistoryLogger simHistoryLogger;
  private final ISimpleConfigEditDAO configEditDAO;

  public RemoteHistoryLogger(final ISimHistoryLogger simHistoryLogger, final ISimpleConfigEditDAO configEditDAO) {
    this.simHistoryLogger = simHistoryLogger;
    this.configEditDAO = configEditDAO;
  }

  @Override
  public void handleSimEvent(final SimEvent simEvent) throws RemoteException {
    simHistoryLogger.handleEvent(simEvent);
  }

  @Override
  public void handleGsmChannelLock(final ChannelUID channel, final boolean lock, final String lockReason) throws RemoteException {
    configEditDAO.lockGsmChannel(channel, lock, lockReason);
  }

}
