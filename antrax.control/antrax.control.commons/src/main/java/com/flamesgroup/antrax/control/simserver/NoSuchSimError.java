/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;

public class NoSuchSimError extends Exception {

  public NoSuchSimError(final ICCID uid) {
    super("No sim with uid=" + uid);
  }

  public NoSuchSimError(final ChannelUID uid) {
    super("No sim in " + uid);
  }

  private static final long serialVersionUID = -5480276810760653278L;

}
