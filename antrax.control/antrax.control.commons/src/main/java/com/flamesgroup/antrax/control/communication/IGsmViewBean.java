/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import static com.flamesgroup.antrax.control.authorization.UserGroup.ADMIN;
import static com.flamesgroup.antrax.control.authorization.UserGroup.USER;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IGsmViewBean extends PermissionChecker {

  @PermitTo(groups = {ADMIN, USER}, description = "get GSM view diff")
  List<GraphComparator.Delta> getGsmViewDiff(ClientUID clientUid, String server) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "get GSM view")
  List<GsmView> getGsmView(ClientUID clientUid, String server) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "get last update Network Survey")
  Map<String, Date> getLastNetworkSurvey(ClientUID clientUid) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "save GSM view")
  void saveGsmView(ClientUID clientUid, String server, List<GsmView> gsmViews) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "clear GSM view")
  void clearGsmView(ClientUID clientUID, String server) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "get CellEqualizer Algorithm")
  CellEqualizerAlgorithm getCellEqualizerAlgorithm(ClientUID clientUID, String server) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "update CellEqualizer Algorithm")
  void updateCellEqualizerAlgorithm(ClientUID clientUID, String server, CellEqualizerAlgorithm cellEqualizerAlgorithm) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER}, description = "reset GSM view Statistic")
  void resetGsmViewStatistic(ClientUID clientUID, List<GsmView> gsmViews, String server) throws NotPermittedException;

}
