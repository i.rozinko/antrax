/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.configuration;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISimServerManager;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.storage.commons.IServerData;

//TODO: add extends Remote interface and check to refactor
public interface SysCfgBean {

  IServerData getServer(String serverName) throws NoSuchServerException;

  long getControlServerCurrentTime(); // TODO: need fix - replace this method to another interface

  IPingSimServerManager getPingSimServerManager();

  IPingVoiceServerManager getPingVoiceServerManager();

  IActionExecutionManager getActionExecutionManager();

  IRemoteHistoryLogger getHistoryLogger();

  IRemoteVSConfigurator getRemoteVSConfigurator();

  RemoteRegistryAccess getRemoteRegistryAccess();

  ISmsManager getSmsManager();

  IGsmViewManager getGsmViewManager();

  ISimUnitManager getSimUnitManager();

  ISimServerManager getSimServerManager();

  IGsmChannelManagerHandler getGsmChannelManagerHandler();

  IGsmUnitManger getGsmUnitManager();

  ISimCallHistory getSimCallHistory();
}
