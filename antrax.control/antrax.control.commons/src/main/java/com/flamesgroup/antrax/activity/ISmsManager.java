/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SMSStatusReport;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface ISmsManager extends Remote {

  void saveReceivedSms(ICCID iccid, SMSMessageInbound smsMessageInbound) throws RemoteException;

  UUID updateReceivedSmsIdForSms(ICCID iccid, String senderPhoneNumber, int referenceNumber) throws RemoteException;

  boolean isReceivedAllMultiPartSms(ICCID iccid, String senderPhoneNumber, int referenceNumber, int countOfSmsParts) throws RemoteException;

  Pair<String, Date> getFullReceivedSmsAndDate(UUID smsId) throws RemoteException;

  void saveMultiPartOutboundSms(ICCID iccid, UUID smsId, List<SMSMessageOutbound> smsInfoOutbounds) throws RemoteException;

  Pair<UUID, SMSMessageOutbound> saveSmsStatusReport(ICCID iccid, SMSStatusReport smsStatusReport) throws RemoteException;

}
