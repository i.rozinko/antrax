/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class IdentificationInjectionProxyTest {

  private class FullBean {

    private final List<String> asked = new LinkedList<>();
    private final List<Long> ids = new LinkedList<>();

    void ask(final long id, final String name) {
      this.ids.add(id);
      this.asked.add(name);
    }

  }

  private interface CutBean {
    void ask(String name);
  }

  @Test
  public void proxy() {
    FullBean bean = new FullBean();

    CutBean proxy30 = IdentificationInjectionProxy.proxy(bean, CutBean.class, 30l, Long.TYPE);

    proxy30.ask("hello");
    proxy30.ask("world");

    assertEquals(2, bean.asked.size());
    assertEquals("hello", bean.asked.get(0));
    assertEquals("world", bean.asked.get(1));
    assertEquals(new Long(30l), bean.ids.get(0));
    assertEquals(new Long(30l), bean.ids.get(1));

    CutBean proxy40 = IdentificationInjectionProxy.proxy(bean, CutBean.class, 40l, Long.TYPE);

    proxy40.ask("newone");
    assertEquals("newone", bean.asked.get(2));
    assertEquals(new Long(40), bean.ids.get(2));

  }

}
