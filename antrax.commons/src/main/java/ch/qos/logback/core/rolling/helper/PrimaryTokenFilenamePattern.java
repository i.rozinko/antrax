/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.qos.logback.core.rolling.helper;

import ch.qos.logback.core.Context;
import ch.qos.logback.core.pattern.Converter;
import ch.qos.logback.core.pattern.LiteralConverter;

import java.util.Date;

/**
 * This class is hack to override toRegexForFixedDate method to convert date token only for primary token.
 * All other tokens marked as auxiliary will be replaced by their regex pattern.
 */
public class PrimaryTokenFilenamePattern extends FileNamePattern {

  public PrimaryTokenFilenamePattern(final String patternArg, final Context contextArg) {
    super(patternArg, contextArg);
  }

  @Override
  public String toRegexForFixedDate(final Date date) {
    StringBuilder buf = new StringBuilder();
    Converter<Object> p = headTokenConverter;
    while (p != null) {
      if (p instanceof LiteralConverter) {
        buf.append(p.convert(null));
      } else if (p instanceof IntegerTokenConverter) {
        buf.append("(\\d{1,3})");
      } else if (p instanceof DateTokenConverter) {
        DateTokenConverter dtc = (DateTokenConverter) p;
        if (dtc.isPrimary()) {
          buf.append(p.convert(date));
        } else {
          buf.append(dtc.toRegex());
        }
      }
      p = p.getNext();
    }
    return buf.toString();
  }

}
