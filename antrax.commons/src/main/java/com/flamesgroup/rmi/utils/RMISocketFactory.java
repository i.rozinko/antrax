/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi.utils;

import com.flamesgroup.properties.PropUtils;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;

public final class RMISocketFactory implements RMIClientSocketFactory, RMIServerSocketFactory, Serializable {

  private static final long serialVersionUID = 9170242134363017312L;

  public static final int DEFAULT_BACKLOG = 500;

  private final boolean compressing;
  private final int readTimeout;
  private final int openTimeout;

  private RMISocketFactory(final boolean compressing, final int readTimeout, final int openTimeout) {
    this.compressing = compressing;
    this.readTimeout = readTimeout;
    this.openTimeout = openTimeout;
  }

  public static RMISocketFactory createSocketFactory(final int readTimeout, final int openTimeout) {
    boolean compressing = PropUtils.getInstance().getCommonsProperties().getRmiCompressing();
    return new RMISocketFactory(compressing, readTimeout, openTimeout);
  }

  public static RMISocketFactory createSocketFactory(final int readTimeout) {
    long openTimeout = PropUtils.getInstance().getCommonsProperties().getRmiOpenSocketTimeout();
    return createSocketFactory(readTimeout, (int) openTimeout);
  }

  public static RMISocketFactory createSocketFactory() {
    long readTimeout = PropUtils.getInstance().getCommonsProperties().getRmiReadSocketTimeout();
    return createSocketFactory((int) readTimeout);
  }

  public boolean isCompressing() {
    return compressing;
  }

  public long getReadTimeout() {
    return readTimeout;
  }

  @Override
  public ServerSocket createServerSocket(final int port) throws IOException {
    if (compressing) {
      return new CompressingServerSocket(port, DEFAULT_BACKLOG);
    } else {
      return new ServerSocket(port, DEFAULT_BACKLOG);
    }
  }

  @Override
  public Socket createSocket(final String host, final int port) throws IOException {
    Socket socket = createSocket();
    socket.setSoTimeout(readTimeout);
    socket.setSoLinger(false, 0);
    socket.connect(new InetSocketAddress(host, port), openTimeout);
    return socket;
  }

  @Override
  public int hashCode() {
    return Boolean.valueOf(compressing).hashCode() + readTimeout + openTimeout;
  }

  @Override
  public boolean equals(final Object obj) {
    return this.getClass() == obj.getClass();
  }

  private Socket createSocket() {
    if (compressing) {
      return new CompressingSocket();
    } else {
      return new Socket();
    }
  }

}
