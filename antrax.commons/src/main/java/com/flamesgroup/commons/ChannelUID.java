/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import com.flamesgroup.device.DeviceUID;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ChannelUID implements Serializable, Comparable<ChannelUID> {

  private static final long serialVersionUID = -5290580443047674075L;

  private static final Pattern CANONICAL_NAME_PARSE_PATTERN = Pattern.compile("(?<deviceuid>.+)(?<delimiter>(:|#))(?<channel>\\d+)");

  private static final BiFunction<String, Byte, String> createCanonicalNameFunction = (name, number) -> name + "#" + (number + 1);

  private final DeviceUID deviceUID;
  private final String aliasDeviceUID;
  private final byte channelNumber;

  private transient String toS;

  public ChannelUID(final DeviceUID deviceUID, final String aliasDeviceUID, final byte channelNumber) {
    Objects.requireNonNull(deviceUID, "deviceUID mustn't be null");

    this.deviceUID = deviceUID;
    this.aliasDeviceUID = aliasDeviceUID;
    this.channelNumber = channelNumber;
  }

  public ChannelUID(final DeviceUID deviceUID, final byte channelNumber) {
    this(deviceUID, null, channelNumber);
  }

  public DeviceUID getDeviceUID() {
    return deviceUID;
  }

  public String getAliasDeviceUID() {
    return aliasDeviceUID;
  }

  public byte getChannelNumber() {
    return channelNumber;
  }

  public String getCanonicalName() {
    return createCanonicalNameFunction.apply(deviceUID.getCanonicalName(), channelNumber);
  }

  public String getAliasCanonicalName() {
    if (aliasDeviceUID == null) {
      return getCanonicalName();
    } else {
      return createCanonicalNameFunction.apply(aliasDeviceUID, channelNumber);
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = deviceUID.hashCode();
    result = prime * result + channelNumber;
    return result;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ChannelUID)) {
      return false;
    }
    ChannelUID that = (ChannelUID) object;

    return deviceUID.equals(that.deviceUID) && channelNumber == that.channelNumber;
  }

  @Override
  public int compareTo(final ChannelUID channelUID) {
    int cmp;
    if (aliasDeviceUID == null) {
      if (channelUID.aliasDeviceUID == null) {
        cmp = deviceUID.getCanonicalName().compareTo(channelUID.deviceUID.getCanonicalName());
      } else {
        cmp = deviceUID.getCanonicalName().compareTo(channelUID.aliasDeviceUID);
      }
    } else {
      if (channelUID.aliasDeviceUID == null) {
        cmp = aliasDeviceUID.compareTo(channelUID.deviceUID.getCanonicalName());
      } else {
        cmp = aliasDeviceUID.compareTo(channelUID.aliasDeviceUID);
      }
    }

    if (cmp == 0) {
      cmp = channelNumber - channelUID.channelNumber;
    }
    return cmp;
  }

  @Override
  public String toString() {
    if (toS == null) {
      StringBuilder builder = new StringBuilder(getCanonicalName());
      if (aliasDeviceUID != null) {
        builder.append("[").append(getAliasCanonicalName()).append("]");
      }
      toS = builder.toString();
    }
    return toS;
  }

  public static ChannelUID valueFromCanonicalName(final String channelUIDCanonicalName) {
    Objects.requireNonNull(channelUIDCanonicalName, "channelUIDCanonicalName mustn't be null");

    Matcher matcher = CANONICAL_NAME_PARSE_PATTERN.matcher(channelUIDCanonicalName);
    if (matcher.matches()) {
      DeviceUID deviceUID = DeviceUID.valueFromCanonicalName(matcher.group("deviceuid"));
      byte channelNumber = Byte.parseByte(matcher.group("channel"));
      if (matcher.group("delimiter").equals("#")) {
        channelNumber -= 1;
      }

      return new ChannelUID(deviceUID, channelNumber);
    } else {
      throw new IllegalArgumentException(String.format("[%s] isn't correspond to pattern '<device uid><:|#><channel number>'", channelUIDCanonicalName));
    }
  }

}
