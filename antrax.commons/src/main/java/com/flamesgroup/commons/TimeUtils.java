/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

public class TimeUtils {

  public static String writeTime(final int time) {
    int hours = time / (60 * 60);
    int min = (time - hours * 60 * 60) / 60;
    int sec = time - hours * 60 * 60 - min * 60;
    return write(hours) + ":" + write(min) + ":" + write(sec);
  }

  private static String write(final int time) {
    if (time > 9) {
      return Integer.toString(time);
    }
    return "0" + time;
  }

  public static String writeTime(final long millis) {
    int time = (int) (millis / 1000);
    return writeTime(time);
  }

  public static String writeTimeMs(final long millis) {
    return String.format("%s.%03d", writeTime(millis), (millis % 1000));
  }

}
