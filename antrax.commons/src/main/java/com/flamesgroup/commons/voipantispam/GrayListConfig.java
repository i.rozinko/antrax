/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons.voipantispam;

import java.io.Serializable;
import java.util.Objects;

public class GrayListConfig implements Serializable {

  private static final long serialVersionUID = 7296565478048034186L;

  private final int period;
  private final int maxRoutingRequestPerPeriod;
  private final int blockPeriod;
  private final int maxBlockCountBeforeMoveToBlackList;

  private final AcdConfig acdConfig;

  public GrayListConfig(final int period, final int maxRoutingRequestPerPeriod, final int blockPeriod, final int maxBlockCountBeforeMoveToBlackList) {
    this(period, maxRoutingRequestPerPeriod, blockPeriod, maxBlockCountBeforeMoveToBlackList, null);
  }

  public GrayListConfig(final int period, final int maxRoutingRequestPerPeriod, final int blockPeriod, final int maxBlockCountBeforeMoveToBlackList, final AcdConfig acdConfig) {
    this.period = period;
    this.maxRoutingRequestPerPeriod = maxRoutingRequestPerPeriod;
    this.blockPeriod = blockPeriod;
    this.maxBlockCountBeforeMoveToBlackList = maxBlockCountBeforeMoveToBlackList;
    this.acdConfig = acdConfig;
  }

  public int getPeriod() {
    return period;
  }

  public int getMaxRoutingRequestPerPeriod() {
    return maxRoutingRequestPerPeriod;
  }

  public int getBlockPeriod() {
    return blockPeriod;
  }

  public int getMaxBlockCountBeforeMoveToBlackList() {
    return maxBlockCountBeforeMoveToBlackList;
  }

  public AcdConfig getAcdConfig() {
    return acdConfig;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof GrayListConfig)) {
      return false;
    }
    final GrayListConfig that = (GrayListConfig) object;

    return period == that.period
        && maxRoutingRequestPerPeriod == that.maxRoutingRequestPerPeriod
        && blockPeriod == that.blockPeriod
        && maxBlockCountBeforeMoveToBlackList == that.maxBlockCountBeforeMoveToBlackList
        && Objects.equals(acdConfig, that.acdConfig);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = period;
    result = prime * result + maxRoutingRequestPerPeriod;
    result = prime * result + blockPeriod;
    result = prime * result + maxBlockCountBeforeMoveToBlackList;
    result = prime * result + Objects.hashCode(acdConfig);
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("GrayListConfig{");
    sb.append("period=").append(period);
    sb.append(", maxRoutingRequestPerPeriod=").append(maxRoutingRequestPerPeriod);
    sb.append(", blockPeriod=").append(blockPeriod);
    sb.append(", maxBlockCountBeforeMoveToBlackList=").append(maxBlockCountBeforeMoveToBlackList);
    sb.append(", acdConfig=").append(acdConfig);
    sb.append('}');
    return sb.toString();
  }
}
