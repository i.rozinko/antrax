/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.properties;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class CommonsProperties implements ServerProperties {

  private static final String RMI_COMPRESSING = "rmi.compressing";
  private static final String RMI_READ_SOCKET_TIMEOUT = "rmi.read.socket.timeout";
  private static final String RMI_OPEN_SOCKET_TIMEOUT = "rmi.open.socket.timeout";
  private static final String CONTROL_SERVER_MAX_ALLOWABLE_TIME_DIFFERENCE = "control.server.max.allowable.time.difference";

  private final AtomicBoolean rmiCompressing = new AtomicBoolean(false);
  private final AtomicLong rmiReadSocketTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(2));
  private final AtomicLong rmiOpenSocketTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(2));
  private final AtomicInteger controlServerMaxAllowableTimeDifference = new AtomicInteger(1000);

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);

    rmiCompressing.set(propertiesLoader.getBoolean(RMI_COMPRESSING));
    rmiReadSocketTimeout.set(propertiesLoader.getPeriod(RMI_READ_SOCKET_TIMEOUT));
    rmiOpenSocketTimeout.set(propertiesLoader.getPeriod(RMI_OPEN_SOCKET_TIMEOUT));
    controlServerMaxAllowableTimeDifference.set(propertiesLoader.getInt(CONTROL_SERVER_MAX_ALLOWABLE_TIME_DIFFERENCE));
  }

  public boolean getRmiCompressing() {
    return rmiCompressing.get();
  }

  public long getRmiReadSocketTimeout() {
    return rmiReadSocketTimeout.get();
  }

  public long getRmiOpenSocketTimeout() {
    return rmiOpenSocketTimeout.get();
  }

  public int getControlServerMaxAllowableTimeDifference() {
    return controlServerMaxAllowableTimeDifference.get();
  }

}
