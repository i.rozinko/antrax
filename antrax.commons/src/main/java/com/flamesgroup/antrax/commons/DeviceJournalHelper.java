/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.commons;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.channel.ChannelInvalidCommandDataException;
import com.flamesgroup.device.commonb.ICPUEntry;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.JournalRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DeviceJournalHelper {

  private DeviceJournalHelper() {
  }

  public static void readJournal(final IDevice device, final DeviceUID deviceUID) throws InterruptedException, ChannelException {
    Logger deviceJournal = LoggerFactory.getLogger("cpu-journal");
    for (ICPUEntry cpuEntry : device.getCPUEntries()) {
      final String fileName = String.format("%s_cpu%02d", deviceUID.getCanonicalName().toLowerCase(), cpuEntry.getNumber());
      MDC.put("fileName", fileName);
      final ILoggerChannel loggerChannel = cpuEntry.getLoggerChannel();
      loggerChannel.enslave();
      try {
        boolean hasJournalData = false;
        try {
          short recordNumber = 0;
          while (true) {
            JournalRecord record = loggerChannel.readJournalRecord(recordNumber++);
            if (record.isEmpty()) {
              break;
            }
            deviceJournal.info("{}: {} - {}", record.getIndex(), record.getTimestamp(), record.getComment());
            hasJournalData = true;
          }
        } catch (ChannelInvalidCommandDataException ignored) {
          // exception ignored, because it throws when records in journal ended
        }
        if (hasJournalData) {
          loggerChannel.eraseJournal();
        }
      } finally {
        loggerChannel.free();
      }
    }
  }

}
