/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.distributor;

import java.io.Serializable;
import java.util.Objects;

public final class Server implements Serializable {

  private static final long serialVersionUID = 91206212089254057L;

  private final String name;
  private final ServerType type;

  private final String toS;

  public Server(final String name, final ServerType type) {
    Objects.requireNonNull(name);
    Objects.requireNonNull(type);

    this.name = name;
    this.type = type;

    toS = String.format("[%s:%s]", name, type);
  }

  public String getName() {
    return name;
  }

  public ServerType getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return name.hashCode() + type.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj instanceof Server) {
      Server that = (Server) obj;
      return this.name.equals(that.name) && this.type.equals(that.type);
    }
    return false;
  }

  @Override
  public String toString() {
    return toS;
  }

}
