/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.timemachine;

public class ConditionalStateImpl implements ConditionalState {

  private final State nextState;

  public ConditionalStateImpl() {
    this(null);
  }

  public ConditionalStateImpl(final State nextState) {
    this.nextState = nextState;
  }

  @Override
  public void enterState() {
  }

  @Override
  public State getNextState() {
    return nextState;
  }

  @Override
  public boolean isFinished() {
    return true;
  }

  @Override
  public void tickRoutine() {
  }

}
