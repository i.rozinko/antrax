/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.timemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Analog of state machine, which also supports states in which it should be
 * during some period of time.
 * <p>
 * It uses initial state, which is implementor of one of the
 * {@link ConditionalState} or {@link TransientState}. Then each state is
 * responsible to determine next state by implementing method
 * {@link State#getNextState()}.
 * </p>
 * Example of usage:
 * <p>
 * <pre>
 * TimeMachine tm = new TimeMachine(myInitialState);
 * while (!tm.isFinished()) {
 *   tm.tick();
 *   Thread.sleep(200);
 * }
 * </pre>
 *
 * @version 1.0
 * @see State
 * @see ConditionalState
 * @see TransientState
 */
public final class TimeMachine {
  private final Logger logger;

  long lastTickTime;
  State currentState;

  private final String name;

  public TimeMachine(final String name, final State initialState) {
    if (initialState != null) {
      currentState = new InitialState(initialState);
    } else {
      currentState = null;
    }
    logger = LoggerFactory.getLogger(TimeMachine.class.getName() + "[" + name + "]");
    this.name = name;
  }

  public void tick() {
    while (isStateFinished()) {
      changeState();
    }
    if (currentState != null) {
      currentState.tickRoutine();
    }
  }

  public void restate() {
    if (currentState != null && currentState instanceof TransientState) { // need restate timeouts only for idle states
      currentState.enterState();
    }
  }

  boolean isStateFinished() {
    if (currentState == null) {
      return false;
    }
    if (currentState instanceof ConditionalState) {
      return ((ConditionalState) currentState).isFinished();
    }
    if (currentState instanceof TransientState) {
      long period = ((TransientState) currentState).getPeriod();
      return System.currentTimeMillis() - lastTickTime >= period;
    }
    throw new IllegalStateException("States should implement whether ConditionalState nor TransientState. This state is invalid: " + currentState);
  }

  void changeState() {
    State prevState = currentState;
    currentState = currentState.getNextState();
    if (currentState != null) {
      currentState.enterState();
    }
    if (prevState instanceof TransientState) {
      lastTickTime += ((TransientState) prevState).getPeriod();
    } else {
      lastTickTime = System.currentTimeMillis();
    }
    logger.trace("[{}] - changing State from {} to {}", this, prevState, currentState);
  }

  public boolean isFinished() {
    return currentState == null;
  }

  private static class InitialState implements ConditionalState {
    private final State nextState;

    public InitialState(final State state) {
      nextState = state;
    }

    @Override
    public void tickRoutine() {
    }

    @Override
    public void enterState() {
      // Will never be invoked
      throw new IllegalStateException("Should not be called");
    }

    @Override
    public State getNextState() {
      return nextState;
    }

    @Override
    public boolean isFinished() {
      return true;
    }

    @Override
    public String toString() {
      return "Initial_" + nextState;
    }
  }

}
