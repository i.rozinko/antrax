/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserv.utils.delivery;

import java.lang.reflect.Method;
import java.util.Arrays;

class ExecutionCommand {

  private final Object delegate;
  private final Method method;
  private final Object[] args;
  private final ExecutionHandler handler;
  private final DeliveryConfig deliveryConfig;

  public ExecutionCommand(final Object delegate, final Method m, final Object[] args, final ExecutionHandler handler, final DeliveryConfig deliveryConfig) {
    this.delegate = delegate;
    this.method = m;
    this.args = args;
    this.handler = handler;
    this.deliveryConfig = deliveryConfig;
  }

  public Object getDelegate() {
    return delegate;
  }

  public Method getMethod() {
    return method;
  }

  public Object[] getArgs() {
    return args;
  }

  public ExecutionHandler getHandler() {
    return handler;
  }

  public DeliveryConfig getDeliveryConfig() {
    return deliveryConfig;
  }

  @Override
  public String toString() {
    return String.format("%s.%s(%s)", method.getDeclaringClass().getSimpleName(), method.getName(), Arrays.toString(args));
  }
}
